﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@

'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "evaluacion360"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = ""
	' your own variables
	
	Public calif As Int = 5
	Dim contenedores As List = Array As String ("A","B","C","D","E","F","G","H","I","J")
	Dim cmbDepartamento,cmbPuesto,cmbempleados,cmbsupervisores, cmbrelacion As ABMCombo
	
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------
	Log("Connected")
	ws = WebSocket1
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
		' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
		page.Refresh
		page.FinishedLoading
	Else
		If page.WebsocketReconnected Then
			Log("Websocket reconnected")
			' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
			' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
			ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
		Else
			' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
			Log("Websocket first connection")
			page.Prepare
			ConnectPage
		End If
	End If
	Log(ABMPageId)
	'----------------------MODIFICATION-------------------------------
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")
End Sub

Sub Page_ParseEvent(Params As Map)
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)
		End Select
	End If
End Sub

public Sub BuildTheme()
	' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "evaluacion360.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
	'page.AlwaysShowVerticalScrollBar = True
	
		
	' adding a navigation bar
	ABMShared.BuildNavigationBar(page, "Evaluación 360°","../images/klogo3.png", "", "Retroción 360°", "")
			
	' create the page grid
	page.AddRows(5,False,"").AddCells12(4,"")
	page.AddRows(5,False,"").AddCellsOSMP(2,0,0,0,6,6,6,10,0,5,5,"")
	page.AddRows(1,False,"").AddCells12(1,"")
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	
		
End Sub

public Sub ConnectPage()
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
	
	
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
	
		
'	Dim lbl As ABMLabel
'	lbl.Initialize(page,"lbl","Evaluación de retroalimentación 360°",ABM.SIZE_H5,False,"")
'	page.Cell(1,1).AddComponent(lbl)
'	page.Cell(1,1).PaddingLeft = "5%"
	
	
	'Depatamentos
	cmbDepartamento.Initialize(page,"cmbDepartamento","Seleccione un departamento",250,"combo")
	page.Cell(3,1).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(3,1).AddComponent(cmbDepartamento)
	rst=sql1.ExecQuery("Select * from [karismaHC].[dbo].RH_Dept")
	pos=0
	cmbDepartamento.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbDepartamento.AddItem( rst.GetString2(0),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
		pos=pos+1
	Loop
	rst.close
	'Puesto
	cmbPuesto.Initialize(page,"cmbPuesto","Seleccione una posición",250,"combo")
	page.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(3,2).AddComponent(cmbPuesto)
	
	
	'Empleados
	cmbempleados.Initialize(page,"cmbempleados","Seleccione un evaluado",250,"combo")
	page.Cell(4,1).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(4,1).AddComponent(cmbempleados)
	
	
	'supervisores
	cmbsupervisores.Initialize(page,"cmbsupervisores","Seleccione evaluador",250,"combo")
	page.Cell(4,2).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(4,2).AddComponent(cmbsupervisores)
	'rst=sql1.ExecQuery("Select * from [karismaHC].[dbo].ER_USERS")
	'pos=0
	'cmbsupervisores.AddItem("P","",BuildSimpleItem("P","",""))
	'Do While rst.NextRow
	'cmbsupervisores.AddItem( rst.GetString2(1),rst.GetString2(5),BuildSimpleItem("P"& pos,"",rst.GetString2(5)))
	cmbsupervisores.AddItem( ws.Session.GetAttribute("idsesion"),ws.Session.GetAttribute("nombre"),BuildSimpleItem("P"& pos,"",ws.Session.GetAttribute("nombre")))
	'	pos=pos+1
	'Loop
	'rst.close
	cmbrelacion.Initialize(page,"cmbrelacion","Seleccione la relación",250,"combo")
	cmbrelacion.AddItem("auto","Auto evaluación", BuildSimpleItem("", "", "{NBSP}{NBSP}"&"Auto evaluación"))
	cmbrelacion.AddItem("subordinado","Subordinado", BuildSimpleItem("", "", "{NBSP}{NBSP}"&"Subordinado"))
	cmbrelacion.AddItem("companero","Compañero", BuildSimpleItem("", "", "{NBSP}{NBSP}"&"Compañero"))
	cmbrelacion.AddItem("cliente","Cliente Int-Ext", BuildSimpleItem("", "", "{NBSP}{NBSP}"&"Cliente Int-Ext"))
	cmbrelacion.AddItem("supervisor","Supervisor", BuildSimpleItem("", "", "{NBSP}{NBSP}"&"Supervisor"))
	
	page.Cell(3,3).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(3,3).AddComponent(cmbrelacion)
	
	
	Dim year As ABMCombo
	year.Initialize(page,"year", "Seleccione el año", 250,"combo")
	page.Cell(4,3).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(4,3).AddComponent(year)
	
	
	
	
	For k=2017 To DateTime.GetYear(DateTime.Now)+50
	
		year.AddItem(k,k, BuildSimpleItem(k,"",k))
		
	Next
	
	year.Refresh
	
	
	Dim rbgroup As ABMRadioGroup
	rbgroup.Initialize(page, "rbgroup", "radio")
	rbgroup.AddRadioButton("Anual", True)
	rbgroup.AddRadioButton("Semestre I", True)
	rbgroup.AddRadioButton("Semestre II", True)
	rbgroup.SetActive(0)

	page.Cell(5,2).AddComponent(rbgroup)
	page.Cell(5,2).SetOffsetSize(0,0,1,12,6,3)
	
	
	
	comunicacion ("A",6,1,"COMUNICACIÓN","A")
	comunicacion ("B",6,2,"PENSAMIENTO ESTRATÉGICO","B")
	comunicacion("C",7,1,"ORIENTACIÓN A RESULTADOS","C")
	comunicacion("D",7,2,"ACEPTAR EL CAMBIO","D")
	comunicacion("E",8,1,"TOMA DE DECISIONES","E")
	comunicacion("F",8,2,"LIDERAZGO","F")
	comunicacion("G",9,1,"OPERAR CON DISCIPLINA","G")
	comunicacion("H",9,2,"AMBIENTE DE TRABAJO","H")
	comunicacion("I",10,1,"INNOVAR Y REAPLICAR ","I")
	comunicacion("J",10,2,"DESARROLLO DE PERSONAL","J")
	
	Dim salvar As ABMButton
	salvar.InitializeFlat(page,"salvar","","","Salvar","btn")
	page.Cell(11,1).AddComponent(salvar)
	page.Cell(11,1).SetOffsetSize(0,0,5,12,6,6)
	page.Cell(11,1).PaddingTop = "2%"
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub

Sub comunicacion(conteiner As String,Rx As Int,Cx As Int,titulo As String,archivo As String)
	Dim cntA As ABMContainer
	cntA.Initialize(page,conteiner,"cnt")
	Dim lst As List
	lst.Initialize
	lst.Clear
	lst=File.ReadList(Main.dirfiles,archivo & ".txt")
	
	cntA.AddRows(12,False,"").AddCellsOS(1,0,0,0,5,5,5,"").AddCellsOS(8,0,0,0,1,1,1,"")
	cntA.BuildGrid
	Dim lbltA1 As ABMLabel
	lbltA1.Initialize(page,"lbltA1","{B}" & titulo & "{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,1).SetFixedHeight(50,False)
	cntA.Cell(1,1).UseTheme("celda2")
	cntA.Cell(1,1).AddComponent(lbltA1)
	Dim lbltA2 As ABMLabel
	lbltA2.Initialize(page,"lbltA2","{B}Nunca{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,2).SetFixedHeight(50,False)
	cntA.Cell(1,2).UseTheme("celda2")
	cntA.Cell(1,2).AddComponent(lbltA2)
	Dim lbltA3 As ABMLabel
	lbltA3.Initialize(page,"lbltA3","{B}Rara vez{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,3).SetFixedHeight(50,False)
	cntA.Cell(1,3).UseTheme("celda2")
	cntA.Cell(1,3).AddComponent(lbltA3)
	Dim lbltA4 As ABMLabel
	lbltA4.Initialize(page,"lbltA4","{B}Algunas veces{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,4).SetFixedHeight(50,False)
	cntA.Cell(1,4).UseTheme("celda2")
	cntA.Cell(1,4).AddComponent(lbltA4)
	Dim lbltA5 As ABMLabel
	lbltA5.Initialize(page,"lbltA5","{B}Casi siempre{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,5).SetFixedHeight(50,False)
	cntA.Cell(1,5).UseTheme("celda2")
	cntA.Cell(1,5).AddComponent(lbltA5)
	Dim lbltA6 As ABMLabel
	lbltA6.Initialize(page,"lbltA6","{B}Siempre{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,6).SetFixedHeight(50,False)
	cntA.Cell(1,6).UseTheme("celda2")
	cntA.Cell(1,6).AddComponent(lbltA6)
	Dim lbltA7 As ABMLabel
	lbltA7.Initialize(page,"lbltA7","{B}NS/NC{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,7).SetFixedHeight(50,False)
	cntA.Cell(1,7).UseTheme("celda2")
	cntA.Cell(1,7).AddComponent(lbltA7)
	Dim lbltA8 As ABMLabel
	lbltA8.Initialize(page,"lbltA8","{B}NOTA{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,8).SetFixedHeight(50,False)
	cntA.Cell(1,8).UseTheme("celda2")
	cntA.Cell(1,8).AddComponent(lbltA8)
	
	
	Dim c As Int = 1
	
	For Rn=0 To lst.size-1
		Dim rnmas1 As Int = Rn +1
		Dim lblcA1 As ABMLabel
		lblcA1.Initialize(page,"O"&conteiner&rnmas1 ,lst.Get(Rn),ABM.SIZE_SMALL,False,"")
		cntA.Cell(2+Rn,1).AddComponent(lblcA1)
		cntA.Cell(2+Rn,1).PaddingLeft = "1%"
		cntA.Cell(2+Rn+1,1).AddComponent(ABMShared.Buildline(page,"1"))
		
		
		For c= 1 To calif
			
				
				Dim inp1 As ABMCheckbox
				inp1.Initialize(page,conteiner&rnmas1&c,c,False,"")
				inp1.IsFilled = True
				cntA.Cell(2+Rn,c+1).UseTheme("celda3")
				cntA.Cell(2+Rn,c+1).AddArrayComponent(inp1,"C")
				cntA.Cell(2+Rn+1,c+1).AddComponent(ABMShared.Buildline(page,"1"))
		Next		
		
		Dim lblAR1 As ABMLabel
		lblAR1.Initialize(page,"lblAR1","-",ABM.SIZE_A,False,"")
		cntA.Cell(2+Rn,8).UseTheme("celda4")
		cntA.Cell(2+Rn,8).AddComponent(lblAR1)
		cntA.Cell(2+Rn+1,8).AddComponent(ABMShared.Buildline(page,"1"))
	
		Dim lblAR As ABMLabel
		lblAR.Initialize(page,"T"&conteiner&rnmas1,"0",ABM.SIZE_A,False,"")
		cntA.Cell(2+Rn,9).UseTheme("celda4")
		cntA.Cell(2+Rn,9).AddComponent(lblAR)
		cntA.Cell(2+Rn+1,9).AddComponent(ABMShared.Buildline(page,"1"))
		
	Next
	page.Cell(Rx ,Cx).AddComponent(cntA)
End Sub


Sub cmbPuesto_Clicked(itemId As String)
	'Dim cmbx As ABMCombo=page.Component("cmbPuesto")
	'Dim cmby As ABMCombo=page.Component("cmbDepartamento")
	Dim cmbz As ABMCombo=page.Component("cmbempleados")
	cmbz.Clear
	cmbz.refresh
	Dim query As String
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	page.Pause
	sql1.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
	query ="Select * FROM OPENQUERY(GIN, 'select * from "&ws.Session.GetAttribute("DBUse")&".dbo.vw_lgen_altasybajas where Pkey_Pos_Key=''"&itemId&"''')"
	rst=sql1.ExecQuery(query)
	pos=0
	'cmbempleados.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbz.AddItem( rst.GetString2(0),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
		pos=pos+1
	Loop
	rst.close
	page.resume
	cmbz.Refresh
End Sub



Sub cmbDepartamento_Clicked(itemId As String)
	Dim cmbx As ABMCombo=page.Component("cmbPuesto")
	Dim cmby As ABMCombo=page.Component("cmbPuesto")
	cmby.Clear
	cmby.Refresh
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_Positons where Pos_Dep_Key='" & itemId & "' order by 3")
	cmbx.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbx.AddItem(rst.GetString2(0),rst.GetString2(2),BuildSimpleItem("P"& pos,"",rst.GetString2(2)))
		pos=pos+1
	Loop
	rst.Close
	cmbx.Refresh
End Sub

Sub salvar_Clicked(Target As String)
	
	
	Dim sql1 As SQL
	Dim valores(), valores2()  As Object
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	Dim rst As ResultSet
	Dim pos As Int=0
	Dim idobj As Int
	Dim sqlt As String
		
   
	'	Dim C0 As ABMCombo=page.Component("cmbSubDivision")
	'	Dim T0 As String=C0.GetActiveItemId
	'	Dim L0 As ABMLabel=C0.GetComponent(T0)
	
	Dim C1 As ABMCombo=page.Component("year")
	Dim T1 As String=C1.GetActiveItemId
	Dim L1 As ABMLabel=C1.GetComponent(T1)

	Dim R1 As ABMRadioGroup = page.Component("rbgroup")
	Dim periodo As Int = R1.GetActive
	Dim P1 As String = periodo
	
	Dim C2 As ABMCombo=page.Component("cmbempleados")
	Dim T2 As String=C2.GetActiveItemId
	Dim L2 As ABMLabel=C2.GetComponent(T2)
		
	'OBJETIVO 4
	'FOCUS  5
	'Puntos del onjetivo 6
	'EVAL   7 ""
	'COMEN  8 ""
	
	Dim C3 As ABMCombo=page.Component("cmbsupervisores")
	Dim T3 As String=C3.GetActiveItemId
	Dim L3 As ABMLabel=C3.GetComponent(T3)
		
	'Puntos del focus 10
	'Puntos totales adquiridos 11
	'Measure 12
	'Obj txt 13
	
	Dim C4 As ABMCombo=page.Component("cmbDepartamento")
	Dim T4 As String=C4.GetActiveItemId
	Dim L4 As ABMLabel=C4.GetComponent(T4)
	
	Dim C5 As ABMCombo=page.Component("cmbPuesto")
	Dim T5 As String=C5.GetActiveItemId
	Dim L5 As ABMLabel=C5.GetComponent(T5)
	
	Dim C6 As ABMCombo=page.Component("cmbrelacion")
	Dim T6 As String=C6.GetActiveItemId
	Dim L6 As ABMLabel=C6.GetComponent(T6)
	
	

	
	
	
	Dim i,j As Int = 1
	For i =1 To contenedores.Size
		
		Dim lst As List
		lst.Initialize
		lst.Clear
		lst=File.ReadList(Main.dirfiles,contenedores.Get(i-1) & ".txt")
		Dim contenedor As ABMContainer =page.Component(contenedores.Get(i-1))
		Dim sumar As Int = 0
		
		For j = 1 To lst.Size   	
			
			Dim total As ABMLabel = contenedor.Cell(1+j,9).Component("T"&contenedores.Get(i-1)&j)
			Dim obj As ABMLabel = contenedor.Cell(1+j,1).Component("O"&contenedores.Get(i-1)&j)
			
			
			Dim nota As Int = total.Text
			Log (nota)
			sumar  = sumar + nota
			' AQUI SE SALVAN LAS NOTAS EN LA BASE DE DATOS, "NOTA" ES EL TOTAL DE CADA LINEA DEL CONTENEDOR
			' contenedores.Get(i-1) trae la letra del contenedor
			' j trae la linea
			
			Dim valores()  As Object
			valores= Array As Object (contenedores.Get(i-1),j,obj.Text,T3,T2,T6,nota,L1.Text,P1,DateTime.Date(DateTime.Now),Null,T4, T5, ws.Session.GetAttribute("DBUse") )
			sql1.ExecNonQuery2("INSERT INTO RH_EVALUACION360 VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,? )", valores)
			
			
						
		Next
		
		' VARIABLE DEL PROMEDIO DEL TOTAL SI SE DESEA MOSTRAR
		Dim promedio As Float = sumar / lst.Size
	
	Next
	ABMShared.NavigateToPage(page.ws, page.GetPageID, "../evaluacion360/evaluacion360.html")
	
End Sub

Sub  C_Clicked(Target As String)
	
	
	
	Dim a As String = Target
	
	Log (Target)
	
	Dim cont As String = a.SubString2(1,2)
	Dim fila As Int = a.SubString2(2,3)
	Dim pos As Int =  a.SubString2(3,4)
	
	Dim contenedor As ABMContainer =page.Component(cont)
	
	
	Dim i As Int
	
	For i = 1 To calif
		Dim check As ABMCheckbox = contenedor.Cell(1+fila,i+1).Component("c"&cont&fila&i)
		
		If Not(i = pos) Then
			
			check.State = False
			check.Refresh
		End If
	Next
	
	Dim total As ABMLabel = contenedor.Cell(1+fila,9).Component("T"&cont&fila)
	total.Text = pos
	total.Refresh
End Sub





' clicked on the navigation bar
Sub Page_NavigationbarClicked(Action As String, Value As String)
	' saving the navigation bar position
	page.SaveNavigationBarPosition
	If Action = "LogOff" Then
		ABMShared.LogOff(page)
		Return
	End If

	ABMShared.NavigateToPage(ws, ABMPageId, Value)
End Sub
'	Dim inpR1C1 As ABMInput
'	inpR1C1.Narrow=True
'	inpR1C1.Initialize(page,"inpR1C1",ABM.INPUT_text,"",False,"")
'	cntA.Cell(2,2).AddComponent(inpR1C1)
'	Dim inpR1C2 As ABMInput
'	inpR1C2.Narrow=True
'	inpR1C2.Initialize(page,"inpR1C2",ABM.INPUT_text,"",False,"")
'	cntA.Cell(2,3).AddComponent(inpR1C2)
'	Dim inpR1C3	 As ABMInput
'	inpR1C3.Narrow=True
'	inpR1C3.Initialize(page,"inpR1C3",ABM.INPUT_text,"",False,"")
'	cntA.Cell(2,4).AddComponent(inpR1C3)
'	Dim inpR1C4 As ABMInput
'	inpR1C4.Narrow=True
'	inpR1C4.Initialize(page,"inpR1C4",ABM.INPUT_text,"",False,"")
'	cntA.Cell(2,5).AddComponent(inpR1C4)
'	Dim inpR1C5 As ABMInput
'	inpR1C5.Narrow=True
'	inpR1C5.Initialize(page,"inpR1C5",ABM.INPUT_text,"",False,"")
'	cntA.Cell(2,6).AddComponent(inpR1C5)
'	Dim inpR1C6	 As ABMInput
'	inpR1C6.Narrow=True
'	inpR1C6.Initialize(page,"inpR1C6",ABM.INPUT_text,"",False,"")
'	cntA.Cell(2,7).AddComponent(inpR1C6)

Sub Page_FileUploaded(FileName As String, success As Boolean)
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
	'	If icon <> "" Then
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	'	Else
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
		combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next
	
	'page.Refresh
	'combo1.Refresh
	Return combo1
	

End Sub
