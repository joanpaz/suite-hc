﻿Type=StaticCode
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
	
Sub Process_Globals
	Public AvoidDuplicates As Map
	Private connections As Map
	'Private LastMessages As List
	
End Sub


Public Sub Init
	'this map is accessed from other threads so it needs to be a thread safe map
	AvoidDuplicates = Main.srvr.CreateThreadSafeMap
	connections.Initialize

End Sub

Public Sub NewConnection(cht As chatroom, name As String)
	connections.Put(name, cht)
	Log("NewConnection: " & name)

	For Each c As chatroom In connections.Values
		
		CallSubDelayed2(c, "UpdateMembersList", connections)
	Next
	
End Sub

'Public Sub NewMessage(Name As String, msg As String)
Public Sub NewMessage(ws As chatroom, msg As String)
	For Each c As chatroom In connections.Values
		CallSubDelayed3(c, "NewMessage", msg, ws.username)
	Next
End Sub

Public Sub Disconnect(cht As chatroom, name As String)
	If connections.ContainsKey(name) = False Or connections.Get(name) <> cht Then Return
		connections.Remove(name)
		
	For Each c As chatroom In connections.Values
		CallSub2(c, "RemoveMembersList", name)
	Next
		
End Sub


