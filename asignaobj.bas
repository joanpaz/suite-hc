﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "asignaobj"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "15"
	' your own variables		
	Dim myToastId As Int = 1
	Public o(5) As Int
	
	Dim cantobj As List
	Dim cmbDivision,cmbSubDivision,cmbLocation,cmbAreas,cmbDepartamento,cmbPuesto,cmbcto,cmbTipo,cmbNum,cmbnivel,cmbempleados,cmbsupervisores As ABMCombo
	Dim i,j,k As Int
	Dim enfoques As List = Array As String ("ENFOQUE FINANCIERO","ENFOQUE AL CLIENTE","ENFOQUE A LOS PROCESOS","ENFOQUE AL PERSONAL")
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	ConnectPage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")	
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
End Sub

public Sub BuildTheme()
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	theme.AddContainerTheme("conte")
	theme.Container("conte").ZDepth=ABM.ZDEPTH_3
	theme.AddDividerTheme("div1")
	theme.Divider("div1").ForeColor=ABM.COLOR_BLUE
	theme.Divider("div1").ForeColorIntensity=ABM.INTENSITY_LIGHTEN4
	theme.AddImageSliderTheme("sl1")
	theme.ImageSlider("sl1").Height=200
	theme.ImageSlider("sl1").Indicators=False
	theme.ImageSlider("sl1").FullWidth=True
	theme.ImageSlider("sl1").ZDepth=ABM.ZDEPTH_3
	
	
		'Combos
	theme.AddComboTheme("combo")
	theme.Combo("combo").ZDepth=ABM.ZDEPTH_3
		'Radio
	theme.AddRadioGroupTheme("radio")
	theme.RadioGroup("radio").WithGab = True
	
		'label
	theme.AddlabelTheme("label1")
	theme.Label("label1").ForeColor = ABM.COLOR_WHITE	
	theme.Label("label1").BackColor = ABM.COLOR_BROWN
    theme.Label("label1").BackColorIntensity = ABM.INTENSITY_DARKEN4
	theme.Label("label1").Align = ABM.TEXTALIGN_CENTER
	theme.Label("label1").ZDepth = ABM.ZDEPTH_1
	
	
	'tema de celda titulo
	theme.AddInputTheme("input1")
	theme.Input("input1").FocusForeColor = ABM.COLOR_WHITE
	theme.Input("input1").InputColor = ABM.COLOR_black
	theme.Input("input1").ValidColor = ABM.COLOR_WHITE
	theme.Input("input1").InvalidColor = ABM.COLOR_WHITE 
	theme.Input("input1").AutoCompleteBackColor = ABM.COLOR_WHITE 
	theme.Input("input1").AutoCompleteHoverBackColor = ABM.COLOR_WHITE 
	theme.Input("input1").ZDepth = ABM.ZDEPTH_1
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "asignaobj.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' Añadir Navigation Bar
	ABMShared.BuildNavigationBar(page,"POC","../images/klogo3.png","","construccion","")
	page.AddRows(5,False,"").AddCellsOSMP(4,0,0,2,12,6,4,0,0,0,0,"")
	
	page.AddRows(200,False,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()

	
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
	
	Dim oi As Int
	For oi = 1 To 4
		o(oi) = 1
	Next
	
	
	Dim lbl As ABMLabel
	lbl.Initialize(page,"lbl","Asignación de objetivos",ABM.SIZE_H5,False,"")
	page.Cell(1,1).AddComponent(lbl)
	
	
	
	page.AddModalSheetTemplate(Mensajes)
	
	
	
'	cmbDivision.Initialize(page,"cmbDivision",  "División",150,"combo")
'	cmbDivision.AddItem("P","",BuildSimpleItem("P","",""))
'	page.Cell(2,1).SetOffsetSize(0,0,1,12,6,3)
'	page.Cell(2,1).AddComponent(cmbDivision)
'	
'	
'	rst=sql1.ExecQuery("Select * from RH_dIVISION order by 1")
'	Do While rst.NextRow
'		cmbDivision.AddItem("P"& pos,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
'		pos=pos+1
'	Loop
'	rst.Close
'	
'	'SubDivision	
'	cmbSubDivision.Initialize(page,"cmbSubDivision","Seleccione un país",250,"combo")
'	page.Cell(2,2).SetOffsetSize(0,0,1,12,6,3)
'	page.Cell(2,2).AddComponent(cmbSubDivision)
'	rst=sql1.ExecQuery("Select * from RH_Country order by 1")
'	cmbSubDivision.AddItem("P","",BuildSimpleItem("P","",""))
'	pos=0
'	Do While rst.NextRow
'		cmbSubDivision.AddItem("P"& pos,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
'		pos=pos+1
'	Loop
'	rst.Close
'	
'	'Location
'	cmbLocation.Initialize(page,"cmbLocation","Seleccione una locación",250,"combo")
'	page.Cell(2,3).SetOffsetSize(0,0,1,12,6,2)
'	page.Cell(2,3).AddComponent(cmbLocation)
	

	'Depatamentos
	cmbDepartamento.Initialize(page,"cmbDepartamento","Seleccione un departamento",250,"combo")
	page.Cell(3,1).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(3,1).AddComponent(cmbDepartamento)
	rst=sql1.ExecQuery("Select * from [karismaHC].[dbo].RH_Dept")
	pos=0
	cmbDepartamento.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbDepartamento.AddItem( rst.GetString2(0),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
		pos=pos+1
	Loop
	rst.close
	'Puesto
	cmbPuesto.Initialize(page,"cmbPuesto","Seleccione una posición",250,"combo")
	page.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(3,2).AddComponent(cmbPuesto)
	
	
	'Empleados
	cmbempleados.Initialize(page,"cmbempleados","Seleccione un empleado",250,"combo")
	page.Cell(4,1).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(4,1).AddComponent(cmbempleados)
	
	
		'supervisores
	cmbsupervisores.Initialize(page,"cmbsupervisores","Seleccione un supervisor",250,"combo")
	page.Cell(4,2).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(4,2).AddComponent(cmbsupervisores)
	'rst=sql1.ExecQuery("Select * from [karismaHC].[dbo].ER_USERS")
	'pos=0
	'cmbsupervisores.AddItem("P","",BuildSimpleItem("P","",""))
	'Do While rst.NextRow
		'cmbsupervisores.AddItem( rst.GetString2(1),rst.GetString2(5),BuildSimpleItem("P"& pos,"",rst.GetString2(5)))
	cmbsupervisores.AddItem( ws.Session.GetAttribute("idsesion"),ws.Session.GetAttribute("nombre"),BuildSimpleItem("P"& pos,"",ws.Session.GetAttribute("nombre")))
	'	pos=pos+1
	'Loop
	'rst.close
	
	
	
	Dim year As ABMCombo
	year.Initialize(page,"year", "Seleccione el año", 250,"combo")
	page.Cell(5,1).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(5,1).AddComponent(year)
	
	
	
	
	For k=2017 To 2099 
	
		year.AddItem(k,k, BuildSimpleItem(k,"",k))	
		
	Next
	
	year.Refresh
	
	
	Dim rbgroup As ABMRadioGroup
		rbgroup.Initialize(page, "rbgroup", "radio")
		rbgroup.AddRadioButton("Anual", True)
		rbgroup.AddRadioButton("Semestre I", True)
		rbgroup.AddRadioButton("Semestre II", True)
		rbgroup.SetActive(0) 

		page.Cell(5,2).AddComponent(rbgroup)


		' create the container with animation	
		
		
								' you ALWAYS need an Initial state, duration should be 0 for the initial state
'									Dim myAnim4 As ABMAnimation
'									myAnim4.Initialize("initialblur", 0, ABM.TWEEN_EASEINQUAD)
'									myAnim4.Opacity(0.0)
'									myAnim4.Blur(20)
'									page.AddAnimation(myAnim4)
'									
'									' we create an animation to 'unblur'
'									Dim myAnim5 As ABMAnimation
'									myAnim5.Initialize("unblur", 500, ABM.TWEEN_EASEINQUAD)
'									myAnim5.Opacity(1)
'									myAnim5.Blur(0)
'									page.AddAnimation(myAnim5)	
'									
'									' we create an animation to 'blur'
'									Dim myAnim6 As ABMAnimation
'									myAnim6.Initialize("blur", 500, ABM.TWEEN_EASEINQUAD)
'									myAnim6.Opacity(0.0)
'									myAnim6.Blur(20)
'									page.AddAnimation(myAnim6)

	
			
	
	Dim fila As Int = 6
'	Dim cmbobjetive As ABMCombo
	
	
		For i = 1 To enfoques.Size	
	
						'linea 1
						
						Dim contenedorb As ABMContainer
						contenedorb.Initialize(page, "contenedorb"&i, "")
						contenedorb.AddRows(1,True,"").AddCellsOS(2,0,0,0,6,6,6,"")
						contenedorb.BuildGrid
					
						Dim subtitulo As ABMLabel	
						subtitulo.Initialize(page,"subtitulo"&i,enfoques.Get(i-1),ABM.SIZE_PARAGRAPH, False,"label1")
						'page.Cell(fila,1).AddComponent(subtitulo)
						contenedorb.Cell(1,1).SetOffsetSize(0,0,1,12,6,3)
						contenedorb.Cell(1,1).AddComponent(subtitulo)
								
						Dim pointsf As ABMCombo
						pointsf.Initialize(page,"pointsf"&i, "Selecciones los puntos (total)", 250,"combo")
						
'						page.Cell(fila,2).SetOffsetSize(0,0,1,12,6,3)
'						page.Cell(fila,2).AddComponent(pointsf)
						contenedorb.Cell(1,2).SetOffsetSize(0,0,1,12,6,3)
						contenedorb.Cell(1,2).AddComponent(pointsf)
				
						For k=5 To 100 Step 5 
							pointsf.AddItem(k,k, BuildSimpleItem(k,"",k&"%"))	
						Next
						pointsf.Refresh
								
						
						page.Cell(fila,1).AddComponent(contenedorb)
											
						fila = fila +1
										
						' linea 2 - 5
						
						For j = 1 To 4 
													
								
								Dim contenedor As ABMContainer
							'	contenedor.InitializeAnimated(page, "contenedor"&i&j,"initialblur", "")
								contenedor.Initialize(page, "contenedor"&i&j, "")
								
							'	If j = 1 Then
							'		contenedor.InitializeAnimated(page, "contenedor"&i&j,"unblur", "")
							'	Else
							'		contenedor.InitializeAnimated(page, "contenedor"&i&j,"blur", "")
							'	End If
								
								contenedor.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,6,5,0,0,10,0,"").AddCellsOSMP(1,0,0,0,12,6,5,0,0,10,0,"").AddCellsOSMP(1,0,0,0,12,6,2,0,0,10,0,"")
								contenedor.BuildGrid
								
								If j > 1 Then
									contenedor.CloseContent
								'	contenedor.Cell(1,1).SetFixedHeight(1,False)
								'	contenedor.Cell(1,2).SetFixedHeight(1,False)
								'	contenedor.Cell(1,3).SetFixedHeight(1,False)
								
								Else
									contenedor.OpenContent
								'	contenedor.Cell(1,1).SetFixedHeight(75,False)
								'	contenedor.Cell(1,2).SetFixedHeight(75,False)
								'	contenedor.Cell(1,3).SetFixedHeight(75,False)
								End If
								
								Dim cmbobjetive As ABMCombo
								cmbobjetive.Initialize(page,"O"&i&j, "Seleccione un objetivo", 250,"combo")
								
							'	cmbobjetive(i,j).Initialize(page,"cmbobjetive"&i&j, "Select an objetive", 250,"combo")
								
								
								'contenedor.Cell(1,1).SetOffsetSize(0,0,0,12,6,1,5)
								contenedor.Cell(1,1).AddArrayComponent(cmbobjetive,"cmbobjetive")
								
								'sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
								rst=sql1.ExecQuery("Select * from [karismaHC].[dbo].RH_Objetive order by 1")
								'objetive.AddItem("O","",BuildSimpleItem("P","",""))
								pos=0
								Do While rst.NextRow
									cmbobjetive.AddItem("O"&i&j&"P"& rst.GetString2(0),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
									pos=pos+1
								Loop
								rst.Close
								
								Dim objetivet As ABMInput
								objetivet.Initialize(page,"objetivet"&i&j,ABM.INPUT_TEXT,"",False,"")
								
								contenedor.Cell(1,1).AddComponent(objetivet)
								
								
								Dim measure As ABMInput
								measure.Initialize(page,"measure"&i&j,ABM.INPUT_TEXT,"¿Como debe ser medido?",True,"input1")
								'contenedor.Cell(1,2).SetOffsetSize(0,0,1,12,6,5)
								contenedor.Cell(1,2).AddComponent(measure)
								
								
								Dim pointso As ABMCombo
								pointso.Initialize(page,"pointso"&i&j, "Seleccione los puntos (parcial)", 250,"combo")
						

								'contenedor.Cell(1,3).SetOffsetSize(0,0,1,12,6,2)
								contenedor.Cell(1,3).AddComponent(pointso)
				
								For k=5 To 100 Step 5 
									pointso.AddItem(k,k, BuildSimpleItem(k,"",k&"%"))	
								Next
								pointso.Refresh
								
								
								
								'objetive.Refresh
								'page.Cell(i+fila+j,1).SetOffsetSize(0,0,1,12,6,3)
								page.Cell(fila,1).AddComponent(contenedor)
								
								fila = fila +1
						Next
						
						'fila = fila +1
					
						
						
						'linea 6
						
						
						Dim contenedorc As ABMContainer
						contenedorc.Initialize(page, "contenedorc"&i, "")
						contenedorc.AddRows(1,True,"").AddCellsOS(2,0,0,0,6,6,6,"")
						contenedorc.BuildGrid
						
						Dim btnaggobj As ABMButton
						btnaggobj.InitializeFlat(page,"b"&i,"mdi-content-add", ABM.ICONALIGN_LEFT ,"Agregar","btn")
						'page.Cell(i+fila,1).SetOffsetSize(0,0,0,10,3,3)
						'page.Cell(i+fila,1).AddComponent(btnaggobj)
					
						Dim btnrestobj As ABMButton
						btnrestobj.InitializeFlat(page,"b"&i,"mdi-content-remove", ABM.ICONALIGN_LEFT ,"Remover","btn")
						'page.Cell(i+fila,2).SetOffsetSize(0,0,0,10,3,3)
						'page.Cell(i+fila,2).AddComponent(btnrestobj)
						
						contenedorc.Cell(1,1).AddarrayComponent(btnaggobj,"btna")
						contenedorc.Cell(1,1).SetOffsetSize(0,0,3,10,3,3)
						contenedorc.Cell(1,2).AddarrayComponent(btnrestobj,"btnr")
						contenedorc.Cell(1,2).SetOffsetSize(0,0,3,10,3,3)
						
						contenedorc.Cell(1,1).SetFixedHeight(50,False)
						contenedorc.Cell(1,2).SetFixedHeight(50,False)
						page.Cell(fila,1).AddComponent(contenedorc)
						
						fila = fila + 1
					
		Next
		
		
		Dim contenedord As ABMContainer
		contenedord.Initialize(page, "contenedord"&i, "")
		contenedord.AddRows(1,True,"").AddCellsOS(3,0,0,0,4,4,4,"")
		contenedord.BuildGrid
		
		Dim salvar As ABMButton
		salvar.InitializeFlat(page,"salvar","", ABM.ICONALIGN_LEFT ,"Salvar","btn")
		'contenedord.Cell(1,1).SetOffsetSize(0,0,3,10,3,3)
		contenedord.Cell(1,1).AddComponent(salvar)
		
		Dim cancelar As ABMButton
		cancelar.InitializeFlat(page,"cancelar","", ABM.ICONALIGN_LEFT ,"Cancelar","btn")
		'contenedord.Cell(1,2).SetOffsetSize(0,0,3,10,3,3)
		contenedord.Cell(1,2).AddComponent(cancelar)
		
		Dim template As ABMButton
		template.InitializeFlat(page,"template","", ABM.ICONALIGN_LEFT ,"Salvar modelo","btn")
		'contenedord.Cell(1,3).SetOffsetSize(0,0,3,10,3,3)
		contenedord.Cell(1,3).AddComponent(template)
		
		
		
		page.Cell(fila,1).SetOffsetSize(0,0,3,10,4,4)
		page.Cell(fila,1).AddComponent(contenedord)
	
	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub

Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then 
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub



Sub cmbobjetive_Clicked(itemId As String)
	
	
	Dim nomcombo As String = itemId.SubString2(0,3)
	Dim id As String = itemId.SubString2(1,3)
	
	
	Dim container As ABMContainer = page.Component("contenedor"&id)
	Dim combo As ABMCombo = container.Cell(1,1).Component("cmbobjetive"&nomcombo)
	Dim input As ABMInput = container.Cell(1,1).Component("objetivet"&id)
	Dim label As ABMLabel= combo.GetComponent(itemId)
	input.Text = label.Text
	input.Refresh
	
End Sub


Sub cmbPuesto_Clicked(itemId As String)
	'Dim cmbx As ABMCombo=page.Component("cmbPuesto")
	'Dim cmby As ABMCombo=page.Component("cmbDepartamento")
	Dim cmbz As ABMCombo=page.Component("cmbempleados")
	cmbz.Clear
	cmbz.Refresh
	Dim query As String
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	page.Pause
	sql1.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
	query ="Select * FROM OPENQUERY(GIN, 'select * from "&ws.Session.GetAttribute("DBUse")&".dbo.vw_lgen_altasybajas where Pkey_Pos_Key=''"&itemId&"''')"
	rst=sql1.ExecQuery(query)
	pos=0
	'cmbempleados.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbz.AddItem( rst.GetString2(0),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
		pos=pos+1
	Loop
	rst.close
	page.resume
	cmbz.Refresh
End Sub



Sub cmbDepartamento_Clicked(itemId As String)
	Dim cmbx As ABMCombo=page.Component("cmbPuesto")
	Dim cmby As ABMCombo=page.Component("cmbPuesto")
	cmby.Clear
	cmby.Refresh
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_Positons where Pos_Dep_Key='" & itemId & "' order by 3")
	cmbx.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbx.AddItem(rst.GetString2(0),rst.GetString2(2),BuildSimpleItem("P"& pos,"",rst.GetString2(2)))
		pos=pos+1
	Loop
	page.resume
	rst.Close
	cmbx.Refresh
End Sub


'Sub cmbSubDivision_Clicked(itemId As String)
'	Dim cmbx As ABMCombo=page.Component("cmbLocation")
'	Dim c1 As ABMCombo=page.Component("cmbSubDivision")
'	Dim id1 As String=c1.GetActiveItemId
'	Dim lb1 As ABMLabel=c1.GetComponent(id1)
'	Dim sql1 As SQL
'	Dim rst As ResultSet
'	Dim pos As Int=0
'	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
'	rst=sql1.ExecQuery("Select distinct nom_propiedad from RH_SubDivision where nom_pais='" & lb1.Text & "'")
'	cmbx.AddItem("P","",BuildSimpleItem("P","",""))
'	Do While rst.NextRow
'		cmbx.AddItem("P"& pos ,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
'		pos=pos+1
'	Loop
'	rst.Close
'	cmbx.Refresh
'End Sub



Sub btna_Clicked(Target As String) 
	
	
	Dim i As Int = Target.SubString2(5,6)
	
	
  	If o(i) < 4 Then
		 o(i) = o(i) + 1
     Dim contenedor As ABMContainer = page.Component("contenedor"&i&o(i)) 
	 contenedor.OpenContent
 	' contenedor.RunAnimation("unblur")
'	 contenedor.Cell(1,1).SetFixedHeight(95,False)
'	 contenedor.Cell(1,2).SetFixedHeight(95,False)
'	 contenedor.Cell(1,3).SetFixedHeight(95,False)


 	 contenedor.Refresh	
 	Else
	End If	
 End Sub



Sub btnr_Clicked(Target As String)
	
	Dim i As Int = Target.SubString2(5,6)
	
	
	If o(i) > 1 Then
     Dim contenedor As ABMContainer = page.Component("contenedor"&i&o(i)) 
 	 contenedor.CloseContent
	 'contenedor.RunAnimation("blur")
	 'contenedor.Cell(1,1).SetFixedHeight(1,False)
	 'contenedor.Cell(1,2).SetFixedHeight(1,False)
	 'contenedor.Cell(1,3).SetFixedHeight(1,False)
 	 contenedor.Refresh	
	 o(i) = o(i) - 1	
 	Else
	End If	
End Sub




Sub salvar_Clicked(Target As String)
	
		Dim sql1 As SQL
		Dim valores(), valores2()  As Object
		sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
		Dim rst As ResultSet
		Dim pos As Int=0
		Dim idobj As Int
		Dim sqlt As String 
		
   
'	Dim C0 As ABMCombo=page.Component("cmbSubDivision")
'	Dim T0 As String=C0.GetActiveItemId
'	Dim L0 As ABMLabel=C0.GetComponent(T0)
	
	Dim C1 As ABMCombo=page.Component("year")
	Dim T1 As String=C1.GetActiveItemId
	Dim L1 As ABMLabel=C1.GetComponent(T1)

	Dim R1 As ABMRadioGroup = page.Component("rbgroup")
	Dim periodo As Int = R1.GetActive 
	Dim P1 As String = periodo
	
	Dim C2 As ABMCombo=page.Component("cmbempleados")
	Dim T2 As String=C2.GetActiveItemId
	Dim L2 As ABMLabel=C2.GetComponent(T2)
		
	'OBJETIVO 4
	'FOCUS  5
	'Puntos del onjetivo 6
	'EVAL   7 ""
	'COMEN  8 ""
	
	Dim C3 As ABMCombo=page.Component("cmbsupervisores")
	Dim T3 As String=C3.GetActiveItemId
	Dim L3 As ABMLabel=C3.GetComponent(T3)
		
	'Puntos del focus 10
	'Puntos totales adquiridos 11
	'Measure 12
	'Obj txt 13
	
	Dim C4 As ABMCombo=page.Component("cmbDepartamento")
	Dim T4 As String=C4.GetActiveItemId
	Dim L4 As ABMLabel=C4.GetComponent(T4)
	
	Dim C5 As ABMCombo=page.Component("cmbPuesto")
	Dim T5 As String=C5.GetActiveItemId
	Dim L5 As ABMLabel=C5.GetComponent(T5)

	
	' BORRADO TEMPORAL HASTA QUE SE REALICE EL UPDATE
	
	sqlt = "Delete from RH_ObjEval where User_Id='"& T2 &"' and User_Id_Eval='"& T3 &"'"
	sql1.ExecNonQuery(sqlt)

	page.pause
	For i = 1 To enfoques.Size
		
		
				Dim CN As ABMContainer = page.Component("contenedorb"&i) 
				Dim C6 As ABMCombo= CN.cell(1,2).Component("pointsf"&i)
				Dim T6 As String=C6.GetActiveItemId
				Dim L6 As ABMLabel=C6.GetComponent(T6)
				
		For j = 1 To o(i)
				

				Dim contenedor As ABMContainer = page.Component("contenedor"&i&j)

				Dim C7 As ABMCombo=contenedor.Cell(1,1).Component("cmbobjetive"&"O"&i&j)
				Dim T7 As String=C7.GetActiveItemId
				Dim L7 As ABMLabel	
				
				If Not(T7 = -1) Then
					L7=C7.GetComponent(T7)
				Else
					L7=Null	
				End	if
				
				Dim Tx1 As ABMInput = contenedor.Cell(1,2).Component("measure"&i&j)

				Dim C8 As ABMCombo=contenedor.Cell(1,3).Component("pointso"&i&j)
				Dim T8 As String=C8.GetActiveItemId
				Dim L8 As ABMLabel=C8.GetComponent(T8)
	
				Dim objnew As ABMInput = contenedor.Cell(1,1).Component("objetivet"&i&j)
				
				
				If Not(objnew.Text = "") And (L7=Null) Then
				
				' and Dept_Key='"& T4 &"'
				sqlt = "Select * from RH_Objetive where nom_objetive='" & objnew.Text & "' and Pos_key='"& T5 &"'  and nom_db='"& ws.Session.GetAttribute("DBUse") &"' and id_focus='"& i &"' "
						
					rst=sql1.ExecQuery(sqlt)	
					pos = 0
					Do While rst.NextRow
					 pos = 1
						Dim getinobj = rst.GetInt2(0)
					Loop
					rst.Close
					
						
					If pos = 0 Then	
						
						valores2= Array As Object (objnew.Text,Tx1.Text,i,T5,T4,ws.Session.GetAttribute("DBUse"))
						sql1.ExecNonQuery2("INSERT INTO RH_Objetive VALUES (?,?,?,?,?,? )", valores2)
						
						rst=sql1.ExecQuery(sqlt)
						
						Do While rst.NextRow
							idobj = rst.GetInt2(0)	
							pos=pos+1
						Loop
						rst.Close
						
						
					Else	
						
						idobj = getinobj
							
					End If
										
				Else
					
						idobj = T7.SubString(4)
					
					
				End If			
						
			
						
			valores=Array As Object  (L1.Text,P1,T2,idobj,i,(L8.Text.Replace("%","")/100)," "," ", T3,(L6.Text.Replace("%","")/100),0,Tx1.Text,objnew.Text)
			 			'sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
			 			sql1.ExecNonQuery2("INSERT INTO RH_ObjEval VALUES (?,?,?,?,?,?,?,?,?,?,?,?,? )", valores)	
					
			
			
			
		Next
	Next
	
	page.Resume
	page.ShowToast("toast1", "toastgreen", "GUARDADO FINALIZADO", 5000)
	Log ("FINALIZANDO GUARDADO")
	'ABMShared.NavigateToPage(page.ws, page.GetPageID, "../asignaobj/asignaobj.html")
	
'	Dim C1 As ABMCombo=page.Component("year")
'	Dim T1 As String=C1.GetActiveItemId
'	Dim L1 As ABMLabel=C1.GetComponent(T1)
'	
'	Dim R1 As ABMRadioGroup = page.Component("rbgroup")
'	Dim periodo As Int = R1.GetActive 
'	Dim p1 As String = periodo
'	
'	
'	Dim C2 As ABMCombo=page.Component("cmbempleados")
'	Dim T2 As String=C2.GetActiveItemId
'	Dim L2 As ABMLabel=C2.GetComponent(T2)
'	
'	Dim C3 As ABMCombo=page.Component("cmbsupervisores")
'	Dim T3 As String=C3.GetActiveItemId
'	Dim L3 As ABMLabel=C3.GetComponent(T3)
	
	
	
	
End Sub


Sub template_Clicked(Target As String)
		
		Dim sql1 As SQL
		Dim valores(), valores2() As Object
		sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
		Dim rst As ResultSet
		Dim pos As Int=0
		Dim idobj As Int
	
	Dim sqlt As String
	
'	Dim C1 As ABMCombo=page.Component("cmbDivision")
'	Dim T1 As String=C1.GetActiveItemId
'	Dim L1 As ABMLabel=C1.GetComponent(T1)
'	
'	Dim C2 As ABMCombo=page.Component("cmbSubDivision")
'	Dim T2 As String=C2.GetActiveItemId
'	Dim L2 As ABMLabel=C2.GetComponent(T2)
'	
'	Dim C3 As ABMCombo=page.Component("cmbLocation")
'	Dim T3 As String=C3.GetActiveItemId
'	Dim L3 As ABMLabel=C3.GetComponent(T3)
	
	Dim C4 As ABMCombo=page.Component("cmbDepartamento")
	Dim T4 As String=C4.GetActiveItemId
	Dim L4 As ABMLabel=C4.GetComponent(T4)
	
	Dim C5 As ABMCombo=page.Component("cmbPuesto")
	Dim T5 As String=C5.GetActiveItemId
	Dim L5 As ABMLabel=C5.GetComponent(T5)
	
	
	'limpia el template

	sqlt = "Delete from RH_ObjTemp where Pos_key='"& T5 &"' and Dept_Key='"& T4 &"' and nom_db='"& ws.Session.GetAttribute("DBUse") &"'"
				
	sql1.ExecNonQuery(sqlt)

	page.pause
	For i = 1 To enfoques.Size
		
		
				Dim CN As ABMContainer = page.Component("contenedorb"&i) 
				Dim C6 As ABMCombo= CN.cell(1,2).Component("pointsf"&i)
				Dim T6 As String=C6.GetActiveItemId
				Dim L6 As ABMLabel=C6.GetComponent(T6)
				
		For j = 1 To o(i)
				

				Dim contenedor As ABMContainer = page.Component("contenedor"&i&j)

				Dim C7 As ABMCombo=contenedor.Cell(1,1).Component("cmbobjetive"&"O"&i&j)
				Dim T7 As String=C7.GetActiveItemId
				Dim L7 As ABMLabel=C7.GetComponent(T7)
				Dim obj As String
				
				If Not(T7=-1) Then
				  
					obj = L7.text
				Else	
					obj = ""
				End If
				
				
				Dim Tx1 As ABMInput = contenedor.Cell(1,2).Component("measure"&i&j)

				Dim C8 As ABMCombo=contenedor.Cell(1,3).Component("pointso"&i&j)
				Dim T8 As String=C8.GetActiveItemId
				Dim L8 As ABMLabel=C8.GetComponent(T8)
	
	
				Dim objnew As ABMInput = contenedor.Cell(1,1).Component("objetivet"&i&j)
				
				
				
				
				
				
				
				
				If Not(objnew.Text = obj) And Not(objnew.Text = "")  Then
				
										
				sqlt = "Select * from RH_Objetive where nom_objetive='" & objnew.Text & "' and Pos_key='"& T5 &"' and Dept_Key='"& T4 &"' and nom_db='"& ws.Session.GetAttribute("DBUse") &"' and id_focus='"& i &"' "
								
							rst=sql1.ExecQuery(sqlt)	
							pos = 0
							Do While rst.NextRow
							 pos = 1
							 Dim getobj = rst.GetInt2(0)
							Loop
							rst.Close
							
								
							If pos = 0 Then	
								valores2= Array As Object (objnew.Text,Tx1.Text,i,T5,T4,ws.Session.GetAttribute("DBUse"))
								sql1.ExecNonQuery2("INSERT INTO RH_Objetive VALUES (?,?,?,?,?,? )", valores2)
								
								rst=sql1.ExecQuery(sqlt)
								
								Do While rst.NextRow
									idobj = rst.GetInt2(0)	
									pos=pos+1
								Loop
								rst.Close
								
								
							Else	
								
									idobj = getobj
									
							End If
										
				Else
					If (objnew.Text = obj) Then
						idobj = T7.SubString(4)
					Else
						Log ("Debe ser llenado el campo de objetivo")
					End If	
					
				End If
			
			
					 valores=Array As Object  (ws.Session.GetAttribute("DBUse"),T4,T5,i,(L6.Text.Replace("%","")/100),idobj,Tx1.Text,(L8.Text.Replace("%","")/100))
			 		 sql1.ExecNonQuery2("INSERT INTO RH_ObjTemp VALUES (?,?,?,?,?,?,?,? )", valores)	
			
				
		Next
	Next
	
	page.Resume
	page.ShowToast("toast1", "toastgreen", "GUARDADO FINALIZADO", 5000)
	Log ("FINALIZANDO GUARDADO")
	
	
'	Dim C1 As ABMCombo=page.Component("year")
'	Dim T1 As String=C1.GetActiveItemId
'	Dim L1 As ABMLabel=C1.GetComponent(T1)
'	
'	Dim R1 As ABMRadioGroup = page.Component("rbgroup")
'	Dim periodo As Int = R1.GetActive 
'	Dim p1 As String = periodo
'	
'	
'	Dim C2 As ABMCombo=page.Component("cmbempleados")
'	Dim T2 As String=C2.GetActiveItemId
'	Dim L2 As ABMLabel=C2.GetComponent(T2)
'	
'	Dim C3 As ABMCombo=page.Component("cmbsupervisores")
'	Dim T3 As String=C3.GetActiveItemId
'	Dim L3 As ABMLabel=C3.GetComponent(T3)
		
	
	
End Sub


Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
'	If icon <> "" Then
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
'	Else
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int 
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
	     combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next 
	
	'page.Refresh
	'combo1.Refresh	   
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid 
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)	
	Return myModal
End Sub

Sub Page_Ready()
	Log("Lista asignaobj")
	ConnectPage
	page.RestoreNavigationBarPosition
End Sub
