﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "aceptalista"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = ""
	' your own variables
	Dim myToastId As Int = 1
	
	Public lista As Map
	Public elementos As List
	Public idl As Int = 0
	Public idencuesta,nombreboss,correoboss, guiboss As String
	Public guiX , idEncuestado As String
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------
	Log("Connected")
	ws = WebSocket1
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
		' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
		page.Refresh
		page.FinishedLoading
	Else
		If page.WebsocketReconnected Then
			Log("Websocket reconnected")
			' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
			' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
			ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
		Else
			' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
			Log("Websocket first connection")
			page.Prepare
			ConnectPage
		End If
	End If
	Log(ABMPageId)
	'----------------------MODIFICATION-------------------------------
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")
End Sub

Sub Page_ParseEvent(Params As Map)
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)
		End Select
	End If
End Sub

public Sub BuildTheme()
	' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	' add additional themes specific for this page
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "aceptalista.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' adding a navigation bar
	ABMShared.BuildNavigationBar(page,"Generación de lista de evaluación","../images/klogo3.png","","generaretro","")
	
			
	' create the page grid
	page.AddRows(2,True, "").AddCells12(1,"")
	page.AddRows(3,True,"").AddCellsOS(2,0,0,0,12,6,5,"")
	page.AddRows(2,True,"").AddCells12(1,"")
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
	'busco id
	idencuesta=ws.UpgradeRequest.GetParameter("id")
	Log ("id encuesta " & idencuesta)
	Dim sql1,sql2 As SQL
	Dim rst As ResultSet
	sql1.Initialize(Main.strDrv,"jdbc:sqlserver://25.9.192.248:1433;databaseName=KarismaHC;user=karismapass;password=SuiteHC$;")
	sql2.Initialize(Main.strDrv,"jdbc:sqlserver://25.9.192.248:1433;databaseName=KarismaHC;user=karismapass;password=SuiteHC$;")
	
	rst=sql1.ExecQuery("Select * from RH_lista360 where id_guiencuesta='" & idencuesta & "'")	
	lista.Initialize
	'carga lista
	elementos.Initialize
	
	Do While rst.NextRow
		idEncuestado=rst.GetString("id_user")
		elementos.Add(idEncuestado)
		elementos.Add(rst.GetString("fec_registro"))
		elementos.Add(rst.GetString("nom_nombre"))
		elementos.Add(rst.GetString("nom_puesto"))
		elementos.Add(rst.GetString("nom_email"))
		elementos.Add("0")
		elementos.Add(rst.GetString("nom_tipo"))
		elementos.Add(rst.GetString("id_guiencuesta"))
		lista.Put(idl, elementos)
		idl = idl + 1
		'elementos.Clear		
	Loop


	
	contruyetbl

	Dim cntT As ABMContainer
	cntT.Initialize(page,"cntt","cnt")
	cntT.AddRows(3,True,"").AddCells12(1,"")
	cntT.BuildGrid
	page.Cell(2,1).AddComponent(cntT)
	
	Dim lbl As ABMLabel
	lbl.Initialize(page,"lbl","Usted debera de seleccionar :",ABM.SIZE_H6,False,"")
	cntT.Cell(1,1).PaddingTop = "2%"
	cntT.Cell(1,1).AddComponent(lbl)

	Dim tabs As ABMTabs
	tabs.Initialize(page,"tabs","")
	
	tabs.AddTab("tab1", "{B}Reporte directo{/B}", BuildTabContainer( "DIRECTO", "{B}Datos del reporte directo{/B}"),3,3,3,12,12,12,True,True,"","")
	tabs.AddTab("tab2", "Reporte indirecto", BuildTabContainer( "INDIRECTO", "Datos del reporte indirecto"),3,3,3,12,12,12,True,False,"","")
	tabs.AddTab("tab3", "Reporte igual", BuildTabContainer( "IGUAL", "Datos del reporte igual"),3,3,3,12,12,12,True,False,"","")
	tabs.AddTab("tab4", "Reporte subordinado", BuildTabContainer( "SUBORDINADO", "Datos del reporte subordinado"),3,3,3,12,12,12,True,False,"","")
	tabs.AddTab("tab5", "Reporte externo", BuildTabContainer( "EXTERNO", "Datos del reporte externo"),3,3,3,12,12,12,True,False,"","")
	
	cntT.Cell(3,1).AddComponent(tabs)

	Dim btnS As ABMButton
	btnS.InitializeFlat(page,"btnS","","","Salvar","btn")
	page.Cell(7,1).UseTheme("celda3")
	page.Cell(7,1).AddComponent(btnS)

	page.Cell(6,1).SetFixedHeight(300,False)
		
	
	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub
Sub BuildTabContainer(id As String, Text As String) As ABMContainer
	Dim Tabc As ABMContainer
	Tabc.Initialize(page, id,"")
	
	Tabc.AddRows(1,True, "").AddCells12(1,"")
	Tabc.AddRows(1,True, "").AddCells12(1,"")
	Tabc.AddRows(1,False, "").AddCellsOS(1,0,0,0,12,12,4,"").AddCellsOS(1,0,0,1,12,12,3,"").AddCellsOS(1,0,0,1,12,12,3,"")
	Tabc.AddRows(3,True,"").AddCells12(3,"")
	Tabc.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	Dim txtD As ABMInput
	txtD.InitializeWithSize(page,"txtd",ABM.INPUT_TEXT,"Nombre completo",0,0,0,11,11,11,False,"")
	txtD.Narrow=True
	Tabc.Cell(3,1).AddComponent(txtD)
	Dim txtDP As ABMInput
	txtDP.InitializeWithSize(page,"txtdp",ABM.INPUT_TEXT,"Puesto",0,0,0,11,11,11,False,"")
	txtDP.Narrow=True
	Tabc.Cell(3,2).AddComponent(txtDP)
	Dim txtDC As ABMInput
	txtDC.InitializeWithSize(page,"txtdc",ABM.INPUT_EMAIL,"Correo",0,0,0,11,11,11,False,"")
	txtDC.Narrow=True
	Tabc.Cell(3,3).AddComponent(txtDC)
	Dim btnD As ABMButton
	btnD.InitializeFlat(page,"btnd","","","Añadir","btn")
	Tabc.Cell(5,2).UseTheme("celda3")
	Tabc.Cell(5,2).AddComponent(btnD)
	btnD.Tag = id
	
	Dim lbl As ABMLabel
	lbl.Initialize(page, id & "lbl", Text, ABM.SIZE_H5, True, "")
	Tabc.Cell(1,1).AddComponent(lbl)
	Return Tabc
End Sub

Sub btnd_Clicked(Target As String)
	
	Dim cntt As ABMContainer = page.Component("cntt")
	
	Dim tabcont As ABMTabs = cntt.cell(3,1).Component("tabs")
	Dim container As ABMContainer = tabcont.GetTabPage(tabcont.GetActive)
	Dim i1 As ABMInput = container.Cell(3,1).Component("txtd")
	Dim i2 As ABMInput = container.Cell(3,2).Component("txtdp")
	Dim i3 As ABMInput = container.Cell(3,3).Component("txtdc")
	Dim b1 As ABMButton =  container.Cell(5,2).Component("btnd")

	If i1.Text.Length<=0 Then
		page.Msgbox("Msg","Deberá escribir el nombre","Error de captura","Ok","")
		Return
	End If
	If i2.Text.Length<=0 Then
		page.Msgbox("Msg","Deberá escribir el puesto","Error de captura","Ok","")
		Return
	End If
	If i3.Text.Length<=0 Then
		page.Msgbox("Msg","Deberá escribir el correo","Error de captura","Ok","")
		Return
	End If

	Dim exD As Boolean=False
	For i=0 To lista.Size-1
		Log(lista.GetValueAt(i))
		Dim strV As String=lista.GetValueAt(i)
		If strV.Contains("DIRECTO") Then
			If strV.Contains("INDIRECTO") Then
				exD=False
			Else
				exD=True
			End If
		End If
	Next
	If exD=True And b1.Tag="DIRECTO" Then
		page.Msgbox("msg","Usted ya capturo su jefe directo","Error","Ok","")
		Return
	End If
	
	elementos.Initialize
	elementos.Add(ws.Session.GetAttribute("idsesion"))
	elementos.Add(DateTime.Date(DateTime.Now))
	elementos.Add(i1.Text.ToUpperCase)
	elementos.Add(i2.text.ToUpperCase)
	elementos.Add(i3.text.ToLowerCase)
	elementos.Add("0")
	elementos.Add(b1.Tag)
	elementos.Add(ABMShared.GUIDX)
	If b1.Tag = "DIRECTO" Then
		guiboss = elementos.Get(7)
		correoboss=i3.Text.ToLowerCase
		nombreboss=i1.text.ToUpperCase
	End If
	lista.Put(idl, elementos)
	idl = idl + 1
	
	contruyetbl
	i1.Text=""
	i1.Refresh
	i2.Text=""
	i2.Refresh
	i3.Text=""
	i3.Refresh
	
End Sub


Sub contruyetbl ()
	Dim tbl As ABMTable
	tbl.InitializeScrollable(page, "tbl", False, False, True, Array As Int(100, 100,250,150,150,100,100,100), "tablas")
	tbl.SetHeaders(Array As String("ID USUARIO","FECHA","EVALUADOR", "POSICION","EMAIL","STATUS","TIPO","ACCION" ))
	tbl.SetHeaderThemes(Array As String("headerfooter", "headerfooter", "headerfooter", "headerfooter", "headerfooter","headerfooter","headerfooter","headerfooter"))
	
	tbl.SetFooter("Lista de evaluación.", 8,"headerfooter")
	

	Dim r As List
	Dim rCellThemes As List
	Dim i ,j As Int
	
	i= 0
	For Each ele As List In lista.values
		r.Initialize
		rCellThemes.Initialize
		For j=0 To 6
			Dim label As  ABMLabel
			label.Initialize(page,i&j,ele.Get(j),ABM.SIZE_SMALL,False,"")
			r.Add(label)
			rCellThemes.Add("celdas")
		Next
		Dim erase As ABMButton
		erase.InitializeFlat(page,"erase","mdi-action-delete",ABM.ICONALIGN_CENTER,"","btn")
		r.Add(erase)
		rCellThemes.Add("celdas")
		tbl.AddRow("uid", r)
		tbl.SetRowThemes(rCellThemes)
		tbl.Refresh
		i=i+1
	Next
	page.Cell(6,1).RemoveAllComponents
	page.Cell(6,1).AddComponent(tbl)
	page.refresh
	
End Sub


Sub tbl_Clicked(PassedRowsAndColumns As List)
	
	
	' fill with the active values
	Dim tblCellInfo As ABMTableCell = PassedRowsAndColumns.Get(0)
	Dim tblCases As ABMTable = page.Component(tblCellInfo.TableName)
	
	If tblCellInfo.Column = 7 Then ' edit
				
		idl = idl - 1
		
		Dim num As Int = tblCellInfo.Row
		lista.Remove(num)
	
		Dim temp As Map
		temp.Initialize
		
		Dim x As Int = 0
		For Each ele As List In lista.Values
		
			temp.Put(x,	ele)
				
			x= x +1
		Next
		
		lista.Clear
		
		lista = temp
		
		contruyetbl
		
		
		
	End If

End Sub



Sub btnS_Clicked(Target As String)
	
	Dim sql As SQL
	Dim rst As ResultSet
	Dim query As String
	Dim valores() As Object
	Dim gui As String
	sql.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	For Each element As List In lista.values
		'valores=Array As Object  (ws.Session.GetAttribute("id_user"),DateTime.Date(DateTime.Now),i1.Text,i2.text,i3.text,0,"tipo","guisuperior")
		valores=Array As Object  (element.Get(7),element.Get(0),element.Get(1),element.Get(2),element.Get(3),element.Get(4),element.Get(5),element.Get(6), guiboss)
		'sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
		sql.ExecNonQuery2("INSERT INTO RH_lista360 VALUES (?,?,?,?,?,?,?,?,? )", valores)
	Next
	
	Log ("guardo")
	'envia correo
	Dim cuerpo() As String
	Dim subjet As String = "Solicitud de aprobación de lista de evaluación"
	Dim tittle As String = "Usted recibio una solicitud de arobación de lista de evaluación"
	Dim body As String ="Presione el link para acceder"
	Dim idget As String
	idget = guiboss
	Dim link As String
	Dim img As String
	#IF REALSE	
	 	link="http://34.193.69.179:8099/suite/enableuser/enableuser.html?id&#61;"&idget&""
	 	img="http://34.193.69.179:8099/suite/images/klogo.png"
	#ELSE
	link="http://localhost:8099/suite/enableuser/enableuser.html?id&#61;"&idget&""
	img="http://localhost:8099/suite/images/klogo.png"
	#End If
	cuerpo= Array As String (subjet,tittle,body,img,link)
	
	Dim destino() As String = Array As String (correoboss)
	CallSubDelayed3(emailutils,"useremailsend",destino,cuerpo)
	
End Sub

Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub manual_Clicked(Target As String)
	Dim pdf As ABMPDFViewer
	pdf.Initialize(page, "pdf", 800, "../files/360.pdf","pdf")
	pdf.PreparePrintingText = "Preparing to print..."
	pdf.ReadDirection = ABM.PDF_READDIRECTION_LTR
	pdf.AllowDownload = True
	pdf.AllowOpen = False
	pdf.AllowPrint = True
	pdf.AllowSideBar = True
	pdf.AllowViewBookmark = False
	page.Cell(6,1).PaddingTop = "5%"
	page.Cell(6,1).AddComponent(pdf)
	page.Refresh
End Sub

Sub objetivos_Clicked(Target As String)
	ABMShared.NavigateToPage(page.ws, page.GetPageID, "../evaluacion360/evaluacion360.html")
End Sub

Sub resultados_Clicked(Target As String)
	ABMShared.NavigateToPage(page.ws, page.GetPageID, "../resultados360/resultados360.html")
End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
	'	If icon <> "" Then
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	'	Else
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
		combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next
	
	'page.Refresh
	'combo1.Refresh
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)
	Return myModal
End Sub

