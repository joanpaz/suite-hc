﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "percategory"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = ""
	' your own variables		
	Dim myToastId As Int = 1
	Dim BackDate1 As Long
	Dim BackDate2 As Long
	
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
		'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	ConnectPage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------	
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")	
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
End Sub

public Sub BuildTheme()
	' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	
	theme.AddCellTheme("c1")
	theme.Cell("c1").BackColor=ABM.COLOR_LIME

	theme.AddLabelTheme("tlbl")
	theme.Label("tlbl").ForeColor=ABM.COLOR_RED
	' add additional themes specific for this page
	' add additional themes specific for this page
	 theme.AddTableTheme("tbl1theme")
	 theme.Table("tbl1theme").ZDepth = ABM.ZDEPTH_1
	 
	 theme.Table("tbl1theme").AddCellTheme("headerfooter")
	 theme.Table("tbl1theme").Cell("headerfooter").BackColor = ABM.COLOR_BROWN
	 theme.Table("tbl1theme").Cell("headerfooter").BackColorIntensity=ABM.INTENSITY_DARKEN4
	 theme.Table("tbl1theme").Cell("headerfooter").ActiveBackColorIntensity = ABM.INTENSITY_DARKEN4
	 theme.Table("tbl1theme").Cell("headerfooter").ForeColor = ABM.COLOR_WHITE 
	 theme.Table("tbl1theme").Cell("headerfooter").Align = ABM.TABLECELL_HORIZONTALALIGN_CENTER
	 
	 
	 theme.Table("tbl1theme").AddCellTheme("headerfooterright")
	 theme.Table("tbl1theme").Cell("headerfooterright").BackColor = ABM.COLOR_BLUEGREY
	 theme.Table("tbl1theme").Cell("headerfooterright").ForeColor = ABM.COLOR_WHITE
	 theme.Table("tbl1theme").Cell("headerfooterright").Align = ABM.TABLECELL_HORIZONTALALIGN_RIGHT
	 
	 theme.Table("tbl1theme").AddCellTheme("nocolor")
	 theme.Table("tbl1theme").Cell("nocolor").ActiveBackColor = ABM.COLOR_BLUEGREY
	 theme.Table("tbl1theme").Cell("nocolor").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN2
	 
	 theme.Table("tbl1theme").AddCellTheme("nocoloredit")
	 theme.Table("tbl1theme").Cell("nocoloredit").ActiveBackColor = ABM.COLOR_BLUEGREY
	 theme.Table("tbl1theme").Cell("nocoloredit").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN2
	 theme.Table("tbl1theme").Cell("nocoloredit").IsEditable = True
	 theme.Table("tbl1theme").Cell("nocoloredit").InputMask = "'alias':'decimal', 'digits': 2"
	 theme.Table("tbl1theme").Cell("nocoloredit").AllowEnterKey = False
	 
	 theme.Table("tbl1theme").AddCellTheme("positive")
	 theme.Table("tbl1theme").Cell("positive").BackColor = ABM.COLOR_GREEN
	 theme.Table("tbl1theme").Cell("positive").BackColorIntensity = ABM.INTENSITY_LIGHTEN3
	 theme.Table("tbl1theme").Cell("positive").ActiveBackColor = ABM.COLOR_GREEN
	 theme.Table("tbl1theme").Cell("positive").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN1
	 theme.Table("tbl1theme").Cell("positive").Align = ABM.TABLECELL_HORIZONTALALIGN_RIGHT
	 
	 theme.Table("tbl1theme").AddCellTheme("negative")
	 theme.Table("tbl1theme").Cell("negative").BackColor = ABM.COLOR_RED
	 theme.Table("tbl1theme").Cell("negative").BackColorIntensity = ABM.INTENSITY_LIGHTEN3
	 theme.Table("tbl1theme").Cell("negative").ActiveBackColor = ABM.COLOR_RED
	 theme.Table("tbl1theme").Cell("negative").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN1
	 theme.Table("tbl1theme").Cell("negative").Align = ABM.TABLECELL_HORIZONTALALIGN_RIGHT
	 
	 theme.Table("tbl1theme").AddCellTheme("aligntop")
	 theme.Table("tbl1theme").Cell("aligntop").VerticalAlign = ABM.TABLECELL_VERTICALALIGN_TOP
	 theme.Table("tbl1theme").Cell("aligntop").BorderColor = ABM.COLOR_BLACK
	 theme.Table("tbl1theme").Cell("aligntop").BorderWidth = 1
	 'theme.Table("tbl1theme").Cell("aligntop").ZDepth = ABM.ZDEPTH_1
	 theme.Table("tbl1theme").Cell("aligntop").Align = ABM.TABLECELL_HORIZONTALALIGN_CENTER

End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "percategory.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' adding a navigation bar
	ABMShared.BuildNavigationBar(page,"Indicadores por categoría","../images/klogo3.png","","percategory","")
	
			
	' create the page grid
	'page.AddRows(6,True, "").AddCellsOSMP(1,0,0,0,12,12,12,0,0,-10,-5,"")
	page.AddRows(6,False, "").AddCellsOSMP(1,0,0,0,12,12,12,0,0,-5,-10,"")
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub
Sub getmonths() As List
	Dim months As List
	months.Initialize
	months.Add("Enero")
	months.Add("Febrerp")
	months.Add("Marzo")
	months.Add("Abril")
	months.Add("Mayo")
	months.Add("Junio")
	months.Add("Julio")
	months.Add("Agosto")
	months.Add("Septiembre")
	months.Add("Octubre")
	months.Add("Noviembre")
	months.Add("Diciembre")
	Return months
End Sub
Sub getyears() As List
	Dim year As List
	year.Initialize
	For i=DateTime.GetYear(DateTime.Now) To 1990 Step -1
		year.Add(i)
	Next
	Return year
End Sub
public Sub ConnectPage()
'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
'	init all your own variables (like a List, Map) and add your components
	Dim cnt As ABMContainer
	cnt.Initialize(page,"cnt","cnt")
	cnt.AddRows(1,True,"").AddCells12(1,"")
	cnt.AddRows(2,True,"").AddCellsOSMP(1,0,0,0,6,2,2,0,0,0,0,"").AddCellsOSMP(3,0,1,1,6,2,2,0,0,0,0,"")
	cnt.AddRow(1,True,"","")
	cnt.AddRow(2,True,"","")
	cnt.BuildGrid
	page.Cell(2,1).AddComponent(cnt)
	Dim lblt As ABMLabel
	lblt.Initialize(page,"lblt","Filters",ABM.SIZE_A,False,"etiq")
	cnt.Cell(1,1).MarginTop="1%"
	cnt.Cell(1,1).UseTheme("celda2")
	cnt.Cell(1,1).AddComponent(lblt)
	cnt.Cell(2,1).AddComponent(buildcombo("monthin","","Mes inicial", getmonths))
	cnt.Cell(2,2).AddComponent(buildcombo("yearin","","Año inicial", getyears))
	cnt.Cell(2,3).AddComponent(buildcombo("monthfin","","Mes final", getmonths))
	cnt.Cell(2,4).AddComponent(buildcombo("yearfin","","Año final", getyears))
	Dim btna As ABMButton
	btna.InitializeFlat(page,"btna","","","Ok","btn")
	cnt.Cell(4,1).UseTheme("celda3")
	cnt.Cell(4,1).AddComponent(btna)
	creaflotante

'	Dim cnt As ABMContainer
'	cnt.Initialize(page,"cnt","cnt")
'	cnt.AddRows(3,False,"").AddCellsOS(1,0,1,1,12,4,4,"").AddCellsOS(1,0,1,1,12,4,4,"")
'	cnt.AddRow(1,True,"","")
'	cnt.AddRow(2,True,"","")
'	
'	cnt.BuildGrid
'	page.Cell(2,1).AddComponent(cnt)
'	Dim NewDate As Long = DateTime.Now  
'	Dim fecin,fecfin As ABMDateTimeScroller
'	fecin.Initialize(page, "fecin", ABM.DATETIMESCROLLER_TYPE_DATE, ABM.DATETIMESCROLLER_MODE_MIXED, NewDate, "Pick initial date", "")
'	fecin.DateDayNames = "['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']"
'	fecin.DateMonthNamesShort = "['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']"
'	fecin.TitleDateFormat = "DDD"
'	fecin.ReturnDateFormat = "mm/dd/yy" '// <--- see demo source code For settings
'	fecin.ReturnTimeFormat = "HH:ii"  '  // <--- see demo source code For settings  
'	fecin.DateOrder = "Mddyy"           '// <--- see demo source code For settings
'	fecin.TimeShowAMPM = False
'	fecin.CancelText = "Close"
'	fecin.PickText = "OK"
'
'	fecfin.Initialize(page, "fecfin", ABM.DATETIMESCROLLER_TYPE_DATE, ABM.DATETIMESCROLLER_MODE_MIXED, NewDate, "Pick final date", "")
'	fecfin.DateDayNames = "['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']"
'	fecfin.DateMonthNamesShort = "['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']"
'	fecfin.TitleDateFormat = "DDD"
'	fecfin.ReturnDateFormat = "mm/dd/yy" '// <--- see demo source code For settings
'	fecfin.ReturnTimeFormat = "HH:ii"  '  // <--- see demo source code For settings  
'	fecfin.DateOrder = "Mddyy"           '// <--- see demo source code For settings
'	fecfin.TimeShowAMPM = False
'	fecfin.CancelText = "Close"
'	fecfin.PickText = "OK"
'
'	cnt.Cell(2,1).AddComponent(fecin)
'	cnt.Cell(2,2).AddComponent(fecfin)
'	
'	Dim btna As ABMButton
'	btna.InitializeFlat(page,"btna","","","Ok","btn")
'	cnt.Cell(4,1).UseTheme("celda3")
'	cnt.Cell(4,1).AddComponent(btna)
'	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub
Sub fecin_Changed(dateMilliseconds As String)
	BackDate1=dateMilliseconds
End Sub
Sub fecfin_Changed(dateMilliseconds As String)
	BackDate2=dateMilliseconds
End Sub
Sub btna_Clicked(Target As String)
	'crea	
	'-----inicio
	Dim cnt As ABMContainer = page.Component("cnt")
	Dim monthin, monthfin, yearin, yearfin As ABMCombo
	monthin = cnt.Cell(2,1).Component("monthin") 
	yearin = cnt.Cell(2,2).Component("yearin") 
	monthfin = cnt.Cell(2,3).Component("monthfin") 
	yearfin = cnt.Cell(2,4).Component("yearfin") 
	If monthin.GetActiveItemId = -1 Then
		page.Msgbox("msg1", "Seleccione la fecha inicial","Error","ok","")
		Return
	End If
	If monthfin.GetActiveItemId = -1 Then
		page.Msgbox("msg1", "Seleccione la fecha final","Error","ok","")
		Return
	End If
	If yearin.GetActiveItemId = -1 Then
		page.Msgbox("msg1", "Seleccione la fecha inicial","Error","ok","")
		Return
	End If
	If yearfin.GetActiveItemId = -1 Then
		page.Msgbox("msg1", "Seleccione la fecha final","Error","ok","")
		Return
	End If
	DateTime.DateFormat="MM/dd/yyyy"	
	Dim fecha1, fecha2, year As String
	Dim y,m1, m2,d As Int
	Dim l As ABMLabel 
	l=yearin.GetComponent(yearin.GetActiveItemId)
	y=l.ID
	year = y
	m1=monthin.GetActiveItemId+1
	fecha1 = DateTime.Date(DateUtils.SetDate( y,m1,1))
	l=yearfin.GetComponent(yearfin.GetActiveItemId)
	y=l.id
	m2=monthfin.GetActiveItemId+1
	d=DateUtils.NumberOfDaysInMonth(m2,y)
	fecha2 = DateTime.Date(DateUtils.SetDate( y,m2,d))
	Dim tam As Int = (m2 - m1) + 3
	'---Fin
'	DateTime.DateFormat="MM/dd/yyyy"	
'	Dim fecha1, fecha2 As String
'	fecha1=DateTime.Date(BackDate1) 
'	fecha2=DateTime.Date(BackDate2) 
'
'	If fecha1.Contains("/1969") Then
'		'fecha1=DateTime.Date(DateTime.Now) & " " &DateTime.Time(DateTime.Now)		
'		page.Msgbox("msg1", "Select another initial date","Error","ok","")
'		Return
'	End If
'
'	If fecha2.Contains("/1969") Then
'		'fecha2=DateTime.Date(DateTime.Now) & " " &DateTime.Time(DateTime.Now)		
'		page.Msgbox("msg1", "Select another final date","Error","ok","")
'		Return
'	End If

	page.Pause
	Dim cnt2 As ABMContainer
	cnt2.Initialize(page,"cnt2","cnt")
	cnt2.AddRow(1,False,"","")
	cnt2.AddRow(1,False,"","")
	cnt2.AddRow(1,False,"","")
	cnt2.AddRow(1,False,"","")
	cnt2.AddRows(3,False,"").AddCellsOS(2,0,1,1,12,3,3,"")
	cnt2.AddRows(1,False,"").AddCellsOSMP(1,0,0,0,12,6,6,0,0,20,0,"").AddCellsOSMP(1,0,0,0,12,6,6,0,0,10,0,"")
	cnt2.AddRow(1,False,"","")
	cnt2.BuildGrid
	Dim img As ABMImage
	img.Initialize(page,"img","../images/klogo2.jpg",3)
	cnt2.Cell(2,1).SetOffsetSize(1,1,1,12,12,12)
	cnt2.Cell(2,1).AddComponent(img)
	cnt2.Cell(3,1).UseTheme("celda3")
	cnt2.Cell(3,1).AddComponent(ABMShared.BuildParagraph(page,"p","Indicators by category",ABM.SIZE_H5))
	
	cnt2.Cell(5,1).AddComponent(ABMShared.BuildParagraph(page,"p","{B}Hotel{/B}",ABM.SIZE_A))
	cnt2.Cell(6,1).AddComponent(ABMShared.BuildParagraph(page,"p","{B}Cuartos ocupados{/B}",ABM.SIZE_A))
	cnt2.Cell(7,1).AddComponent(ABMShared.BuildParagraph(page,"p","{B}Periodo consultado{/B}",ABM.SIZE_A))

	Dim tbl1 As ABMTable	
	tbl1.Initialize(page,"tbl1",False,False,True,"tbl1theme")
	tbl1.SetHeaders(Array As String("Categoría","Personal","Índice","Personal ejecutivo","Personal por ocupación"))
	tbl1.SetHeaderThemes(Array As String("headerfooter","headerfooter","headerfooter","headerfooter","headerfooter"))
	
	
	Dim tbl2 As ABMTable	
	tbl2.Initialize(page,"tbl2",False,False,True,"tbl1theme")
	tbl2.SetHeaders(Array As String("Categoría","Personal","Indeterminado","Temporal","% Indeterminado","% Temporal"))
	tbl2.SetHeaderThemes(Array As String("headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter"))

	Dim sql1 As SQL
	Dim rst As ResultSet
	'sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
	Log("Antes " & DateTime.Time(DateTime.Now))
	
	Log(fecha1)
	Dim Consulta1, Consulta2 As String
	Consulta1="EXEC [GIN]." & ws.Session.GetAttribute("DBUse") & ".dbo.PT_POR_CATEGORIA '456465', 'epaz10','"  & fecha1 & "','" & fecha2& "'"
	Log(Consulta1)
	sql1.ExecNonQuery(Consulta1)
	Dim Propiedad,rooms As String
	Consulta2="Select * FROM OPENQUERY(GIN,'select * from " & ws.Session.GetAttribute("DBUse") & ".dbo.vw_PT_POR_CATEGORIAepaz10')"
	Log(Consulta2)
	rst=sql1.ExecQuery(Consulta2)
	Log("despues " & DateTime.Time(DateTime.Now))
	Do While rst.NextRow
		Dim valor(),valor2() As String
		valor=Array As String(rst.GetString(	"CATEGORIA"),rst.GetString("TOTAL"),Round2(((rst.GetString("INDICE"))*100),2) & "%",rst.GetString("COL X EJE"),rst.GetString("COL X HAB"))
		If rst.GetString("CATEGORIAS").Trim="" Then
			valor2=Array As String("  "," "," "," "," ","-")			
		Else
			valor2=Array As String(rst.GetString("CATEGORIAS"),rst.GetString("STAFF"),rst.GetString("PLANTAS"),rst.GetString("EVENTUALES"),rst.GetString("%_PLANTAS"),rst.GetString("%_EVENTUALES"))
		End If
		tbl1.AddRow("P" & rst.GetInt("idciunter"),	valor)
		tbl2.AddRow("P" & rst.GetInt("idciunter"),	valor2)
		tbl1.SetRowThemes(Array As String("aligntop","aligntop","aligntop","aligntop","aligntop"))
		tbl2.SetRowThemes(Array As String("aligntop","aligntop","aligntop","aligntop","aligntop","aligntop"))
		rooms=rst.GetString("CUARTOS")
		Propiedad=rst.GetString("PROPIEDAD")
	Loop
	tbl1.SetFooter("Tabla informativa", 12,"headerfooter")
	tbl2.SetFooter("Tabla informativa", 12,"headerfooter")

	cnt2.Cell(5,2).AddComponent(ABMShared.BuildParagraph(page,"p1",Propiedad,ABM.SIZE_A))
	cnt2.Cell(6,2).AddComponent(ABMShared.BuildParagraph(page,"p1",rooms,ABM.SIZE_A))
	cnt2.Cell(7,2).AddComponent(ABMShared.BuildParagraph(page,"p1","Period consulted from " & ABMShared.ConvertDate(fecha1,"MM/dd/yyy","MMMM dd yyyy") & " To " & ABMShared.ConvertDate(fecha2,"MM/dd/yyy","MMMM dd yyyy"),ABM.SIZE_A))



	cnt2.Cell(8,1).AddComponent(tbl1)
	cnt2.Cell(8,2).AddComponent(tbl2)
	page.Cell(6,1).AddComponent(cnt2)
	page.Refresh
	page.Resume
End Sub




Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then 
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
'	If icon <> "" Then
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
'	Else
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,200,"")
	
	Dim i As Int 
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
	     combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next 
	
	'page.Refresh
	'combo1.Refresh	   
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid 
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)	
	Return myModal
End Sub

