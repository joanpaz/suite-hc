﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "organigrama3"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = ""
	' your own variables
	Dim myToastId As Int = 1
	'variables de uplload
	Public DownloadFolder As String = "/www/" & ABMShared.AppName & "/prueba/"
	Public DownloadMaxSize As String = 1000*1024
	Private form1 As JQueryElement
	Public ChatName As String
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------
	Log("Connected")
	ws = WebSocket1
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
		' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
		page.Refresh
		page.FinishedLoading
	Else
		If page.WebsocketReconnected Then
			Log("Websocket reconnected")
			' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
			' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
			ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
		Else
			' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
			Log("Websocket first connection")
			page.Prepare
			ConnectPage
		End If
	End If
	Log(ABMPageId)
	'----------------------MODIFICATION-------------------------------
	'----------------------MODIFICATION-------------------------------
	' this page uses uploads, so needs some settings
	ws.Session.SetAttribute("abmcallback", Me)
	ws.Session.SetAttribute("abmdownloadfolder", DownloadFolder)
	ws.Session.SetAttribute("abmmaxsize", DownloadMaxSize)
	form1.RunMethod("ajaxForm", Null)
	
	ws.Session.SetAttribute("file_upload_sender", Me)
	ws.Session.SetAttribute("dfolder", DownloadFolder)
	ws.Session.SetAttribute("msize", DownloadMaxSize)
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")
	' and remove them again
	Try
		ws.Session.RemoveAttribute("abmcallback")
		ws.Session.RemoveAttribute("abmdownloadfolder")
		ws.Session.RemoveAttribute("abmmaxsize")
	Catch
		Log(LastException.Message)
	End Try
End Sub

Sub Page_ParseEvent(Params As Map)
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)
		End Select
	End If
End Sub

public Sub BuildTheme()
	' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	
	' add additional themes specific for this page
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "organigrama3.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
	
	Dim extraSideBar As ABMSideBar
	extraSideBar.Initialize(page, "extrasidebar", 530, 48, 56, Null, ABM.COLLAPSE_ACCORDION, "nav2theme")
	extraSideBar.AddSideBarComponent("barchat", BuildSideBarComponent( "contchat",  "This is a sidebar component", "With an image and an editor"))
	page.NavigationBar.AddTopItemWithSideBar("SideBar", "", "mdi-action-dashboard", "", False, extraSideBar)
		
	' adding a navigation bar
	ABMShared.BuildNavigationBar(page,"Organigrama 3","../images/klogo3.png","","organigrama3","")
	
	
			
	' create the page grid
	page.AddRows(10,True, "").AddCells12(1,"")
	
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	'page.AddExtraCSSFile("custom/organigrama/Treant.css")
	'page.AddExtraCSSFile("custom/organigrama/basic-example.css")
	
	'page.AddExtraJavaScriptFile("custom/organigrama/raphael.js")
	'page.AddExtraJavaScriptFile("custom/organigrama/Treant.js")
	
	'NO page.AddExtraJavaScriptFile("custom/organigrama/basic-example.js")
	
	page.AddExtraJavaScriptFile("custom/loader.js")
	
		
End Sub

public Sub ConnectPage()
	'	connecting the navigation bar
		
	'ABMShared.ConnectNavigationBar(page)
	'	init all your own variables (like a List, Map) and add your components
	Dim sql1,sql2 As SQL
	Dim rst As ResultSet
	Dim rst2 As String
	Dim consulta2 As String
	Dim consulta As String
'	consulta="Select * FROM OPENQUERY (GIN,'Select person_id, pkey_key1,PKey_Status1, pkey_dept_key, pkey_pos_key, pos_name, pos_uplevel from (RH_DoradoSensimar1.dbo.rh_pers_keys inner join RH_DoradoSensimar1.dbo.rh_positons on RH_DoradoSensimar1.dbo.rh_pers_keys.pkey_pos_key=RH_DoradoSensimar1.dbo.rh_positons.pos_key) where PKey_Dept_Key=''9032'' and PKey_Status1=''a'' and person_id<>''5468'' order by 2')"
'
'
'	sql1.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
'	sql2.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
'
'	rst=sql1.ExecQuery(consulta)
'	Dim otrosx As String
'	Dim configX As Map
'	configX.Initialize
'	configX.Put("c9032201","c9032201")

'	Dim mapO As Map
'	mapO.Initialize
'	Do While rst.NextRow
'		mapO.Put(rst.GetString("pkey_key1"),rst.GetString("pkey_pos_key") & "-" & rst.GetString("pos_uplevel"))
'		consulta2="Select * FROM OPENQUERY (GIN,'Select (ltrim(rtrim(pinfo_first_name)) + '' '' + ltrim(rtrim(pinfo_second_name)) + '' '' + ltrim(rtrim(pinfo_last_name)) + '' '' + ltrim(rtrim(pinfo_last_name2))) as nombre from RH_DoradoSensimar1.dbo.rh_pers_info where person_id=''" & rst.GetString("person_id") & "''' )"
'		rst2=sql2.ExecQuerySingleResult(consulta2)
'		'Log(rst2)
'		If configX.ContainsKey("c" &rst.GetString("pkey_pos_key")) =False Then
'			otrosx=otrosx &"c" & rst.GetString("pkey_pos_key") & "= {" _
'			&"parent:c" & rst.GetString("pos_uplevel") & "," _
'			&"text:{" _
'			&"name: '" & rst2 & "'," _
'			&"title: '" & rst.GetString("pos_name") & "'," _
'			&"contact: '" & rst.GetString("pkey_key1") & "'," _
'			&"}," _
'			&"stackChildren: false," _
'			&"image: '../headshots/1.jpg'" _
'			&"},"
'			configX.Put("c" & rst.GetString("pkey_pos_key"),"c" &rst.GetString("pkey_pos_key"))
'		Else
'			otrosx=otrosx &"d" & rst.GetString("pkey_pos_key") & "= {" _
'			&"parent:c" & rst.GetString("pos_uplevel") & "," _
'			&"text:{" _
'			&"name: '" & rst2 & "'," _
'			&"title: '" & rst.GetString("pos_name") & "'," _
'			&"contact: '" & rst.GetString("pkey_key1") & "'," _
'			&"}," _
'			&"stackChildren: false," _
'			&"image: '../headshots/1.jpg'" _
'			&"},"
'			configX.Put("d" & rst.GetString("pkey_pos_key"),"d" &rst.GetString("pkey_pos_key"))
'			
'		End If
'	Loop
'
'	Dim lstS As String
'	For i=0 To configX.Size-1
'		lstS=lstS & configX.GetKeyAt(i) & ","
'	Next
'	lstS=lstS.SubString2(0,lstS.Length-1)
'	otrosx=otrosx.SubString2(0,otrosx.Length-1)
'	
'	Dim strX As String
'	strX="c9032201 = {"  _
'			&"text: {" _
'			&"name: 'ARZOLA GARCIA COYNTA '," _ 
'			&"title: 'GERENTE DE RECURSOS HUMANOS'," _
'			&"contact: 'K02192661'," _
'			&"}," _
'			&"image: '../photos/RH_DoradoSensimar1/5468.jpg'" _
'            &"}," 
'	'	otrosx="c9032310 = {" _
'	'			&"parent:c9032201," _
'	'			&"text:{" _
'	'			&"name: 'Monis'," _
'	'			&"title: 'Chalana'," _
'	'			&"}," _
'	'			&"stackChildren: true," _
'	'			&"image: '../headshots/1.jpg'" _
'	'			&"}"
'	
'
'	Log (strX)
'	Log(otrosx)
'	Log(lstS)

'	Dim script As String = $" var config = {
'        container: "#basic-example",
'        connectors: { 
'            type: 'step'
'        },
'        node: {
'            HTMLclass: 'nodeExample1'
'        }
'    },
'
'    
'	${ strX & otrosx}
'    chart_config = [
'        config,
'		${ lstS }
'       ];
' "$
	'

	Dim a As ABMButton
	a.InitializeFlat(page,"prueba","","","prueba","")
	page.Cell(10,1).AddComponent(a)

	'creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position     -----action="/suite/organigrama3/filehelpe r"
	page.RestoreNavigationBarPosition
	
	
	ABM.AddHTML(page,"r3c1",$"<div id="chart_div"></div>"$)
	
	ABM.AddHTML(page,"r2c1",$"<form id='form1'  method="post" enctype="multipart/form-data">
								<input type="file" name="file1" id="file1">	
								<input type="submit" name="submit" value="Submit" id='save'>
								</form>
								<output id="list">
								</output>"$)
								
	page.Refresh
 	
	Dim script As String = $"
	
	google.charts.load('current', {packages:["orgchart"]});
    google.charts.setOnLoadCallback(drawChart);
	
	      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Name');
        data.addColumn('string', 'Manager');
        data.addColumn('string', 'ToolTip');

        // For each orgchart box, provide the name, manager, and tooltip to show.
        data.addRows([
          [{v:'Mike', f:'Mike<div style="color:red; font-style:italic">President</div><label>HOLA<label/>'},
	'', 'The President'],
          [{v:'Jim', f:'<div id="kk" class="hola" data-value="kkkkkk">Jim<div style="color:red; font-style:italic">Vice President</div><img src="../images/download2.jpg" alt="Smiley face" width="42" height="42"></div>'},
	'Mike', 'VP'],
	['Alice', 'Mike', ''],
	['Bob', 'Jim', 'Bob Sponge'],
	['Carol', 'Bob', ''],
	['Carol2', 'Bob', '']
        ]);

        // Create the chart.
        var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
        // Draw the chart, setting the allowHtml option to true for the tooltips.
        chart.draw(data, {allowHtml:true});
      }
 "$
	page.ws.Eval(script,Null)
	Dim script As String = $"
	$(document).on("click", '.hola', function(){
    		alert($(this).data('value'));
		}); 

	   "$
	page.ws.Eval(script,Null)
	
	Dim script As String = $"
	
		// Variable to store your files
		var files;

		// Add events
		$('input[type=file]').on('change', prepareUpload);

		// Grab the files and set them to our variable
		function prepareUpload(event)
		{
		  files = event.target.files;
		}
		
	$('#form1').on('submit', uploadFiles);

// Catch the form submit and upload the files
		function uploadFiles(event)
		{
		  	event.stopPropagation(); // Stop stuff happening
		    event.preventDefault(); // Totally stop stuff happening

		    // START A LOADING SPINNER HERE

		    // Create a formdata object and add the files
		    var data = new FormData();
		    $.each(files, function(key, value)
		    {
		        data.append(key, value);
		    });

			    $.ajax({
			        url: 'filehelper',
			        type: 'POST',
			        data: data,
			        cache: false,   
			        processData: false, // Don't process the files
			        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
			      /* success: function(data, textStatus, jqXHR)
			        {
			            if(typeof data.error === 'undefined')
			            {
			                // Success so call function to process the form
			                submitForm(event, data);
			            }
			            else
			            {
			                // Handle errors here
			                console.log('ERRORS: ' + data.error);
			            }
			        },
			        error: function(jqXHR, textStatus, errorThrown)
			        {
			            // Handle errors here
			            console.log('ERRORS: ' + textStatus);
			            // STOP LOADING SPINNER
			        }*/
			    });

			}
		

	   "$
	page.ws.Eval(script,Null)
	
	'	// dataType: "html",  dataType: 'json',
	'<input type="file" id="files" name="files[]" multiple /> para varios
			
'	//var data = new FormData();
'	//data.append('file1', $('#file1')[0].files[0]);
'	//console.log(data);
'			
'			
'	//var oData = new FormData(document.forms.namedItem("file1"));
'	//var url="/suite/organigrama3/filehelper";
	
	''return false;
	
	
	
	
	
'	Dim script As String = $"
'	
'  function handleFileSelect(evt) {
'    var files = evt.target.files; // FileList object
'
'    // files is a FileList of File objects. List some properties.
'    var output = [];
'    for (var i = 0, f; f = files[i]; i++) {
'      output.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
'                  f.size, ' bytes, last modified: ',
'                  f.lastModifiedDate.toLocaleDateString(), '</li>');
'    }
'    document.getElementById('list').innerHTML = '<ul>' + output.join('') + '</ul>';
'	
'	console.log(files[0]);
'  }
'
'
'
'
'
'	document.getElementById('files').addEventListener('change', handleFileSelect, false);
'	
'	"$
'	page.ws.Eval(script,Null)
	
	script = ""
	
'	Dim script2 As String = $"
'	  $(document).on("submit","#form1", function(){
'    	alert("prueba");  
'		return false;   
'		});​	  
'	 "$
'	page.ws.Eval(script2,Null)
'	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
End Sub

'
'$" var config = {
'        container: "#basic-example",
'        
'        connectors: {
'            type: 'step'
'        },
'        node: {
'            HTMLclass: 'nodeExample1'
'        }
'    },
'    ceo = {
'        text: {
'            name: "Ricardo Torres",
'            title: "Chief executive officer",
'            contact: "Tel: 01 213 123 134",
'        },
'        image: "../headshots/2.jpg"
'    },
'
'    cto = {
'        parent: ceo,
'        text:{
'            name: "Joan Paz",
'            title: "Chief Technology Officer",
'        },
'        stackChildren: true,
'        image: "../headshots/1.jpg"
'    },
'    cbo = {
'        parent: ceo,
'        stackChildren: true,
'        text:{
'            name: "Otro Monito",
'            title: "Chief Business Officer",
'        },
'        image: "../headshots/5.jpg"
'    },
'    cdo = {
'        parent: ceo,
'        text:{
'            name: "Monito 3",
'            title: "Chief accounting officer",
'            contact: "Tel: 01 213 123 134",
'        },
'        image: "../headshots/6.jpg"
'    },
'    cio = {
'        parent: cto,
'        text:{
'            name: "Monito 4",
'            title: "Chief Information Security Officer"
'        },
'        image: "../headshots/8.jpg"
'    },
'    ciso = {
'        parent: cto,
'        text:{
'            name: "Monito 5",
'            title: "Chief Innovation Officer",
'            contact: {val: "we@aregreat.com", href: "mailto:we@aregreat.com"}
'        },
'        image: "../headshots/9.jpg"
'    },
'    cio2 = {
'        parent: ceo,
'        text:{
'            name: "Monito 6",
'            title: "Chief Customer Officer"
'        },
'        link: {
'            href: "http://www.google.com"
'        },
'        image: "../headshots/10.jpg"
'    },
'    ciso2 = {
'        parent: cbo,
'        text:{
'            name: "Monito 7",
'            title: "Chief Communications Officer"
'        },
'        image: "../headshots/7.jpg"
'    },
'    ciso3 = {
'        parent: cbo,
'        text:{
'            name: "Monito 8",
'            title: "Chief Brand Officer"
'        },
'        image: "../headshots/4.jpg"
'    },
'    ciso4 = {
'        parent: cbo,
'        text:{
'            name: "Monito 9",
'            title: "Chief Business Development Officer"
'        },
'        image: "../headshots/11.jpg"
'    }
'
'    chart_config = [
'        config,
'        ceo,
'        cto,
'        cbo,
'        cdo,
'        cio,
'        ciso,
'        cio2,
'        ciso2,
'        ciso3,
'        ciso4
'    ];
' "$





Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

'Public Sub FileUploaded(parts As Map)
'	Dim filePart As Part = parts.Get("file1")
'	Log("File uploaded successfully: " & filePart.SubmittedFilename & _
'		" size = " & NumberFormat(File.Size("", filePart.TempFile) / 1000, 0, 2) & "kb")
'
'	
'	File.Delete("", filePart.TempFile)
'	ws.Flush 'this is a server event so we need to explicitly call Flush
'End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)
	myToastId = myToastId + 1
	

	'Dim contenedorb As ABMContainer = page.Component("contenedorb")
	' Dim inp1 As ABMFileInput  = contenedorb.Cell(2,1).Component("inp1")
  
	If success Then
		page.ShowToast("toast" & myToastId, "toastgreen", "File " & FileName & " uploaded success!", 5000)
		
	Else
		page.ShowToast("toast" & myToastId, "toastred", "File " & FileName & " not uploaded!", 5000)
	End If
   
	'Dim inp1 As ABMFileInput  = page.Cell(3,1).Component("inp1")
	'inp1.Clear
   
	page.Resume
	page.ws.Flush
	page.Refresh
End Sub

Public Sub FileError(Message As String)
	Log("Error uploading file: " & Message)
	
	ws.Flush
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
	'	If icon <> "" Then
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	'	Else
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
		combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next
	
	'page.Refresh
	'combo1.Refresh
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)
	Return myModal
End Sub


Sub ChatClear_Clicked(Target As String)
	
	
	Dim sb As ABMSideBar = page.GetSidebar("Sidebar")
	Dim extra As ABMcomponent = page.Component("extrasidebar")
	''Dim cont As ABMContainer = extra.cell(1,1).Component("contchat")
	''Dim Chat As ABMChat = cont.Cell(1,1).Component("conversation")

	''Chat.ClearConversation
End Sub

Sub page_MsgboxResult(returnName As String, result As String)
	Log(returnName & ": " & result)
End Sub

Sub ChatInput_Changed(value As String)
	If ChatShared.HasIdentified(ABMPageId) Then
		CallSubDelayed3(ChatShared, "IsTyping", ABMPageId, value)
	End If
End Sub

Sub ChatInput_EnterPressed(value As String)
	DateTime.TimeFormat = "HH:mm"
	
	If ChatShared.HasIdentified(ABMPageId) Then
		Dim bubble As ABMChatBubble
		bubble.Initialize(ChatName, value, ChatName & " " & DateTime.Time(DateTime.Now), "me", "them")
		CallSubDelayed3(ChatShared, "NewMessage", ABMPageId, bubble)
		CallSubDelayed3(ChatShared, "IsTyping", ABMPageId, "")
	Else
		If value = "" Then Return
		If ChatShared.AvoidDuplicates.ContainsKey(value.ToLowerCase) Then
			Dim sb As ABMSideBar = page.GetSidebar("extrasidebar")
			Dim Chat As ABMChat = sb.Content.Component("conversation")
			Chat.AddBubble("Server", "Sorry, but " & value & " is already taken!", "The Server", "server", "server")
			Chat.Refresh
		Else
			ChatName = value
			ChatShared.AvoidDuplicates.Put(value.ToLowerCase, value)
			Dim sb As ABMSideBar = page.GetSidebar("extrasidebar")
			Dim Chat As ABMChat = sb.Content.Component("conversation")
			Chat.SetMyFrom(ChatName)
			Chat.AddBubble("Server", "Welcome to ABMChat " & ChatName & "!", "The Server", "server", "server")
			Chat.Refresh
			CallSubDelayed3(ChatShared, "NewConnection", Me, ABMPageId)
		End If
	End If
	
	
	Dim sb As ABMSideBar = page.GetSidebar("extrasidebar")
	Dim ChatInput As ABMInput = sb.Content.Component("ChatInput")
	ChatInput.Text = ""
	ChatInput.Refresh
End Sub

public Sub NewMessage(bubble As ABMChatBubble)
	Dim sb As ABMSideBar = page.GetSidebar("extrasidebar")
	Dim chat As ABMChat = sb.Content.Component("conversation")
	chat.AddBubble2(bubble)
	chat.Refresh
	chat.ScrollToBottom	' scroll to the end of the chat
End Sub

public Sub IsTyping(pageID As String, typeBool As Boolean)
	Dim sb As ABMSideBar = page.GetSidebar("extrasidebar")
	Dim chat As ABMChat = sb.Content.Component("conversation")
	If pageID <> ABMPageId Then
		If typeBool = False Then
			chat.ThemAreTyping(False, "") 'hide the 'is typing' bubble
		Else
			chat.ThemAreTyping(True, "") ' show the 'is typing' bubble if not yet shown
		End If
		chat.Refresh
		chat.ScrollToBottom	' scroll to the end of the chat
	End If
End Sub

public Sub CurrentConversation(LastMessages As List)
	If LastMessages.Size > 0 Then
		Dim sb As ABMSideBar = page.GetSidebar("extrasidebar")
		Dim chat As ABMChat = sb.Content.Component("conversation")
		chat.SetConversation(LastMessages)
		chat.Refresh
		chat.ScrollToBottom	' scroll to the end of the chat
	End If
End Sub

Sub BuildSideBarComponent( id As String, Title As String, Subtitle As String) As ABMContainer 'ignore
	
	Dim ItemCont As ABMContainer
	ItemCont.Initialize(page, id, "")
	ItemCont.AddRowsM(1,False,0,0, "").AddCells12(1, "") ''AddCellsOSMP(1,0,0,0,3,3,3,4,0,0,0,"").AddCellsOSMP(1,0,0,0,9,9,9,4,0,0,0,"")
	ItemCont.AddRowsM(1,False,0,0, "").AddCells12(1, "")
	ItemCont.AddRowsM(1,False,0,0, "").AddCells12(1, "")
	ItemCont.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	Dim Chat As ABMChat
	Chat.Initialize2(page, "conversation", 450, 255, "mychat")
	Chat.AddBubble("Server", "I'm Server, what is your name?", "The Server", "server", "server")
	ItemCont.Cell(1,1).AddComponent(Chat)
	'page.Cell(1,1).AddComponent(Chat)
		
	Dim ChatInput As ABMInput
	ChatInput.Initialize(page, "ChatInput", ABM.INPUT_TEXT, "", False, "")
	ChatInput.PlaceHolderText = "Type your message here...."
	ChatInput.RaiseChangedEvent = True
	ItemCont.Cell(2,1).AddComponent(ChatInput)
	
	Dim ChatClear As ABMButton
	ChatClear.InitializeFloating(page, "chatClear", "chat_bubble_outline", "")
	ItemCont.Cell(3,1).AddComponent(ChatClear)
	
	' refresh the page
	''page.Refresh
	' Tell the browser we finished loading
	''page.FinishedLoading
	' restoring the navigation bar position
	''page.RestoreNavigationBarPosition
	
	'ItemCont.Refresh
   
'	Dim SubItemCont As ABMContainer
'	SubItemCont.Initialize(page, id & "SubItemCont", "")
'	SubItemCont.AddRowsM(1,False, 0,0,"").AddCells12MP(1,-6,0,0,0,"").AddCells12(1,"")
'	SubItemCont.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
'   
'	ItemCont.Cell(1,2).AddComponent(SubItemCont)
    
'	Dim img As ABMImage
'	img.Initialize(page, id & "img", image, 1)
'	img.SetFixedSize(48,48)
'	img.IsCircular = True
'	img.IsResponsive = True
'   	
'	ItemCont.Cell(1,1).AddComponent(img)
'   
'	Dim lbl1 As ABMLabel
'	lbl1.Initialize(page, id & "lbl1", Title, ABM.SIZE_H6, False, "lightblue")
'	lbl1.VerticalAlign = True
'   
'	SubItemCont.Cell(1,1).AddComponent(lbl1)
'   
'	Dim lbl2 As ABMLabel
'	lbl2.Initialize(page, id & "lbl2", Subtitle, ABM.SIZE_H6, False, "")
'	lbl2.VerticalAlign = True
'   
'	SubItemCont.Cell(1,2).AddComponent(lbl2)
'   
'	Dim editor As ABMEditor
'	editor.Initialize(page, "editor", True, True, "editor")
'	ItemCont.Cell(2,1).AddComponent(editor)
   
	Return ItemCont
End Sub


