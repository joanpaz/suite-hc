﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "controldevacantes"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "5"
	' your own variables		
	Dim myToastId As Int = 1
	
	' your own variables
	Dim ActR As String
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	ConnectPage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------		
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")	
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
End Sub

public Sub BuildTheme()
   ' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	
	' add additional themes specific for this page
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "controldevacantes.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' adding a navigation bar
	ABMShared.BuildNavigationBar(page,"Control de vacantes ","../images/klogo3.png","","controldevacantes","")
			
	' create the page grid
	page.AddRows(1,True, "").AddCells12(1,"")
	page.AddRows(1,True, "").AddCells12(1,"")

	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
	
	Dim lbl As ABMLabel
	lbl.Initialize(page,"lbl","Vacancy Control",ABM.SIZE_H3,False,"")
	page.Cell(1,1).AddComponent(lbl)

	Dim tbl1 As ABMTable
	tbl1.Initialize(page,"tbl1",False,False,True,"tablas")
	tbl1.SetHeaders(Array As String("División","Sub-división","Locación","Área","Departamento","Posición","Fecha de captura","Cantidad"))
	
	tbl1.SetHeaderThemes(Array As String("headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter"))

	Dim sql1 As SQL
	Dim rst As ResultSet
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_NewPosition")
	Do While rst.NextRow
		Dim valor() As String
		Dim Datex As String
		Datex=rst.GetString("fec_capture")
		Datex=Datex.Replace(" 00:00:00.0","")
		Datex=Datex.SubString2(5,7) & "/" & Datex.SubString2(8,10) & "/" & Datex.SubString2(1,4)
		valor=Array As String(rst.GetString(	"nom_division"),rst.GetString("nom_subdivision"),rst.GetString("nom_location"),rst.GetString("nom_area"),rst.GetString("nom_department"),rst.GetString("nom_position"),Datex,rst.GetInt("num_quantity"))
		tbl1.AddRow("P" & rst.GetInt("id_newposition"),	valor)
	Loop
	
	tbl1.SetFooter("Tabla informativa", 12,"headerfooter")
	page.Cell(2,1).AddComponent(tbl1)
	
	creaflotante
	
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub

Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then 
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

' clicked on the navigation bar
Sub tbl1_Clicked(PassedRowsAndColumns As List)
	Dim tblCellInfo As ABMTable = page.Component("tbl1")
	ActR=tblCellInfo.GetActiveRow
	page.AddModalSheetTemplate(DetallePuesto)
	page.Refresh
	
	page.ShowModalSheet("myModal")
End Sub

Sub DetallePuesto() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "myModal", True, False, "")
	myModal.Content.UseTheme("mypagem")
	myModal.Footer.UseTheme("modalfooter")
	myModal.IsDismissible = False
	
	' create the grid for the content
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")
	
	myModal.Content.AddRows(2,True, "").AddCellsOS(2,0,0,0,6,6,6,"") 
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")
	myModal.Content.AddRows(2,True, "").AddCellsOS(3,0,0,0,4,4,4,"") 
	
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")
	myModal.Content.AddRows(2,True, "").AddCellsOS(3,0,0,0,4,4,4,"") 
	
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")
	myModal.Content.AddRows(2,True, "").AddCellsOS(3,0,0,0,4,4,4,"") 
	
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")
	myModal.Content.AddRows(2,True, "").AddCellsOS(3,0,0,0,4,4,4,"") 

	myModal.Content.AddRows(1,True, "").AddCells12(1,"")
	myModal.Content.AddRows(2,True, "").AddCellsOS(3,0,0,0,4,4,4,"") 

	myModal.Content.AddRows(1,True, "").AddCells12(1,"")
	myModal.Content.AddRows(2,True, "").AddCellsOS(3,0,0,0,4,4,4,"") 
	
	myModal.Content.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	Dim lblT As ABMLabel
	lblT.Initialize(page,"lblT","Detalle de vancantes",ABM.SIZE_H4,False,"etiq")
	
	' add paragraph	
	myModal.Content.Cell(1,1).UseTheme("celda2")
	myModal.Content.Cell(1,1).AddComponent(lblT)
	Dim sqlt As SQL
	Dim rstT As ResultSet
	sqlt.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	If ActR="" Then ActR="P0"
	ActR=ActR.Replace("P","")
	
	Dim rowx As Int=ActR
	rstT=sqlt.ExecQuery("Select * from RH_NewPosition where id_newposition=" & rowx)
	Dim position As String
	rstT.NextRow
			position=rstT.GetString("nom_position")
	
	Dim lblst1,lblst2,lblst3,lblst4,lblst5,lblst6,lblst7,lblst8,lblst9,lblst10,lblst11,lblst12,lblst13,lblst14,lblst15,lblst16,lblst17,lblst18,lblst19 As ABMLabel
	Dim lblst20,lblst21,lblst22,lblst23,lblst24,lblst25,lblst26,lblst27 As ABMLabel
	
	lblst1.Initialize(page,"lblst1" ,position,ABM.SIZE_h5,False,"etiq") 
	myModal.Content.Cell(2,1).UseTheme("celda2")
	myModal.Content.Cell(2,1).AddComponent(lblst1)

	lblst2.Initialize(page,"lblst2" ,"Fecha de aplicación",ABM.SIZE_SPAN,False,"etiq") 
	lblst3.Initialize(page,"lblst3" ,"Fecha estimada de entrega",ABM.SIZE_SPAN,False,"etiq") 
	
	
	myModal.Content.Cell(3,1).UseTheme("celda2")
	myModal.Content.Cell(3,2).UseTheme("celda2")
	myModal.Content.Cell(3,1).AddComponent(lblst2)
	myModal.Content.Cell(3,2).AddComponent(lblst3)
	
	Dim lblF1,lblF2,lblF3,lblF4,lblF5,lblF6,lblF7,lblF8,lblF9,lblF10,lblF11,lblF12,lblF13,lblF14,lblF15,lblF16,lblF17,lblF18,lblF19,lblF20 As ABMLabel
	'2016-12-01 00:00:00
	lblF1.Initialize(page,"lblF1", ABMShared.ConvertDate( rstT.GetString("fec_capture"),"yyyy-MM-dd HH:mm:ss","dd MMMM yyyy") ,ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(4,1).UseTheme("celda")
	myModal.Content.Cell(4,1).AddComponent(lblF1)
	lblF2.Initialize(page,"lblF2",rstT.GetString("fec_start"),ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(4,2).UseTheme("celda")
	myModal.Content.Cell(4,2).AddComponent(lblF2)
	'busqueda
	lblst4.Initialize(page,"lblst4" ,"Buscar aplicante",ABM.SIZE_h6,False,"etiq") 
	myModal.Content.Cell(5,1).UseTheme("celda2")
	myModal.Content.Cell(5,1).AddComponent(lblst4)
	lblst5.Initialize(page,"lblst5" ,"Fecha planeada",ABM.SIZE_SPAN,False,"etiq") 
	lblst6.Initialize(page,"lblst6" ,"Fecha real",ABM.SIZE_SPAN,False,"etiq") 
	lblst7.Initialize(page,"lblst7" ,"Días reales acumulados",ABM.SIZE_SPAN,False,"etiq") 
	myModal.Content.Cell(6,1).UseTheme("celda2")
	myModal.Content.Cell(6,2).UseTheme("celda2")
	myModal.Content.Cell(6,3).UseTheme("celda2")
	myModal.Content.Cell(6,1).AddComponent(lblst5)
	myModal.Content.Cell(6,2).AddComponent(lblst6)
	myModal.Content.Cell(6,3).AddComponent(lblst7)
	
	lblF3.Initialize(page,"lblF3","Noviembre 11, 2016",ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(7,1).UseTheme("celda")
	myModal.Content.Cell(7,1).AddComponent(lblF3)
	lblF4.Initialize(page,"lblF4","Noviembre 30, 2016",ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(7,2).UseTheme("celda")
	myModal.Content.Cell(7,2).AddComponent(lblF4)
	lblF5.Initialize(page,"lblF5","5",ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(7,3).UseTheme("celda")
	myModal.Content.Cell(7,3).AddComponent(lblF5)
	
	'Entrevista1	
	lblst8.Initialize(page,"lblst8" ,"1ra entrevista filtro con RRHH",ABM.SIZE_h6,False,"etiq") 
	myModal.Content.Cell(8,1).UseTheme("celda2")
	myModal.Content.Cell(8,1).AddComponent(lblst8)
	lblst9.Initialize(page,"lblst9" ,"Fecha planeada",ABM.SIZE_SPAN,False,"etiq") 
	lblst10.Initialize(page,"lblst10" ,"Fecha real",ABM.SIZE_SPAN,False,"etiq") 
	lblst11.Initialize(page,"lblst11" ,"Días reales acumilados",ABM.SIZE_SPAN,False,"etiq") 
	myModal.Content.Cell(9,1).UseTheme("celda2")
	myModal.Content.Cell(9,2).UseTheme("celda2")
	myModal.Content.Cell(9,3).UseTheme("celda2")
	myModal.Content.Cell(9,1).AddComponent(lblst9)
	myModal.Content.Cell(9,2).AddComponent(lblst10)
	myModal.Content.Cell(9,3).AddComponent(lblst11)

	lblF6.Initialize(page,"lblF6","Noviembre 11, 2016",ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(10,1).UseTheme("celda")
	myModal.Content.Cell(10,1).AddComponent(lblF6)
	lblF7.Initialize(page,"lblF7","Noviembre 30, 2016",ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(10,2).UseTheme("celda")
	myModal.Content.Cell(10,2).AddComponent(lblF7)
	lblF8.Initialize(page,"lblF8","5",ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(10,3).UseTheme("celda")
	myModal.Content.Cell(10,3).AddComponent(lblF8)
	
	'Entrevista2	
	lblst12.Initialize(page,"lblst12" ,"2da entrevista filtro con el supervisor directo",ABM.SIZE_h6,False,"etiq") 
	myModal.Content.Cell(11,1).UseTheme("celda2")
	myModal.Content.Cell(11,1).AddComponent(lblst12)
	lblst13.Initialize(page,"lblst13" ,"Fecha planeada",ABM.SIZE_SPAN,False,"etiq") 
	lblst14.Initialize(page,"lblst14" ,"Fecha Real",ABM.SIZE_SPAN,False,"etiq") 
	lblst15.Initialize(page,"lblst15" ,"Días reales acumulados",ABM.SIZE_SPAN,False,"etiq") 
	myModal.Content.Cell(12,1).UseTheme("celda2")
	myModal.Content.Cell(12,2).UseTheme("celda2")
	myModal.Content.Cell(12,3).UseTheme("celda2")
	myModal.Content.Cell(12,1).AddComponent(lblst13)
	myModal.Content.Cell(12,2).AddComponent(lblst14)
	myModal.Content.Cell(12,3).AddComponent(lblst15)

	lblF9.Initialize(page,"lblF9","Noviembre 11, 2016",ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(13,1).UseTheme("celda")
	myModal.Content.Cell(13,1).AddComponent(lblF9)
	lblF10.Initialize(page,"lblF7","Noviembre 30, 2016",ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(13,2).UseTheme("celda")
	myModal.Content.Cell(13,2).AddComponent(lblF10)
	lblF11.Initialize(page,"lblF8","5",ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(13,3).UseTheme("celda")
	myModal.Content.Cell(13,3).AddComponent(lblF11)
	
	'Entrevista3
	lblst16.Initialize(page,"lblst16" ,"3ra entrevista filtro con el gerente de área",ABM.SIZE_h6,False,"etiq") 
	myModal.Content.Cell(14,1).UseTheme("celda2")
	myModal.Content.Cell(14,1).AddComponent(lblst16)
	lblst17.Initialize(page,"lblst17" ,"Fecha planeada",ABM.SIZE_SPAN,False,"etiq") 
	lblst18.Initialize(page,"lblst18" ,"Fecha real",ABM.SIZE_SPAN,False,"etiq") 
	lblst19.Initialize(page,"lblst19" ,"Días reales acumulados",ABM.SIZE_SPAN,False,"etiq") 
	myModal.Content.Cell(15,1).UseTheme("celda2")
	myModal.Content.Cell(15,2).UseTheme("celda2")
	myModal.Content.Cell(15,3).UseTheme("celda2")
	myModal.Content.Cell(15,1).AddComponent(lblst17)
	myModal.Content.Cell(15,2).AddComponent(lblst18)
	myModal.Content.Cell(15,3).AddComponent(lblst19)

	lblF12.Initialize(page,"lblF9","Noviembre 11, 2016",ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(16,1).UseTheme("celda")
	myModal.Content.Cell(16,1).AddComponent(lblF12)
	lblF13.Initialize(page,"lblF7","Noviembre 30, 2016",ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(16,2).UseTheme("celda")
	myModal.Content.Cell(16,2).AddComponent(lblF13)
	lblF14.Initialize(page,"lblF8","5",ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(16,3).UseTheme("celda")
	myModal.Content.Cell(16,3).AddComponent(lblF14)
	
	'Aplicacio de Examenes
	lblst20.Initialize(page,"lblst16" ,"Aplicación de prueba",ABM.SIZE_h6,False,"etiq") 
	myModal.Content.Cell(17,1).UseTheme("celda2")
	myModal.Content.Cell(17,1).AddComponent(lblst20)
	lblst21.Initialize(page,"lblst17" ,"Fecha planeada",ABM.SIZE_SPAN,False,"etiq") 
	lblst22.Initialize(page,"lblst18" ,"Fecha real",ABM.SIZE_SPAN,False,"etiq") 
	lblst23.Initialize(page,"lblst19" ,"Días reales acumulados",ABM.SIZE_SPAN,False,"etiq") 
	myModal.Content.Cell(18,1).UseTheme("celda2")
	myModal.Content.Cell(18,2).UseTheme("celda2")
	myModal.Content.Cell(18,3).UseTheme("celda2")
	myModal.Content.Cell(18,1).AddComponent(lblst21)
	myModal.Content.Cell(18,2).AddComponent(lblst22)
	myModal.Content.Cell(18,3).AddComponent(lblst23)

	lblF15.Initialize(page,"lblF15","Noviembre 11, 2016",ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(19,1).UseTheme("celda")
	myModal.Content.Cell(19,1).AddComponent(lblF15)
	lblF16.Initialize(page,"lblF16","Noviembre 30, 2016",ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(19,2).UseTheme("celda")
	myModal.Content.Cell(19,2).AddComponent(lblF16)
	lblF17.Initialize(page,"lblF17","5",ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(19,3).UseTheme("celda")
	myModal.Content.Cell(19,3).AddComponent(lblF17)
	
	'Seleccion de candidatos
	lblst24.Initialize(page,"lblst24" ,"Selección del aplicante / Propuesta",ABM.SIZE_h6,False,"etiq") 
	myModal.Content.Cell(20,1).UseTheme("celda2")
	myModal.Content.Cell(20,1).AddComponent(lblst24)
	lblst25.Initialize(page,"lblst25" ,"Fecha planeada",ABM.SIZE_SPAN,False,"etiq") 
	lblst26.Initialize(page,"lblst26" ,"Fecha real",ABM.SIZE_SPAN,False,"etiq") 
	lblst27.Initialize(page,"lblst27" ,"Días reales acumulados",ABM.SIZE_SPAN,False,"etiq") 
	myModal.Content.Cell(21,1).UseTheme("celda2")
	myModal.Content.Cell(21,2).UseTheme("celda2")
	myModal.Content.Cell(21,3).UseTheme("celda2")
	myModal.Content.Cell(21,1).AddComponent(lblst25)
	myModal.Content.Cell(21,2).AddComponent(lblst26)
	myModal.Content.Cell(21,3).AddComponent(lblst27)

	lblF18.Initialize(page,"lblF15","Noviembre 11, 2016",ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(22,1).UseTheme("celda")
	myModal.Content.Cell(22,1).AddComponent(lblF18)
	lblF19.Initialize(page,"lblF16","Noviembre 30, 2016",ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(22,2).UseTheme("celda")
	myModal.Content.Cell(22,2).AddComponent(lblF19)
	lblF20.Initialize(page,"lblF17","5",ABM.SIZE_SPAN,False,"")
	myModal.Content.Cell(22,3).UseTheme("celda")
	myModal.Content.Cell(22,3).AddComponent(lblF20)
		
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,6,6,6,3,3,3,"").AddCellsOS(1,0,0,0,3,3,3, "")
	myModal.Footer.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	Dim msbtn2 As ABMButton
	msbtn2.InitializeFlat(page, "msbtn2", "", "", "Cerrar", "btn")
	myModal.Footer.Cell(1,2).SetOffsetSize(0,0,1,1,1,1)
	myModal.Footer.Cell(1,2).AddComponent(msbtn2)
	
	Return myModal
End Sub
Sub msbtn2_Clicked(Target As String)
	page.CloseModalSheet("mymodal")
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
'	If icon <> "" Then
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
'	Else
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int 
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
	     combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next 
	
	'page.Refresh
	'combo1.Refresh	   
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid 
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)	
	Return myModal
End Sub

Sub Page_Ready()
	Log("Control de Vacantes!")
	ConnectPage
	page.RestoreNavigationBarPosition
End Sub

