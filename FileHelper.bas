﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Handler class
Sub Class_Globals
	Dim ABM As ABMaterial
End Sub

Public Sub Initialize
	
End Sub

Sub Handle(req As ServletRequest, resp As ServletResponse)
	'get the callback module from the session (multiple modules can use this handler)
'	Dim callback As Object = req.GetSession.GetAttribute("file_upload_sender")
'	Try
'		Dim data As Map = req.GetMultipartData(Main.TempDir, 100000)
'		CallSubDelayed2(callback, "FileUploaded", data)
'	Catch
'		CallSubDelayed2(callback, "FileError", LastException.Message)
	'		resp.SendError(500, LastException.Message)
	'	End Try
	
	'get the callback page from the session (multiple modules can use this handler)
	Dim callback As Object = req.GetSession.GetAttribute("file_upload_sender")
	Dim downloadfolder As String = File.Combine(File.DirApp, req.GetSession.GetAttribute("dfolder"))
	Dim MaxSize As Int
	Try
		MaxSize = req.GetSession.GetAttribute("msize")
	Catch
		resp.SendError(500, LastException.Message)
		Return
	End Try
	Dim data As Map
	Dim fileName As String
	Dim tmpFileName As String
	
	Try
		data = req.GetMultipartData(downloadfolder, MaxSize)
		Dim filePart As Part = data.Get("0")
		'If filePart.IsInitialized Then
			fileName = 	filePart.SubmittedFilename
			tmpFileName = filePart.TempFile
			If ABM.HandleUpload(downloadfolder, tmpFileName, fileName) Then
				If SubExists(callback, "Page_FileUploaded") Then
					CallSubDelayed3(callback, "Page_FileUploaded", fileName, True)
				End If
			Else
				If SubExists(callback, "Page_FileUploaded") Then
					CallSubDelayed3(callback, "Page_FileUploaded", fileName, False)
				End If
			End If
		
		
	Catch
		resp.SendError(500, LastException.Message)
		If SubExists(callback, "Page_FileUploaded") Then
			CallSubDelayed3(callback, "Page_FileUploaded", LastException.Message , False)
		End If
	End Try



End Sub