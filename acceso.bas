﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@

'Class module
Sub Class_Globals
	Private ws As WebSocket 
	Public page As ABMPage
	Private theme As ABMTheme
	Private ABM As ABMaterial 'ignore	
	Public Name As String = "acceso" 
	Dim myToastId As Int = 1
	Private cntU,cntL As ABMContainer
	Private txtU As ABMInput
	Private sql1 As SQL
	Private ABMPageId As String = "1"
	Private correorh As String = ""
End Sub

Public Sub Initialize
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	Connectpage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconectado acceso")
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
End Sub

public Sub BuildTheme()
	theme.Initialize("pagetheme")
	' modal 
	
	theme.AddABMTheme(ABMShared.MyTheme)
	theme.AddContainerTheme("conte")
	theme.Container("conte").ZDepth=ABM.ZDEPTH_3
	theme.AddDividerTheme("div1")
	theme.Divider("div1").ForeColor=ABM.COLOR_BLACK
	theme.Divider("div1").ForeColorIntensity=ABM.INTENSITY_DARKEN4
	theme.AddImageSliderTheme("sl1")
	theme.ImageSlider("sl1").Height=200
	theme.ImageSlider("sl1").Indicators=False
	theme.ImageSlider("sl1").FullWidth=True
	theme.ImageSlider("sl1").ZDepth=ABM.ZDEPTH_3
	
	' modal
	theme.AddContainerTheme("modalcontent")
	theme.Container("modalcontent").BackColor = ABM.COLOR_WHITE	
	' modal 
	theme.AddContainerTheme("modalfooter")
	theme.Container("modalfooter").BackColor = ABM.COLOR_WHITE
	theme.Container("modalfooter").BackColorIntensity=ABM.INTENSITY_DARKEN4
End Sub

public Sub BuildPage()
		' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "acceso.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	page.ShowConnectedIndicator = True
	'page.AddRows(1,False,"").
	'page.AddRows(1,True,"").AddCellsOSMP(12,0,0,0,6,6,1,400,0,0,0,"")
	page.AddRows(1,False,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	
	page.SetBackgroundImage("../slides/inicio.png")
	
	page.BuildGrid 
End Sub
Sub Connectpage
	
	
	
	' POR SI DESPUES DE LOGUEADOS INTENTAN IR FORZADO A LA PAGINA DE ACCESO POR DIRECCION DEL NAVEGADOR
	ws.Session.SetAttribute("authType", "")
	ws.Session.SetAttribute("authName", "")
	ws.Session.SetAttribute("IsAuthorized", "false")	
	ws.Session.SetAttribute("idsesion", "")
	Main.idsesion = ""
	Main.correo = ""
	Main.nombrecompleto = ""
	Main.username = ""
	'----------------------------------------------------------------------
	page.AddModalSheetTemplate(modalbien)
		
	page.AddModalSheetTemplate(modalerror)	
	
	page.AddModalSheetTemplate(modalinactive)
	
	page.AddModalSheetTemplate(modalcorreo)
	
	page.AddModalSheetTemplate(modalnew)
	
	cntU.Initialize(page,"cntU","cnta")'cnta
	cntU.AddRows(1,False,"").AddCellsOS(1,0,0,3,6,3,3,"").AddCellsOS(1,0,0,0,6,8,8,"")
	cntU.BuildGrid
	'cntU.Cell(1,1).SetFixedHeight(10,False)
	page.Cell(1,1).AddComponent(cntU)

	Dim imglogo As ABMImage
	imglogo.Initialize(page,"imglogo","../images/klogo3.png",3)
	cntU.Cell(1,1).SetOffsetSize(0,0,0,3,3,3)
	cntU.Cell(1,1).AddComponent(imglogo)	
	
	
	
	Dim cntL As ABMContainer
	cntL.Initialize(page,"cntl","")
	cntL.AddRows(1,False,"").AddCellsOSMP(6,0,0,0,2,2,2,5,0,0,0,"")
	cntL.BuildGrid
	cntU.Cell(1,2).AddComponent(cntL)
	
	
	Dim lblU As ABMLabel
	lblU.Initialize(page,"lblU","{B}Usuario{/B}",ABM.SIZE_A,False,"etiq")
	cntL.Cell(1,1).UseTheme("celda5")
	cntL.Cell(1,1).AddComponent(lblU)
	cntL.Cell(1,1).MarginTop="1.5%"
	txtU.Initialize(page,"txtU",ABM.INPUT_TEXT,"",False,"caja2")
	txtU.Narrow=True
	cntL.Cell(1,2).UseTheme("celda5")
	cntL.Cell(1,2).AddComponent(txtU)
'
	Dim lblP As ABMLabel
	lblP.Initialize(page,"lblP","{B}Clave{/B}",ABM.SIZE_A,False,"etiq")
	'cntL.Cell(1,3).SetOffsetSize(0,1,1,2,2,2)
	cntL.Cell(1,3).UseTheme("celda5")
	cntL.Cell(1,3).AddComponent(lblP)
	cntL.Cell(1,3).MarginTop="1.5%"

	Dim txtP As ABMInput
	txtP.Initialize(page,"txtp", ABM.INPUT_PASSWORD  ,"",False,"caja2")
	txtP.Narrow=True
	cntL.Cell(1,4).UseTheme("celda5")
	cntL.Cell(1,4).AddComponent(txtP)

	Dim btnA, btnB, btnL As ABMButton
	btnA.InitializeRaised(page,"btna","","","Acceder","btn2")
	cntL.Cell(1,5).SetOffsetSize(0,1,1,2,2,2)
	cntL.Cell(1,5).AddComponent(btnA)
	cntL.Cell(1,5).MarginTop="1.5%"



'	cntU.Initialize(page,"cntU","cnta")
'	cntU.AddRowsM(2,False,0,0,"").AddCellsOS(2,0,0,0,12,12,6,"")
'	
'	cntU.BuildGrid
'	page.Cell(1,1).SetOffsetSize(0,2,2,12,8,8)
'	page.Cell(1,1).AddComponent(cntU)	
'
'	Dim imgl2 As ABMImage
'	imgl2.Initialize(page,"imgl2","../images/klogo.png",3)
'	cntU.Cell(1,1).AddComponent(imgl2)
'
'	cntL.Initialize(page,"cntL","")
'	cntL.AddRows(1,False,"").AddCellsOSMP(1,0,0,0,6,3,3,8,0,0,2,"").AddCellsOS(1,0,0,0,6,6,6,"")
'	cntL.AddRows(1,False,"").AddCellsOSMP(1,0,0,0,6,3,3,8,0,0,2,"").AddCellsOS(1,0,0,0,6,6,6,"")
'	cntL.AddRows(1,False,"").AddCellsOS(3,0,0,0,12,6,4,"")
'	cntL.BuildGrid
'	cntU.Cell(1,2).AddComponent(cntL)
'	Dim lblU As ABMLabel
'	lblU.Initialize(page,"lblU","Usuario",ABM.SIZE_H6,False,"")
'	cntL.Cell(1,1).UseTheme("")
'	cntL.Cell(1,1).AddComponent(lblU)
'	txtU.Initialize(page,"txtU",ABM.INPUT_TEXT,"",False,"caja")
'	txtU.Narrow=True
'	cntL.Cell(1,2).UseTheme("celdainp")
'	cntL.Cell(1,2).AddComponent(txtU)
'
'	Dim lblP As ABMLabel
'	lblP.Initialize(page,"lblP","Clave",ABM.SIZE_H6,False,"")
'	cntL.Cell(2,1).UseTheme("")
'	cntL.Cell(2,1).AddComponent(lblP)
'	Dim txtP As ABMInput
'	txtP.Initialize(page,"txtp", ABM.INPUT_PASSWORD  ,"",False,"")
'	txtP.Narrow=True
'	cntL.Cell(2,2).UseTheme("celdainp")
'	cntL.Cell(2,2).AddComponent(txtP)
'
'	Dim btnA, btnB, btnL As ABMButton
'	btnA.InitializeRaised(page,"btna","","","Acceder","btn")
'	cntL.Cell(3,1).AddComponent(btnA)	
'	
'	
'	btnB.InitializeRaised(page,"btnb","","","Registro","btn")
'	cntL.Cell(3,2).AddComponent(btnB)	
'
'	btnL.InitializeRaised(page,"btnL","","","Limpiar","btn")
'	cntL.Cell(3,3).AddComponent(btnL)	
'
'	Dim lblrst As ABMLabel
'	lblrst.Initialize(page,"lblrst","Si olvidó su clave haga clic {AL}{AT}aquí{/AL}",ABM.SIZE_SPAN,False,"")
'	cntU.Cell(2,2).SetOffsetSize(0,0,8,12,12,12)
'	cntU.Cell(2,2).AddComponent(lblrst)

	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub
Sub btna_Clicked(Target As String)
	
	
	Dim contenedoru As ABMContainer = page.Component("cntU")
	Dim contenedorl As ABMContainer = contenedoru.Component("cntL")
	Dim input As ABMInput = contenedorl.Component("txtU")
	Dim clave As ABMInput = contenedorl.Component("txtP")
	
	Dim valor , valor2 As String
	'valor = usuario.Text
	valor = txtU.ID
	valor = input.Text
	valor2 = clave.Text
	sql1.Initialize(Main.strDrv,Main.strUrl)
	Dim rst As ResultSet
    rst=sql1.ExecQuery("Select * from RH_UserConf where nom_user='"&valor&"' and nom_pass='"&valor2&"'")
	Do While rst.NextRow
		Log("logueando usuario " & rst.GetString("nom_nombre"))
		ws.Session.SetAttribute("authType", "local")
		If rst.GetString("st_active") = 1 Then
			ws.Session.SetAttribute("IsAuthorized", "true")	
		Else
			ws.Session.SetAttribute("IsAuthorized", "inactive")					
		End If	
		ws.Session.SetAttribute("authName", rst.GetString("nom_user"))
		ws.Session.SetAttribute("idsesion", rst.GetString("id_user"))
		ws.Session.SetAttribute("nombre", rst.GetString("nom_nombre"))
		ws.Session.SetAttribute("location", rst.GetString("nom_locacion"))
		ws.Session.SetAttribute("pais", rst.GetString("nom_pais"))
		ws.Session.SetAttribute("correo", rst.GetString("nom_email"))
		ws.Session.SetAttribute("Person_id",rst.GetString("Person_id"))
		ws.Session.SetAttribute("DBUse",rst.GetString("nom_db"))
				
		Main.idsesion = rst.GetString("id_user")
		Main.correo = rst.GetString("nom_email")
		Main.username = rst.GetString("nom_user")
		Main.fotoe = rst.GetString("nom_foto")
		Main.fondoe = rst.GetString("nom_fondo")
	    Main.nombrecompleto = rst.GetString("nom_nombre")
	Loop
    If ws.Session.GetAttribute2("IsAuthorized", "") = "true" Then
		ws.Session.MaxInactiveInterval= 5000
		ABMShared.NavigateToPage(ws,ABMPageId,"../bienvenida/bienvenida.html")
	Else
		If ws.Session.GetAttribute2("IsAuthorized", "") = "inactive" Then
			page.ShowModalSheet("modalinactive")
		Else
			page.ShowModalSheet("modalerror")
		End If	
	   	ws.Session.SetAttribute("authType", "")
		ws.Session.SetAttribute("authName", "")
		ws.Session.SetAttribute("IsAuthorized", "false")	
		ws.Session.SetAttribute("idsesion", "")	 	
	End If
End Sub

Sub btnb_Clicked(Target As String)
	 page.ShowModalSheet("modalnew")
End Sub


Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Ready()

	Connectpage
	page.RestoreNavigationBarPosition
End Sub

Sub modalbien() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "modalbien", False, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("modalfooter")
	myModal.IsDismissible = True
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModal.Content.BuildGrid
	Dim c As String = Main.correo
	Dim Message As String = "We are sending a new password to:{BR} "&c&" {BR} If the mail is correct press accept {BR}"
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_PARAGRAPH, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(2,0,0,0,12,6,6,"")
	myModal.Footer.BuildGrid 
	Dim aceptar As ABMButton
	aceptar.InitializeFlat(page, "aceptar", "", "", "Accept", "btn")
	myModal.Footer.Cell(1,1).AddComponent(aceptar)	
	Dim cancelar As ABMButton
	cancelar.InitializeFlat(page, "cancelar", "", "", "Cancel", "btn")
	myModal.Footer.Cell(1,2).AddComponent(cancelar)	
	Return myModal
End Sub


Sub modalcorreo() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "modalcorreo", False, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("modalfooter")
	myModal.Content.AddRows(2,True, "").AddCells12(1,"")
			
	myModal.Content.BuildGrid

	Dim c As String = Main.correo
	Dim Message As String = "Enter the correct email address: {BR}"
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_PARAGRAPH, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	Dim inp1 As ABMInput
    inp1.Initialize(page, "inp1", ABM.INPUT_TEXT, "Email", False, "")

	myModal.Content.Cell(2,1).AddComponent(inp1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(2,0,0,0,12,6,6,"")
	myModal.Footer.BuildGrid 
	Dim aceptar2 As ABMButton
	aceptar2.InitializeFlat(page, "aceptar2", "", "", "Accept", "btn")
	myModal.Footer.Cell(1,1).AddComponent(aceptar2)	
	Dim cancelar2 As ABMButton
	cancelar2.InitializeFlat(page, "cancelar2", "", "", "Cancel", "btn")
	myModal.Footer.Cell(1,2).AddComponent(cancelar2)	
	
	
	Return myModal
End Sub

Sub modalnew() As ABMModalSheet
	
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	
	sql1.Initialize(Main.strDrv,Main.strUrl)
	
	
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "modalnew", False, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("modalfooter")
		
	myModal.Content.AddRows(10,True, "").AddCells12(1,"")
			
	myModal.Content.BuildGrid

	'Dim c As String = Main.correo
	Dim Message As String = "Enter your personal data for log up: {BR}"
	
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_PARAGRAPH, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	
	Dim cmbSubDivision As ABMCombo
	cmbSubDivision.Initialize(page,"cmbSubDivision","Select Country or Division",250,"combo")
	myModal.Content.Cell(3,1).AddComponent(cmbSubDivision)
	rst=sql1.ExecQuery("Select * from RH_Country order by 1")
	
	pos=0
	Do While rst.NextRow
		cmbSubDivision.AddItem(rst.GetString2(0),rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	
	
		'Location
	Dim cmbLocation As ABMCombo	
	cmbLocation.Initialize(page,"cmbLocation","Select Location",250,"combo")
	myModal.Content.Cell(4,1).AddComponent(cmbLocation)
	
	
	Dim inp1 As ABMInput
    inp1.Initialize(page, "id", ABM.INPUT_TEXT, "USER ID", False, "")
	myModal.Content.Cell(5,1).AddComponent(inp1)
	
	Dim inp1 As ABMInput
    inp1.Initialize(page, "login", ABM.INPUT_TEXT, "USER LOGIN", False, "")
	myModal.Content.Cell(6,1).AddComponent(inp1)
	
	Dim inp1 As ABMInput
    inp1.Initialize(page, "email", ABM.INPUT_EMAIL, "EMAIL", False, "")
	myModal.Content.Cell(7,1).AddComponent(inp1)
	
	Dim inp1 As ABMInput
    inp1.Initialize(page, "password1", ABM.INPUT_PASSWORD, "PASSWORD", False, "")
	myModal.Content.Cell(8,1).AddComponent(inp1)
	
	Dim inp1 As ABMInput
    inp1.Initialize(page, "password2", ABM.INPUT_PASSWORD, "REPEAT PASSWORD", False, "")
	myModal.Content.Cell(9,1).AddComponent(inp1)
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(2,0,0,0,12,6,6,"")
	myModal.Footer.BuildGrid 
	Dim aceptar3 As ABMButton
	aceptar3.InitializeFlat(page, "aceptar3", "", "", "Accept", "btn")
	myModal.Footer.Cell(1,1).AddComponent(aceptar3)	
	Dim cancelar3 As ABMButton
	cancelar3.InitializeFlat(page, "cancelar3", "", "", "Cancel", "btn")
	myModal.Footer.Cell(1,2).AddComponent(cancelar3)	
	Return myModal
End Sub

Sub cmbSubDivision_Clicked(itemId As String)
	
	Dim modala As ABMModalSheet = page.ModalSheet("modalnew")
	Dim cmbx As ABMCombo=modala.Content.Cell(4,1).Component("cmbLocation")
	Dim c1 As ABMCombo=modala.Content.Cell(3,1).Component("cmbSubDivision")
	Dim id1 As String=c1.GetActiveItemId
	Dim lb1 As ABMLabel=c1.GetComponent(id1)
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	
	cmbx.Clear
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select distinct nom_propiedad from RH_SubDivision where nom_pais='" & lb1.Text & "'")
	'cmbx.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbx.AddItem(rst.GetString2(0) ,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	cmbx.Refresh
End Sub


Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub modalerror()  As ABMModalSheet
	Dim myModalError As ABMModalSheet
	myModalError.Initialize(page, "modalerror", False, False, "")
	myModalError.Content.UseTheme("modalcontent")
	myModalError.Footer.UseTheme("modalfooter")
	myModalError.IsDismissible = True
	
	' create the grid for the content
	myModalError.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModalError.Content.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", "The login or password are incorrect!",ABM.SIZE_PARAGRAPH, False, "")
	myModalError.Content.Cell(1,1).AddComponent(lbl1)
	
	Return myModalError
End Sub


Sub modalinactive()  As ABMModalSheet
	Dim myModalinactive As ABMModalSheet
	myModalinactive.Initialize(page, "modalinactive", False, False, "")
	myModalinactive.Content.UseTheme("modalcontent")
	myModalinactive.Footer.UseTheme("modalfooter")
	myModalinactive.IsDismissible = True
	
	' create the grid for the content
	myModalinactive.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModalinactive.Content.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", "Inactive User!",ABM.SIZE_PARAGRAPH, False, "")
	myModalinactive.Content.Cell(1,1).AddComponent(lbl1)
	
	Return myModalinactive
End Sub

Sub aceptar_Clicked(Target As String)
	
	
	ABMShared.NavigateToPage(ws,ABMPageId,"../miperfil/miperfil.html")			
End Sub

Sub cancelar_Clicked(Target As String)
	page.ShowModalSheet("modalcorreo")
	page.CloseModalSheet("modalbien")				
End Sub

Sub aceptar2_Clicked(Target As String)
	Dim mymodal As ABMModalSheet = page.ModalSheet("modalcorreo")
	Dim input1 As ABMInput = mymodal.Content.Cell(2,1).Component("inp1")
	Dim tucorreobueno As String = input1.Text
	
	
	ABMShared.NavigateToPage(ws,ABMPageId,"../miperfil/miperfil.html")			
End Sub

Sub cancelar2_Clicked(Target As String)
	ws.Session.SetAttribute("authType", "")
	ws.Session.SetAttribute("authName", "")
	ws.Session.SetAttribute("IsAuthorized", "false")	
	ws.Session.SetAttribute("idsesion", "")
	Main.idsesion = ""
	Main.correo = ""
	Main.nombrecompleto = ""
	Main.username = ""
	
	page.CloseModalSheet("modalcorreo")				
End Sub

Sub masuno (idtoast As Int) As Int
		myToastId=myToastId+1
		Return myToastId
End Sub


Sub aceptar3_Clicked(Target As String)
	Dim mymodal As ABMModalSheet = page.ModalSheet("modalnew")
	Dim sql1 , sql2 As SQL

	sql1.Initialize(Main.strDrv,Main.strUrl)
	Dim c1 As ABMCombo =  mymodal.Content.cell(3,1).Component("cmbSubDivision")
	Dim t1 As String = c1.GetActiveItemId
	If c1.GetActiveItemId ="-1" Then 
		page.ShowToast(masuno(myToastId),"toast1","You must select the country",3000)
		Return
	End If 
	Dim c2 As ABMCombo =  mymodal.Content.cell(4,1).Component("cmbLocation")
	Dim t2 As String = c2.GetActiveItemId
	If c2.GetActiveItemId ="-1" Then 
		page.ShowToast(masuno(myToastId),"toast1","You must select the location",3000)
		Return
	End If 
	Dim i1 As ABMInput = mymodal.Content.Cell(5,1).Component("id")
	Dim i2 As ABMInput = mymodal.Content.Cell(6,1).Component("login")
	Dim i3 As ABMInput = mymodal.Content.Cell(7,1).Component("email")
	Dim i4 As ABMInput = mymodal.Content.Cell(8,1).Component("password1")
	Dim i5 As ABMInput = mymodal.Content.Cell(9,1).Component("password2")
	If ABMShared.validatxt(i1.text) Then 
		page.ShowToast(masuno(myToastId),"toast1","You must fill the id",3000)
		Return
	End If
	If ABMShared.validatxt(i2.text) Then 
		page.ShowToast(masuno(myToastId),"toast1","You must fill the login",3000)
		Return
	End If
	If ABMShared.validatxt(i3.text) Then 
		page.ShowToast(masuno(myToastId),"toast1","You must fill the email",3000)
		Return
	End If
	If ABMShared.validatxt(i4.text) Then 
		page.ShowToast(masuno(myToastId),"toast1","You must fill the password1",3000)
		Return
	End If
	If ABMShared.validatxt(i5.text) Then 
		page.ShowToast(masuno(myToastId),"toast1","You must fill the password2",3000)
		Return
	End If	
	If Not(i5.text = i4.Text) Then 
		page.ShowToast(masuno(myToastId),"toast1","You must repeat the same password",3000)
		Return
	End If		
	Dim db As String
	db = sql1.ExecQuerySingleResult("select nom_db from RH_RelacionPropiedad where nom_pais='"&t1&"' and nom_propiedad='"&t2&"'") 
	correorh = sql1.ExecQuerySingleResult("select nom_correo from RH_RelacionPropiedad where nom_pais='"&t1&"' and nom_propiedad='"&t2&"'")
	sql2.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
	Dim nombre As String
	Dim consulta As String = "Select * FROM OPENQUERY(GIN, 'select nombrecompleto from "&db&".dbo.vw_lgen_altasybajas where PKey_Key1=''"&i1.text&"''')"
	Log(consulta)
	nombre = sql2.ExecQuerySingleResult(consulta) 		
	If nombre =Null Then
		page.ShowToast(masuno(myToastId),"toast1","You must use a correct id",3000)
		Return
	End If
 	Dim Person_id As String
	Dim consulta2 As String = "Select * FROM OPENQUERY(GIN, 'select Person_id from "&db&".dbo.vw_lgen_altasybajas where PKey_Key1=''"&i1.text&"''')"
	Person_id = sql2.ExecQuerySingleResult(consulta2) 		

 
 
	Dim nombrecompleto As String 
	nombrecompleto = nombre
	 
	Dim id_old As String
	id_old = "Old"   
	Dim cuenta As Int
	cuenta=sql1.ExecQuerySingleResult("select count(*) from RH_UserConf where nom_user='"&i2.text&"'")
	If cuenta>0 Then
		page.ShowToast(masuno(myToastId),"toast1","User already exist",3000)
		Return	 
	End If
	cuenta=sql1.ExecQuerySingleResult("select count(*) from RH_UserConf where id_User='"&i1.text&"'")
	If cuenta>0 Then
		page.ShowToast(masuno(myToastId),"toast1","id user already exist",3000)
		Return	 
	End If
	cuenta=sql1.ExecQuerySingleResult("select count(*) from RH_UserConf where nom_email='"&i3.text&"'")
	If cuenta>0 Then
		page.ShowToast(masuno(myToastId),"toast1","email user already exist",3000)
		Return	 
	End If
	page.ShowToast(masuno(myToastId),"toast1","Creating the user for "&nombrecompleto,3000)
	Dim valores() As Object
	valores=Array As Object (i1.Text,i2.Text,i5.Text,i3.Text,Null,Null,"0",id_old,Null,nombrecompleto,i1.Text,db,t1,t2,Person_id)
	sql1.ExecNonQuery2("INSERT INTO RH_UserConf VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )", valores)
	page.CloseModalSheet("modalnew")	
	Dim cuerpo() As String 
	Dim subjet As String = "User activation"
	Dim tittle As String = nombrecompleto&" Activation"
	Dim body As String ="Click on the link below to allow access to "&nombrecompleto&":"
	Dim idget As String
	idget = i1.text
	
	Dim link As String ="http://34.193.69.179:8099/suite/enableuser/enableuser.html?id&#61;"&idget&""
	Dim img As String ="http://34.193.69.179:8099/suite/images/klogo.png"
	cuerpo= Array As String (subjet,tittle,body,img,link)
	Dim destino() As String = Array As String (correorh)
	CallSubDelayed3(emailutils,"useremailsend",destino,cuerpo)
End Sub

Sub cancelar3_Clicked(Target As String)
	ws.Session.SetAttribute("authType", "")
	ws.Session.SetAttribute("authName", "")
	ws.Session.SetAttribute("IsAuthorized", "false")	
	ws.Session.SetAttribute("idsesion", "")
	Main.idsesion = ""
	Main.correo = ""
	Main.nombrecompleto = ""
	Main.username = ""
	
	page.CloseModalSheet("modalnew")				
End Sub

Sub btnL_Clicked(Target As String)
	  'ENTRADA FORZADA
	            Main.idsesion = "prueba"
				Main.correo = "joanpaz@gmail.com"
				Main.nombrecompleto = "Joan paz"
				Main.username = "jpaz"
				ws.Session.SetAttribute("authType", "local")
				ws.Session.SetAttribute("authName", "jpaz")
				ws.Session.SetAttribute("IsAuthorized", "true")	
				ws.Session.SetAttribute("idsesion","prueba")
				
				ABMShared.NavigateToPage(ws,ABMPageId,"../miperfil/miperfil.html")	
End Sub


Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

