﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "eval5"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "20"
	' your own variables		
	Dim myToastId As Int = 1
	Dim chkact As String
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	ConnectPage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------		
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")	
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
End Sub

public Sub BuildTheme()
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	theme.AddContainerTheme("conte")
	theme.Container("conte").ZDepth=ABM.ZDEPTH_3
	theme.AddDividerTheme("div1")
	theme.Divider("div1").ForeColor=ABM.COLOR_BLUE
	theme.Divider("div1").ForeColorIntensity=ABM.INTENSITY_LIGHTEN4
	theme.AddImageSliderTheme("sl1")
	theme.ImageSlider("sl1").Height=200
	theme.ImageSlider("sl1").Indicators=False
	theme.ImageSlider("sl1").FullWidth=True
	theme.ImageSlider("sl1").ZDepth=ABM.ZDEPTH_3
	
	theme.AddCellTheme("celt")
	theme.Cell("celt").BorderColor=ABM.COLOR_BLACK
	theme.Cell("celt").BorderWidth=1
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "eval5.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' Añadir Navigation Bar
	ABMShared.BuildNavigationBar(page,"Entrevista conductual","../images/klogo3.png","","eval5","")
	'page.AddRows(3,True,"").AddCells12(4,"")
	page.AddRows(2,True,"").AddCells12(1,"")
	
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
	
	chkact=""
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	Dim cmbDivision,cmbSubDivision,cmbLocation,cmbAreas,cmbDepartamento,cmbPuesto,cmbapplicant As ABMCombo

	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	Dim contenedora As ABMContainer	
	contenedora.Initialize(page,"contenedora","cardtheme2")
	contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	contenedora.AddRows(1,True, "").AddCellsOSMP(4,0,0,0,12,6,3,5,0,0,10,"")
	contenedora.AddRows(1,True, "").AddCellsOSMP(4,0,0,0,12,6,3,5,10,0,10,"")
	contenedora.BuildGrid
	page.Cell(1,1).SetOffsetSize(0,0,0,12,11,11)
	page.Cell(1,1).AddComponent(contenedora)
	Dim lbl As ABMLabel
	lbl.Initialize(page,"lbl","Datos de la postulación",ABM.SIZE_H6,False,"etiq")
	contenedora.Cell(1,1).AddComponent(lbl)
	contenedora.Cell(1,1).UseTheme("celda2")
	'Division
	cmbDivision.Initialize(page,"cmbDivision",  "División",150,"combo")
	cmbDivision.AddItem("P","",BuildSimpleItem("P","",""))
	contenedora.Cell(2,1).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(2,1).AddComponent(cmbDivision)
	rst=sql1.ExecQuery("Select * from RH_dIVISION order by 1")
	Do While rst.NextRow
		cmbDivision.AddItem(rst.GetString2(0),rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	
	'SubDivision	
	cmbSubDivision.Initialize(page,"cmbSubDivision","Seleccione el país",250,"combo")
	contenedora.Cell(2,2).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(2,2).AddComponent(cmbSubDivision)
	rst=sql1.ExecQuery("Select * from RH_Country order by 1")
	pos=0
	Do While rst.NextRow
		cmbSubDivision.AddItem(rst.GetString2(0),rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	
	'Location
	cmbLocation.Initialize(page,"cmbLocation","Seleccione la locación",250,"combo")
	contenedora.Cell(2,3).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(2,3).AddComponent(cmbLocation)
	
	'Areas
	cmbAreas.Initialize(page,"cmbAreas","Seleccione el Área",250,"combo")
	contenedora.Cell(2,4).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(2,4).AddComponent(cmbAreas)
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_Area order by 1")
	Do While rst.NextRow
		cmbAreas.AddItem(rst.GetString2(0),rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	
	cmbDepartamento.Initialize(page,"cmbDepartamento","Departamento",250,"combo")
	contenedora.Cell(3,1).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(3,1).AddComponent(cmbDepartamento)
	rst=sql1.ExecQuery("Select * from RH_Dept")
	pos=0
	Do While rst.NextRow
		cmbDepartamento.AddItem(rst.GetString2(0),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
		pos=pos+1
	Loop
	rst.close
'			'supervisores
'	cmbsupervisores.Initialize(page,"cmbsupervisores","Head Manager",250,"combo")
'	contenedora.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
'	contenedora.Cell(3,2).AddComponent(cmbsupervisores)
'	rst=sql1.ExecQuery("Select * from ER_USERS")
'	pos=0
'	'cmbsupervisores.AddItem("P","",BuildSimpleItem("P","",""))
'	Do While rst.NextRow
'		cmbsupervisores.AddItem( rst.GetString2(1),rst.GetString2(5),BuildSimpleItem("P"& pos,"",rst.GetString2(5)))
'		pos=pos+1
'	Loop
'	rst.close
	
	'Puesto
	cmbPuesto.Initialize(page,"cmbVacancy","Seleccione la vacante",250,"combo")
	contenedora.Cell(3,2).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(3,2).AddComponent(cmbPuesto)
	
	cmbapplicant.Initialize(page,"cmbApplicant","Seleccione al aplicante",250,"combo")
	contenedora.Cell(3,3).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(3,3).AddComponent(cmbapplicant)

	
	'pagina3
	Dim cnt1 As ABMContainer
	cnt1.Initialize(page,"cnt1","cardtheme2")
	cnt1.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,10,5,0,0,"celda2")
	cnt1.AddRows(1,False,"").AddCellsOSMP(1,0,1,1,12,10,10,5,0,0,0,"")
	cnt1.AddRows(5,False,"").AddCellsOSMP(1,0,1,1,12,10,10,0,0,30,0,"")
	cnt1.AddRows(1,False,"").AddCellsOSMP(3,0,0,1,12,4,4,10,0,0,0,"")
	cnt1.AddRows(1,False,"").AddCellsOSMP(1,0,1,1,10,10,10,0,0,0,0,"")'div
	cnt1.AddRows(1,False,"").AddCellsOSMP(1,0,0,1,12,4,4,10,0,0,0,"").AddCellsOSMP(2,0,0,1,12,4,4,0,0,0,0,"")
	cnt1.AddRows(1,False,"").AddCellsOSMP(1,0,1,1,10,10,10,0,0,0,0,"")'div
	cnt1.AddRows(1,False,"").AddCellsOSMP(1,0,0,1,12,4,4,10,0,0,0,"").AddCellsOSMP(2,0,0,1,12,4,4,0,0,0,0,"")
	cnt1.AddRows(1,False,"").AddCellsOSMP(1,0,1,1,10,10,10,0,0,0,0,"")'div
	cnt1.AddRows(1,False,"").AddCellsOSMP(1,0,0,1,12,4,4,10,0,0,0,"").AddCellsOSMP(2,0,0,1,12,4,4,0,0,0,0,"")
	cnt1.AddRows(1,False,"").AddCellsOSMP(1,0,1,1,10,10,10,0,0,0,0,"")'div
	cnt1.AddRows(1,False,"").AddCellsOSMP(1,0,0,1,12,4,4,10,0,0,0,"").AddCellsOSMP(2,0,0,1,12,4,4,0,0,0,0,"")
	cnt1.AddRows(1,False,"").AddCellsOSMP(1,0,1,1,10,10,10,0,0,0,0,"")'div
	cnt1.AddRows(1,False,"").AddCellsOSMP(1,0,0,1,12,4,4,10,0,0,0,"").AddCellsOSMP(2,0,0,1,12,4,4,0,0,0,0,"")
	cnt1.AddRows(1,False,"").AddCellsOSMP(1,0,1,1,10,10,10,0,0,0,0,"")'div
	cnt1.AddRows(1,False,"").AddCellsOSMP(1,0,0,1,12,4,4,10,0,0,0,"").AddCellsOSMP(2,0,0,1,12,4,4,0,0,0,0,"")
	cnt1.AddRows(1,False,"").AddCellsOSMP(1,0,1,1,10,10,10,0,0,0,0,"")'div
	cnt1.AddRows(1,False,"").AddCellsOSMP(1,0,0,1,12,4,4,10,0,0,0,"").AddCellsOSMP(2,0,0,1,12,4,4,0,0,0,0,"")
	cnt1.AddRows(1,False,"").AddCellsOSMP(1,0,1,1,10,10,10,0,0,0,0,"")'div
	cnt1.AddRows(1,False,"").AddCellsOSMP(1,0,0,1,12,4,4,10,0,0,0,"").AddCellsOSMP(2,0,0,1,12,4,4,0,0,0,0,"")'24
	cnt1.AddRows(1,False,"").AddCellsOSMP(1,0,1,1,10,10,10,0,0,0,0,"")'div25
	cnt1.AddRows(1,False,"").AddCellsOSMP(3,0,0,1,12,2,2,10,0,0,0,"").AddCellsOSMP(1,0,0,0,12,2,2,10,0,0,0,"")'26
	cnt1.AddRows(5,False,"").AddCellsOSMP(1,0,1,1,10,10,10,0,0,0,0,"")'div27-30

	
	cnt1.BuildGrid
	page.Cell(2,1).AddComponent(cnt1)
	Dim lblT1 As ABMLabel
	lblT1.Initialize(page,"lblT1","{B}Proceso de evaluación{/B}",ABM.SIZE_H6,False,"etiq")
	cnt1.Cell(1,1).UseTheme("celda2")
	cnt1.Cell(1,1).AddComponent(lblT1)
	cnt1.Cell(2,1).AddComponent(ABMShared.BuildParagraph(page,"0","{B}INSTRUCCIONES{/B}",ABM.SIZE_A))	
	cnt1.Cell(3,1).AddComponent(ABMShared.BuildParagraph(page,"0","1. Revise sus notas inmediatamente después de la entrevista",ABM.SIZE_A))
	cnt1.Cell(4,1).AddComponent(ABMShared.BuildParagraph(page,"0","2. Para cada pregunta, coloque una marca en {I}{B} la medida de desempeño {/ B} {/ I} que usted considera que está más en línea con su impresión del candidato, de acuerdo a las respuestas dadas",ABM.SIZE_A))	
	cnt1.Cell(5,1).AddComponent(ABMShared.BuildParagraph(page,"0","3. Haga un circulo en el número (1 - 9), indicando su puntuación en relación con el desempeño del candidato en su evaluación",ABM.SIZE_A))	
	cnt1.Cell(6,1).AddComponent(ABMShared.BuildParagraph(page,"0","4.	Repita el procedimiento en todas las competencias y traspase los puntos a la tabla de abajo",ABM.SIZE_A))	
	cnt1.Cell(7,1).AddComponent(ABMShared.BuildParagraph(page,"0","5. Reunirse con los otros entrevistadores para llegar a un consenso sobre el solicitante, indicando en la tabla los puntajes finales acordados en el grupo entrevistador (promedio de todos)",ABM.SIZE_A))	
	
	Dim lblS1 As ABMLabel
	lblS1.Initialize(page,"lblS1","COMPETICIÓN",ABM.SIZE_H6,False,"etiq")
	cnt1.Cell(8,1).UseTheme("celda2")
	cnt1.Cell(8,1).AddComponent(lblS1)
	
	Dim lblS2 As ABMLabel
	lblS2.Initialize(page,"lbls2","Puntaje individual",ABM.SIZE_H6,False,"etiq")
	cnt1.Cell(8,2).UseTheme("celda2")
	'cnt1.Cell(8,2).SetOffsetSize(0,1,1,12,2,2)
	cnt1.Cell(8,2).AddComponent(lblS2)
	'Espacio
	cnt1.Cell(9,1).AddComponent(ABMShared.Buildline(page,""))
	cnt1.Cell(10,1).AddComponent(ABMShared.BuildParagraph(page,"0","1. Experiencia previa, Educación",ABM.SIZE_H6))	
	Dim cmbs1 As ABMCombo
	cmbs1.Initialize(page,"cmbs1","",150,"")
	cmbs1.Narrow=True
	cargacombos(cmbs1)
	cnt1.Cell(10,2).UseTheme("")	
	cnt1.Cell(10,2).SetOffsetSize(0,1,1,12,2,2)
	cnt1.Cell(10,2).AddComponent(cmbs1)
	
	cnt1.Cell(11,1).AddComponent(ABMShared.Buildline(page,""))
	cnt1.Cell(12,1).AddComponent(ABMShared.BuildParagraph(page,"0","2. Exceder las expectativas de los huéspedes",ABM.SIZE_H6))	
	Dim cmbs2 As ABMCombo
	cmbs2.Initialize(page,"cmbs2","",150,"")
	cmbs2.Narrow=True
	cargacombos(cmbs2)
	cnt1.Cell(12,2).UseTheme("")	
	cnt1.Cell(12,2).SetOffsetSize(0,1,1,12,2,2)
	cnt1.Cell(12,2).AddComponent(cmbs2)

	cnt1.Cell(13,1).AddComponent(ABMShared.Buildline(page,""))
	cnt1.Cell(14,1).AddComponent(ABMShared.BuildParagraph(page,"0","3. Trabajo en equipo",ABM.SIZE_H6))	
	Dim cmbs3 As ABMCombo
	cmbs3.Initialize(page,"cmbs3","",150,"")
	cmbs3.Narrow=True
	cargacombos(cmbs3)
	cnt1.Cell(14,2).UseTheme("")	
	cnt1.Cell(14,2).SetOffsetSize(0,1,1,12,2,2)
	cnt1.Cell(14,2).AddComponent(cmbs3)
	
	cnt1.Cell(15,1).AddComponent(ABMShared.Buildline(page,""))
	cnt1.Cell(16,1).AddComponent(ABMShared.BuildParagraph(page,"0","4. Atención a los detalles",ABM.SIZE_H6))	
	Dim cmbs4 As ABMCombo
	cmbs4.Initialize(page,"cmbs4","",150,"")
	cmbs4.Narrow=True
	cargacombos(cmbs4)
	cnt1.Cell(16,2).UseTheme("")	
	cnt1.Cell(16,2).SetOffsetSize(0,1,1,12,2,2)
	cnt1.Cell(16,2).AddComponent(cmbs4)

	cnt1.Cell(17,1).AddComponent(ABMShared.Buildline(page,""))
	cnt1.Cell(18,1).AddComponent(ABMShared.BuildParagraph(page,"0","5. Aprendizaje y desarrollo",ABM.SIZE_H6))	
	Dim cmbs5 As ABMCombo
	cmbs5.Initialize(page,"cmbs5","",150,"")
	cmbs5.Narrow=True
	cargacombos(cmbs5)
	cnt1.Cell(18,2).UseTheme("")	
	cnt1.Cell(18,2).SetOffsetSize(0,1,1,12,2,2)
	cnt1.Cell(18,2).AddComponent(cmbs5)

	cnt1.Cell(19,1).AddComponent(ABMShared.Buildline(page,""))
	cnt1.Cell(20,1).AddComponent(ABMShared.BuildParagraph(page,"0","6. Communicación",ABM.SIZE_H6))	
	Dim cmbs6 As ABMCombo
	cmbs6.Initialize(page,"cmbs6","",150,"")
	cmbs6.Narrow=True
	cargacombos(cmbs6)
	cnt1.Cell(20,2).UseTheme("")	
	cnt1.Cell(20,2).SetOffsetSize(0,1,1,12,2,2)
	cnt1.Cell(20,2).AddComponent(cmbs6)

	cnt1.Cell(21,1).AddComponent(ABMShared.Buildline(page,""))
	cnt1.Cell(22,1).AddComponent(ABMShared.BuildParagraph(page,"0","7. Multiples tareas",ABM.SIZE_H6))	
	Dim cmbs7 As ABMCombo
	cmbs7.Initialize(page,"cmbs7","",150,"")
	cmbs7.Narrow=True
	cargacombos(cmbs7)
	cnt1.Cell(22,2).UseTheme("")	
	cnt1.Cell(22,2).SetOffsetSize(0,1,1,12,2,2)
	cnt1.Cell(22,2).AddComponent(cmbs7)

	cnt1.Cell(23,1).AddComponent(ABMShared.Buildline(page,""))
	cnt1.Cell(24,1).AddComponent(ABMShared.BuildParagraph(page,"0","8. Aceptación al cambio",ABM.SIZE_H6))	
	Dim cmbs8 As ABMCombo
	cmbs8.Initialize(page,"cmbs8","",150,"")
	cmbs8.Narrow=True
	cargacombos(cmbs8)
	cnt1.Cell(24,2).UseTheme("")	
	cnt1.Cell(24,2).SetOffsetSize(0,1,1,12,2,2)
	cnt1.Cell(24,2).AddComponent(cmbs8)

	cnt1.Cell(25,1).AddComponent(ABMShared.Buildline(page,""))
	
	Dim chk1,chk2,chk3 As ABMCheckbox
	chk1.Initialize(page,"chk1","Recomendar para contratar",False,"")
	chk1.IsFilled=True
	chk2.Initialize(page,"chk2","No recomendar para contratar",False,"")
	chk2.IsFilled=True
	chk3.Initialize(page,"chk3","En espera",False,"")
	chk3.IsFilled=True
	cnt1.Cell(26,1).AddComponent(chk1)
	cnt1.Cell(26,2).AddComponent(chk2)
	cnt1.Cell(26,3).AddComponent(chk3)
	Dim txtother As ABMInput
	txtother.Initialize(page,"txtother",ABM.INPUT_TEXT,"Recomendar para otra posición",False,"")
	txtother.Enabled=False
	cnt1.Cell(26,4).AddComponent(txtother)
	cnt1.Cell(27,1).AddComponent(ABMShared.Buildline(page,""))

	Dim txtname As ABMInput
	txtname.Initialize(page,"txtname",ABM.INPUT_TEXT,"NOMBRE DEL ENTREVISTADOR",False,"")	
	cnt1.Cell(29,1).AddComponent(txtname)
	
	Dim btnsave As ABMButton
	btnsave.InitializeFlat(page,"btnsave","","","Salvar","btn")
	cnt1.Cell(30,1).UseTheme("celdacenter")
	cnt1.Cell(30,1).AddComponent(btnsave)
	
	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub

Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then 
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub masuno (idtoast As Int) As Int
		myToastId=myToastId+1
		Return myToastId
End Sub
Sub chk1_Clicked(Target As String)
	chkact=Target	
	verifchk
End Sub
Sub chk2_Clicked(Target As String)
	chkact=Target	
	verifchk
End Sub
Sub chk3_Clicked(Target As String)
	chkact=Target	
	verifchk
End Sub

Sub verifchk
	Dim cntX As ABMContainer=page.Component("cnt1")
	Dim ch1 As ABMCheckbox=cntX.Component("chk1")
	Dim ch2 As ABMCheckbox=cntX.Component("chk2")
	Dim ch3 As ABMCheckbox=cntX.Component("chk3")
	Dim txtotherx As ABMInput=cntX.Component("txtother")
	If ch1.ID.Replace("cnt1-","")=chkact Then
		ch2.State=False
		ch2.Refresh
		ch3.State=False
		ch3.Refresh
		txtotherx.Enabled=False
		txtotherx.Text=""
		txtotherx.Refresh
	else if ch2.ID.Replace("cnt1-","")=chkact Then
		ch1.State=False
		ch1.Refresh
		ch3.State=False
		ch3.Refresh
		txtotherx.Enabled=False
		txtotherx.Text=""
		txtotherx.Refresh
	else if ch3.ID.Replace("cnt1-","")=chkact Then
		ch1.State=False
		ch1.Refresh
		ch2.State=False
		ch2.Refresh
		txtotherx.Enabled=True
		txtotherx.Text=""
		txtotherx.Refresh
	End If
End Sub
Sub btnsave_Clicked(Target As String)
	'validar
	myToastId=1
	Dim cnty As ABMContainer=page.Component("contenedora")
	Dim c1 As ABMCombo = cnty.Component("cmbApplicant")

	Dim cntX As ABMContainer=page.Component("cnt1")
	Dim cmbX1 As ABMCombo=cntX.Component("cmbs1")
	Dim cmbX2 As ABMCombo=cntX.Component("cmbs2")
	Dim cmbX3 As ABMCombo=cntX.Component("cmbs3")
	Dim cmbX4 As ABMCombo=cntX.Component("cmbs4")
	Dim cmbX5 As ABMCombo=cntX.Component("cmbs5")
	Dim cmbX6 As ABMCombo=cntX.Component("cmbs6")
	Dim cmbX7 As ABMCombo=cntX.Component("cmbs7")
	Dim cmbX8 As ABMCombo=cntX.Component("cmbs8")
	
	If c1.GetActiveItemId ="-1" Then page.ShowToast(masuno(myToastId),"toast1","seleccione el aplicante",3000)
	If cmbX1.GetActiveItemId ="-1" Then page.ShowToast(masuno(myToastId),"toast1","seleccione  la calificación del punto 1",3000)
	If cmbX2.GetActiveItemId ="-1" Then page.ShowToast(masuno(myToastId),"toast1","seleccione  la calificación del punto 2",3000)
	If cmbX3.GetActiveItemId ="-1" Then page.ShowToast(masuno(myToastId),"toast1","seleccione  la calificación del punto 3",3000)
	If cmbX4.GetActiveItemId ="-1" Then page.ShowToast(masuno(myToastId),"toast1","seleccione  la calificación del punto 4",3000)
	If cmbX5.GetActiveItemId ="-1" Then page.ShowToast(masuno(myToastId),"toast1","seleccione  la calificación del punto 5",3000)
	If cmbX6.GetActiveItemId ="-1" Then page.ShowToast(masuno(myToastId),"toast1","seleccione  la calificación del punto 6",3000)
	If cmbX7.GetActiveItemId ="-1" Then page.ShowToast(masuno(myToastId),"toast1","seleccione  la calificación del punto 7",3000)
	If cmbX8.GetActiveItemId ="-1" Then page.ShowToast(masuno(myToastId),"toast1","seleccione  la calificación del punto 8",3000)
	
	If myToastId>1 Then Return
	
	Dim txtotherx As ABMInput=cntX.Component("txtother")
	Dim txtnamex As ABMInput=cntX.Component("txtname")
	Dim Ck1,Ck2,Ck3 As Int
	If chkact="" Then
		page.ShowToast(masuno(myToastId),"toast1","Select the hiring option",3000)
		Return
	Else
		If chkact="chk1" Then
			Ck1=1
			Ck2=0
			Ck3=0
		else if chkact="chk2" Then
			Ck1=0
			Ck2=1
			Ck3=0
		else if chkact="chk3" Then
			Ck1=0
			Ck2=0
			Ck3=1			
		End If
	End If
	If txtnamex.Text="" Then 
		page.ShowToast(masuno(myToastId),"toast1","Escriba el nombre del entrevistador",3000)
		Return
 	End If
	Dim sql1 As SQL	
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	Dim valores() As String
	
	Dim L1 As ABMLabel =cmbX1.GetComponent(cmbX1.GetActiveItemId)
	Dim L2 As ABMLabel =cmbX2.GetComponent(cmbX2.GetActiveItemId)
	Dim L3 As ABMLabel =cmbX3.GetComponent(cmbX3.GetActiveItemId)
	Dim L4 As ABMLabel =cmbX4.GetComponent(cmbX4.GetActiveItemId)
	Dim L5 As ABMLabel =cmbX5.GetComponent(cmbX5.GetActiveItemId)
	Dim L6 As ABMLabel =cmbX6.GetComponent(cmbX6.GetActiveItemId)
	Dim L7 As ABMLabel =cmbX7.GetComponent(cmbX7.GetActiveItemId)
	Dim L8 As ABMLabel =cmbX8.GetComponent(cmbX8.GetActiveItemId)
	Dim strO As String
	
	If txtotherx.Text="" Then
		strO="-"
	Else
		strO=txtotherx.Text
	End If
	Dim fecha As String
	fecha=(DateTime.Date(DateTime.Now) & " " & DateTime.time(DateTime.Now))
	Dim idApl As String
	idApl=c1.GetActiveItemId	
	valores=Array As Object(idApl,L1.text,L2.Text,L3.Text,L4.Text,L5.Text,L6.Text,L7.Text,L8.Text,Ck1,Ck2,Ck3,	strO,txtnamex.Text.ToUpperCase,fecha)
	sql1.ExecNonQuery2("INSERT INTO RH_Behavioralinterview VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", valores)
	'valores=Array As Object("8282")
	'sql1.ExecNonQuery2("INSERT INTO RH_Behavioralinterview VALUES (?)", valores)
	page.ShowToast(masuno(myToastId),"toast1","Saved",3000)
	
	ABMShared.NavigateToPage(ws,ABMPageId,"../eval5/eval5.html")

	
End Sub


Sub cargacombos (combox As ABMCombo)
	For i=1 To 9
		Dim lblI As ABMLabel
		lblI.Initialize(page,"lblI"&i,i,ABM.SIZE_H6,True,"")
		combox.AddItem(i,i,lblI)
	Next
End Sub

Sub cmbDepartamento_Clicked(itemId As String)
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	Dim contenedora As ABMContainer = page.Component("contenedora")
	 Dim c1 As ABMCombo = contenedora.cell(2,1).Component("cmbDivision")
	 Dim t1 As String = c1.GetActiveItemId
	 Dim l1 As ABMLabel = c1.GetComponent(t1)
	  If c1.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar la división",3000)
		Return
	  End If
	 Dim c2 As ABMCombo = contenedora.cell(2,2).Component("cmbSubDivision")
	 Dim t2 As String = c2.GetActiveItemId
	 Dim l2 As ABMLabel = c2.GetComponent(t2)
	  If c2.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar el país",3000)
		Return
	  End If 
	 Dim c3 As ABMCombo = contenedora.cell(2,3).Component("cmbLocation")
	 Dim t3 As String = c3.GetActiveItemId
	 Dim l3 As ABMLabel = c3.GetComponent(t3)
	  If c3.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar la locación",3000)
		Return
	  End If 
	 Dim c4 As ABMCombo = contenedora.cell(2,4).Component("cmbAreas")
	 Dim t4 As String = c4.GetActiveItemId
	 Dim l4 As ABMLabel = c4.GetComponent(t4)
	 If c4.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar el área",3000)
		Return
	 End If
	Dim cmbx As ABMCombo=contenedora.Cell(3,2).Component("cmbVacancy")
	Dim cmby As ABMCombo=contenedora.Cell(3,2).Component("cmbVacancy")
	cmby.Clear
	cmby.Refresh
	Dim nom_departamento As String 
	nom_departamento = sql1.ExecQuerySingleResult("Select Dept_Name from RH_Dept where Dept_Key='" & itemId & "' order by 1")
	rst=sql1.ExecQuery("Select * from RH_PersonnelRequisition where nom_division='"&t1&"' and nom_country='"&t2&"' and nom_location='"&t3&"' and nom_area='"&t4&"' and nom_department='" & nom_departamento & "' order by 1")
	Do While rst.NextRow
		cmbx.AddItem(rst.GetString2(0),rst.GetString2(6),BuildSimpleItem("P"& pos,"",rst.GetString2(6)))
		pos=pos+1
	Loop
	rst.Close
	cmbx.Refresh
End Sub
Sub cmbVacancy_Clicked(itemId As String)
	Dim contenedora As ABMContainer = page.Component("contenedora")
	Dim cmby As ABMCombo=contenedora.Cell(3,3).Component("cmbApplicant")
	cmby.Clear
	cmby.Refresh
	Dim sql1, sql2 As SQL
	Dim rst, rst2 As ResultSet
	Dim pos , pos2 As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	sql2.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
'	Dim posname As String
'	posname =sql1.ExecQuerySingleResult("Select nom_position from RH_PersonnelRequisition where id_personnelreq = '"&itemId&"'")
'	Dim poskey As String
'	poskey = sql1.ExecQuerySingleResult("Select Pos_Key from RH_Positons where Pos_Name='" & posname & "' order by 1")
'	rst=sql1.ExecQuery("Select * from RH_Application where Pos_Key='" &poskey& "'")
															
	rst=sql1.ExecQuery("Select * from RH_Application where Pos_Key='" &itemId& "'")	
	Do While rst.NextRow
		rst2 = 	sql2.ExecQuery("Select * from RH_Applicant where id_Applicant='" &rst.GetString2(7)& "'")	
		Do While rst2.NextRow
			Dim Name As String = rst2.GetString2(4)&" "&rst2.GetString2(5)&" "&rst2.GetString2(2)&" "&rst2.GetString2(3)
			 cmby.AddItem(rst2.GetString2(1),Name,BuildSimpleItem("P"& pos,"",Name))
			pos2 = pos2+1	
		Loop
		rst2.Close
		cmby.Refresh
		pos=pos+1
	Loop
	rst.Close
End Sub

Sub cmbSubDivision_Clicked(itemId As String)
		Dim contenedora As ABMContainer = page.Component("contenedora")
	Dim cmbx As ABMCombo=contenedora.Cell(2,3).Component("cmbLocation")
	Dim c1 As ABMCombo=contenedora.Cell(2,2).Component("cmbSubDivision")
	Dim id1 As String=c1.GetActiveItemId
	Dim lb1 As ABMLabel=c1.GetComponent(id1)
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select distinct nom_propiedad from RH_SubDivision where nom_pais='" & lb1.Text & "'")
	cmbx.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbx.AddItem(rst.GetString2(0) ,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	cmbx.Refresh

End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
'	If icon <> "" Then
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
'	Else
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int 
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
	     combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next 
	
	'page.Refresh
	'combo1.Refresh	   
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid 
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)	
	Return myModal
End Sub

Sub Page_Ready()
	Log("Lista eval5")
	ConnectPage
	page.RestoreNavigationBarPosition
End Sub

