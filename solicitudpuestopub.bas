﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "solicitudpuestopub"  '<-------------------------------------------------------- IMPORTANT
	' name of the app, same as in ABMApplication
	'Public AppName As String = "suite"          '<-------------------------------------------------------- IMPORTANT
	
	Private ABMPageId As String = "0"
	
	' your own variables
	Dim cmbDivision,cmbSubDivision,cmbLocation,cmbAreas,cmbDepartamento,cmbPuesto,cmbcto,cmbTipo,cmbNum,cmbnivel,cmbempleados,cmbsupervisores As ABMCombo
	Dim anos As List
	
	
    Dim cantidiomas As Int = 1 
	Dim cantexp As Int = 1 
	Dim cantedu As Int = 1 
	Dim cantsk As Int = 1 
	
	
	Private listidiomas As List = Array As String ("English", "German", "Chinesse")
	Private listskills As List = Array As String ("Software", "Working Skills", "Other")
	Dim myToastId As Int = 1
	Private cantref As Int = 3
	
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
		'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	Connectpage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")
End Sub


Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
End Sub


public Sub BuildTheme()
	' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	
	
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	theme.AddContainerTheme("conte")
	theme.Container("conte").ZDepth=ABM.ZDEPTH_3
	theme.AddDividerTheme("div1")
	theme.Divider("div1").ForeColor=ABM.COLOR_BLUE
	theme.Divider("div1").ForeColorIntensity=ABM.INTENSITY_LIGHTEN4
	theme.AddImageSliderTheme("sl1")
	theme.ImageSlider("sl1").Height=200
	theme.ImageSlider("sl1").Indicators=False
	theme.ImageSlider("sl1").FullWidth=True
	theme.ImageSlider("sl1").ZDepth=ABM.ZDEPTH_3
	
	theme.AddInputTheme("inp1")
	theme.Input("inp1").ZDepth = ABM.ZDEPTH_3
	
	theme.AddDateTimePickerTheme("date1")
	'theme.DateTimePicker("date1"). 
	
	' add additional themes specific for this page
End Sub


public Sub BuildPage()
	' initialize the theme
	BuildTheme
	page.SetLoaderJUMPINGBALL(ABM.COLOR_BLACK, ABM.INTENSITY_NORMAL, ABM.COLOR_WHITE, ABM.INTENSITY_NORMAL, "Por favor espere")
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "solicitudpuestopub.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' adding a navigation bar
	
	
		'ABMshared.CrearNavBar(page,"Job Application ","../images/klogo3.png","","solicitudpuesto","")
	
	
			
	' create the page grid
	page.AddRows(1,True, "").AddCells12MP(1,20,0,0,0,"") '1
	page.AddRows(1,True, "").AddCells12MP(1,20,0,0,0,"") '2
	page.AddRows(1,True, "").AddCells12(2,"") '3
	page.AddRows(1,True, "").AddCells12(2,"") '4
	page.AddRows(1,True, "").AddCells12(1,"") '5
	page.AddRows(1,True, "").AddCells12(3,"") '6
	page.AddRows(1,True, "").AddCells12(1,"") '7
	page.AddRows(1,True, "").AddCells12(1,"") '8
	page.AddRows(1,True, "").AddCells12(3,"") '9
	page.AddRows(1,True, "").AddCells12(1,"") '10
	page.AddRows(1,True, "").AddCells12(3,"") '11
	page.AddRows(1,True, "").AddCells12(1,"") '12
	page.AddRows(1,True, "").AddCells12(1,"") '13
	page.AddRows(1,True, "").AddCells12(1,"") '14   idioma
	page.AddRows(1,True, "").AddCells12(2,"") '15 
	page.AddRows(1,True, "").AddCells12(1,"") '16
	page.AddRows(1,True, "").AddCells12(1,"") '17
	page.AddRows(1,True, "").AddCells12(1,"") '18
	page.AddRows(1,True, "").AddCells12(1,"") '19
	page.AddRows(1,True, "").AddCells12(1,"") '20
	page.AddRows(1,True, "").AddCells12(1,"") '21
	page.AddRows(1,True, "").AddCells12(1,"") '22
	page.AddRows(1,True, "").AddCells12(1,"") '23
	page.AddRows(1,True, "").AddCells12(1,"") '24
	page.AddRows(1,True, "").AddCells12(1,"") '25
	page.AddRows(1,True, "").AddCells12(1,"") '26
	page.AddRows(1,True, "").AddCells12(1,"") '27
	page.AddRows(1,True, "").AddCells12(1,"") ' 28 EL BOTON ACEPTAR PARA GUARDAR
	page.AddRows(1,True, "").AddCells12(1,"") '29

	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
	
End Sub

Sub Connectpage
	
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	
	anos.Initialize
	anos.Add("En proceso")
	For i=2016 To 1970 Step -1
		anos.Add(i)
	Next
	anos.Add("N/A")
	page.AddModalSheetTemplate(Mensajes)
	
	
	
	
	
	'-----------------------------------------------------------------------------     PRIMER FILTRO 
	
	Dim contenedora As ABMContainer	
	contenedora.Initialize(page,"contenedora","cardtheme2")
	'contenedora.AddRows
	
	contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	contenedora.AddRows(1,True, "").AddCellsOSMP(4,0,0,0,12,6,3,5,0,0,10,"")
	contenedora.AddRows(1,True, "").AddCells12MP(1,0,30,0,0,"").AddCells12(1,"").AddCells12(1,"")
	'contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	'contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	
	contenedora.BuildGrid
	page.Cell(1,1).SetOffsetSize(0,0,0,12,11,11)
	page.Cell(1,1).AddComponent(contenedora)
	
	
	
	Dim lbl As ABMLabel
	lbl.Initialize(page,"lbl","Seleccion de vacante:",ABM.SIZE_H6,False,"etiq")
	contenedora.Cell(1,1).AddComponent(lbl)
	contenedora.Cell(1,1).UseTheme("celda2")
	
	
	
		
		
	'Division
	cmbDivision.Initialize(page,"cmbDivision",  "División",150,"combo")
	cmbDivision.AddItem("P","",BuildSimpleItem("P","",""))
	contenedora.Cell(2,1).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(2,1).AddComponent(cmbDivision)
	
	rst=sql1.ExecQuery("Select * from RH_dIVISION order by 1")
	Do While rst.NextRow
		cmbDivision.AddItem(rst.GetString2(0),rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	
	'SubDivision	
	cmbSubDivision.Initialize(page,"cmbSubDivision","Seleccione de país",250,"combo")
	contenedora.Cell(2,2).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(2,2).AddComponent(cmbSubDivision)
	
	rst=sql1.ExecQuery("Select * from RH_Country order by 1")
	
	pos=0
	Do While rst.NextRow
		cmbSubDivision.AddItem(rst.GetString2(0),rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	
	'Location
	cmbLocation.Initialize(page,"cmbLocation","Seleccione locación",250,"combo")
	contenedora.Cell(2,3).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(2,3).AddComponent(cmbLocation)
	
	
	'Areas
	cmbAreas.Initialize(page,"cmbAreas","Select Area",250,"combo")
	contenedora.Cell(2,4).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(2,4).AddComponent(cmbAreas)
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_Area order by 1")
	Do While rst.NextRow
		cmbAreas.AddItem(rst.GetString2(0),rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	
	
	
	
	
	
	cmbDepartamento.Initialize(page,"cmbDepartamento","Departamento",250,"combo")
	contenedora.Cell(3,1).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(3,1).AddComponent(cmbDepartamento)
	rst=sql1.ExecQuery("Select * from RH_Dept")
	pos=0
	'cmbDepartamento.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbDepartamento.AddItem(rst.GetString2(0),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
		pos=pos+1
	Loop
	rst.close
	
	
			'supervisores
	cmbsupervisores.Initialize(page,"cmbsupervisores","Supervisor",250,"combo")
	contenedora.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
	contenedora.Cell(3,2).AddComponent(cmbsupervisores)
	rst=sql1.ExecQuery("Select * from ER_USERS")
	pos=0
	'cmbsupervisores.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbsupervisores.AddItem( rst.GetString2(1),rst.GetString2(5),BuildSimpleItem("P"& pos,"",rst.GetString2(5)))
		pos=pos+1
	Loop
	rst.close
	
	
	'Puesto
	cmbPuesto.Initialize(page,"cmbVacancy","Seleccione la vacante",250,"combo")
	contenedora.Cell(3,3).SetOffsetSize(0,0,1,12,6,4)
	contenedora.Cell(3,3).AddComponent(cmbPuesto)
	
	
  	'-------------------------------------------------------------------------------------    DATOS GENERALES
	
	Dim contenedorb As ABMContainer	
	contenedorb.Initialize(page,"contenedorb","cardtheme2")
	'contenedora.AddRows
	
	contenedorb.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	contenedorb.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	contenedorb.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	contenedorb.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	contenedorb.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	contenedorb.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	contenedorb.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(1,0,0,10,0,"")
	
	contenedorb.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	contenedorb.AddRows(1,True, "").AddCells12MP(1,0,30,0,0,"").AddCells12(1,"").AddCells12(1,"")
	'contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	'contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	
	contenedorb.BuildGrid
	page.Cell(2,1).SetOffsetSize(0,0,0,12,11,11)
	page.Cell(2,1).AddComponent(contenedorb)
	
	
	
	
	
	Dim lb2 As ABMLabel
	lb2.Initialize(page,"lb2","Datos generales del aplicante",ABM.SIZE_H6,False,"etiq")
	contenedorb.Cell(1,1).AddComponent(lb2)
	contenedorb.Cell(1,1).UseTheme("celda2")

    Dim inp1 As ABMInput
    inp1.Initialize(page, "name", ABM.INPUT_TEXT, "Nombre", False, "inp1")
    inp1.IconName = "mdi-action-account-circle"
    contenedorb.Cell(2,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(2,1).AddComponent(inp1)
	
	 Dim inp1_1 As ABMInput
    inp1_1.Initialize(page, "name2", ABM.INPUT_TEXT, "Segundo nombre", False, "inp1")
    inp1_1.IconName = "mdi-action-account-circle"
    contenedorb.Cell(2,2).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(2,2).AddComponent(inp1_1)
	
	 Dim inp2 As ABMInput
    inp2.Initialize(page, "last", ABM.INPUT_TEXT, "Apellido", False, "inp1")
    inp2.IconName = "mdi-action-account-circle"
    contenedorb.Cell(2,3).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(2,3).AddComponent(inp2)
	
	 Dim inp2_1 As ABMInput
    inp2_1.Initialize(page, "last2", ABM.INPUT_TEXT, "Segundo apellido", False, "inp1")
    inp2_1.IconName = "mdi-action-account-circle"
    contenedorb.Cell(2,4).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(2,4).AddComponent(inp2_1)
	
'	Dim edad As ABMCombo
'	edad.Initialize(page,"edad", "Age" ,650,"combo")
'	For i=15 To 100
'		edad.AddItem("E" & i, i,BuildSimpleItem("E"&i,"",i))
'	Next

	

'	Dim nac As ABMDateTimePicker
'	Dim NewDate As Long = DateTime.Now
'	nac.Initialize(page, "birth", ABM.DATETIMEPICKER_TYPE_DATE, NewDate, "Birth", "")
'	nac.CancelText = "Back"
'	nac.PickText = "OK"
'	nac.TodayText = "Today"
'	nac.idioma = "en"
'	nac.ReturnDateFormat = "MM/DD/YYYY"   
'	'nac.ReturnTimeFormat = "HH:mm"  
'	     
'	nac.FirstDayOfWeek = 0
'	nac.ClickThrough = True 
	
	Dim nac As ABMDateTimeScroller
	Dim NewDate As Long = DateTime.Now
	nac.Initialize(page, "birth", ABM.DATETIMESCROLLER_TYPE_DATE, ABM.DATETIMESCROLLER_MODE_MIXED, NewDate, "Fecha de nacimiento", "")
	nac.DateDayNames = "['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado']"
	nac.DateMonthNamesShort = "['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']"
	nac.TitleDateFormat = "DDD"
	nac.ReturnDateFormat = "mm/dd/yy" ' <--- see demo source code for settings
	nac.TitleTimeFormat = ""          ' <--- see demo source code for settings
	nac.ReturnTimeFormat = "HH:ii"    ' <--- see demo source code for settings  
	nac.DateOrder = "Mddyy"           ' <--- see demo source code for settings
	nac.TimeShowAMPM = False
	nac.CancelText = "Cerrar"
	nac.PickText = "OK"
	
	
	
	contenedorb.Cell(3,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(3,1).AddComponent(nac)

	Dim ecivil As ABMCombo 
	ecivil.Initialize(page,"ecivil","Estado civil",650,"combo")
	ecivil.IconName = "mdi-action-account-circle"
	ecivil.WrongMessage="Seleccione el estado civil"
	ecivil.AddItem("Casado","Casado",BuildSimpleItem("Casado", "mdi-action-account-circle", "{NBSP}{NBSP}Casado"))
	ecivil.AddItem("Soltero","Soltero",BuildSimpleItem("Soltero", "mdi-action-account-circle", "{NBSP}{NBSP}Soltero"))
	ecivil.AddItem("Viudo","Viudo",BuildSimpleItem("Viudo", "mdi-action-account-circle", "{NBSP}{NBSP}Viudo"))
	contenedorb.Cell(3,2).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(3,2).AddComponent(ecivil)
	
	Dim inp2 As ABMInput
    inp2.Initialize(page, "id", ABM.INPUT_TEXT, "Documento de identidad", False, "inp1")
    inp2.IconName = "mdi-action-account-circle"
    contenedorb.Cell(3,3).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(3,3).AddComponent(inp2)
	
	Dim inp2 As ABMInput
    inp2.Initialize(page, "passport", ABM.INPUT_TEXT, "Pasaporte", False, "inp1")
    inp2.IconName = "mdi-action-account-circle"
    contenedorb.Cell(3,4).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(3,4).AddComponent(inp2)
	
	
	Dim inp3 As ABMInput
    inp3.Initialize(page, "nac", ABM.INPUT_TEXT, "Nacionalidad", False, "inp1")
    inp3.IconName = "mdi-action-account-circle"
    contenedorb.Cell(4,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(4,1).AddComponent(inp3)
	
	
	Dim pais As ABMCombo
	'Pais	
	pais.Initialize(page,"country","País",250,"combo")
	contenedorb.Cell(4,2).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(4,2).AddComponent(pais)
	rst=sql1.ExecQuery("Select * from RH_Country order by 1")
	pais.AddItem("P","",BuildSimpleItem("P","",""))
	pos=0
	Do While rst.NextRow
		pais.AddItem("P"& pos,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	
	Dim inp4 As ABMInput
    inp4.Initialize(page, "state", ABM.INPUT_TEXT, "Estado", False, "inp1")
    inp4.IconName = "mdi-action-account-circle"
    contenedorb.Cell(4,3).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(4,3).AddComponent(inp4)
	
	Dim inp5 As ABMInput
    inp5.Initialize(page, "city", ABM.INPUT_TEXT, "Ciudad", False, "inp1")
    inp5.IconName = "mdi-action-account-circle"
    contenedorb.Cell(4,4).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(4,4).AddComponent(inp5)
	
	Dim inp6 As ABMInput
    inp6.Initialize(page, "addr", ABM.INPUT_TEXT, "Dirección", False, "inp1")
    inp6.IconName = "mdi-action-account-circle"
    contenedorb.Cell(5,1).SetOffsetSize(0,0,0,12,12,12)
	contenedorb.Cell(5,1).AddComponent(inp6)
	
	Dim inp6 As ABMInput
    inp6.Initialize(page, "phone1", ABM.INPUT_TEL, "Número de teléfono", False, "inp1")
    inp6.IconName = "mdi-action-account-circle"
    contenedorb.Cell(6,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(6,1).AddComponent(inp6)
	
	Dim inp7 As ABMInput
    inp7.Initialize(page, "phone2", ABM.INPUT_TEL, "Número móvil", False, "inp1")
    inp7.IconName = "mdi-action-account-circle"
    contenedorb.Cell(6,2).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(6,2).AddComponent(inp7)
	
	Dim inp8 As ABMInput
    inp8.Initialize(page, "phone3", ABM.INPUT_TEL, "Número móvil 2", False, "inp1")
    inp8.IconName = "mdi-action-account-circle"
    contenedorb.Cell(6,3).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(6,3).AddComponent(inp8)
	
	
	Dim inp9 As ABMInput
    inp9.Initialize(page, "email", ABM.INPUT_EMAIL, "Correo electrónico", False, "inp1")
    inp9.IconName = "mdi-action-account-circle"
    contenedorb.Cell(6,4).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(6,4).AddComponent(inp9)
	
	
	
	Dim depend As ABMCombo
	depend.Initialize(page,"depend", "Dependientes" ,650,"combo")
	For i=0 To 10
		depend.AddItem("D" & i, i,BuildSimpleItem("D"&i,"",i))
	Next
	contenedorb.Cell(7,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(7,1).AddComponent(depend)
	
	
	Dim inp9 As ABMInput
    inp9.Initialize(page, "family", ABM.INPUT_TEXT, "Información familiar adic.", False, "inp1")
    contenedorb.Cell(7,2).SetOffsetSize(0,0,0,12,6,9)
	contenedorb.Cell(7,2).AddComponent(inp9)
	
	
	'-------------------------------------------------------------------------------------------    EDUCACION
	
	Dim contenedorc As ABMContainer	
	contenedorc.Initialize(page,"contenedorc","cardtheme2")
	'contenedora.AddRows
	
	contenedorc.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	contenedorc.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	contenedorc.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	'contenedorc.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	'contenedorc.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	'contenedorc.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	'contenedorc.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(1,0,0,10,0,"")
	
	'contenedorc.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	contenedorc.AddRows(1,True, "").AddCells12MP(1,0,30,0,0,"").AddCells12(1,"").AddCells12(1,"")
	'contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	'contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	
	contenedorc.BuildGrid
	page.Cell(3,1).SetOffsetSize(0,0,0,12,11,11)
	page.Cell(3,1).AddComponent(contenedorc)
	
	
	
	
	
	Dim lb3 As ABMLabel
	lb3.Initialize(page,"lb3","Educación",ABM.SIZE_H6,False,"etiq")
	contenedorc.Cell(1,1).UseTheme("celda2")
	contenedorc.Cell(1,1).AddComponent(lb3)
	
	
	
	cantedu = 1
	Dim contenedor3 As ABMContainer
	contenedor3.Initialize(page, "contenedor3", "")
	contenedor3.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(2,0,0,10,0,"")
	contenedor3.BuildGrid
	
	contenedor3.Cell(1,3).SetOffsetSize(0,0,0,12,6,4)
	contenedor3.Cell(1,3).AddComponent(buildcombo("edut1","","Fecha de finalización", anos))
	
	 Dim inp3 As ABMInput
     inp3.Initialize(page, "univ1", ABM.INPUT_TEXT, "Grado educativo", False, "inp1")
	 contenedor3.Cell(1,1).SetOffsetSize(0,0,0,12,6,4)
	 contenedor3.Cell(1,1).AddComponent(inp3)
	
	
	 Dim inp4 As ABMInput
     inp4.Initialize(page, "cert1", ABM.INPUT_TEXT, "Certificación o diploma", False, "inp1")
	 
	 contenedor3.Cell(1,2).SetOffsetSize(0,0,0,12,6,4)
	 contenedor3.Cell(1,2).AddComponent(inp4)
	 
     contenedorc.Cell(2,1).AddComponent(contenedor3)


	Dim btnaggedu As ABMButton
	btnaggedu.InitializeFlat(page,"btnaggedu","mdi-content-add", ABM.ICONALIGN_LEFT ,"Add","btn")
	contenedorc.Cell(3,1).SetOffsetSize(0,0,0,10,3,3)
	contenedorc.Cell(3,1).AddComponent(btnaggedu)
	
	Dim btnrestedu As ABMButton
	btnrestedu.InitializeFlat(page,"btnrestedu","mdi-content-remove", ABM.ICONALIGN_LEFT ,"Remove","btn")
	contenedorc.Cell(3,2).SetOffsetSize(0,0,1,10,3,3)
	contenedorc.Cell(3,2).AddComponent(btnrestedu)

	'-------------------------------------------------------------------------------------------    EXPERIENCIA PROF
	
	Dim contenedord As ABMContainer	
	contenedord.Initialize(page,"contenedord","cardtheme2")
	'contenedora.AddRows
	
	contenedord.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	contenedord.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	contenedord.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	contenedord.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	'contenedord.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	'contenedord.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	'contenedord.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	'contenedord.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(1,0,0,10,0,"")
	
	'contenedord.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	contenedord.AddRows(1,True, "").AddCells12MP(1,0,30,0,0,"").AddCells12(1,"").AddCells12(1,"")
	'contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	'contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	
	contenedord.BuildGrid
	page.Cell(4,1).SetOffsetSize(0,0,0,12,11,11)
	page.Cell(4,1).AddComponent(contenedord)
	
	
	
	
	Dim lb4 As ABMLabel
	lb4.Initialize(page,"lb4","Experiencia profesional",ABM.SIZE_H6,False,"etiq")
	contenedord.Cell(1,1).UseTheme("celda2")
	contenedord.Cell(1,1).AddComponent(lb4)
	
	
	cantexp = 1
	Dim contenedor2 As ABMContainer
	contenedor2.Initialize(page, "contenedor2", "")
	
	contenedor2.AddRows(1,True,"").AddCells12MP(1,0,0,0,0,"").AddCells12MP(5,0,0,10,0,"")
	contenedor2.AddRows(1,True,"").AddCells12MP(1,0,0,0,0,"")
	contenedor2.BuildGrid
	
	contenedor2.Cell(1,3).SetOffsetSize(0,0,0,12,6,2)
	contenedor2.Cell(1,3).AddComponent(buildcombo("expi1","","Fecha inicio", anos))
	contenedor2.Cell(1,4).SetOffsetSize(0,0,0,12,6,2)
	contenedor2.Cell(1,4).AddComponent(buildcombo("expf1","","Fecha fin", anos))
	
	Dim inp5 As ABMInput
    inp5.Initialize(page, "position1", ABM.INPUT_TEXT, "Cargo", False, "inp1")
	contenedor2.Cell(1,2).SetOffsetSize(0,0,0,12,6,3)
	contenedor2.Cell(1,2).AddComponent(inp5)
	

	
	Dim inp5 As ABMInput
    inp5.Initialize(page, "enterprice1", ABM.INPUT_TEXT, "Compañia", False, "inp1")
	contenedor2.Cell(1,1).SetOffsetSize(0,0,0,12,6,3)
	contenedor2.Cell(1,1).AddComponent(inp5)
	
	Dim inp5 As ABMInput
    inp5.Initialize(page, "jobphone1", ABM.INPUT_TEXT, "Teléfono de contacto", False, "inp1")
	contenedor2.Cell(1,5).SetOffsetSize(0,0,0,12,12,2)
	contenedor2.Cell(1,5).AddComponent(inp5)
	
	Dim inp5 As ABMInput
    inp5.Initialize(page, "reason1", ABM.INPUT_TEXT, "Razón", False, "inp1")
	contenedor2.Cell(1,6).SetOffsetSize(0,0,0,12,12,12)
	contenedor2.Cell(1,6).AddComponent(inp5)
	
	
	
	
	contenedord.Cell(2,1).AddComponent(contenedor2)
	
	
	
	Dim btnaggexp As ABMButton
	btnaggexp.InitializeFlat(page,"btnaggexp","mdi-content-add", ABM.ICONALIGN_LEFT ,"+","btn")
	contenedord.Cell(3,1).SetOffsetSize(0,0,0,10,3,3)
	contenedord.Cell(3,1).AddComponent(btnaggexp)
	
	Dim btnrestexp As ABMButton
	btnrestexp.InitializeFlat(page,"btnrestexp","mdi-content-remove", ABM.ICONALIGN_LEFT ,"-","btn")
	contenedord.Cell(3,2).SetOffsetSize(0,0,0,10,3,3)
	contenedord.Cell(3,2).AddComponent(btnrestexp)
		
	Dim inp6 As ABMInput
    inp6.Initialize(page, "great", ABM.INPUT_TEXT, "Mayores logros en su carrera profesional", False, "inp1")
    'page.Cell(12,1).SetOffsetSize(0,0,0,12,6,3)
	contenedord.Cell(4,1).AddComponent(inp6)
	
	'-------------------------------------------------------------------------------------------   IDIOMAS
	
	Dim contenedore As ABMContainer	
	contenedore.Initialize(page,"contenedore","cardtheme2")
	'contenedora.AddRows
	
	contenedore.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	contenedore.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	contenedore.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	contenedore.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	'contenedore.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	'contenedore.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	'contenedore.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	'contenedore.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(1,0,0,10,0,"")
	
	'contenedore.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	contenedore.AddRows(1,True, "").AddCells12MP(1,0,30,0,0,"").AddCells12(1,"").AddCells12(1,"")
	'contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	'contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	
	contenedore.BuildGrid
	page.Cell(5,1).SetOffsetSize(0,0,0,12,11,11)
	page.Cell(5,1).AddComponent(contenedore)
	
	
	
	Dim lb5 As ABMLabel
	lb5.Initialize(page,"lb5","Lenguajes",ABM.SIZE_H6,False,"etiq")
	contenedore.Cell(1,1).UseTheme("celda2")
	contenedore.Cell(1,1).AddComponent(lb5)
	
	
	Dim porcentajes As List
	porcentajes.Initialize
	
	For i=10 To 100 Step 10
		porcentajes.Add(i&"%")
	Next
	
	cantidiomas = 1
	Dim contenedor1 As ABMContainer
	contenedor1.Initialize(page, "contenedor1", "")
	contenedor1.AddRows(1,True,"").AddCellsOSMP(3,0,0,0,12,4,4,0,0,10,0,"")
	contenedor1.BuildGrid
	contenedor1.Cell(1,1).AddComponent(buildcombo("idioma1","","Idioma", listidiomas))
	contenedor1.Cell(1,2).AddComponent(buildcombo("idiomar1","","Lec./Escr.", porcentajes))
	contenedor1.Cell(1,3).AddComponent(buildcombo("idiomaw1","","Oral", porcentajes))
	
	
	contenedore.Cell(2,1).AddComponent(contenedor1)
	
	
	Dim btnaggidi As ABMButton
	btnaggidi.InitializeFlat(page,"btnaggidi","mdi-content-add", ABM.ICONALIGN_LEFT ,"+","btn")
	contenedore.Cell(3,1).SetOffsetSize(0,0,0,12,3,3)
	contenedore.Cell(3,1).AddComponent(btnaggidi)
	
	Dim btnrestidi As ABMButton
	btnrestidi.InitializeFlat(page,"btnrestidi","mdi-content-remove", ABM.ICONALIGN_LEFT ,"-","btn")
	contenedore.Cell(3,2).SetOffsetSize(0,0,0,12,3,3)
	contenedore.Cell(3,2).AddComponent(btnrestidi)
	
	
	
	'-------------------------------------------------------------------------------------------- SKILLS
	
	Dim contenedorf As ABMContainer	
	contenedorf.Initialize(page,"contenedorf","cardtheme2")
	'contenedora.AddRows
	
	contenedorf.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	contenedorf.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	contenedorf.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	contenedorf.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	contenedorf.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	'contenedorf.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	'contenedorf.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	'contenedorf.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	'contenedorf.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	'contenedorf.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(1,0,0,10,0,"")
	
	'contenedorf.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	contenedorf.AddRows(1,True, "").AddCells12MP(1,0,30,0,0,"").AddCells12(1,"").AddCells12(1,"")
	'contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	'contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	
	contenedorf.BuildGrid
	page.Cell(6,1).SetOffsetSize(0,0,0,12,11,11)
	page.Cell(6,1).AddComponent(contenedorf)
	
	
	Dim contenedor4 As ABMContainer
	contenedor4.Initialize(page, "contenedor4", "")
	contenedor4.AddRows(1,True,"").AddCellsOSMP(2,0,0,0,12,6,6,0,0,10,0,"")
	contenedor4.BuildGrid
	
	
	contenedorf.Cell(2,1).AddComponent(contenedor4)
	
	Dim lb5 As ABMLabel
	lb5.Initialize(page,"lb5","Habilidades",ABM.SIZE_H6,False,"etiq")
	contenedorf.Cell(1,1).UseTheme("celda2")
	contenedorf.Cell(1,1).AddComponent(lb5)
	
	contenedor4.Cell(1,1).AddComponent(buildcombo("skill1","","Habilidad", listskills))
	
	Dim inp7 As ABMInput
    inp7.Initialize(page, "skilld1", ABM.INPUT_TEXT, "Detalle", False, "inp1")
    'page.Cell(15,1).SetOffsetSize(0,0,0,12,6,3)
	contenedor4.Cell(1,2).AddComponent(inp7)
	
	Dim btnaggsk As ABMButton
	btnaggsk.InitializeFlat(page,"btnaggsk","mdi-content-add", ABM.ICONALIGN_LEFT ,"+","btn")
	contenedorf.Cell(3,1).SetOffsetSize(0,0,0,12,3,3)
	contenedorf.Cell(3,1).AddComponent(btnaggsk)
	
	Dim btnrestsk As ABMButton
	btnrestsk.InitializeFlat(page,"btnrestsk","mdi-content-remove", ABM.ICONALIGN_LEFT ,"-","btn")
	contenedorf.Cell(3,2).SetOffsetSize(0,0,0,12,3,3)
	contenedorf.Cell(3,2).AddComponent(btnrestsk)
	
		
	Dim inp7 As ABMInput
    inp7.Initialize(page, "softext", ABM.INPUT_TEXT, "Conocimiento de herramientas informáticas (Detalles adic.)", True, "inp1")
    'page.Cell(15,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorf.Cell(4,1).AddComponent(inp7)
	
	Dim inp8 As ABMInput
    inp8.Initialize(page, "skillext", ABM.INPUT_TEXT, "Habilidades de trabajo (Detalles adic.)", True, "inp1")
    'page.Cell(15,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorf.Cell(5,1).AddComponent(inp8)
	

	'-------------------------------------------------------------------------------------------  OTRAS
	
	Dim contenedorg As ABMContainer	
	contenedorg.Initialize(page,"contenedorg","cardtheme2")
	'contenedora.AddRows
	
	contenedorg.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	contenedorg.AddRows(7,True, "").AddCells12MP(1,0,0,0,0,"")
	'contenedorg.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	'contenedorg.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	'contenedorg.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	
	contenedorg.AddRows(1,True, "").AddCells12MP(1,0,30,0,0,"").AddCells12(1,"").AddCells12(1,"")
	contenedorg.BuildGrid
	page.Cell(8,1).SetOffsetSize(0,0,0,12,11,11)
	page.Cell(8,1).AddComponent(contenedorg)
	
	
	
	
	
	Dim lb6 As ABMLabel
	lb6.Initialize(page,"lb6","Otros",ABM.SIZE_H6,False,"etiq")
	contenedorg.Cell(1,1).UseTheme("celda2")
	contenedorg.Cell(1,1).AddComponent(lb6)
	
	
	Dim inp9 As ABMInput
    inp9.Initialize(page, "main", ABM.INPUT_TEXT, "Cuentenos sobre usted", False, "inp1")
    'contenedorg.Cell(15,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorg.Cell(2,1).AddComponent(inp9)
	
	Dim inp10 As ABMInput
    inp10.Initialize(page, "streng", ABM.INPUT_TEXT, "¿Qué espectativas le genera trabajar en un hotel?", False, "inp1")
    'contenedorg.Cell(15,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorg.Cell(3,1).AddComponent(inp10)
	
	Dim inp11 As ABMInput
    inp11.Initialize(page, "oport", ABM.INPUT_TEXT, "¿De qué maneras pudiera contribuir con el desarrollo de esta compañia?", False, "inp1")
    'contenedorg.Cell(15,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorg.Cell(4,1).AddComponent(inp11)
	
	Dim inp12 As ABMInput
    inp12.Initialize(page, "how", ABM.INPUT_TEXT, "¿Por qué quiere trabajar con nuestra empresa?", False, "inp1")
    'contenedorg.Cell(15,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorg.Cell(5,1).AddComponent(inp12)
	
	Dim inp13 As ABMInput
    inp13.Initialize(page, "reason", ABM.INPUT_TEXT, "¿Dónde espera estar en los próximos cinco años?", False, "inp1")
    'contenedorg.Cell(15,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorg.Cell(6,1).AddComponent(inp13)
	
	Dim inp14 As ABMInput
    inp14.Initialize(page, "salary", ABM.INPUT_TEXT, "¿Cuáles son sus objetivos profesionales?", False, "inp1")
    'contenedorg.Cell(15,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorg.Cell(7,1).AddComponent(inp14)
	
	Dim inp15 As ABMInput
    inp15.Initialize(page, "look", ABM.INPUT_TEXT, "¿Qué tipo de trabajador eres tú?", False, "inp1")
    'contenedorg.Cell(15,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorg.Cell(8,1).AddComponent(inp15)
	

	
		'-------------------------------------------------------------------------------------------- REFERENCIAS PERS
     Dim contenedorh As ABMContainer	
	contenedorh.Initialize(page,"contenedorh","cardtheme2")
	'contenedora.AddRows
	
	contenedorh.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	contenedorh.BuildGrid
	'contenedorh.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	'contenedorh.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	'contenedorh.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	
	
	page.Cell(8,1).SetOffsetSize(0,0,0,12,11,11)
	page.Cell(8,1).AddComponent(contenedorh)
	
	
	Dim lb6 As ABMLabel
	lb6.Initialize(page,"lb6","Referencias personales",ABM.SIZE_H6,False,"etiq")
	contenedorh.Cell(1,1).UseTheme("celda2")
	contenedorh.Cell(1,1).AddComponent(lb6)
	
	Dim r As Int		
	
	For r=1 To cantref
	
		contenedorh.AddRows(1,True, "").AddCellsOSMP(4,0,0,0,12,6,3,0,0,10,0,"")
		contenedorh.BuildGrid
			
		Dim inp9 As ABMInput
	    inp9.Initialize(page, "idref"&r, ABM.INPUT_TEXT, "Número de ident.", False, "inp1")
	    'contenedorh.Cell(15,1).SetOffsetSize(0,0,0,12,6,3)
		contenedorh.Cell(r+1,1).AddComponent(inp9)
		
		Dim inp10 As ABMInput
	    inp10.Initialize(page, "nameref"&r, ABM.INPUT_TEXT, "Nombre completo", False, "inp1")
	    'contenedorh.Cell(15,1).SetOffsetSize(0,0,0,12,6,3)
		contenedorh.Cell(r+1,2).AddComponent(inp10)
		
		Dim inp11 As ABMInput
	    inp11.Initialize(page, "phoneref"&r, ABM.INPUT_TEL, "Teléfono n°1", False, "inp1")
	    'contenedorh.Cell(15,1).SetOffsetSize(0,0,0,12,6,3)
		contenedorh.Cell(r+1,3).AddComponent(inp11)
			
		Dim inp11 As ABMInput
	    inp11.Initialize(page, "phoneref2"&r, ABM.INPUT_TEL, "Teléfono n°2", False, "inp1")
	    'contenedorh.Cell(15,1).SetOffsetSize(0,0,0,12,6,3)
		contenedorh.Cell(r+1,4).AddComponent(inp11)	
	
	Next
	
	
	contenedorh.AddRows(1,True, "").AddCells12MP(1,0,30,0,0,"").AddCells12(1,"").AddCells12(1,"")
	contenedorh.BuildGrid
	
	
	
		'------------------------------------------------------------------------------------------- CRITERION
	Dim contenedori As ABMContainer	
	contenedori.Initialize(page,"contenedori","cardtheme2")
	'contenedora.AddRows
	
	contenedori.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	contenedori.AddRows(2,True, "").AddCells12MP(1,0,0,0,0,"")
	'contenedori.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"").AddCells12MP(3,0,0,10,0,"")
	'contenedori.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	'contenedori.AddRows(1,True, "").AddCells12MP(1,0,0,0,0,"")
	
	contenedori.AddRows(1,True, "").AddCells12MP(1,0,30,0,0,"").AddCells12(1,"").AddCells12(1,"")
	contenedori.BuildGrid
	page.Cell(9,1).SetOffsetSize(0,0,0,12,11,11)
	page.Cell(9,1).AddComponent(contenedori)
	
	
	Dim lb7 As ABMLabel
	lb7.Initialize(page,"lb7","Notas para el siguiente filtro",ABM.SIZE_H6,False,"etiq")
	contenedori.Cell(1,1).UseTheme("celda2")
	contenedori.Cell(1,1).AddComponent(lb7)
	
	Dim inp16 As ABMInput
    inp16.Initialize(page, "inter", ABM.INPUT_TEXT, "Comentarios del entrevistador", True, "inp1")
    'page.Cell(15,1).SetOffsetSize(0,0,0,12,6,3)
	contenedori.Cell(2,1).AddComponent(inp16)
	
	
	'MODIFICAR AL FINAL
	Dim btnAceptar As ABMButton
	btnAceptar.InitializeFlat(page,"btnaceptar","","","Aceptar","btn")
	page.Cell(10,1).SetOffsetSize(0,0,0,12,4,4)
	page.Cell(10,1).AddComponent(btnAceptar)
	
	'creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	'page.RestoreNavigationBarPosition
End Sub




Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then 
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

'-----------------------------------------------------

Sub btnaceptar_Clicked(Target As String)
	
	 'page.ShowToast("toast" & 1, "toastred", txt, 5000)
	
		myToastId = 1
	
		Dim sql1 As SQL
		Dim valores(), valores2()  As Object
		sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
		Dim rst As ResultSet
		Dim pos As Int = 0
		Dim id1, id2, id3 As Int
		Dim sqlt, sqlt2, sqlt3 As String 
		Dim i As Int

	
    	
	 
	 
	 
	 
	 Dim containera As ABMContainer = page.Component("contenedora")
	 Dim containerb As ABMContainer = page.Component("contenedorb")
	 Dim containerc As ABMContainer = page.Component("contenedorc")
	 Dim containerd As ABMContainer = page.Component("contenedord")
	 Dim containere As ABMContainer = page.Component("contenedore")
	 Dim containerf As ABMContainer = page.Component("contenedorf")
	 Dim containerg As ABMContainer = page.Component("contenedorg")
	 Dim containerh As ABMContainer = page.Component("contenedorh")
	
	 
	 
	 Dim container1 As ABMContainer = containere.Component("contenedor1")
	 Dim container2 As ABMContainer = containerd.Component("contenedor2")
	 Dim container3 As ABMContainer = containerc.Component("contenedor3")
	 Dim container4 As ABMContainer = containerf.Component("contenedor4")
	 
	 
	 
	 Dim c1_a As ABMCombo = containera.cell(2,1).Component("cmbDivision")
	 Dim t1_a As String = c1_a.GetActiveItemId
	 Dim l1_a As ABMLabel = c1_a.GetComponent(t1_a)
	 
	  If c1_a.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1_a","Debe seleccionar una divisón",3000)
		Return
	  End If
	  
	 Dim c2_a As ABMCombo = containera.cell(2,2).Component("cmbSubDivision")
	 Dim t2_a As String = c2_a.GetActiveItemId
	 Dim l2_a As ABMLabel = c2_a.GetComponent(t2_a)
	 
	  If c2_a.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1_a","Debe seleccionar un país",3000)
		Return
	  End If 
	  
	 Dim c3_a As ABMCombo = containera.cell(2,3).Component("cmbLocation")
	 Dim t3_a As String = c3_a.GetActiveItemId
	 Dim l3_a As ABMLabel = c3_a.GetComponent(t3_a)
	 
	  If c3_a.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1_a","Debe seleccionar una locación",3000)
		Return
	  End If 
	  
	 Dim c4_a As ABMCombo = containera.cell(2,4).Component("cmbAreas")
	 Dim t4_a As String = c4_a.GetActiveItemId
	 Dim l4_a As ABMLabel = c4_a.GetComponent(t4_a)
	 
	 If c4_a.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1_a","Debe seleccionar un área",3000)
		Return
	 End If
	 
	 
	  
	 
	 
	 Dim c1 As ABMCombo = containera.cell(3,1).Component("cmbDepartamento")
	 Dim t1 As String = c1.GetActiveItemId
	 Dim l1 As ABMLabel = c1.GetComponent(t1)
	 
	 If c1.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar un Departamento",3000)
		Return
	End If
	 
	 
	 Dim c2 As ABMCombo = containera.cell(3,2).Component("cmbSupervisores")
	 Dim t2 As String = c2.GetActiveItemId
	 Dim l2 As ABMLabel = c2.GetComponent(t2)
	 
	  If c2.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar un supervisor",3000)
		Return
	 End If
	 
	 
	 Dim c22 As ABMCombo = containera.cell(3,3).Component("cmbVacancy")
	 Dim t22 As String = c22.GetActiveItemId
	 Dim l22 As ABMLabel = c22.GetComponent(t22)
	 
	   If c22.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar una vacante",3000)
		Return
	  End If
	 
	 
	 Dim i1 As ABMInput = containerb.cell(2,1).Component("name")
	 
	  If ABMShared.validatxt(i1.text) Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar un nombre",3000)
		Return
	  End If
	 
	 Dim i2 As ABMInput = containerb.cell(2,2).Component("name2")
	 Dim i3 As ABMInput = containerb.cell(2,3).Component("last")
	   
	  If ABMShared.validatxt(i3.text) Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar un apellido",3000)
		Return
	  End If
	 
	 
	 Dim i4 As ABMInput = containerb.cell(2,4).Component("last2")
	
	 Dim nac As ABMDateTimeScroller = containerb.cell(3,1).Component("birth")
	 
	 Dim c3 As ABMCombo = containerb.cell(3,2).Component("ecivil")
	 Dim t3 As String = c3.GetActiveItemId
	 Dim l3 As ABMLabel = c3.GetComponent(t3)
	 
	  If c3.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar un estado civil",3000)
		Return
	  End If
	
	 Dim i5 As ABMInput = containerb.cell(3,3).Component("id")	
	
	
	 If ABMShared.validatxt(i5.text) Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar un documento de identidad",3000)
		
		Return
	 Else	
		sql1.ExecNonQuery("DELETE from RH_Applicant where id_applicant='"&i5.Text.ToUpperCase&"'")
		sql1.ExecNonQuery("DELETE from RH_Application where id_applicant='"&i5.Text.ToUpperCase&"'")
		sql1.ExecNonQuery("DELETE from RH_ApplicantDat where id_applicant='"&i5.Text.ToUpperCase&"'")
	 End If
		
	
	
	 Dim i6 As ABMInput = containerb.cell(4,1).Component("nac")
	 
	 
	 Dim c3_1 As ABMCombo = containerb.cell(4,2).Component("country")
	 Dim t3_1 As String = c3_1.GetActiveItemId
	 Dim l3_1 As ABMLabel = c3_1.GetComponent(t3_1)
	
	  If c3_1.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar un país",3000)
		Return
	  End If
	 
	 
	 
	 Dim i8 As ABMInput = containerb.cell(4,3).Component("state")
	 Dim i8_1 As ABMInput = containerb.cell(4,4).Component("city")
	 
	 Dim i9 As ABMInput = containerb.cell(5,1).Component("addr")
	 
	  If ABMShared.validatxt(i9.text) Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar la dirección",3000)
		Return
	  End If
	 
	 Dim i10 As ABMInput = containerb.cell(6,1).Component("phone1")
	 
	 If ABMShared.validatxt(i10.text) Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe fill el Número telefónico",3000)
		Return
	 End If
	 
	 Dim i11 As ABMInput = containerb.cell(6,2).Component("phone2")
	 Dim i12 As ABMInput = containerb.cell(6,3).Component("phone3")
	 Dim i13 As ABMInput = containerb.cell(6,4).Component("email")
	 
	 If ABMShared.validatxt(i13.text) Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe fill el correo",3000)
		Return
	 End If
	 
	 Dim c4 As ABMCombo = containerb.cell(7,1).Component("depend")
	 Dim t4 As String = c4.GetActiveItemId
	 Dim l4 As ABMLabel = c4.GetComponent(t4)
	 
	  If c4.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar los dependientes",3000)
		Return
	  End If
	 
	 
	 Dim i14 As ABMInput = containerb.cell(7,2).Component("family")
	 
	 
	 
	 For i=1 To cantedu
	 
		Dim i144 As ABMInput = container3.cell(i,1).Component("univ"&i)
		 
		If ABMShared.validatxt(i144.text) Then 
	 		page.ShowToast(masuno(myToastId),"toast1","Debe llenar una institución",3000)
			Return
	 	End If
		 Dim i155 As ABMInput = container3.cell(i,2).Component("cert"&i)
		 If ABMShared.validatxt(i155.text) Then 
	 		page.ShowToast(masuno(myToastId),"toast1","Debe llenar un certificado",3000)
			Return
	 	End If
		 
		 
		 Dim c5 As ABMCombo = container3.cell(i,3).Component("edut"&i)
		 Dim t5 As String = c5.GetActiveItemId
		 Dim l5 As ABMLabel = c5.GetComponent(t5)
		 
		If c5.GetActiveItemId ="-1" Then 
	 		page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar una fecha (Educación)",3000)
			Return
	    End If
		 
		 
		 
		  valores=Array As Object (i5.Text.ToUpperCase,"EDUCACION",i144.Text.ToUpperCase,i155.Text.ToUpperCase,"","",Null,t5.ToUpperCase)
	  	  sql1.ExecNonQuery2("INSERT INTO RH_ApplicantDat VALUES (?,?,?,?,?,?,?,? )", valores)
		 
		 
	 Next
	 
	 
	 
	 For i=1 To cantexp
	 
		  Dim i144 As ABMInput = container2.cell(i,1).Component("enterprice"&i)
		  If ABMShared.validatxt(i144.text) Then 
	 		page.ShowToast(masuno(myToastId),"toast1","Debe llenar una empresa",3000)
			Return
	      End If
		  Dim i155 As ABMInput = container2.cell(i,2).Component("position"&i)
		  If ABMShared.validatxt(i155.text) Then 
	 		page.ShowToast(masuno(myToastId),"toast1","Debe llenar un cargo",3000)
			Return
	 	  End If
		  Dim c6 As ABMCombo = container2.cell(i,3).Component("expi"&i)
		  Dim t6 As String = c6.GetActiveItemId
		  Dim l6 As ABMLabel = c6.GetComponent(t6)
		  If c6.GetActiveItemId ="-1" Then 
	 		page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar una fecha inicial (Profesional)",3000)
			Return
	      End If
		  
		  Dim c7 As ABMCombo = container2.cell(i,4).Component("expf"&i)
		  Dim t7 As String = c7.GetActiveItemId
		  Dim l7 As ABMLabel = c7.GetComponent(t7)
		  If c7.GetActiveItemId ="-1" Then 
	 		page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar una fecha final (Profesional)",3000)
			Return
	      End If
		  
		  
		  
		  Dim i16 As ABMInput = container2.cell(i,5).Component("jobphone"&i)
		  If ABMShared.validatxt(i16.text) Then 
	 		page.ShowToast(masuno(myToastId),"toast1","Debe llenar un teléfono(Profesional)",3000)
			Return
	      End If
		  Dim i16_1 As ABMInput = container2.cell(i,6).Component("reason"&i)
		  If ABMShared.validatxt(i16_1.text) Then 
	 		page.ShowToast(masuno(myToastId),"toast1","Debe llenar una razón (Profesional)",3000)
			Return
	      End If
		  
		  valores=Array As Object (i5.Text.ToUpperCase,"PROFESION",i144.Text.ToUpperCase,i155.Text.ToUpperCase,i16.text.ToUpperCase,i16_1.text.ToUpperCase,t6.ToUpperCase,t7.ToUpperCase)
	  	  sql1.ExecNonQuery2("INSERT INTO RH_ApplicantDat VALUES (?,?,?,?,?,?,?,? )", valores)
	  
	  Next
	  
	  
	  Dim i17 As ABMInput = containerd.cell(4,1).Component("great")
	  
	  
	  
	For i=1 To cantidiomas  
	  
	  Dim c8 As ABMCombo = container1.cell(i,1).Component("idioma"&i)
	  Dim t8 As String = c8.GetActiveItemId
	  Dim l8 As ABMLabel = c8.GetComponent(t8)
	  If c8.GetActiveItemId ="-1" Then 
	 		page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar al menos un idioma (idioma)",3000)
			Return
	  End If
	  
	  
	  Dim c9 As ABMCombo = container1.cell(i,2).Component("idiomar"&i)
	  Dim t9 As String = c9.GetActiveItemId
	  Dim l9 As ABMLabel = c9.GetComponent(t9)
	  If c9.GetActiveItemId ="-1" Then 
	 		page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar un porcentaje (idioma)",3000)
			Return
	  End If
	  
	  Dim c10 As ABMCombo = container1.cell(i,3).Component("idiomaw"&i)
	  Dim t10 As String = c10.GetActiveItemId
	  Dim l10 As ABMLabel = c10.GetComponent(t10)
	  If c10.GetActiveItemId ="-1" Then 
	 		page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar un porcentaje (idioma)",3000)
			Return
	  End If
	  
	  valores=Array As Object (i5.Text.ToUpperCase,"LANGUAJE",t8.ToUpperCase,t9.ToUpperCase,t10.ToUpperCase,"",Null,Null)
	  sql1.ExecNonQuery2("INSERT INTO RH_ApplicantDat VALUES (?,?,?,?,?,?,?,? )", valores)
	  
	 
	Next 
	
	
	For i=1 To cantsk
	 
	  Dim c11 As ABMCombo = container4.cell(i,1).Component("skill"&i)
	  Dim t11 As String = c11.GetActiveItemId
	  Dim l11 As ABMLabel = c11.GetComponent(t11)	
	  If c11.GetActiveItemId ="-1" Then 
	 		page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar una habilidad (Habilidades)",3000)
			Return
	  End If
	  
	  
	  Dim i18 As ABMInput = container4.cell(i,2).Component("skilld"&i)	
	   If ABMShared.validatxt(i18.text) Then 
	 		page.ShowToast(masuno(myToastId),"toast1","Debe llenar un detalle (Habilidades)",3000)
			Return
	   End If
	  
	  
	  valores=Array As Object (i5.Text.ToUpperCase,"SKILL",t11.ToUpperCase,i18.text.ToUpperCase,"","",Null,Null)
	  sql1.ExecNonQuery2("INSERT INTO RH_ApplicantDat VALUES (?,?,?,?,?,?,?,? )", valores)
	 
	Next 
	 
	 
	 
	  Dim i19 As ABMInput = containerf.cell(4,1).Component("softext")
	  Dim i20 As ABMInput = containerf.cell(5,1).Component("skillext")
	  
	  Dim i21 As ABMInput = containerg.cell(2,1).Component("main")
	  Dim i22 As ABMInput = containerg.cell(3,1).Component("streng")
	  Dim i23 As ABMInput = containerg.cell(4,1).Component("oport")
	  Dim i24 As ABMInput = containerg.cell(5,1).Component("how")
	  Dim i25 As ABMInput = containerg.cell(6,1).Component("reason")
	  Dim i26 As ABMInput = containerg.cell(7,1).Component("salary")
	  Dim i27 As ABMInput = containerg.cell(8,1).Component("look")
	 
	 
	 
	 
	 For i=1 To cantref
	 
	  Dim i28 As ABMInput = containerh.cell(i+1,1).Component("idref"&i)
	  If ABMShared.validatxt(i28.text) Then 
	 		page.ShowToast(masuno(myToastId),"toast1","Debe llenar un id (Referencias)",3000)
			Return
	  End If
	  Dim i29 As ABMInput = containerh.cell(i+1,2).Component("nameref"&i)
	  If ABMShared.validatxt(i29.text) Then 
	 		page.ShowToast(masuno(myToastId),"toast1","Debe llenar el nombre (Referencias)",3000)
			Return
	  End If
	  Dim i30 As ABMInput = containerh.cell(i+1,3).Component("phoneref"&i)
	  If ABMShared.validatxt(i30.text) Then 
	 		page.ShowToast(masuno(myToastId),"toast1","Debe llenar el teléfono (Referencias)",3000)
			Return
	  End If
	  Dim i31 As ABMInput = containerh.cell(i+1,4).Component("phoneref2"&i)
	  
	  
	   valores=Array As Object (i5.Text.ToUpperCase,"REFERENCE",i28.text.ToUpperCase,i29.text.ToUpperCase,i30.text.ToUpperCase,i31.text.ToUpperCase,Null,Null)
	  sql1.ExecNonQuery2("INSERT INTO RH_ApplicantDat VALUES (?,?,?,?,?,?,?,? )", valores)
	  
	 Next 
	 
	 
	 If myToastId = 1 Then
	 
	 	valores=Array As Object (i5.Text.ToUpperCase,i1.Text.ToUpperCase,i2.Text.ToUpperCase,i3.Text.ToUpperCase,i4.Text.ToUpperCase,DateTime.Date(nac.GetDate),t3.ToUpperCase,i6.text.ToUpperCase,l3_1.text.ToUpperCase,i8.text.ToUpperCase,i8_1.Text.ToUpperCase,i9.Text.ToUpperCase,i10.Text.ToUpperCase,i11.Text.ToUpperCase,i12.Text.ToUpperCase,i13.Text.ToUpperCase,l4.Text.ToUpperCase,i14.Text.ToUpperCase,i19.text.ToUpperCase,i20.text.ToUpperCase,i21.text.ToUpperCase,i22.text.ToUpperCase,i23.text.ToUpperCase,i24.text.ToUpperCase,i25.text.ToUpperCase,i26.text.ToUpperCase,i27.text.ToUpperCase)
	 	sql1.ExecNonQuery2("INSERT INTO RH_Applicant VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )", valores)
	 
	 
	  	valores=Array As Object (t1_a.ToUpperCase,t2_a.ToUpperCase,t3_a.ToUpperCase,t4_a.ToUpperCase,t1.ToUpperCase,t22.ToUpperCase,i5.Text.ToUpperCase,t2.ToUpperCase, DateTime.Date(DateTime.Now)&" "&DateTime.Time(DateTime.Now),"-")
	  	sql1.ExecNonQuery2("INSERT INTO RH_Application VALUES (?,?,?,?,?,?,?,?,?,? )", valores)
		page.ShowToast(masuno(myToastId),"toast1","SAVING",3000)
		
		
		Dim cuerpo() As String 
		Dim subjet As String = "Gracias por aplicar con Karisma"
		Dim tittle As String = "Hola "&i1.Text&" "&i3.Text
		Dim body As String ="Gracias por aplicar para una oportunidad de trabajo con nosotros, le estaremos contactando si fue seleccionado"
		Dim img As String ="http://34.193.69.179:8099/suite/images/klogo.png"

		cuerpo= Array As String (subjet,tittle,body,img)

		Dim destino() As String = Array As String (i13.Text)

		CallSubDelayed3(emailutils,"emailsend",destino,cuerpo)

		
	   ABMShared.NavigateToPage(ws, ABMPageId,"../solicitudpuestopub/solicitudpuestopub.html")
	 End If
	  
	 
	 
	 
	 
	 
	 
	 
	 

		'page.ShowModalSheet("fixedsheet")		
End Sub

Sub msbtn3_Clicked(Target As String)
	page.CloseModalSheet("fixedsheet")			
End Sub


Sub btnaggsk_Clicked(Target As String) 'boton para agregar idioma
	
	
	Dim contenedorf As ABMContainer = page.Component("contenedorf")
	Dim contenedor4 As ABMContainer = contenedorf.Component("contenedor4")
	If cantsk < 8 Then
	
		contenedorf.Cell(2,1).RemoveAllComponents
		cantsk = cantsk + 1
	
		Dim contenedor4 As ABMContainer
		contenedor4.Initialize(page, "contenedor4", "")
		contenedor4.AddRows(cantsk,True,"").AddCellsOSMP(2,0,0,0,12,6,6,0,0,10,0,"")
		contenedor4.BuildGrid
		
		
	   	For i=1 To cantsk
					 
				contenedor4.Cell(i,1).AddComponent(buildcombo("skill"&i,"","Habilidad", listskills))
	
				Dim inp7 As ABMInput
			    inp7.Initialize(page, "skilld"&i, ABM.INPUT_TEXT, "Detalle", False, "inp1")
			   	contenedor4.Cell(i,2).AddComponent(inp7)
			 
	 			
		Next
		
		 contenedorf.Cell(2,1).AddComponent(contenedor4)
		 contenedor4.Refresh
         contenedorf.Cell(2,1).Refresh
	End If
End Sub



Sub btnrestsk_Clicked(Target As String) 'boton para agregar idioma
	Dim contenedorf As ABMContainer = page.Component("contenedorf")
	Dim contenedor4 As ABMContainer = contenedorf.Component("contenedor4")
	If cantsk > 1 Then
	
		contenedorf.Cell(2,1).RemoveAllComponents
		cantsk = cantsk - 1
	
		Dim contenedor4 As ABMContainer
		contenedor4.Initialize(page, "contenedor4", "")
		contenedor4.AddRows(cantsk,True,"").AddCellsOSMP(2,0,0,0,12,6,6,0,0,10,0,"")
		contenedor4.BuildGrid
				
	   	For i=1 To cantsk
			
			contenedor4.Cell(i,1).AddComponent(buildcombo("skill"&i,"","Habilidad", listskills))
	
			Dim inp7 As ABMInput
			inp7.Initialize(page, "skilld"&i, ABM.INPUT_TEXT, "Detalle", False, "inp1")
			contenedor4.Cell(i,2).AddComponent(inp7)
				
		Next
		
		 contenedorf.Cell(2,1).AddComponent(contenedor4)
		 contenedor4.Refresh
         contenedorf.Cell(2,1).Refresh
	End If
End Sub

Sub btnaggidi_Clicked(Target As String) 'boton para agregar idioma
	
	Dim contenedore As ABMContainer = page.Component("contenedore")
	Dim contenedor1 As ABMContainer = contenedore.cell(2,1).Component("contenedor1")
	
	If cantidiomas < 8 Then
	
		 contenedore.Cell(2,1).RemoveAllComponents
		cantidiomas = cantidiomas + 1
	
		
	
		Dim porcentajes As List
		porcentajes.Initialize
	
		For i=10 To 100 Step 10
			porcentajes.Add(i&"%")
		Next
	
	    Dim contenedor1 As ABMContainer
		contenedor1.Initialize(page, "contenedor1", "")
		contenedor1.AddRows(cantidiomas,True,"").AddCellsOSMP(3,0,0,0,12,4,4,0,0,10,0,"")
		contenedor1.BuildGrid
	    'contenedor1.DebugPrintGrid
		
		For i=1 To cantidiomas
			contenedor1.Cell(i,1).AddComponent(buildcombo("idioma"&i,"","Idioma", listidiomas))
			
			contenedor1.Cell(i,2).AddComponent(buildcombo("idiomar"&i,"","Lec./Escr.", porcentajes))
			contenedor1.Cell(i,3).AddComponent(buildcombo("idiomaw"&i,"","Oral", porcentajes))
			
			
		Next
		
		contenedore.Cell(2,1).AddComponent(contenedor1)
		contenedor1.Refresh
        contenedore.Cell(2,1).Refresh
	End If
End Sub


Sub btnrestidi_Clicked(Target As String) 'boton para restar idioma
	Dim contenedore As ABMContainer = page.Component("contenedore")
	Dim contenedor1 As ABMContainer = contenedore.cell(2,1).Component("contenedor1")
		
	If cantidiomas > 1 Then
				
		contenedore.Cell(2,1).RemoveAllComponents
		cantidiomas = cantidiomas - 1
	
		
		
	
		Dim porcentajes As List
		porcentajes.Initialize
	
		For i=10 To 100 Step 10
			porcentajes.Add(i&"%")
		Next
	
	    Dim contenedor1 As ABMContainer
		contenedor1.Initialize(page, "contenedor1", "")
		contenedor1.AddRows(cantidiomas,True,"").AddCellsOSMP(3,0,0,0,12,4,4,0,0,10,0,"")
		contenedor1.BuildGrid
	
		
		For i=1 To cantidiomas
			
			contenedor1.Cell(i,1).AddComponent(buildcombo("idioma"&i,"","Idioma", listidiomas))
			
			contenedor1.Cell(i,2).AddComponent(buildcombo("idiomar"&i,"","Lec./Escr.", porcentajes))
			contenedor1.Cell(i,3).AddComponent(buildcombo("idiomaw"&i,"","Oral", porcentajes))
			
		Next
		
		contenedore.Cell(2,1).AddComponent(contenedor1)
		contenedor1.Refresh
        contenedore.Cell(2,1).Refresh
	End If
End Sub






Sub btnaggedu_Clicked(Target As String) 'boton para agregar idioma
	
	
	Dim contenedorc As ABMContainer = page.Component("contenedorc")
	Dim contenedor3 As ABMContainer = contenedorc.Component("contenedor3")
	If cantedu < 8 Then
	
		contenedorc.Cell(2,1).RemoveAllComponents
		cantedu = cantedu + 1
	
		Dim contenedor3 As ABMContainer
		contenedor3.Initialize(page, "contenedor3", "")
		contenedor3.AddRows(cantedu,True,"").AddCells12MP(1,0,0,0,0,"").AddCells12MP(2,0,0,10,0,"")
		contenedor3.BuildGrid
		
		
	   	For i=1 To cantedu
			contenedor3.Cell(i,3).SetOffsetSize(0,0,0,12,6,4)
			contenedor3.Cell(i,3).AddComponent(buildcombo("edut"&i,"","Fecha de finalización", anos))
	
			Dim inp3 As ABMInput
     		inp3.Initialize(page, "univ"&i, ABM.INPUT_TEXT, "Grado educativo", False, "inp1")
			contenedor3.Cell(i,1).SetOffsetSize(0,0,0,12,6,4)
	 		contenedor3.Cell(i,1).AddComponent(inp3)
	
	
			 Dim inp4 As ABMInput
    		 inp4.Initialize(page, "cert"&i, ABM.INPUT_TEXT, "Certificación o diploma", False, "inp1")
			 contenedor3.Cell(i,2).SetOffsetSize(0,0,0,12,6,4)
			 contenedor3.Cell(i,2).AddComponent(inp4)
	 			
		Next
		
		 contenedorc.Cell(2,1).AddComponent(contenedor3)
		 contenedor3.Refresh
         contenedorc.Cell(2,1).Refresh
	End If
End Sub



Sub btnrestedu_Clicked(Target As String) 'boton para agregar idioma
	Dim contenedorc As ABMContainer = page.Component("contenedorc")
	Dim contenedor3 As ABMContainer = contenedorc.Component("contenedor3")
	If cantedu > 1 Then
	
		contenedorc.Cell(2,1).RemoveAllComponents
		cantedu = cantedu - 1
	
		Dim contenedor3 As ABMContainer
		contenedor3.Initialize(page, "contenedor3", "")
		contenedor3.AddRows(cantedu,True,"").AddCells12MP(1,0,0,0,0,"").AddCells12MP(2,0,0,10,0,"")
		contenedor3.BuildGrid
				
	   	For i=1 To cantedu
			
			contenedor3.Cell(i,3).SetOffsetSize(0,0,0,12,6,4)
			contenedor3.Cell(i,3).AddComponent(buildcombo("edut"&i,"","Fecha de finalización", anos))
	
			Dim inp3 As ABMInput
     		inp3.Initialize(page, "univ"&i, ABM.INPUT_TEXT, "Grado educativo", False, "inp1")
			contenedor3.Cell(i,1).SetOffsetSize(0,0,0,12,6,4)
	 		contenedor3.Cell(i,1).AddComponent(inp3)
	
	
			 Dim inp4 As ABMInput
    		 inp4.Initialize(page, "cert"&i, ABM.INPUT_TEXT, "Certificación o diploma", False, "inp1")
			 contenedor3.Cell(i,2).SetOffsetSize(0,0,0,12,6,4)
			 contenedor3.Cell(i,2).AddComponent(inp4)
				
		Next
		
		 contenedorc.Cell(2,1).AddComponent(contenedor3)
		 contenedor3.Refresh
         contenedorc.Cell(2,1).Refresh
	End If
End Sub



'---------------------------------------------- BOTONES EXP

Sub btnaggexp_Clicked(Target As String) 'boton para agregar idioma
	
	Dim contenedord As ABMContainer = page.Component("contenedord")
	Dim contenedor2 As ABMContainer = contenedord.Component("contenedor2")
	If cantexp < 8 Then
	
		contenedord.Cell(2,1).RemoveAllComponents
		cantexp = cantexp + 1
	
		Dim contenedor2 As ABMContainer
		contenedor2.Initialize(page, "contenedor2", "")
		contenedor2.AddRows(cantexp,True,"").AddCells12MP(1,0,0,0,0,"").AddCells12MP(5,0,0,10,0,"")
		'contenedor2.AddRows(1,True,"").AddCells12MP(1,0,0,0,0,"")
		contenedor2.BuildGrid
				
	   	For i=1 To cantexp
			
			contenedor2.Cell(i,3).SetOffsetSize(0,0,0,12,12,2)
			contenedor2.Cell(i,3).AddComponent(buildcombo("expi"&i,"","fecha inicio", anos))
			contenedor2.Cell(i,4).SetOffsetSize(0,0,0,12,12,2)
			contenedor2.Cell(i,4).AddComponent(buildcombo("expf"&i,"","fecha fin", anos))
	
			Dim inp5 As ABMInput
    		inp5.Initialize(page, "position"&i, ABM.INPUT_TEXT, "Cargo", False, "inp1")
			contenedor2.Cell(i,2).SetOffsetSize(0,0,0,12,12,3)
			contenedor2.Cell(i,2).AddComponent(inp5)
	 			
		
			Dim inp5 As ABMInput
    		inp5.Initialize(page, "enterprice"&i, ABM.INPUT_TEXT, "Empresa", False, "inp1")
			contenedor2.Cell(i,1).SetOffsetSize(0,0,0,12,12,3)
			contenedor2.Cell(i,1).AddComponent(inp5)
			
			Dim inp5 As ABMInput
    		inp5.Initialize(page, "jobphone"&i, ABM.INPUT_TEXT, "Teléfono de contacto", False, "inp1")
			contenedor2.Cell(i,5).SetOffsetSize(0,0,0,12,12,2)
			contenedor2.Cell(i,5).AddComponent(inp5)
			
			Dim inp5 As ABMInput
    		inp5.Initialize(page, "reason"&i, ABM.INPUT_TEXT, "Razón", False, "inp1")
			contenedor2.Cell(i,6).SetOffsetSize(0,0,0,12,12,12)
			contenedor2.Cell(i,6).AddComponent(inp5)	
				
				
		Next
		
		 contenedord.Cell(2,1).AddComponent(contenedor2)
		 contenedor2.Refresh
         contenedord.Cell(2,1).Refresh
	End If
End Sub




Sub btnrestexp_Clicked(Target As String) 'boton para agregar idioma
	
	
	Dim contenedord As ABMContainer = page.Component("contenedord")
	Dim contenedor2 As ABMContainer = contenedord.Component("contenedor2")
	If cantexp > 1 Then
	
		contenedord.Cell(2,1).RemoveAllComponents
		cantexp = cantexp - 1
	
		Dim contenedor2 As ABMContainer
		contenedor2.Initialize(page, "contenedor2", "")
		contenedor2.AddRows(cantexp,True,"").AddCells12MP(1,0,0,0,0,"").AddCells12MP(5,0,0,10,0,"")
		contenedor2.BuildGrid
		
		
	   	For i=1 To cantexp
			
			contenedor2.Cell(i,3).SetOffsetSize(0,0,0,12,12,2)
			contenedor2.Cell(i,3).AddComponent(buildcombo("expi"&i,"","fecha inicio", anos))
			contenedor2.Cell(i,4).SetOffsetSize(0,0,0,12,12,2)
			contenedor2.Cell(i,4).AddComponent(buildcombo("expf"&i,"","fecha fin", anos))
	
			Dim inp5 As ABMInput
    		inp5.Initialize(page, "position"&i, ABM.INPUT_TEXT, "Cargo", False, "inp1")
			contenedor2.Cell(i,2).SetOffsetSize(0,0,0,12,12,3)
			contenedor2.Cell(i,2).AddComponent(inp5)
	 			
		
			Dim inp5 As ABMInput
    		inp5.Initialize(page, "enterprice"&i, ABM.INPUT_TEXT, "Empresa", False, "inp1")
			contenedor2.Cell(i,1).SetOffsetSize(0,0,0,12,12,3)
			contenedor2.Cell(i,1).AddComponent(inp5)
			
			Dim inp5 As ABMInput
    		inp5.Initialize(page, "jobphone"&i, ABM.INPUT_TEXT, "Teléfono de contacto", False, "inp1")
			contenedor2.Cell(i,5).SetOffsetSize(0,0,0,12,12,2)
			contenedor2.Cell(i,5).AddComponent(inp5)
			
			Dim inp5 As ABMInput
    		inp5.Initialize(page, "reason"&i, ABM.INPUT_TEXT, "Razón", False, "inp1")
			contenedor2.Cell(i,6).SetOffsetSize(0,0,0,12,12,12)
			contenedor2.Cell(i,6).AddComponent(inp5)	
	 
		Next
		
		 contenedord.Cell(2,1).AddComponent(contenedor2)
		 contenedor2.Refresh
         contenedord.Cell(2,1).Refresh
	End If
End Sub

Sub masuno (idtoast As Int) As Int
		myToastId=myToastId+1
		Return myToastId
End Sub

Sub cmbSubDivision_Clicked(itemId As String)
	
	Dim contenedora As ABMContainer = page.Component("contenedora")
	Dim cmbx As ABMCombo=contenedora.Cell(2,3).Component("cmbLocation")
	Dim c1 As ABMCombo=contenedora.Cell(2,2).Component("cmbSubDivision")
	Dim id1 As String=c1.GetActiveItemId
	Dim lb1 As ABMLabel=c1.GetComponent(id1)
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select distinct nom_propiedad from RH_SubDivision where nom_pais='" & lb1.Text & "'")
	'cmbx.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbx.AddItem(rst.GetString2(0) ,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	cmbx.Refresh
End Sub

Sub cmbDepartamento_Clicked(itemId As String)
	
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	
	Dim contenedora As ABMContainer = page.Component("contenedora")
	
	
	 Dim c1 As ABMCombo = contenedora.cell(2,1).Component("cmbDivision")
	 Dim t1 As String = c1.GetActiveItemId
	 Dim l1 As ABMLabel = c1.GetComponent(t1)
	 
	  If c1.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar the division",3000)
		Return
	  End If
	  
	 Dim c2 As ABMCombo = contenedora.cell(2,2).Component("cmbSubDivision")
	 Dim t2 As String = c2.GetActiveItemId
	 Dim l2 As ABMLabel = c2.GetComponent(t2)
	 
	  If c2.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar the division",3000)
		Return
	  End If 
	  
	 Dim c3 As ABMCombo = contenedora.cell(2,3).Component("cmbLocation")
	 Dim t3 As String = c3.GetActiveItemId
	 Dim l3 As ABMLabel = c3.GetComponent(t3)
	 
	  If c3.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar the location",3000)
		Return
	  End If 
	  
	 Dim c4 As ABMCombo = contenedora.cell(2,4).Component("cmbAreas")
	 Dim t4 As String = c4.GetActiveItemId
	 Dim l4 As ABMLabel = c4.GetComponent(t4)
	 
	 If c4.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar the division",3000)
		Return
	 End If
	
	
	Dim cmbx As ABMCombo=contenedora.Cell(3,3).Component("cmbVacancy")
	Dim cmby As ABMCombo=contenedora.Cell(3,3).Component("cmbVacancy")
	cmby.Clear
	cmby.Refresh
	
	
	Dim nom_departamento As String 
	nom_departamento = sql1.ExecQuerySingleResult("Select Dept_Name from RH_Dept where Dept_Key='" & itemId & "' order by 1")
	
	
	
	rst=sql1.ExecQuery("Select * from RH_PersonnelRequisition where nom_division='"&t1.ToUpperCase&"' and nom_country='"&t2.ToUpperCase&"' and nom_location='"&t3.ToUpperCase&"' and nom_area='"&t4.ToUpperCase&"' and nom_department='" & nom_departamento.ToUpperCase & "' order by 1")
	'cmbx.AddItem("P","",BuildSimpleItem("P","",""))
	
	
	Do While rst.NextRow
		cmbx.AddItem(rst.GetString2(0),rst.GetString2(6),BuildSimpleItem("P"& pos,"",rst.GetString2(6)))
		pos=pos+1
	Loop
	
	
	rst.Close
	cmbx.Refresh
End Sub

'Sub Page_NavigationbarClicked(Action As String, Value As String)
'	page.SaveNavigationBarPosition
'	'Abmshared.NavigateToPage(ws, Value)
'	
'	If Action = "ABMNavigationBar" Then Return
'	
'	If Action = "salida" Then
'		ABMShared.LogOff(page)
'		Return
'	End If
'	
'	If Action = "miperfil" Then
'		Main.username = ws.Session.GetAttribute2("authName", "")
'		Main.subordinado = False
'	End If
'	
'	ABMShared.NavigateToPage(ws,ABMPageId, Value)
'	
'End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
'	If icon <> "" Then
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
'	Else
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int 
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
	     combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next 
	
	'page.Refresh
	'combo1.Refresh	   
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid 
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)	
	Return myModal
End Sub


Sub Page_Ready()
	Log("solicitudpuestopub!")
	Connectpage
	'page.RestoreNavigationBarPosition
End Sub