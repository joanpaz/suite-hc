﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
' PENDIENTE EN LA JALADA DE LA FIRMA ID CUANDO EL USUARIO HAGA LOGON (EN ESTE MOMENTO ESTA EN MODO DE PRUEBA)

'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "referencias"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "21"
	' your own variables		
	Dim myToastId As Int = 1
	Dim  cmbelaborado,cmbrevisado,cmbaprobado, cmbDivision,cmbSubDivision,cmbLocation,cmbAreas,cmbDepartamento,cmbPuesto,cmbcto,cmbTipo,cmbNum,cmbnivel,cmbempleados,cmbsupervisores, cmbapplicant, cmbEnterprice As ABMCombo
	Dim anos As List
	
	Public DownloadFolder As String = "/www/" & ABMShared.AppName & "/uploads/"
	
	'Public DownloadFolder As String = File.DirApp
	Public DownloadMaxSize As String = 2*1024*1024
	Dim SignCounter As Int = 1
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
		'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	ConnectPage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------	
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")	
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
End Sub

public Sub BuildTheme()
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	theme.AddContainerTheme("conte")
	theme.Container("conte").ZDepth=ABM.ZDEPTH_3
	theme.AddDividerTheme("div1")
	theme.Divider("div1").ForeColor=ABM.COLOR_BLUE
	theme.Divider("div1").ForeColorIntensity=ABM.INTENSITY_LIGHTEN4
	theme.AddImageSliderTheme("sl1")
	theme.ImageSlider("sl1").Height=200
	theme.ImageSlider("sl1").Indicators=False
	theme.ImageSlider("sl1").FullWidth=True
	theme.ImageSlider("sl1").ZDepth=ABM.ZDEPTH_3
	
	theme.AddInputTheme("inp1")
	theme.Input("inp1").ZDepth = ABM.ZDEPTH_3
	
	theme.AddDateTimePickerTheme("date1")
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "referencias.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' Añadir Navigation Bar
	ABMShared.BuildNavigationBar(page,"Verificación de referencias","../images/klogo3.png","","referencias","")

	page.AddRows(16,False,"").AddCellsOSMP(1,0,0,0,12,12,12,10,0,-5,0,"")
	
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()
'Dim lbl As ABMLabel
	'lbl.Initialize(page,"lbl","REFERENCES VERIFICATION",ABM.SIZE_H6,False,"")
	'page.Cell(1,1).AddComponent(lbl)
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
	
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	
	anos.Initialize
	anos.Add("En proceso")
	For i=2016 To 1970 Step -1
		anos.Add(i)
	Next
	anos.Add("N/A")
	page.AddModalSheetTemplate(Mensajes)
	
	
	
	
	
	
	'-----------------------------------------------------------------------------     PRIMER FILTRO 
	
	Dim contenedora As ABMContainer	
	contenedora.Initialize(page,"contenedora","cardtheme2")
	'contenedora.AddRows
	
	contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	contenedora.AddRows(1,True, "").AddCellsOSMP(4,0,0,0,12,6,3,5,0,0,10,"")
	contenedora.AddRows(1,True, "").AddCellsOSMP(4,0,0,0,12,6,3,5,10,0,10,"")
	'contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	'contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	
	contenedora.BuildGrid
	page.Cell(1,1).SetOffsetSize(0,0,0,12,11,11)
	page.Cell(1,1).AddComponent(contenedora)
	
	
	
	Dim lbl As ABMLabel
	lbl.Initialize(page,"lbl","Selección del aplicante:",ABM.SIZE_H6,False,"etiq")
	contenedora.Cell(1,1).AddComponent(lbl)
	contenedora.Cell(1,1).UseTheme("celda2")
	
	
	
		
		
	'Division
	cmbDivision.Initialize(page,"cmbDivision",  "División",150,"combo")
	cmbDivision.AddItem("P","",BuildSimpleItem("P","",""))
	contenedora.Cell(2,1).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(2,1).AddComponent(cmbDivision)
	
	rst=sql1.ExecQuery("Select * from RH_dIVISION order by 1")
	Do While rst.NextRow
		cmbDivision.AddItem(rst.GetString2(0),rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	
	'SubDivision	
	cmbSubDivision.Initialize(page,"cmbSubDivision","Seleccione el país",250,"combo")
	contenedora.Cell(2,2).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(2,2).AddComponent(cmbSubDivision)
	
	rst=sql1.ExecQuery("Select * from RH_Country order by 1")
	
	pos=0
	Do While rst.NextRow
		cmbSubDivision.AddItem(rst.GetString2(0),rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	
	'Location
	cmbLocation.Initialize(page,"cmbLocation","Seleccione la locación",250,"combo")
	contenedora.Cell(2,3).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(2,3).AddComponent(cmbLocation)
	
	
	'Areas
	cmbAreas.Initialize(page,"cmbAreas","Seleccione el área",250,"combo")
	contenedora.Cell(2,4).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(2,4).AddComponent(cmbAreas)
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_Area order by 1")
	Do While rst.NextRow
		cmbAreas.AddItem(rst.GetString2(0),rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	
	
	
	
	
	
	cmbDepartamento.Initialize(page,"cmbDepartamento","Departamento",250,"combo")
	contenedora.Cell(3,1).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(3,1).AddComponent(cmbDepartamento)
	rst=sql1.ExecQuery("Select * from RH_Dept")
	pos=0
	'cmbDepartamento.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbDepartamento.AddItem(rst.GetString2(0),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
		pos=pos+1
	Loop
	rst.close
	
	
'			'supervisores
'	cmbsupervisores.Initialize(page,"cmbsupervisores","Head Manager",250,"combo")
'	contenedora.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
'	contenedora.Cell(3,2).AddComponent(cmbsupervisores)
'	rst=sql1.ExecQuery("Select * from ER_USERS")
'	pos=0
'	'cmbsupervisores.AddItem("P","",BuildSimpleItem("P","",""))
'	Do While rst.NextRow
'		cmbsupervisores.AddItem( rst.GetString2(1),rst.GetString2(5),BuildSimpleItem("P"& pos,"",rst.GetString2(5)))
'		pos=pos+1
'	Loop
'	rst.close
	
	
	'Puesto
	cmbPuesto.Initialize(page,"cmbVacancy","Seleccione la vacante",250,"combo")
	contenedora.Cell(3,2).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(3,2).AddComponent(cmbPuesto)
	
	
	
	cmbapplicant.Initialize(page,"cmbApplicant","Seleccione el aplicante",250,"combo")
	contenedora.Cell(3,3).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(3,3).AddComponent(cmbapplicant)
	
	cmbEnterprice.Initialize(page,"cmbEnterprice","Seleccione la compañia",250,"combo")
	contenedora.Cell(3,4).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(3,4).AddComponent(cmbEnterprice)
	
	
	
	'--------------------------------------------------------------------------------------------------- DATOS DE LA EMPRESA 1
	Dim contenedorb As ABMContainer	
	contenedorb.Initialize(page,"contenedorb","cardtheme2")
	'contenedorb.AddRows
	
	contenedorb.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	contenedorb.AddRows(6,True, "").AddCellsOSMP(4,0,0,0,12,6,3,5,0,0,10,"")
	'contenedorb.AddRows(1,True, "").AddCellsOSMP(4,0,0,0,12,6,3,5,10,0,10,"")
	'contenedorb.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	'contenedorb.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	
	contenedorb.BuildGrid
	page.Cell(2,1).SetOffsetSize(0,0,0,12,11,11)
	page.Cell(2,1).AddComponent(contenedorb)
	
	Dim lbl As ABMLabel
	lbl.Initialize(page,"lbl","Detalle",ABM.SIZE_H6,False,"etiq")
	contenedorb.Cell(1,1).AddComponent(lbl)
	contenedorb.Cell(1,1).UseTheme("celda2")
	
	
	
	Dim inp1 As ABMInput
    inp1.Initialize(page, "addr", ABM.INPUT_TEXT, "Dirección", True, "inp1")
    inp1.IconName = "mdi-action-account-circle"
    contenedorb.Cell(2,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(2,1).AddComponent(inp1)
	
	Dim inp1 As ABMInput
    inp1.Initialize(page, "phone", ABM.INPUT_TEL, "Teléfono", False, "inp1")
    inp1.IconName = "mdi-action-account-circle"
    contenedorb.Cell(2,2).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(2,2).AddComponent(inp1)
	
	Dim contrato As ABMCombo 
	contrato.Initialize(page,"contrato","Tipo de contrato",650,"combo")
	contrato.IconName = "mdi-action-account-circle"
	contrato.WrongMessage="Select the contract type"
	contrato.AddItem("Fixed","Fijo",BuildSimpleItem("Fixed", "mdi-action-account-circle", "{NBSP}{NBSP}Fijo"))
	contrato.AddItem("Eventual","Eventual",BuildSimpleItem("Eventual", "mdi-action-account-circle", "{NBSP}{NBSP}Eventual"))
	contenedorb.Cell(2,3).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(2,3).AddComponent(contrato)
	
	
	Dim nac As ABMDateTimePicker
	Dim NewDate As Long = DateTime.Now
	nac.Initialize(page, "ingress", ABM.DATETIMEPICKER_TYPE_DATE, NewDate, "Fecha", "")
	nac.CancelText = "Atrás"
	nac.PickText = "OK"
	nac.TodayText = "Hoy"
	nac.Language = "en"
	nac.ReturnDateFormat = "MM/DD/YYYY"   
	'nac.ReturnTimeFormat = "HH:mm"  
	     
	nac.FirstDayOfWeek = 0
	nac.ClickThrough = True 
	
	contenedorb.Cell(2,4).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(2,4).AddComponent(nac)
	
								'------------------------------------------------- datos 2
	
	Dim inp1 As ABMInput
    inp1.Initialize(page, "perf", ABM.INPUT_TEXT, "Desempeño", True, "inp1")
    inp1.IconName = "mdi-action-account-circle"
    contenedorb.Cell(3,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(3,1).AddComponent(inp1)
	
	Dim inp1 As ABMInput
    inp1.Initialize(page, "resp", ABM.INPUT_TEXT, "Puntualidad / Responsabilidad", True, "inp1")
    inp1.IconName = "mdi-action-account-circle"
    contenedorb.Cell(3,2).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(3,2).AddComponent(inp1)
	
	Dim inp1 As ABMInput
    inp1.Initialize(page, "char", ABM.INPUT_TEXT, "Carácter", True, "inp1")
    inp1.IconName = "mdi-action-account-circle"
    contenedorb.Cell(3,3).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(3,3).AddComponent(inp1)
	
	Dim contrato As ABMCombo 
	contrato.Initialize(page,"disc","Acciones disciplinarias",650,"combo")
	contrato.IconName = "mdi-action-account-circle"
	contrato.WrongMessage="Seleccione la cantidad"
	contrato.AddItem("1 a 3","1 a 3",BuildSimpleItem("1 a 3", "mdi-action-account-circle", "{NBSP}{NBSP}1 a 3"))
	contrato.AddItem("3+","3+",BuildSimpleItem("3+", "mdi-action-account-circle", "{NBSP}{NBSP}3+"))
	contenedorb.Cell(3,4).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(3,4).AddComponent(contrato)
	
	
						'-------------------------------------------------------------------------------------- DATOS 3
	
	Dim contrato As ABMCombo 
	contrato.Initialize(page,"inca","Incapacidades",650,"combo")
	contrato.IconName = "mdi-action-account-circle"
	contrato.WrongMessage="Seleccione el tipo"
	contrato.AddItem("work","trabajo",BuildSimpleItem("work", "mdi-action-account-circle", "{NBSP}{NBSP}trabajo"))
	contrato.AddItem("G.E.","G.E.",BuildSimpleItem("G.E.", "mdi-action-account-circle", "{NBSP}{NBSP}G.E."))
	contrato.AddItem("pregnancy","embarazo",BuildSimpleItem("pregnancy", "mdi-action-account-circle", "{NBSP}{NBSP}embarazo"))
	contenedorb.Cell(4,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(4,1).AddComponent(contrato)
	
	
	Dim inp1 As ABMInput
    inp1.Initialize(page, "dela", ABM.INPUT_TEXT, "Retrazos / Fallas", True, "inp1")
    inp1.IconName = "mdi-action-account-circle"
    contenedorb.Cell(4,2).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(4,2).AddComponent(inp1)
	
	Dim inp1 As ABMInput
    inp1.Initialize(page, "reti", ABM.INPUT_TEXT, "Razón de retiro", True, "inp1")
    inp1.IconName = "mdi-action-account-circle"
    contenedorb.Cell(4,3).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(4,3).AddComponent(inp1)
	
	Dim nac As ABMDateTimePicker
	Dim NewDate As Long = DateTime.Now
	nac.Initialize(page, "retidate", ABM.DATETIMEPICKER_TYPE_DATE, NewDate, "Fecha de retiro", "")
	nac.CancelText = "Atrás"
	nac.PickText = "OK"
	nac.TodayText = "Hoy"
	nac.Language = "en"
	nac.ReturnDateFormat = "MM/DD/YYYY"   
	'nac.ReturnTimeFormat = "HH:mm"  
	     
	nac.FirstDayOfWeek = 0
	nac.ClickThrough = True 
	
	contenedorb.Cell(4,4).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(4,4).AddComponent(nac)
	
						'--------------------------------------------------------------------------------------------- datos 4
	
	Dim contrato As ABMCombo 
	contrato.Initialize(page,"rehi","¿Lo recontrataría?",650,"combo")
	contrato.IconName = "mdi-action-account-circle"
	contrato.WrongMessage="Seleccione la respuesta"
	contrato.AddItem("si","Si",BuildSimpleItem("Si", "mdi-action-account-circle", "{NBSP}{NBSP}Si"))
	contrato.AddItem("No","No",BuildSimpleItem("No", "mdi-action-account-circle", "{NBSP}{NBSP}No"))
	contenedorb.Cell(5,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(5,1).AddComponent(contrato)
	
	Dim inp1 As ABMInput
    inp1.Initialize(page, "why1", ABM.INPUT_TEXT, "¿Por qué?", True, "inp1")
    inp1.IconName = "mdi-action-account-circle"
    contenedorb.Cell(5,2).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(5,2).AddComponent(inp1)
	
	Dim contrato As ABMCombo 
	contrato.Initialize(page,"reco","¨¿Lo recomendaría?",650,"combo")
	contrato.IconName = "mdi-action-account-circle"
	contrato.WrongMessage="Seleccione la respuesta"
	contrato.AddItem("Si","Si",BuildSimpleItem("Si", "mdi-action-account-circle", "{NBSP}{NBSP}Si"))
	contrato.AddItem("No","No",BuildSimpleItem("No", "mdi-action-account-circle", "{NBSP}{NBSP}No"))
	contenedorb.Cell(5,3).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(5,3).AddComponent(contrato)
	
	Dim inp1 As ABMInput
    inp1.Initialize(page, "why2", ABM.INPUT_TEXT, "¿Por qué?", True, "inp1")
    inp1.IconName = "mdi-action-account-circle"
    contenedorb.Cell(5,4).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(5,4).AddComponent(inp1)
	
	
					'--------------------------------------------------------------------------------------------------- datos 5
	
	Dim nac As ABMDateTimePicker
	Dim NewDate As Long = DateTime.Now
	nac.Initialize(page, "refd", ABM.DATETIMEPICKER_TYPE_DATE, NewDate, "Fecha de referencia", "")
	nac.CancelText = "Atrás"
	nac.PickText = "OK"
	nac.TodayText = "Hoy"
	nac.Language = "en"
	nac.ReturnDateFormat = "MM/DD/YYYY"   
	'nac.ReturnTimeFormat = "HH:mm"  
	
	nac.FirstDayOfWeek = 0
	nac.ClickThrough = True 
	
	contenedorb.Cell(6,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(6,1).AddComponent(nac)
	
	
	Dim inp1 As ABMInput
    inp1.Initialize(page, "gave", ABM.INPUT_TEL, "¿Quién dio la referencia?", False, "inp1")
    inp1.IconName = "mdi-action-account-circle"
    contenedorb.Cell(6,2).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(6,2).AddComponent(inp1)
	
	Dim inp1 As ABMInput
    inp1.Initialize(page, "refe", ABM.INPUT_TEL, "Cargo del referenciador", False, "inp1")
    inp1.IconName = "mdi-action-account-circle"
    contenedorb.Cell(6,3).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(6,3).AddComponent(inp1)
	
	Dim inp1 As ABMInput
    inp1.Initialize(page, "gene", ABM.INPUT_TEL, "Comentario general", True, "inp1")
    inp1.IconName = "mdi-action-account-circle"
    contenedorb.Cell(6,4).SetOffsetSize(0,0,0,12,6,3)
	contenedorb.Cell(6,4).AddComponent(inp1)
	
	
	'------------------------------------------------------------------------------------------------------- DATOS DE VERIF VERIFICADO
	

	
	
	Dim contenedorc As ABMContainer	
	contenedorc.Initialize(page,"contenedorc","cardtheme2")
	'contenedorc.AddRows
	
	contenedorc.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	contenedorc.AddRows(3,True, "").AddCellsOSMP(4,0,0,0,12,6,3,5,0,0,10,"")
	contenedorc.AddRows(1,True, "").AddCellsOSMP(4,0,0,0,12,6,3,5,10,0,10,"")
	'contenedorc.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	'contenedorc.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	
	contenedorc.BuildGrid
	page.Cell(3,1).SetOffsetSize(0,0,0,12,11,11)
	page.Cell(3,1).AddComponent(contenedorc)
	
	
	Dim inp1 As ABMInput
    inp1.Initialize(page, "veri", ABM.INPUT_TEL, "Verificado", True, "")
    inp1.IconName = "mdi-action-account-circle"
    contenedorc.Cell(2,1).SetOffsetSize(0,0,0,12,6,3)
	contenedorc.Cell(2,1).AddComponent(inp1)
	
'	Dim inp1 As ABMInput
'    inp1.Initialize(page, "sign", ABM.INPUT_TEL, "Sign", True, "")
'    inp1.IconName = "mdi-action-account-circle"
'    contenedorc.Cell(2,2).SetOffsetSize(0,0,0,12,6,3)
'	contenedorc.Cell(2,2).AddComponent(inp1)

	
	Dim btnClear As ABMButton
	btnClear.InitializeRaised(page, "btnClear", "", "", "Limpiar", "btn")
	contenedorc.Cell(2,3).SetOffsetSize(0,0,0,12,6,3)
	contenedorc.Cell(2,3).AddComponent( btnClear)
	
	Dim btnGetSign As ABMButton
	btnGetSign.InitializeRaised(page, "btnGetSign", "", "", "Tomar firma", "btn")
	contenedorc.Cell(2,4).SetOffsetSize(0,0,0,12,6,3)
	contenedorc.Cell(2,4).AddComponent(btnGetSign )
	
	
	
		'Empleados
	cmbelaborado.Initialize(page,"cmbelaborado","Elaborado",250,"combo")
	contenedorc.Cell(3,1).SetOffsetSize(0,0,1,12,6,3)
	contenedorc.Cell(3,1).AddComponent(cmbelaborado)
	rst=sql1.ExecQuery("Select * from RH_UserConf")
	pos=0
	'cmbelaborado.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbelaborado.AddItem( rst.GetString2(1),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
		pos=pos+1
	Loop
	rst.close
	
		'Empleados
	cmbrevisado.Initialize(page,"cmbrevisado","Revisado",250,"combo")
	contenedorc.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
	contenedorc.Cell(3,2).AddComponent(cmbrevisado)
	rst=sql1.ExecQuery("Select * from RH_UserConf")
	pos=0
	'cmbrevisado.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbrevisado.AddItem( rst.GetString2(1),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
		pos=pos+1
	Loop
	rst.close
	
		'aprobado
	cmbaprobado.Initialize(page,"cmbaprobado","Aprobado",250,"combo")
	contenedorc.Cell(3,3).SetOffsetSize(0,0,1,12,6,3)
	contenedorc.Cell(3,3).AddComponent(cmbaprobado)
	rst=sql1.ExecQuery("Select * from RH_UserConf")
	pos=0
	'cmbaprobado.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbaprobado.AddItem( rst.GetString2(1),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
		pos=pos+1
	Loop
	rst.close
	
	Dim btnAceptar As ABMButton
	btnAceptar.InitializeFlat(page,"btnaceptar","","","Aceptar","btn")
	page.Cell(4,1).SetOffsetSize(0,0,5,12,4,4)
	page.Cell(4,1).AddComponent(btnAceptar)
	
	Dim sign1 As ABMSignaturePad
	sign1.Initialize(page, "sign1", 640, 360, ABM.COLOR_WHITE, ABM.INTENSITY_NORMAL, ABM.COLOR_BLUEGREY, ABM.INTENSITY_DARKEN2, ABM.ZDEPTH_1)
	'contenedorc.Cell(2,2).SetOffsetSize(0,0,0,12,6,3)
	page.Cell(5,1).AddComponent(sign1)
	
	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub

Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then 
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub btnaceptar_Clicked(Target As String)
	
	 'page.ShowToast("toast" & 1, "toastred", txt, 5000)
	
		myToastId = 1
	
		Dim sql1 As SQL
		Dim valores(), valores2()  As Object
		sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
		Dim rst As ResultSet
		Dim pos As Int = 0
		Dim id1, id2, id3 As Int
		Dim sqlt, sqlt2, sqlt3 As String 
		Dim i As Int
		
	'------------------ JALADA DE LA FIRMA (PENDIENTE COLOCAR ID CUANDO EL USUARIO HAGA LOGON)
	Dim su As StringUtils
	Dim folder As String = File.Combine(File.DirApp, DownloadFolder)
	Dim BT2() As Byte
	Dim ser2 As ByteConverter
	Dim imagen As Image
	Dim imagenenc As String = ""
	
	If File.Exists(folder,"Signature" & SignCounter & ".jpg") Then
		imagen.Initialize(folder,"Signature" & SignCounter & ".jpg")
		BT2=ImageToBytes(imagen)
		Log(BT2.Length)
		imagenenc=su.EncodeBase64(BT2)
		'imagenenc=BytesToString(BT2,0,BT2.Length,"UTF-8")		
		File.WriteString(folder,"Signature" & SignCounter & ".txt",imagenenc)	
	Else
		page.ShowToast(masuno(myToastId),"toast1","Debe firmar al final de la página",3000)
		Return
		
	
	End If

	'------------------------------------------------------------------------------------------
	
	Dim containera As ABMContainer = page.Component("contenedora")
	Dim containerb As ABMContainer = page.Component("contenedorb")
	Dim containerc As ABMContainer = page.Component("contenedorc")
	
	
	'------------------------------------------------------------------------------- CONTAINER A
	
	'-------------- LINEA 2
	Dim c1a As ABMCombo = containera.cell(2,1).Component("cmbDivision")
	Dim t1a As String = c1a.GetActiveItemId
	Dim l1a As ABMLabel = c1a.GetComponent(t1a)
	 
	If c1a.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar la división",3000)
		Return
	End If
	
	Dim c2a As ABMCombo = containera.cell(2,2).Component("cmbSubDivision")
	Dim t2a As String = c2a.GetActiveItemId
	Dim l2a As ABMLabel = c2a.GetComponent(t2a)
	 
	If c2a.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar el país",3000)
		Return
	End If
	 
	Dim c3a As ABMCombo = containera.cell(2,3).Component("cmbLocation")
	Dim t3a As String = c3a.GetActiveItemId
	Dim l3a As ABMLabel = c3a.GetComponent(t3a)
	 
	If c3a.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar la locación",3000)
		Return
	End If
	
	Dim c4a As ABMCombo = containera.cell(2,4).Component("cmbAreas")
	Dim t4a As String = c4a.GetActiveItemId
	Dim l4a As ABMLabel = c4a.GetComponent(t4a)
	 
	If c4a.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar el área",3000)
		Return
	End If
	
	'-----------LINEA 3
	Dim c5a As ABMCombo = containera.cell(3,1).Component("cmbDepartamento")
	Dim t5a As String = c5a.GetActiveItemId
	Dim l5a As ABMLabel = c5a.GetComponent(t5a)
	 
	If c5a.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar el Departamento",3000)
		Return
	End If
	
	Dim c6a As ABMCombo = containera.cell(3,2).Component("cmbVacancy")
	Dim t6a As String = c6a.GetActiveItemId
	Dim l6a As ABMLabel = c6a.GetComponent(t6a)
	 
	If c6a.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar la vacante",3000)
		Return
	End If
	
	Dim c7a As ABMCombo = containera.cell(3,3).Component("cmbApplicant")
	Dim t7a As String = c7a.GetActiveItemId
	Dim l7a As ABMLabel = c7a.GetComponent(t7a)
	 
	If c7a.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar el aplicante",3000)
		Return
	End If
	
	Dim c8a As ABMCombo = containera.cell(3,4).Component("cmbEnterprice")
	Dim t8a As String = c8a.GetActiveItemId
	Dim l8a As ABMLabel = c8a.GetComponent(t8a)
	 
	If c8a.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar la empresa",3000)
		Return
	End If
	
	'-------------------------------------------------------------------------------------- CONTAINER B
	
	
	' LINEA 2
	
	 Dim i1b As ABMInput = containerb.cell(2,1).Component("addr")
	 
	  If ABMShared.validatxt(i1b.text) Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar la dirección",3000)
		Return
	  End If
	  
	  Dim i2b As ABMInput = containerb.cell(2,2).Component("phone")
	 
	  If ABMShared.validatxt(i2b.text) Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar el teléfono",3000)
		Return
	  End If
	
	Dim c1b As ABMCombo = containerb.cell(2,3).Component("contrato")
	Dim t1b As String = c1b.GetActiveItemId
	Dim l1b As ABMLabel = c1b.GetComponent(t1b)
	 
	If c1b.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar el tipo de contrato",3000)
		Return
	End If
	
	Dim f1b As ABMDateTimePicker = containerb.cell(2,4).Component("ingress")
	
	' LINEA 3
	
	Dim i3b As ABMInput = containerb.cell(3,1).Component("perf")
	 
	  If ABMShared.validatxt(i3b.text) Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar el desempeño",3000)
		Return
	  End If
	  
	  
	 Dim i4b As ABMInput = containerb.cell(3,2).Component("resp")
	 
	  If ABMShared.validatxt(i4b.text) Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar la puntualidad",3000)
		Return
	  End If 
	  
	 Dim i5b As ABMInput = containerb.cell(3,3).Component("char")
	 
	  If ABMShared.validatxt(i5b.text) Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar el caracter",3000)
		Return
	  End If 
	
	Dim c2b As ABMCombo = containerb.cell(3,4).Component("disc")
	Dim t2b As String = c2b.GetActiveItemId
	Dim l2b As ABMLabel = c2b.GetComponent(t2b)
	 
	If c2b.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar las acciones disciplinarias",3000)
		Return
	End If
	
	' LINEA 4
	
	Dim c4b As ABMCombo = containerb.cell(4,1).Component("inca")
	Dim t4b As String = c4b.GetActiveItemId
	Dim l4b As ABMLabel = c4b.GetComponent(t4b)
	 
	If c4b.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar la incapacidad",3000)
		Return
	End If
	
	Dim i6b As ABMInput = containerb.cell(4,2).Component("dela")
	 
	  If ABMShared.validatxt(i6b.text) Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar el retraso",3000)
		Return
	  End If 
	
	Dim i7b As ABMInput = containerb.cell(4,3).Component("reti")
	 
	  If ABMShared.validatxt(i7b.text) Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar la razón de retiro",3000)
		Return
	  End If 
	
	Dim f2b As ABMDateTimePicker = containerb.cell(4,4).Component("retidate")
	
	Dim c4b As ABMCombo = containerb.cell(4,1).Component("inca")
	Dim t4b As String = c4b.GetActiveItemId
	Dim l4b As ABMLabel = c4b.GetComponent(t4b)
	 
	If c4b.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar la fecha de egreso",3000)
		Return
	End If
	
	
	' LINEA 5
	
	Dim c5b As ABMCombo = containerb.cell(5,1).Component("rehi")
	Dim t5b As String = c5b.GetActiveItemId
	Dim l5b As ABMLabel = c5b.GetComponent(t5b)
	 
	If c5b.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar si lo recontratarían",3000)
		Return
	End If
	
	
	Dim i8b As ABMInput = containerb.cell(5,2).Component("why1")
	 
	  If ABMShared.validatxt(i8b.text) Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar el por qué lo recontratarían",3000)
		Return
	  End If 
	
	Dim c6b As ABMCombo = containerb.cell(5,3).Component("reco")
	Dim t6b As String = c6b.GetActiveItemId
	Dim l6b As ABMLabel = c6b.GetComponent(t6b)
	 
	If c6b.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe responder si lo recomendarían",3000)
		Return
	End If
	
	Dim i9b As ABMInput = containerb.cell(5,4).Component("why2")
	 
	  If ABMShared.validatxt(i9b.text) Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar el por qué lo recomendarían",3000)
		Return
	  End If 
	
	
	
	' LINEA 6
	
	Dim f3b As ABMDateTimePicker = containerb.cell(6,1).Component("refd")
	
		Dim i10b As ABMInput = containerb.cell(6,2).Component("gave")
	 
	  If ABMShared.validatxt(i9b.text) Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar el nombre del referenciador",3000)
		Return
	  End If 
	  
	  	Dim i11b As ABMInput = containerb.cell(6,3).Component("refe")
	 
	  If ABMShared.validatxt(i9b.text) Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar la posición del referenciador",3000)
		Return
	  End If 
	  
	  	Dim i12b As ABMInput = containerb.cell(6,4).Component("gene")
	 
	  If ABMShared.validatxt(i9b.text) Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar los comentarios generales",3000)
		Return
	  End If 
	
	'------------------------------ CONTENEDOR C
	
	' LINEA 2
	
	  Dim i1c As ABMInput = containerc.cell(2,1).Component("veri")
	 
	  If ABMShared.validatxt(i1c.text) Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar quien verifica",3000)
		Return
	  End If 
	
	
	' LINEA 3
	
	Dim c1c As ABMCombo = containerc.cell(3,1).Component("cmbelaborado")
	Dim t1c As String = c1c.GetActiveItemId
	Dim l1c As ABMLabel = c1c.GetComponent(t1c)
	 
	If c1c.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar quien elabora",3000)
		Return
	End If
	
	
	Dim c2c As ABMCombo = containerc.cell(3,2).Component("cmbrevisado")
	Dim t2c As String = c2c.GetActiveItemId
	Dim l2c As ABMLabel = c2c.GetComponent(t2c)
	 
	If c2c.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar quien revisa",3000)
		Return
	End If
	
	Dim c3c As ABMCombo = containerc.cell(3,3).Component("cmbaprobado")
	Dim t3c As String = c3c.GetActiveItemId
	Dim l3c As ABMLabel = c3c.GetComponent(t3c)
	 
	If c3c.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe llenar quien aprueba",3000)
		Return
	End If
	
	
	
	
	' INSERT
	page.ShowToast(masuno(myToastId),"toast1","Salvando",3000)
	
	valores=Array As Object (t7a.ToUpperCase,t8a.ToUpperCase,l8a.Text.ToUpperCase,i1b.Text.ToUpperCase,i2b.Text.ToUpperCase,t1b.ToUpperCase,DateTime.Date(f1b.GetDate),i3b.Text.ToUpperCase,i4b.Text.ToUpperCase,i5b.Text.ToUpperCase,t2b.ToUpperCase,t4b.ToUpperCase,i6b.Text.ToUpperCase,i7b.Text.ToUpperCase,DateTime.Date(f2b.GetDate),t5b.ToUpperCase,i8b.Text.ToUpperCase,t6b.ToUpperCase,i9b.Text.ToUpperCase,DateTime.Date(f3b.GetDate),i10b.Text.ToUpperCase,i11b.Text.ToUpperCase,i12b.Text.ToUpperCase,i1c.Text.ToUpperCase,t1c.ToUpperCase,t2c.ToUpperCase,t3c.ToUpperCase,imagenenc)
	sql1.ExecNonQuery2("INSERT INTO RH_Reference VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )", valores)
	ABMShared.NavigateToPage(ws,ABMPageId,"../referencias/referencias.html")
	
	
	
	
	
	
End Sub

Sub cmbEnterprice_Clicked(itemId As String)
	
	Dim contenedorb As ABMContainer = page.Component("contenedorb")
	Dim phone As ABMInput=contenedorb.Cell(2,2).Component("phone")
	Dim ret As ABMInput=contenedorb.Cell(4,3).Component("reti")
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_ApplicantDat where id_applicantdat='" & itemId & "'")
	
	Do While rst.NextRow
		phone.Text=rst.GetString2(5)
		ret.Text=rst.GetString2(6)
	Loop
	rst.Close
	phone.Refresh
	ret.Refresh
	
	
	
End Sub


Sub cmbSubDivision_Clicked(itemId As String)
	
	Dim contenedora As ABMContainer = page.Component("contenedora")
	Dim cmbx As ABMCombo=contenedora.Cell(2,3).Component("cmbLocation")
	Dim c1 As ABMCombo=contenedora.Cell(2,2).Component("cmbSubDivision")
	Dim id1 As String=c1.GetActiveItemId
	Dim lb1 As ABMLabel=c1.GetComponent(id1)
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select distinct nom_propiedad from RH_SubDivision where nom_pais='" & lb1.Text & "'")
	cmbx.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbx.AddItem(rst.GetString2(0) ,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	cmbx.Refresh
End Sub


Sub cmbApplicant_Clicked(itemId As String)
	
	Dim contenedora As ABMContainer = page.Component("contenedora")
	Dim cmbx As ABMCombo=contenedora.Cell(3,4).Component("cmbEnterprice")
'	Dim c1 As ABMCombo=contenedora.Cell(2,2).Component("cmbSubDivision")
'	Dim id1 As String=c1.GetActiveItemId
'	Dim lb1 As ABMLabel=c1.GetComponent(id1)
	cmbx.clear
	cmbx.Refresh
	
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_ApplicantDat where id_Applicant='" & itemId & "' and nom_type='profesion'")
	
	Do While rst.NextRow
		cmbx.AddItem(rst.GetString2(0) ,rst.GetString2(3),BuildSimpleItem("P"& pos,"",rst.GetString2(3)))
		pos=pos+1
	Loop
	rst.Close
	cmbx.Refresh
End Sub



Sub cmbVacancy_Clicked(itemId As String)
	
	Dim contenedora As ABMContainer = page.Component("contenedora")
	'Dim cmbx As ABMCombo=contenedora.Cell(2,3).Component("cmbVacancy")
	
	Dim cmby As ABMCombo=contenedora.Cell(3,3).Component("cmbApplicant")
	
	cmby.Clear
	cmby.Refresh
	
'	Dim c1 As ABMCombo=contenedora.Cell(2,2).Component("cmbSubDivision")
'	Dim id1 As String=c1.GetActiveItemId
'	Dim lb1 As ABMLabel=c1.GetComponent(id1)
	Dim sql1, sql2 As SQL
	Dim rst, rst2 As ResultSet
	Dim pos , pos2 As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	sql2.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	
	
	'Dim posname As String
	'posname =sql1.ExecQuerySingleResult("Select nom_position from RH_PersonnelRequisition where id_personnelreq = '"&itemId&"'")
	
	'Dim poskey As String
	'poskey = sql1.ExecQuerySingleResult("Select Pos_Key from RH_Positons where Pos_Name='" & posname & "' order by 1")
	
	'rst=sql1.ExecQuery("Select * from RH_Application where Pos_Key='" &poskey& "'")
	
															
	rst=sql1.ExecQuery("Select * from RH_Application where Pos_Key='" &itemId& "'")	
	
	Do While rst.NextRow
		
		rst2 = 	sql2.ExecQuery("Select * from RH_Applicant where id_Applicant='" &rst.GetString2(7)& "'")	
		Do While rst2.NextRow
		
			Dim Name As String = rst2.GetString2(4)&" "&rst2.GetString2(5)&" "&rst2.GetString2(2)&" "&rst2.GetString2(3)
		  cmby.AddItem(rst2.GetString2(1),Name,BuildSimpleItem("P"& pos,"",Name))
		
		pos2 = pos2+1	
		Loop
		rst2.Close
		cmby.Refresh
		
		pos=pos+1
	Loop
	rst.Close
	
End Sub



Sub cmbDepartamento_Clicked(itemId As String)
	
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	
	Dim contenedora As ABMContainer = page.Component("contenedora")
	
	
	 Dim c1 As ABMCombo = contenedora.cell(2,1).Component("cmbDivision")
	 Dim t1 As String = c1.GetActiveItemId
	 Dim l1 As ABMLabel = c1.GetComponent(t1)
	 
	  If c1.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar la división",3000)
		Return
	  End If
	  
	 Dim c2 As ABMCombo = contenedora.cell(2,2).Component("cmbSubDivision")
	 Dim t2 As String = c2.GetActiveItemId
	 Dim l2 As ABMLabel = c2.GetComponent(t2)
	 
	  If c2.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar el país",3000)
		Return
	  End If 
	  
	 Dim c3 As ABMCombo = contenedora.cell(2,3).Component("cmbLocation")
	 Dim t3 As String = c3.GetActiveItemId
	 Dim l3 As ABMLabel = c3.GetComponent(t3)
	 
	  If c3.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar la locación",3000)
		Return
	  End If 
	  
	 Dim c4 As ABMCombo = contenedora.cell(2,4).Component("cmbAreas")
	 Dim t4 As String = c4.GetActiveItemId
	 Dim l4 As ABMLabel = c4.GetComponent(t4)
	 
	 If c4.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar el área",3000)
		Return
	 End If
	
	
	Dim cmbx As ABMCombo=contenedora.Cell(3,2).Component("cmbVacancy")
	Dim cmby As ABMCombo=contenedora.Cell(3,2).Component("cmbVacancy")
	cmby.Clear
	cmby.Refresh
	
	
	Dim nom_departamento As String 
	nom_departamento = sql1.ExecQuerySingleResult("Select Dept_Name from RH_Dept where Dept_Key='" & itemId & "' order by 1")
	
	
	
	rst=sql1.ExecQuery("Select * from RH_PersonnelRequisition where nom_division='"&t1&"' and nom_country='"&t2&"' and nom_location='"&t3&"' and nom_area='"&t4&"' and nom_department='" & nom_departamento & "' order by 1")
	'cmbx.AddItem("P","",BuildSimpleItem("P","",""))
	
	
	Do While rst.NextRow
		cmbx.AddItem(rst.GetString2(0),rst.GetString2(6),BuildSimpleItem("P"& pos,"",rst.GetString2(6)))
		pos=pos+1
	Loop
	
	
	rst.Close
	cmbx.Refresh
End Sub

Sub btnClear_Clicked(Target As String)
	Dim sign1 As ABMSignaturePad = page.Cell(5,1).Component("sign1")
	'Dim contenedorc As ABMContainer = page.Component("contenedorc")
	'Dim sign1 As ABMSignaturePad = contenedorc.Cell(2,2).Component("sign1")
	sign1.Clear	
End Sub

Sub btnGetSign_Clicked(Target As String)
	'Dim contenedorc As ABMContainer = page.Component("contenedorc")
	'Dim sign1 As ABMSignaturePad = contenedorc.Cell(2,2).Component("sign1")
	Dim sign1 As ABMSignaturePad = page.Cell(5,1).Component("sign1")
	
	SignCounter =SignCounter + 1
	sign1.GetDrawingURI("Signature" & SignCounter & ".jpg")	
End Sub

Sub masuno (idtoast As Int) As Int
		myToastId=myToastId+1
		Return myToastId
End Sub


Sub ImageToBytes(Image As Image) As Byte()
   Dim out As OutputStream
   out.InitializeToBytesArray(0)
   Image.WriteToStream(out)', 100, "JPEG")
   out.Close
   Return out.ToBytesArray
End Sub

Sub BytesToImage(bytes() As Byte) As Image
   Dim In As InputStream
   In.InitializeFromBytesArray(bytes, 0, bytes.Length)
   Dim bmp As Image
   bmp.Initialize2(In)
   
   Return bmp
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
'	If icon <> "" Then
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
'	Else
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int 
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
	     combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next 
	
	'page.Refresh
	'combo1.Refresh	   
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid 
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)	
	Return myModal
End Sub

Sub Page_Ready()
	Log("Lista referencias")
	ConnectPage
	page.RestoreNavigationBarPosition
End Sub