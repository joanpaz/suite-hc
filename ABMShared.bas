﻿Type=StaticCode
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Static code module
Sub Process_Globals
	Public MyTheme As ABMTheme
	Private ABM As ABMaterial 'ignore
	Public NeedsAuthorization As Boolean = False
	Public AppVersion As String = DateTime.now ' NEW 2.01 this helps to get the latest js/css files when the app is started/restarted
	Public AppPublishedStartURL As String = ""
	Public AppName As String = "suite"
	
	Public CachedPages As Map
	Public CacheScavengePeriodSeconds As Int = 15*60 ' 15 minutes
	Public SessionMaxInactiveIntervalSeconds As Int = 1*60 ' 30 minutes '1*60*24 ' one hour ' -1 = immortal but beware! This also means your cache is NEVER emptied!
		
End Sub



Public Sub NavigateToPage(ws As WebSocket, PageId As String, TargetUrl As String)
	'----------------------MODIFICATION-------------------------------
	If AppVersion.Length > 0 Then TargetUrl = $"${TargetUrl}?${AppVersion}"$
	If PageId.Length > 0 Then ABM.RemoveMeFromCache(CachedPages, PageId)
	If ws.Open Then
		' it doesn't keep navigation history in the browser (the back button exists the application)
		'ws.Eval("window.location.replace(arguments[0])", Array As Object(TargetUrl))
		' if you need browser history just comment the lines above and uncomment the lines below
		' it keeps the navigation history in the browser
		ws.Eval("window.location = arguments[0]", Array As Object(TargetUrl))
		ws.Flush
	End If
	'----------------------MODIFICATION-------------------------------
End Sub
Public Sub NavigateToPageNewTab(ws As WebSocket, PageId As String, TargetUrl As String, OpenInNewTab As Boolean)
	If AppVersion <> "" Then
		TargetUrl = TargetUrl & "?" & AppVersion
	End If
	ABM.RemoveMeFromCache(CachedPages, PageId)
	If ws.Open Then
		If OpenInNewTab Then
			Dim s As String
			' check if a mobile phone only
			'			s = $"var check = false;
			'  			(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
			'  			if (check) {
			'  				window.location = arguments[0];
			'  			} else {
			'  				window.open(arguments[0],'_blank');
			'  			}"$
			
			' check if a mobile phone or a tablet
			s = $"var check = false;
  			(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
   			if (check) {
  				window.location = arguments[0];
  			} else {
				window.open(arguments[0],'_blank');  				
  			}"$			
			ws.Eval(s, Array As Object(TargetUrl))
		Else
			ws.Eval("window.location = arguments[0]", Array As Object(TargetUrl))
		End If
		ws.Flush
	End If
End Sub

Sub RedirectOutput (Dir As String, FileName As String)
   #if RELEASE
   Dim out As OutputStream = File.OpenOutput(Dir, FileName, False) 'Set to True to append the logs
   Dim ps As JavaObject
   ps.InitializeNewInstance("java.io.PrintStream", Array(out, True, "utf8"))
   Dim jo As JavaObject
   jo.InitializeStatic("java.lang.System")
   jo.RunMethod("setOut", Array(ps))
   jo.RunMethod("setErr", Array(ps))
   #end if
End Sub

Sub LogOff(page As ABMPage)
	' do whatever you have to do to log off your user
			
	page.ws.Session.SetAttribute("IsAuthorized", "")
	page.ws.Session.SetAttribute("authType", "")
	page.ws.Session.SetAttribute("authName", "")
	'page.ws.Session.SetAttribute("IsAuthorized", "false")
	page.ws.Session.SetAttribute("idsesion", "")
	Main.idsesion = ""
	Main.correo = ""
	Main.nombrecompleto = ""
	Main.username = ""
	
	
	'	"../acceso/acceso.html"
	NavigateToPage(page.ws, page.GetPageID, "../")
End Sub

' build methods for ABM objects
Sub BuildTheme(themeName As String)
	
	
	MyTheme.Initialize(themeName)
	
	
	' the page theme
	MyTheme.Page.BackColor = ABM.COLOR_WHITE
	
	' the navbar theme
	MyTheme.AddNavigationBarTheme("nav1theme")
	MyTheme.NavigationBar("nav1theme").TopBarBackColor = ABM.COLOR_BROWN
	MyTheme.NavigationBar("nav1theme").TopBarBackColorIntensity=ABM.INTENSITY_DARKEN4
	'paneles
	MyTheme.AddContainerTheme("pnltheme")
	MyTheme.Container("pnltheme").BackColor=ABM.COLOR_AMBER

	'Botones
	MyTheme.AddButtonTheme("btn")
	MyTheme.Button("btn").BackColor=ABM.COLOR_BROWN
	MyTheme.Button("btn").BackColorIntensity=ABM.INTENSITY_DARKEN4
	MyTheme.Button("btn").ForeColor=ABM.COLOR_WHITE
	MyTheme.Button("btn").ZDepth=ABM.ZDEPTH_3
	
	MyTheme.AddButtonTheme("btngreen")
	MyTheme.Button("btngreen").BackColor=ABM.COLOR_TEAL
	MyTheme.Button("btngreen").BackColorIntensity=ABM.INTENSITY_DARKEN1
	
	
	'botones inicio
	MyTheme.AddButtonTheme("btn2")
	MyTheme.Button("btn2").BackColor=ABM.COLOR_BROWN
	MyTheme.Button("btn2").BackColorIntensity=ABM.INTENSITY_LIGHTEN5
	MyTheme.Button("btn2").ForeColor=ABM.COLOR_WHITE
	'MyTheme.Button("btn").ZDepth=ABM.ZDEPTH_3
	
	'Conteiner
	MyTheme.AddContainerTheme("cnt")
	'MyTheme.Container("cnt").BackColor = ABM.COLOR_WHITE
	MyTheme.Container("cnt").ZDepth=ABM.ZDEPTH_2
	'container inicio
	MyTheme.AddContainerTheme("cnta")
	MyTheme.Container("cnta").BackColor = ABM.COLOR_BROWN
	MyTheme.Container("cnta").BackColorIntensity = ABM.INTENSITY_DARKEN4
	
	'MyTheme.Container("cnta").ZDepth=ABM.ZDEPTH_2
	
	
	'caja de Texto
	MyTheme.AddInputTheme("caja")
	MyTheme.Input("caja").InputColor=ABM.COLOR_BROWN
	MyTheme.Input("caja").InvalidColor=ABM.COLOR_TRANSPARENT
	
	'caja de Texto inicio
	MyTheme.AddInputTheme("caja2")
	MyTheme.Input("caja2").BackColor=ABM.COLOR_WHITE
	MyTheme.Input("caja2").InputColor=ABM.COLOR_BROWN
	MyTheme.Input("caja2").InvalidColor=ABM.COLOR_TRANSPARENT
	
	
	'celda texto input
	MyTheme.AddCellTheme("celdainp")
	MyTheme.Cell("celdainp").BorderColor=ABM.COLOR_WHITE
	MyTheme.Cell("celdainp").BorderWidth=1
	'MyTheme.Cell("celdainp").ZDepth=ABM.ZDEPTH_2
	'---------------
	
	' a toast theme
	MyTheme.AddToastTheme("toast")
	MyTheme.Toast("toast").Rounded = True
	MyTheme.Toast("toast").ActionForeColor = ABM.COLOR_ORANGE
	
	'letras cafes
	MyTheme.AddLabelTheme("lblbr")
	MyTheme.Label("lblbr").ForeColor=ABM.COLOR_BROWN
	MyTheme.Label("lblbr").ForeColorIntensity=ABM.INTENSITY_DARKEN1
	
	' another toast theme
	MyTheme.AddToastTheme("toastred")
	MyTheme.Toast("toastred").Rounded = True
	MyTheme.Toast("toastred").ActionForeColor = ABM.COLOR_BLACK
	MyTheme.Toast("toastred").BackColor = ABM.COLOR_RED
	
	'tablas
	MyTheme.AddTableTheme ("tablas")
	MyTheme.Table("tablas").ZDepth=ABM.ZDEPTH_1
	MyTheme.Table("tablas").AddCellTheme("headerfooter")
	MyTheme.Table("tablas").Cell("headerfooter").BackColor = ABM.COLOR_BROWN
	MyTheme.Table("tablas").Cell("headerfooter").BackColorIntensity=ABM.INTENSITY_DARKEN4
	MyTheme.Table("tablas").Cell("headerfooter").ForeColor = ABM.COLOR_WHITE
	MyTheme.Table("tablas").Cell("headerfooter").Align=ABM.TABLECELL_HORIZONTALALIGN_CENTER

	MyTheme.Table("tablas").AddCellTheme("celdas")
	MyTheme.Table("tablas").Cell("celdas").BackColor = ABM.COLOR_WHITE
	MyTheme.Table("tablas").Cell("celdas").ActiveBackColor = ABM.COLOR_BROWN
	MyTheme.Table("tablas").Cell("celdas").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN4
	MyTheme.Table("tablas").Cell("celdas").Align = ABM.TABLECELL_HORIZONTALALIGN_CENTER
	MyTheme.Table("tablas").Cell("celdas").BorderColor=ABM.COLOR_BROWN
	MyTheme.Table("tablas").Cell("celdas").BorderColorIntensity=ABM.INTENSITY_DARKEN4
	MyTheme.Table("tablas").Cell("celdas").BorderWidth=1


	
	'mymodal
	MyTheme.AddModalSheetTheme("mypagem")
	MyTheme.ModalSheet("mypagem").ContentBackColor=ABM.COLOR_WHITE
	'Celdas
	MyTheme.AddCellTheme("celda")
	MyTheme.Cell("celda").BorderColor=ABM.COLOR_BROWN
	MyTheme.Cell("celda").Align=ABM.CELL_ALIGN_CENTER
	MyTheme.Cell("celda").BorderWidth=1
	MyTheme.Cell("celda").ZDepth=ABM.ZDEPTH_3
	'Celdas2
	MyTheme.AddCellTheme("celda2")
	MyTheme.Cell("celda2").BackColor=ABM.COLOR_BROWN
	MyTheme.Cell("celda2").BackColorIntensity=ABM.INTENSITY_DARKEN4
	MyTheme.Cell("celda2").Align=ABM.CELL_ALIGN_CENTER
	'MyTheme.Cell("celda2").BorderWidth=2

	'Celdas3
	MyTheme.AddCellTheme("celda3")
	MyTheme.Cell("celda3").Align=ABM.CELL_ALIGN_CENTER
	'celda justi
	MyTheme.AddCellTheme("celda4")
	MyTheme.Cell("celda4").Align=ABM.CELL_ALIGN_CENTER
	MyTheme.Cell("celda4").VerticalAlign=True
	
	'celda justi
	MyTheme.AddCellTheme("celda5")
	MyTheme.Cell("celda5").Align=ABM.CELL_ALIGN_JUSTIFY
	MyTheme.Cell("celda5").VerticalAlign=True
	

	MyTheme.AddLabelTheme("etiq")
	MyTheme.Label("etiq").ForeColor=ABM.COLOR_WHITE
	
	'celdaimg
	MyTheme.AddCellTheme("imgctr")
	MyTheme.Cell("imgctr").Align=ABM.CELL_ALIGN_CENTER
	MyTheme.Cell("imgctr").ZDepth=ABM.ZDEPTH_3
	
	'footehrmodal
	MyTheme.AddContainerTheme("modalfooter")
	MyTheme.Container("modalfooter").BackColor = ABM.COLOR_BROWN
	MyTheme.Container("modalfooter").BackColorIntensity=ABM.INTENSITY_DARKEN4
	
	'divider
	MyTheme.AddDividerTheme("div")
	MyTheme.Divider("div").ForeColor=ABM.COLOR_BROWN
	MyTheme.Divider("div").ForeColorIntensity=ABM.INTENSITY_DARKEN4
	
	'combo
	MyTheme.AddComboTheme("combo")
	MyTheme.Combo("combo").BackColor=ABM.COLOR_WHITE
	MyTheme.Combo("combo").ZDepth=ABM.ZDEPTH_3
	
	
	
	' tema del flotante
	MyTheme.AddButtonTheme("bigbrown")
	MyTheme.Button("bigbrown").BackColor = ABM.COLOR_BROWN
	MyTheme.Button("bigbrown").BackColorIntensity = ABM.INTENSITY_DARKEN4
	
	
	MyTheme.AddButtonTheme("sub1")
	MyTheme.Button("sub1").BackColor = ABM.COLOR_RED
	
	MyTheme.AddButtonTheme("sub2")
	MyTheme.Button("sub2").BackColor = ABM.COLOR_GREEN
	
	MyTheme.AddButtonTheme("sub3")
	MyTheme.Button("sub3").BackColor = ABM.COLOR_YELLOW
	
	MyTheme.AddUploadTheme("upload")
	MyTheme.Upload("upload").ErrorColor = ABM.COLOR_RED
	
	'dates
	MyTheme.AddDateTimePickerTheme("date1")
	MyTheme.DateTimePicker("date1").InputBackColor=ABM.COLOR_WHITE
	MyTheme.DateTimePicker("date1").InputForeColor=ABM.COLOR_BLACK
	MyTheme.DateTimePicker("date1").InputZDepth=ABM.ZDEPTH_3
	MyTheme.DateTimePicker("date1").PickerHeaderDateMonthForeColor=ABM.COLOR_BROWN
	'card theme
	MyTheme.AddContainerTheme("cardtheme")
	MyTheme.Container("cardtheme").BackColor = ABM.COLOR_BROWN
	MyTheme.Container("cardtheme").BackColorIntensity=ABM.INTENSITY_LIGHTEN4
	'card theme2
	MyTheme.AddContainerTheme("cardtheme2")
	MyTheme.Container("cardtheme2").BackColor = ABM.COLOR_WHITE
	MyTheme.Container("cardtheme2").BackColorIntensity = ABM.INTENSITY_LIGHTEN4
	MyTheme.Container("cardtheme2").ZDepth=ABM.ZDEPTH_3
	
	'toast
	MyTheme.AddToastTheme("toast1")
	MyTheme.Toast("toast1").BackColor=ABM.COLOR_LIME
	MyTheme.Toast("toast1").Rounded=True
	MyTheme.Toast("toast1").ForeColor=ABM.COLOR_BLACK
	' calendar
	MyTheme.AddCalendarTheme("calT1")
	MyTheme.Calendar("calT1").ZDepth=ABM.ZDEPTH_3
	'centra celda
	MyTheme.AddCellTheme("celdacenter")
	MyTheme.Cell("celdacenter").Align=ABM.CELL_ALIGN_CENTER
	'radios
	MyTheme.AddRadioGroupTheme("radio")
	MyTheme.RadioGroup("radio").WithGab=False
	MyTheme.RadioGroup("radio").RadioOnColor=ABM.COLOR_LIME
	'check
	MyTheme.AddCheckboxTheme("chk1")
	MyTheme.Checkbox("chk1").ZDepth=ABM.ZDEPTH_3
	'msgbox
	MyTheme.AddMsgBoxTheme("mb")
	MyTheme.MsgBox("mb").ConfirmButtonColor=ABM.COLOR_BROWN
	MyTheme.MsgBox("mb").ConfirmButtonColorIntensity=ABM.INTENSITY_DARKEN4
	MyTheme.MsgBox("mb").ConfirmButtonTextColor=ABM.COLOR_WHITE
	
	'tema de celda titulo
	MyTheme.AddCellTheme("TittleCell")
	MyTheme.Cell("TittleCell").Align=ABM.CELL_ALIGN_CENTER
	MyTheme.Cell("TittleCell").ZDepth=ABM.ZDEPTH_3
	MyTheme.Cell("TittleCell").BackColor=ABM.COLOR_BROWN
	MyTheme.Cell("TittleCell").BackColorIntensity=ABM.INTENSITY_DARKEN4
 	
	'pdf
	MyTheme.AddPDFViewerTheme("pdf1")
	MyTheme.PDFViewer("pdf1").ToolbarColor=ABM.COLOR_BROWN
	MyTheme.PDFViewer("pdf1").ToolbarColorIntensity=ABM.INTENSITY_DARKEN4
	
	
	' the footer theme
'	MyTheme.AddContainerTheme("cardtheme")
'	MyTheme.Container("cardtheme").BackColor = ABM.COLOR_BROWN
'	MyTheme.Container("cardtheme").BackColorIntensity = ABM.INTENSITY_LIGHTEN4
	'theme.Container("cardtheme").ZDepth = ABM.ZDEPTH_5
	' tema del label
	MyTheme.AddLabelTheme("Tittle")
	MyTheme.Label("Tittle").foreColor = ABM.COLOR_WHITE
	
	' tema del label negrita
	MyTheme.AddLabelTheme("subtittle")
	'	theme.Label("subtittle").UseStrikethrough =True
	'	theme.Label("subtittle").StrikethroughColor = ABM.COLOR_BLACK
	MyTheme.Label("subtittle").ZDepth = ABM.ZDEPTH_3
	MyTheme.Label("subtittle").BackColor = ABM.COLOR_BROWN
	MyTheme.Label("subtittle").BackColorIntensity=ABM.INTENSITY_DARKEN4
	MyTheme.Label("subtittle").ForeColor = ABM.COLOR_WHITE
	MyTheme.Label("subtittle").Align = ABM.CELL_ALIGN_CENTER
	
	' another navbar theme
	MyTheme.AddNavigationBarTheme("nav2theme")
	MyTheme.NavigationBar("nav2theme").SideBarWavesEffect = ABM.WAVESEFFECT_NONE
	MyTheme.NavigationBar("nav2theme").SideBarBackColor = ABM.COLOR_BLUEGREY
	MyTheme.NavigationBar("nav2theme").SideBarBackColorIntensity = ABM.INTENSITY_LIGHTEN4
	
	' another navbar theme
	MyTheme.AddNavigationBarTheme("nav3theme")
	MyTheme.NavigationBar("nav3theme").SideBarWavesEffect = ABM.WAVESEFFECT_NONE
	MyTheme.NavigationBar("nav3theme").SideBarBackColor = ABM.COLOR_LIME
	MyTheme.NavigationBar("nav3theme").SideBarBackColorIntensity = ABM.INTENSITY_LIGHTEN4
   
	' another navbar theme
	MyTheme.AddNavigationBarTheme("nav4theme")
	MyTheme.NavigationBar("nav4theme").SideBarWavesEffect = ABM.WAVESEFFECT_NONE
	MyTheme.NavigationBar("nav4theme").SideBarBackColor = ABM.COLOR_TEAL
	MyTheme.NavigationBar("nav4theme").SideBarBackColorIntensity = ABM.INTENSITY_LIGHTEN4
	
End Sub

Sub BuildNavigationBar(page As ABMPage, Title As String, logo As String, ActiveTopReturnName As String, ActiveSideReturnName As String, ActiveSideSubReturnName As String) 	'ignore
	
	
	
	' we have to make an ABMImage from our logo url
	Dim sbtopimg As ABMImage
	sbtopimg.Initialize(page, "sbtopimg", logo, 5)
	sbtopimg.SetFixedSize(236, 56)

	page.NavigationBar.Initialize(page, "nav1", ABM.SIDEBAR_MANUAL_ALWAYSHIDE, Title, True, True, 330, 58, sbtopimg, ABM.COLLAPSE_ACCORDION, "nav1theme")
	'page.NavigationBar.UseTheme("Mytemanav")
	
	
	page.NavigationBar.TopBarDropDownConstrainWidth = False
	page.NavigationBar.ActiveTopReturnName = ActiveTopReturnName
	page.NavigationBar.ActiveSideReturnName = ActiveSideReturnName
	page.NavigationBar.ActiveSideSubReturnName = ActiveSideSubReturnName
	
	' you must add at least ONE dummy item if you want to add items to the topbar	in ConnectNaviagationBar
	page.NavigationBar.AddTopItem("salida", "Salida", "mdi-action-highlight-remove", "", False)
	page.NavigationBar.AddTopItem("idioma", "Idioma", "fa fa-language", "", False)
	
	
	' you must add at least ONE dummy item if you want to add items to the sidebar
	
	page.NavigationBar.AddSideBarItem("miperfil", "Mi Perfil", "", "../miperfil/miperfil.html")
	page.NavigationBar.AddSideBarItem("micarrera", "Mi Carrera", "", "../micarrera/micarrera.html")
	page.NavigationBar.AddSideBarItem("talent", "Atracción de talentos", "", "")
	page.NavigationBar.AddSideBarSubItem("talent","personnelreq","Requisición de personal","","../personnelreq/personnelreq.html")
	page.NavigationBar.AddSideBarSubItem("talent","solicitudpuesto","3.-Solicitud de empleo","","../solicitudpuesto/solicitudpuesto.html")
	page.NavigationBar.AddSideBarsubItem("talent","calendar","4.-Agenda","","../calendar/calendar.html")
	page.NavigationBar.AddSideBarsubItem("talent","referencias","5.-Referencias","","../referencias/referencias.html")
	page.NavigationBar.AddSideBarsubItem("talent","eval5","6.-Entrevista conductual","","../eval5/eval5.html")
	page.NavigationBar.AddSideBarsubItem("talent","antidopinps","7.-Psicometría y Antidopaje","","../antidopinps/antidopinps.html")
	page.NavigationBar.AddSideBarsubItem("talent","summarycandidates","8.-Resumen ","","../summarycandidates/summarycandidates.html")
	page.NavigationBar.AddSideBarsubItem("talent","checklist","9.-Lista de verificación ","","../checklist/checklist.html")
	page.NavigationBar.AddSideBarItem("miequipo", "Mi Equipo", "", "../miequipo/miequipo.html")
	
	page.NavigationBar.AddSideBarItem("jam", "JAM", "", "")
	page.NavigationBar.AddSideBarsubItem("jam","Chat interno","Chat Room","","../chatroom/chatroom.html")
	page.NavigationBar.AddSideBarItem("payroll", "Nómina de sueldos", "", "../PaginaTemplate/PaginaTemplate.html")
	page.NavigationBar.AddSideBarItem("myteam", "Mi equipo", "", "../PaginaTemplate/PaginaTemplate.html")
	
	page.NavigationBar.AddSideBarItem("tad", "Entrenamiento y desarrollo", "", "")
	page.NavigationBar.AddSideBarsubItem("tad","library","Librería","","../library/library.html")
	page.NavigationBar.AddSideBarsubItem("tad","lsm","LSM","","../PaginaTemplate/PaginaTemplate.html")
	page.NavigationBar.AddSideBarsubItem("tad","edl","Enlaces educativos","","../edl2/edl2.html")

	page.NavigationBar.AddSideBarItem("ass", "Evaluación del desempeño", "", "")
	'page.NavigationBar.AddSideBarsubItem("ass","E360","Retroalimentación 360°","","../E360/E360.html")
	page.NavigationBar.AddSideBarsubItem("ass","bienvenida360","Retroalimentación 360°","","../bienvenida360/")
	page.NavigationBar.AddSideBarsubItem("ass","poc","POC","","../POC/POC.html")
	
	page.NavigationBar.AddSideBarItem("kpi", "KPI", "", "")
	page.NavigationBar.AddSideBarsubItem("kpi","trainning","Objetivos","","../indobj/indobj.html")
	page.NavigationBar.AddSideBarsubItem("kpi","trainning","Indicadores por categoría","","../percategory/percategory.html")
	page.NavigationBar.AddSideBarsubItem("kpi","trainning","Indicador de accidente","","../accidentindicators/accidentindicators.html")
	page.NavigationBar.AddSideBarsubItem("kpi","trainning","Ausentismo","","../ausentismo/ausentismo.html")
	page.NavigationBar.AddSideBarsubItem("kpi","trainning","Entrenamiento","","../cpromedio/cpromedio.html")
	page.NavigationBar.AddSideBarsubItem("kpi","trainning","Rotación","","../rotation/rotation.html")
	page.NavigationBar.AddSideBarsubItem("kpi","trainning","Turnover","","../turnover/turnover.html")
	page.NavigationBar.AddSideBarsubItem("kpi","trainning","Costos de compensación","","../PaginaTemplate/PaginaTemplate.html")
	page.NavigationBar.AddSideBarsubItem("kpi","trainning","Justificaciones","","../PaginaTemplate/PaginaTemplate.html")
	page.NavigationBar.AddSideBarsubItem("kpi","trainning","Reclutamiento y selección","","../PaginaTemplate/PaginaTemplate.html")
	page.NavigationBar.AddSideBarsubItem("kpi","trainning","Otros indicadores","","../PaginaTemplate/PaginaTemplate.html")
	
'	Dim extraSideBar As ABMSideBar
'	extraSideBar.Initialize(page, "extrasidebar", 530, 48, 56, Null, ABM.COLLAPSE_ACCORDION, "nav2theme")
'	extraSideBar.AddSideBarComponent("esbHello", BuildSideBarComponent(page, "sidebarcomp", "../images2/16.jpg", "This is a sidebar component", "With an image and an editor"))
'	page.NavigationBar.AddTopItemWithSideBar("SideBar", "", "mdi-action-dashboard", "", False, extraSideBar)

'	Dim extraSideBar2 As ABMSideBar
'	extraSideBar2.Initialize(page, "extrasidebar2", 330, 48,56, Null, ABM.COLLAPSE_ACCORDION, "nav3theme")
'	extraSideBar2.AddSideBarDivider("")
'	extraSideBar2.AddSideBarItem("esbHello2", "Hello Again!", "mdi-action-invert-colors")
'   
'	page.NavigationBar.AddTopItemWithSideBar("SideBar2", "", "mdi-action-dashboard", "", False, extraSideBar2)
'   
'	Dim extraSideBar3 As ABMSideBar
'	extraSideBar3.Initialize(page, "extrasidebar3", 330, 48,56, Null, ABM.COLLAPSE_ACCORDION, "nav4theme")
'	extraSideBar3.AddSideBarDivider("")
'	extraSideBar3.AddSideBarItem("esbHello3", "Hello!", "mdi-action-invert-colors")
'   
'	page.NavigationBar.AddTopItemWithSideBar("SideBar3", "", "mdi-action-dashboard", "", False, extraSideBar3)
	
	'page.NavigationBar.AddSideBarItem("utls", "Utilerias", "", "")
	'page.NavigationBar.AddSideBarsubItem("utls","asignaobj","Maintenance Objetive","","../asignaobj/asignaobj.html")
	'page.NavigationBar.AddSideBarsubItem("utls","evaluacioncompetencias","Evaluation Compte","","../evaluacioncompetencias/evaluacioncompetencias.html")
	'page.NavigationBar.AddSideBarsubItem("utls","objetivosyresultados","Objetive","","../objetivosyresultados/objetivosyresultados.html")
	
	page.NavigationBar.InitializeExtraContent("extracontent", False, "")
	page.NavigationBar.ExtraContent.AddRows(1,False,"").AddCellsOSMP(2,0,0,0,1,1,1,0,0,0,0,"")

	page.NavigationBar.ExtraContent.BuildGrid
	
	
	Dim esp As ABMImage
	esp.Initialize(page,"esp","../images/esp360.png",3)
	esp.SetFixedSize(25,25)
	page.NavigationBar.ExtraContent.Cell(1,1).Addcomponent(esp)
	page.NavigationBar.ExtraContent.Cell(1,1).SetOffsetSize(0,0,10,1,1,1)
	
	Dim ing As ABMImage
	ing.Initialize(page,"ing","../images/ing360.png",3)
	ing.SetFixedSize(25,25)
	page.NavigationBar.ExtraContent.Cell(1,2).Addcomponent(ing)
	page.NavigationBar.ExtraContent.Cell(1,2).SetOffsetSize(0,0,0,1,1,1)
	
	
End Sub

Sub ConnectNavigationBar(page As ABMPage) 'ignore
	' Clear the dummies we created in BuildNavigationBar
	'page.NavigationBar.Clear
	
	'on each top item you can set if it should hide of not on a medium or small device.
	'page.NavigationBar.AddTopItem("Contact", "", "mdi-action-account-circle", "", False)

	'page.NavigationBar.AddSideBarDivider("")
	'page.NavigationBar.AddSideBarItem("About", "About", "mdi-action-dashboard", "../AboutPage/abmaterial-about.html")
	'page.NavigationBar.AddSideBarDivider("")
	

	
		
	page.NavigationBar.Refresh ' IMPORTANT
End Sub

'public Sub BuildParagraph(page As ABMPage, id As String, Text As String) As ABMLabel
'	Dim lbl As ABMLabel	
'	lbl.Initialize(page, id, Text , ABM.SIZE_PARAGRAPH, False, "")
'	Return lbl
'End Sub

public Sub BuildParagraph(page As ABMPage, id As String, Text As String,sizeX As String) As ABMLabel
	Dim lbl As ABMLabel
	lbl.Initialize(page, id, Text , sizeX, False, "")
	Return lbl
End Sub

public Sub BuildParagraphc(page As ABMPage, id As String, Text As String,sizeX As String,color As String) As ABMLabel
	Dim lbl As ABMLabel
	lbl.Initialize(page, id, "{C:"&color&"}"&Text&"{/C}" , sizeX, False, "")
	Return lbl
End Sub

'#795548 Marron
'#000 Negro

public Sub Buildline(page As ABMPage,id As String) As ABMDivider
	Dim Divx As ABMDivider
	Divx.Initialize(page,id,"div")
	Return Divx
End Sub
Sub validatxt ( txt As String) As Boolean
	If txt = "" Then
		Return True
	Else
		Return False
	End If
End Sub

Sub ConvertDate(DateTimeString As String, DateTimeFormat As String, NewDateTimeFormat As String) As String
	Dim rememberMyFormat As String = DateTime.DateFormat
	DateTime.DateFormat = DateTimeFormat
	Dim ticks As Long = DateTime.DateParse(DateTimeString)
	DateTime.DateFormat = NewDateTimeFormat
	Dim newDate As String = DateTime.Date(ticks)
	DateTime.DateFormat = rememberMyFormat
	Return newDate.ToUpperCase
End Sub

Sub Periodo (startD As String , endD As String)As Period
	Dim startDate As Long = DateUtils.SetDateAndTime(startD.SubString2(6,10), startD.SubString2(0,2), startD.SubString2(3,5), startD.SubString2(11,13), startD.SubString2(14,16), 0)
	Dim endDate As Long = DateUtils.SetDateAndTime(endD.SubString2(6,10), endD.SubString2(0,2), endD.SubString2(3,5), endD.SubString2(11,13), endD.SubString2(14,16), 0)
	Dim p As Period = DateUtils.PeriodBetween(startDate, endDate)
	'Dim p As Period = DateUtils.PeriodBetweenInDays(startDate, endDate)
	Return p
End Sub



Sub buildcombo(id As String, icon As String, Title As String, inf As List, page As ABMPage) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
		combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i),page)) ' PARA PODER VER EL VALOR DEL COMBO
	Next
	
	'page.Refresh
	'combo1.Refresh
	Return combo1
	

End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String, page As ABMPage) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub coloreafondo (mypage As ABMPage)
	
	If  Not(Main.fondoe=Null) And Not(Main.fondoe="") And Not(Main.fondoe="null") And Not(Main.fondoe="no") And Not(Main.fondoe="#") And Main.fondoe.Length<10 Then
								
					
		Dim script As String = $"$('body').css('background-color', '${Main.fondoe}');
												     $('body').attr('class','');				 
											"$

							
		mypage.ws.Eval(script,Null)
				
	End If
End Sub

Public Sub GUIDX As String

	Dim joUUID As JavaObject
	Return joUUID.InitializeStatic("java.util.UUID").RunMethod("randomUUID", Null)

End Sub



Sub BuildSideBarComponent(page As ABMPage, id As String, image As String, Title As String, Subtitle As String) As ABMContainer 'ignore
	Dim ItemCont As ABMContainer
	ItemCont.Initialize(page, id, "")
	ItemCont.AddRowsM(1,False,0,0, "").AddCellsOSMP(1,0,0,0,3,3,3,4,0,0,0,"").AddCellsOSMP(1,0,0,0,9,9,9,4,0,0,0,"")
	ItemCont.AddRowsM(1,False,0,0, "").AddCells12(1, "")
	ItemCont.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
   
	Dim SubItemCont As ABMContainer
	SubItemCont.Initialize(page, id & "SubItemCont", "")
	SubItemCont.AddRowsM(1,False, 0,0,"").AddCells12MP(1,-6,0,0,0,"").AddCells12(1,"")
	SubItemCont.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
   
	ItemCont.Cell(1,2).AddComponent(SubItemCont)
   
	Dim img As ABMImage
	img.Initialize(page, id & "img", image, 1)
	img.SetFixedSize(48,48)
	img.IsCircular = True
	img.IsResponsive = True
   
	ItemCont.Cell(1,1).AddComponent(img)
   
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, id & "lbl1", Title, ABM.SIZE_H6, False, "lightblue")
	lbl1.VerticalAlign = True
   
	SubItemCont.Cell(1,1).AddComponent(lbl1)
   
	Dim lbl2 As ABMLabel
	lbl2.Initialize(page, id & "lbl2", Subtitle, ABM.SIZE_H6, False, "")
	lbl2.VerticalAlign = True
   
	SubItemCont.Cell(1,2).AddComponent(lbl2)
   
	Dim editor As ABMEditor
	editor.Initialize(page, "editor", True, True, "editor")
	ItemCont.Cell(2,1).AddComponent(editor)
   
	Return ItemCont
End Sub

