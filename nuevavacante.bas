﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "nuevavacante"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "4"
	' your own variables		
	Dim myToastId As Int = 1
	
	Dim cmbDivision,cmbSubDivision,cmbLocation,cmbAreas,cmbDepartamento,cmbPuesto,cmbcto,cmbTipo,cmbNum,cmbnivel As ABMCombo
	Dim txtcat,txtpto As ABMInput
	Dim txttab1,txttab2,txttab3,txttab4,txttab5 As ABMInput
	Dim myToastId As Int = 1
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
		'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	ConnectPage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------		
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")	
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
End Sub

public Sub BuildTheme()
		' start with the base theme defined in comunes
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
   ' tema del label
    theme.AddLabelTheme("field")
  	theme.Label("field").foreColor = ABM.COLOR_WHITE
	theme.AddCellTheme("TittleCell")
 	theme.Cell("TittleCell").Align=ABM.CELL_ALIGN_CENTER
 	theme.Cell("TittleCell").ZDepth=ABM.ZDEPTH_3
 	theme.Cell("TittleCell").BackColor=ABM.COLOR_BROWN
 	theme.Cell("TittleCell").BackColorIntensity=ABM.INTENSITY_DARKEN4
	'Combos
	theme.AddComboTheme("combo")
	theme.Combo("combo").ZDepth=ABM.ZDEPTH_3
	'blcampo
	theme.AddCellTheme("inpC")
	theme.Cell("inpC").ZDepth=ABM.ZDEPTH_3	
	
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "nuevavacante.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' adding a navigation bar
	ABMShared.BuildNavigationBar(page,"Nueva vacante ","../images/klogo3.png","","nuevavacante","")
'	page.SetBackgroundImage("C:\Desarrollos\Desarrollos 2016\Karisma\B4J\Suite HC\Objects\www\suite\perfiles\fondo.jpg")'valorfondo)
	' create the page grid
	page.AddRows(1,True, "").AddCells12(1,"")
	page.AddRows(3,True, "").AddCells12(4,"")
	page.AddRows(1,True, "").AddCells12(12,"")
	page.AddRows(1,True, "").AddCells12(4,"")
	page.AddRows(1,True, "").AddCells12(4,"")
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
	
	page.AddModalSheetTemplate(Mensajes)
	page.Cell(1,1).AddComponent(ABMShared.BuildParagraph(page,"inst","En esta sección usted puede solicitar un nuevo empleado.",ABM.SIZE_H5))
	Dim lblPais As ABMLabel
	lblPais.Initialize(page,"lblpais","Seleccione la División ",ABM.SIZE_H5,False,"field")
	'Division
	cmbDivision.Initialize(page,"cmbDivision",  "Division",150,"combo")
	cmbDivision.AddItem("P","",BuildSimpleItem("P","",""))
	page.Cell(2,1).SetOffsetSize(0,0,0,12,6,3)
	page.Cell(2,1).AddComponent(cmbDivision)
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_dIVISION order by 1")
	Do While rst.NextRow
		cmbDivision.AddItem("P"& pos,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	'SubDivision	
	cmbSubDivision.Initialize(page,"cmbSubDivision","Seleccione el país",250,"combo")
	page.Cell(2,2).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(2,2).AddComponent(cmbSubDivision)
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_Country order by 1")
	cmbSubDivision.AddItem("P","",BuildSimpleItem("P","",""))
	pos=0
	Do While rst.NextRow
		cmbSubDivision.AddItem("P"& pos,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	'Location
	cmbLocation.Initialize(page,"cmbLocation","Seleccione la locación",250,"combo")
	page.Cell(2,3).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(2,3).AddComponent(cmbLocation)
	'Areas
	cmbAreas.Initialize(page,"cmbAreas","Select Area",250,"combo")
	page.Cell(3,1).SetOffsetSize(0,0,0,12,6,3)
	page.Cell(3,1).AddComponent(cmbAreas)
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_Area order by 1")
	Do While rst.NextRow
		cmbAreas.AddItem("P"& pos,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	'Depatamentos
	cmbDepartamento.Initialize(page,"cmbDepartamento","Seleccione el departamento",250,"combo")
	page.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(3,2).AddComponent(cmbDepartamento)
	rst=sql1.ExecQuery("Select * from RH_Dept")
	pos=0
	cmbDepartamento.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbDepartamento.AddItem( rst.GetString2(0),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
		pos=pos+1
	Loop
	rst.close
	'Puesto
	cmbPuesto.Initialize(page,"cmbPuesto","Seleccione la posición",250,"combo")
	page.Cell(3,3).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(3,3).AddComponent(cmbPuesto)
	'Categoria
	txtcat.Initialize(page,"txtcat",ABM.INPUT_TEXT,"Categoría",False,"")
	txtcat.Enabled=False
	page.Cell(4,1).UseTheme("inpC")
	page.Cell(4,1).SetOffsetSize(0,0,0,12,6,3)
	page.Cell(4,1).AddComponent(txtcat)
	'Centro de costo
	cmbcto.Initialize(page,"cmbcto","Seleccione el centro de costo",250,"combo")
	page.Cell(4,2).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(4,2).AddComponent(cmbcto)
	rst=sql1.ExecQuery("Select * from RH_COSTCENTER")
	pos=0
	cmbcto.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbcto.AddItem( rst.GetString2(0),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
		pos=pos+1
	Loop
	rst.close
	'nivel
	cmbnivel.Initialize(page,"cmbnivel","Nivel",250,"combo")
	page.Cell(4,3).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(4,3).AddComponent(cmbnivel)
	rst=sql1.ExecQuery("Select * from RH_Level")
	pos=0
	cmbnivel.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbnivel.AddItem( "P"&pos,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.close
	'puntos		
	txtpto.Initialize(page,"txtpto",ABM.INPUT_TEXT,"Puntos",False,"")
	txtpto.Enabled=False
	page.Cell(5,1).UseTheme("inpC")
	page.Cell(5,1).SetOffsetSize(0,0,0,12,2,1)
	page.Cell(5,1).AddComponent(txtpto)
	'tabulador
	txttab1.Initialize(page,"txttab1",ABM.INPUT_TEXT,"Mínimo",False,"")
	txttab1.Enabled=True
	page.Cell(5,2).UseTheme("inpC")
	page.Cell(5,2).SetOffsetSize(0,0,1,12,2,1)
	page.Cell(5,2).AddComponent(txttab1)
	
	txttab2.Initialize(page,"txttab2",ABM.INPUT_TEXT,"Q1",False,"")
	txttab2.Enabled=True
	page.Cell(5,3).UseTheme("inpC")
	page.Cell(5,3).SetOffsetSize(0,0,1,12,2,1)
	page.Cell(5,3).AddComponent(txttab2)
	
	txttab3.Initialize(page,"txttab3",ABM.INPUT_TEXT,"promedio",False,"")
	txttab3.Enabled=True
	page.Cell(5,4).UseTheme("inpC")
	page.Cell(5,4).SetOffsetSize(0,0,1,12,2,1)
	page.Cell(5,4).AddComponent(txttab3)
	
	txttab4.Initialize(page,"txttab4",ABM.INPUT_TEXT,"Q3",False,"")
	txttab4.Enabled=True
	page.Cell(5,5).UseTheme("inpC")
	page.Cell(5,5).SetOffsetSize(0,0,1,12,2,1)
	page.Cell(5,5).AddComponent(txttab4)
	
	txttab5.Initialize(page,"txttab5",ABM.INPUT_TEXT,"Máximo",False,"")
	txttab5.Enabled=True
	page.Cell(5,6).UseTheme("inpC")
	page.Cell(5,6).SetOffsetSize(0,0,1,12,2,1)
	page.Cell(5,6).AddComponent(txttab5)

	'Tipo de Contrato	
	cmbTipo.Initialize(page,"cmbtipo",  "Tipo de contrato",150,"combo")
	cmbTipo.AddItem("P","",BuildSimpleItem("P","",""))
	cmbTipo.AddItem("cmbtipo1" , "Temporal",BuildSimpleItem("T1","","TEMPORAL"))
	cmbTipo.AddItem("cmbtipo2" , "Temporal",BuildSimpleItem("T1","","INDETERMINADO"))
	page.Cell(6,1).SetOffsetSize(0,0,0,3,3,3)
	page.Cell(6,1).AddComponent(cmbTipo)
	'cantidad
	cmbNum.Initialize(page,"cmbNum","Seleccione la cantidad de la posición",250,"combo")
	page.Cell(6,2).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(6,2).AddComponent(cmbNum)
	cmbNum.AddItem("P","",BuildSimpleItem("P","",""))
	For i=1 To 5
		cmbNum.AddItem("N" & i, i,BuildSimpleItem("N"&i,"",i))
	Next

	Dim mydate3 As ABMDateTimePicker
	Dim NewDate As Long = DateTime.Now  'Date(DateTime.Now)
	mydate3.Initialize(page, "mydate3", ABM.DATETIMEPICKER_TYPE_DATE, NewDate, "Fecha inicial", "date1")
	mydate3.CancelText = "Cancelar"
	mydate3.PickText = "OK"
	mydate3.TodayText = ""
	mydate3.Language = "en"
	mydate3.ReturnDateFormat = "DD/MM/YYYY" '  // <--- see demo source code For settings
	mydate3.ReturnTimeFormat = ""'"HH:mm"  '      // <--- see demo source code For settings
	mydate3.FirstDayOfWeek = 0
	mydate3.ClickThrough = True ' <--- set To True If, when the user makes a selection, the ok action should be triggered.
	page.Cell(6,3).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(6,3).AddComponent(mydate3)	

	Dim btnAceptar As ABMButton
	btnAceptar.InitializeFlat(page,"btnaceptar","","","        Ok   ","btn")
	page.Cell(7,1).SetOffsetSize(0,1,2,6,2,2)
	page.Cell(7,1).AddComponent(btnAceptar)

	Dim btnCancelar As ABMButton
	btnCancelar.InitializeFlat(page,"btncancelar","","","Cancelar","btn")
	page.Cell(7,2).SetOffsetSize(0,1,2,6,2,2)
	page.Cell(7,2).AddComponent(btnCancelar)
	
	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub

Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then 
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub cmbSubDivision_Clicked(itemId As String)
	Dim cmbx As ABMCombo=page.Component("cmbLocation")
	Dim c1 As ABMCombo=page.Component("cmbSubDivision")
	Dim id1 As String=c1.GetActiveItemId
	Dim lb1 As ABMLabel=c1.GetComponent(id1)
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select distinct nom_propiedad from RH_SubDivision where nom_pais='" & lb1.Text & "'")
	cmbx.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbx.AddItem("P"& pos ,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	cmbx.Refresh
End Sub
Sub cmbDepartamento_Clicked(itemId As String)
	Dim cmbx As ABMCombo=page.Component("cmbPuesto")
	Dim cmby As ABMCombo=page.Component("cmbPuesto")
	cmby.Clear
	cmby.Refresh
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_Positons where Pos_Dep_Key='" & itemId & "' order by 3")
	cmbx.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbx.AddItem("P"& pos & "-" & rst.GetString2(5),rst.GetString2(2),BuildSimpleItem("P"& pos,"",rst.GetString2(2)))
		pos=pos+1
	Loop
	rst.Close
	cmbx.Refresh
End Sub
Sub cmbPuesto_Clicked(itemId As String)
	Dim txtx As ABMInput=page.Component("txtcat")
	Dim strx As String
	Dim inc,fin As Int
	inc=(itemId.IndexOf("-"))+1
	fin=itemId.Length - itemId.IndexOf("-")+2
	strx=itemId.SubString2(inc,fin)
	Dim sqlx As SQL
	Dim rstx  As ResultSet
	sqlx.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rstx=sqlx.ExecQuery("Select * from RH_Category where Cat_key='" & strx & "'")
	Do While rstx.NextRow
		txtx.Text=rstx.GetString2(1)
	Loop
	txtx.Enabled=False
	rstx.close
	txtx.Refresh

End Sub
Sub cmbnivel_Clicked(itemId As String)
	Dim C1 As ABMCombo=page.Component("cmbnivel")
	Dim T1 As String=C1.GetActiveItemId
	Dim L1 As ABMLabel=C1.GetComponent(T1)
	
	Dim txty As ABMInput=page.Component("txtpto")
	Dim sqlx2 As SQL
	Dim rstx2 As ResultSet
	sqlx2.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rstx2=sqlx2.ExecQuery("Select * from RH_Level where nom_level='" & L1.Text & "'")
	Do While rstx2.NextRow
		txty.Text=rstx2.GetString2(1)
	Loop
	txty.Enabled=False
	rstx2.close
	txty.Refresh
	
End Sub
Sub btncancelar_Clicked(Target As String)
		Dim C1 As ABMCombo=page.Component("cmbDivision")
		Dim C2 As ABMCombo=page.Component("cmbSubDivision")
		Dim C3 As ABMCombo=page.Component("cmbLocation")
		Dim C4 As ABMCombo=page.Component("cmbAreas")
		Dim C5 As ABMCombo=page.Component("cmbDepartamento")
		Dim C6 As ABMCombo=page.Component("cmbPuesto")
		Dim C7 As ABMCombo=page.Component("cmbcto")
		Dim C8 As ABMCombo=page.Component("cmbTipo")
		Dim C9 As ABMCombo=page.Component("cmbNum")
		Dim Tx1 As ABMInput =page.Component("txtcat")
		Dim Tx2Niv As ABMCombo =page.Component("cmbnivel")
		Dim Tx3 As ABMInput =page.Component("txtpto")
		Dim Tx4 As ABMInput =page.Component("txttab1")
		Dim Tx5 As ABMInput =page.Component("txttab2")
		Dim Tx6 As ABMInput =page.Component("txttab3")
		Dim Tx7 As ABMInput =page.Component("txttab4")
		Dim Tx8 As ABMInput =page.Component("txttab5")
		C1.SetActiveItemId(0)
		C1.Refresh		
		C2.SetActiveItemId(0)
		C2.Refresh		
		C3.SetActiveItemId(0)
		C3.Refresh		
		C4.SetActiveItemId(0)
		C4.Refresh		
		C5.SetActiveItemId(0)
		C5.Refresh		
		C6.SetActiveItemId(0)
		C6.Refresh		
		C7.SetActiveItemId(0)
		C7.Refresh		
		C8.SetActiveItemId(0)
		C8.Refresh		
		C9.SetActiveItemId(0)
		C9.Refresh		
	
		Tx1.Text=""
		Tx1.Refresh
		
		Tx2Niv.SetActiveItemId(0)
		Tx2Niv.Refresh
		
		Tx3.Text=""
		Tx3.Refresh
		Tx4.Text=""
		Tx4.Refresh
		Tx5.Text=""
		Tx5.Refresh
		Tx6.Text=""
		Tx6.Refresh
		Tx7.Text=""
		Tx7.Refresh
		Tx8.Text=""
		Tx8.Refresh

	
End Sub

Sub btnaceptar_Clicked(Target As String)
		'page.ShowModalSheet("fixedsheet")
		Dim C1 As ABMCombo=page.Component("cmbDivision")
		Dim T1 As String=C1.GetActiveItemId
		Dim L1 As ABMLabel=C1.GetComponent(T1)
		Dim C2 As ABMCombo=page.Component("cmbSubDivision")
		Dim T2 As String=C2.GetActiveItemId
		Dim L2 As ABMLabel=C2.GetComponent(T2)
		Dim C3 As ABMCombo=page.Component("cmbLocation")
		Dim T3 As String=C3.GetActiveItemId
		Dim L3 As ABMLabel=C3.GetComponent(T3)
		Dim C4 As ABMCombo=page.Component("cmbAreas")
		Dim T4 As String=C4.GetActiveItemId
		Dim L4 As ABMLabel=C4.GetComponent(T4)
		Dim C5 As ABMCombo=page.Component("cmbDepartamento")
		Dim T5 As String=C5.GetActiveItemId
		Dim L5 As ABMLabel=C5.GetComponent(T5)
		Dim C6 As ABMCombo=page.Component("cmbPuesto")
		Dim T6 As String=C6.GetActiveItemId
		Dim L6 As ABMLabel=C6.GetComponent(T6)
		Dim C7 As ABMCombo=page.Component("cmbcto")
		Dim T7 As String=C7.GetActiveItemId
		Dim L7 As ABMLabel=C7.GetComponent(T7)
		Dim C8 As ABMCombo=page.Component("cmbTipo")
		Dim T8 As String=C8.GetActiveItemId
		Dim L8 As ABMLabel=C8.GetComponent(T8)
		Dim C9 As ABMCombo=page.Component("cmbNum")
		Dim T9 As String=C9.GetActiveItemId
		Dim L9 As ABMLabel=C9.GetComponent(T9)

		Dim Tx1 As ABMInput =page.Component("txtcat")
		
		
		Dim Tx2C As ABMCombo =page.Component("cmbnivel")
		Dim t10 As String=Tx2C.GetActiveItemId
		Dim L10 As ABMLabel=Tx2C.GetComponent(t10)
		
		Dim Tx3 As ABMInput =page.Component("txtpto")

		Dim Tx4 As ABMInput =page.Component("txttab1")
		Dim Tx5 As ABMInput =page.Component("txttab2")
		
		Dim Tx6 As ABMInput =page.Component("txttab3")
		Dim Tx7 As ABMInput =page.Component("txttab4")
		Dim Tx8 As ABMInput =page.Component("txttab5")

		Dim mydate As ABMDateTimePicker=page.Component("mydate3")
		Log(DateTime.Date(mydate.GetDate))
		
				
		Dim sql1 As SQL

		Dim valores() As Object
		valores=Array As Object  (L1.Text,L2.Text,L3.Text,L4.Text,L5.Text,L6.Text,Tx1.Text,L7.Text,L10.Text,Tx3.Text,Tx4.Text,Tx5.Text,Tx6.Text,Tx7.Text,Tx8.Text,L8.Text,L9.Text,DateTime.Date(mydate.GetDate),DateTime.Date(DateTime.Now),DateTime.time(DateTime.Now),"user")
		
		sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
		sql1.ExecNonQuery2("INSERT INTO RH_NewPosition VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )", valores)
		C1.SetActiveItemId(0)
		C1.Refresh		
		C2.SetActiveItemId(0)
		C2.Refresh		
		C3.SetActiveItemId(0)
		C3.Refresh		
		C4.SetActiveItemId(0)
		C4.Refresh		
		C5.SetActiveItemId(0)
		C5.Refresh		
		C6.SetActiveItemId(0)
		C6.Refresh		
		C7.SetActiveItemId(0)
		C7.Refresh		
		C8.SetActiveItemId(0)
		C8.Refresh		
		C9.SetActiveItemId(0)
		C9.Refresh		
		Tx1.Text=""
		Tx1.Refresh

		Tx3.Text=""
		Tx3.Refresh
		Tx4.Text=""
		Tx4.Refresh
		Tx5.Text=""
		Tx5.Refresh
		Tx6.Text=""
		Tx6.Refresh
		Tx7.Text=""
		Tx7.Refresh
		Tx8.Text=""
		Tx8.Refresh
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
'	If icon <> "" Then
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
'	Else
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int 
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
	     combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next 
	
	'page.Refresh
	'combo1.Refresh	   
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid 
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)	
	Return myModal
End Sub

Sub Page_Ready()
	Log("nuevaposicion!")
	ConnectPage
	page.RestoreNavigationBarPosition
End Sub
