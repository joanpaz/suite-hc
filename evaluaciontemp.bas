﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@

'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "evaluaciontemp"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = ""
	' your own variables
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------
	Log("Connected")
	ws = WebSocket1
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
		' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
		page.Refresh
		page.FinishedLoading
	Else
		If page.WebsocketReconnected Then
			Log("Websocket reconnected")
			' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
			' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
			ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
		Else
			' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
			Log("Websocket first connection")
			page.Prepare
			ConnectPage
		End If
	End If
	Log(ABMPageId)
	'----------------------MODIFICATION-------------------------------
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")
End Sub

Sub Page_ParseEvent(Params As Map)
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)
		End Select
	End If
End Sub

public Sub BuildTheme()
	' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "evaluaciontemp.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
	'page.AlwaysShowVerticalScrollBar = True
	
		
	' adding a navigation bar
	'ABMShared.BuildNavigationBar(page, "Title","../images/logo.png", "", "", "")
			
	' create the page grid
	page.AddRows(5,False,"").AddCells12(1,"")
	page.AddRows(5,False,"").AddCellsOSMP(2,0,0,0,6,6,6,10,0,5,5,"")

	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	
		
End Sub

public Sub ConnectPage()
	'	connecting the navigation bar
	'ABMShared.ConnectNavigationBar(page)
	
	
	
	
	comunicacion ("cntA",6,1,"COMUNICACIÓN","A")
	comunicacion ("cntB",6,2,"PENSAMIENTO ESTRATÉGICO","B")
	comunicacion("cntC",7,1,"ORIENTACIÓN A RESULTADOS","C")
	comunicacion("cntC",7,2,"ACEPTAR EL CAMBIO","D")
	comunicacion("cntC",8,1,"TOMA DE DECISIONES","E")
	comunicacion("cntC",8,2,"LIDERAZGO","F")
	comunicacion("cntC",9,1,"OPERAR CON DISCIPLINA","G")
	comunicacion("cntC",9,2,"AMBIENTE DE TRABAJO","H")
	comunicacion("cntC",9,1,"INNOVAR Y REAPLICAR ","I")
	comunicacion("cntC",9,2,"DESARROLLO DE PERSONAL","J")
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub
Sub comunicacion(conteiner As String,Rx As Int,Cx As Int,titulo As String,archivo As String)
	Dim cntA As ABMContainer
	cntA.Initialize(page,conteiner,"cnt")
	Dim lst As List
	lst.Initialize
	lst.Clear
	lst=File.ReadList(Main.dirfiles,archivo & ".txt")
	
	cntA.AddRows(12,False,"").AddCellsOS(1,0,0,0,5,5,5,"").AddCellsOS(7,0,0,0,1,1,1,"")
	cntA.BuildGrid
	Dim lbltA1 As ABMLabel
	lbltA1.Initialize(page,"lbltA1","{B}" & titulo & "{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,1).SetFixedHeight(50,False)
	cntA.Cell(1,1).UseTheme("celda2")
	cntA.Cell(1,1).AddComponent(lbltA1)
	Dim lbltA2 As ABMLabel
	lbltA2.Initialize(page,"lbltA2","{B}Nunca{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,2).SetFixedHeight(50,False)
	cntA.Cell(1,2).UseTheme("celda2")
	cntA.Cell(1,2).AddComponent(lbltA2)
	Dim lbltA3 As ABMLabel
	lbltA3.Initialize(page,"lbltA3","{B}Rara vez{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,3).SetFixedHeight(50,False)
	cntA.Cell(1,3).UseTheme("celda2")
	cntA.Cell(1,3).AddComponent(lbltA3)
	Dim lbltA4 As ABMLabel
	lbltA4.Initialize(page,"lbltA4","{B}Algunas veces{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,4).SetFixedHeight(50,False)
	cntA.Cell(1,4).UseTheme("celda2")
	cntA.Cell(1,4).AddComponent(lbltA4)
	Dim lbltA5 As ABMLabel
	lbltA5.Initialize(page,"lbltA5","{B}Casi siempre{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,5).SetFixedHeight(50,False)
	cntA.Cell(1,5).UseTheme("celda2")
	cntA.Cell(1,5).AddComponent(lbltA5)
	Dim lbltA6 As ABMLabel
	lbltA6.Initialize(page,"lbltA6","{B}Siempre{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,6).SetFixedHeight(50,False)
	cntA.Cell(1,6).UseTheme("celda2")
	cntA.Cell(1,6).AddComponent(lbltA6)
	Dim lbltA7 As ABMLabel
	lbltA7.Initialize(page,"lbltA7","{B}NS/NC{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,7).SetFixedHeight(50,False)
	cntA.Cell(1,7).UseTheme("celda2")
	cntA.Cell(1,7).AddComponent(lbltA6)
	Dim lbltA8 As ABMLabel
	lbltA8.Initialize(page,"lbltA8","{B}Media{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,8).SetFixedHeight(50,False)
	cntA.Cell(1,8).UseTheme("celda2")
	cntA.Cell(1,8).AddComponent(lbltA8)
	
	For Rn=0 To lst.size-1
		Dim lblcA1 As ABMLabel
		lblcA1.Initialize(page,"lblcA1",lst.Get(Rn),ABM.SIZE_SMALL,False,"")
		cntA.Cell(2+Rn,1).AddComponent(lblcA1)
		cntA.Cell(2+Rn+1,1).AddComponent(ABMShared.Buildline(page,"1"))
		Dim inp1 As ABMInput
		inp1.Initialize(page,"A1",ABM.INPUT_TEXT,"",False,"")
		inp1.Narrow=True
		cntA.Cell(2+Rn,2).UseTheme("celda3")
		cntA.Cell(2+Rn,2).AddComponent(inp1)
		cntA.Cell(2+Rn+1,2).AddComponent(ABMShared.Buildline(page,"1"))
	
		Dim inp2 As ABMInput
		inp2.Initialize(page,archivo&"2"& Rn&"C2",ABM.INPUT_TEXT,"",False,"")
		inp2.Narrow=True
		cntA.Cell(2+Rn,3).UseTheme("celda3")
		cntA.Cell(2+Rn,3).AddComponent(inp2)
		cntA.Cell(2+Rn+1,3).AddComponent(ABMShared.Buildline(page,"1"))

		Dim inp3 As ABMInput
		inp3.Initialize(page,archivo&"3"& Rn&"C3",ABM.INPUT_TEXT,"",False,"")
		inp3.Narrow=True
		cntA.Cell(2+Rn,4).UseTheme("celda3")
		cntA.Cell(2+Rn,4).AddComponent(inp3)
		cntA.Cell(2+Rn+1,4).AddComponent(ABMShared.Buildline(page,"1"))

		Dim inp4 As ABMInput
		inp4.Initialize(page,archivo&"4"& Rn&"C4",ABM.INPUT_TEXT,"",False,"")
		inp4.Narrow=True
		cntA.Cell(2+Rn,5).UseTheme("celda3")
		cntA.Cell(2+Rn,5).AddComponent(inp4)
		cntA.Cell(2+Rn+1,5).AddComponent(ABMShared.Buildline(page,"1"))

		Dim inp5 As ABMInput
		inp5.Initialize(page,archivo&"5"& Rn&"C5",ABM.INPUT_TEXT,"",False,"")
		inp5.Narrow=True
		cntA.Cell(2+Rn,6).UseTheme("celda3")
		cntA.Cell(2+Rn,6).AddComponent(inp5)
		cntA.Cell(2+Rn+1,6).AddComponent(ABMShared.Buildline(page,"1"))

		Dim inp6 As ABMInput
		inp6.Initialize(page,archivo&"6"& Rn&"C6",ABM.INPUT_TEXT,"",False,"")
		inp6.Narrow=True
		cntA.Cell(2+Rn,7).UseTheme("celda3")
		cntA.Cell(2+Rn,7).AddComponent(inp6)
		cntA.Cell(2+Rn+1,7).AddComponent(ABMShared.Buildline(page,"1"))

		
		Dim lblAR As ABMLabel
		lblAR.Initialize(page,"lblAR","0",ABM.SIZE_A,False,"")
		cntA.Cell(2+Rn,8).UseTheme("celda4")
		cntA.Cell(2+Rn,8).AddComponent(lblAR)
		cntA.Cell(2+Rn+1,8).AddComponent(ABMShared.Buildline(page,"1"))
		
	Next
	page.Cell(Rx ,Cx).AddComponent(cntA)
End Sub
Sub A1_Changed(value As String)
	Log("--"&value)
End Sub
Sub A1_LostFocus()
		suma
End Sub
Sub suma 
	Dim cnt As ABMContainer=page.Component("cntA")
	
	
	
End Sub



' clicked on the navigation bar
Sub Page_NavigationbarClicked(Action As String, Value As String)
	' saving the navigation bar position
	page.SaveNavigationBarPosition
	If Action = "LogOff" Then
		ABMShared.LogOff(page)
		Return
	End If

	ABMShared.NavigateToPage(ws, ABMPageId, Value)
End Sub
'	Dim inpR1C1 As ABMInput
'	inpR1C1.Narrow=True
'	inpR1C1.Initialize(page,"inpR1C1",ABM.INPUT_text,"",False,"")
'	cntA.Cell(2,2).AddComponent(inpR1C1)
'	Dim inpR1C2 As ABMInput
'	inpR1C2.Narrow=True
'	inpR1C2.Initialize(page,"inpR1C2",ABM.INPUT_text,"",False,"")
'	cntA.Cell(2,3).AddComponent(inpR1C2)
'	Dim inpR1C3	 As ABMInput
'	inpR1C3.Narrow=True
'	inpR1C3.Initialize(page,"inpR1C3",ABM.INPUT_text,"",False,"")
'	cntA.Cell(2,4).AddComponent(inpR1C3)
'	Dim inpR1C4 As ABMInput
'	inpR1C4.Narrow=True
'	inpR1C4.Initialize(page,"inpR1C4",ABM.INPUT_text,"",False,"")
'	cntA.Cell(2,5).AddComponent(inpR1C4)
'	Dim inpR1C5 As ABMInput
'	inpR1C5.Narrow=True
'	inpR1C5.Initialize(page,"inpR1C5",ABM.INPUT_text,"",False,"")
'	cntA.Cell(2,6).AddComponent(inpR1C5)
'	Dim inpR1C6	 As ABMInput
'	inpR1C6.Narrow=True
'	inpR1C6.Initialize(page,"inpR1C6",ABM.INPUT_text,"",False,"")
'	cntA.Cell(2,7).AddComponent(inpR1C6)

Sub Page_FileUploaded(FileName As String, success As Boolean)
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

