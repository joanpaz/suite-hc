﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "checklist"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "24"
	' your own variables		
	Dim myToastId As Int = 1
	
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	ConnectPage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------		
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")	
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
End Sub

public Sub BuildTheme()
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	theme.AddContainerTheme("conte")
	theme.Container("conte").ZDepth=ABM.ZDEPTH_3
	theme.AddDividerTheme("div1")
	theme.Divider("div1").ForeColor=ABM.COLOR_BLUE
	theme.Divider("div1").ForeColorIntensity=ABM.INTENSITY_LIGHTEN4
	theme.AddImageSliderTheme("sl1")
	theme.ImageSlider("sl1").Height=200
	theme.ImageSlider("sl1").Indicators=False
	theme.ImageSlider("sl1").FullWidth=True
	theme.ImageSlider("sl1").ZDepth=ABM.ZDEPTH_3
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "checklist.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' Añadir Navigation Bar
	ABMShared.BuildNavigationBar(page,"Check List for hiring","../images/klogo3.png","","Check List for hiring","")

	page.AddRows(2,True,"").AddCells12(1,"")
			
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()
	
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
	
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	Dim cmbDivision,cmbSubDivision,cmbLocation,cmbAreas,cmbDepartamento,cmbPuesto,cmbapplicant As ABMCombo

	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	Dim contenedora As ABMContainer	
	contenedora.Initialize(page,"contenedora","cardtheme2")
	contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	contenedora.AddRows(1,True, "").AddCellsOSMP(4,0,0,0,12,6,3,5,0,0,10,"")
	contenedora.AddRows(1,True, "").AddCellsOSMP(4,0,0,0,12,6,3,5,10,0,10,"")
	contenedora.BuildGrid
	page.Cell(1,1).SetOffsetSize(0,0,0,12,11,11)
	page.Cell(1,1).AddComponent(contenedora)
	Dim lbl As ABMLabel
	lbl.Initialize(page,"lbl","Select Applicant",ABM.SIZE_H6,False,"etiq")
	contenedora.Cell(1,1).AddComponent(lbl)
	contenedora.Cell(1,1).UseTheme("celda2")
	'Division
	cmbDivision.Initialize(page,"cmbDivision",  "Division",150,"combo")
	cmbDivision.AddItem("P","",BuildSimpleItem("P","",""))
	contenedora.Cell(2,1).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(2,1).AddComponent(cmbDivision)
	rst=sql1.ExecQuery("Select * from RH_dIVISION order by 1")
	Do While rst.NextRow
		cmbDivision.AddItem(rst.GetString2(0),rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	
	'SubDivision	
	cmbSubDivision.Initialize(page,"cmbSubDivision","Select Country or Division",250,"combo")
	contenedora.Cell(2,2).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(2,2).AddComponent(cmbSubDivision)
	rst=sql1.ExecQuery("Select * from RH_Country order by 1")
	pos=0
	Do While rst.NextRow
		cmbSubDivision.AddItem(rst.GetString2(0),rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	
	'Location
	cmbLocation.Initialize(page,"cmbLocation","Select Location",250,"combo")
	contenedora.Cell(2,3).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(2,3).AddComponent(cmbLocation)
	
	'Areas
	cmbAreas.Initialize(page,"cmbAreas","Select Area",250,"combo")
	contenedora.Cell(2,4).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(2,4).AddComponent(cmbAreas)
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_Area order by 1")
	Do While rst.NextRow
		cmbAreas.AddItem(rst.GetString2(0),rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	
	cmbDepartamento.Initialize(page,"cmbDepartamento","Department",250,"combo")
	contenedora.Cell(3,1).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(3,1).AddComponent(cmbDepartamento)
	rst=sql1.ExecQuery("Select * from RH_Dept")
	pos=0
	Do While rst.NextRow
		cmbDepartamento.AddItem(rst.GetString2(0),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
		pos=pos+1
	Loop
	rst.close
	
	'Puesto
	cmbPuesto.Initialize(page,"cmbVacancy","Select Vacancy",250,"combo")
	contenedora.Cell(3,2).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(3,2).AddComponent(cmbPuesto)
	
	cmbapplicant.Initialize(page,"cmbApplicant","Select Applicant",250,"combo")
	contenedora.Cell(3,3).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(3,3).AddComponent(cmbapplicant)
	
	Dim cnt1 As ABMContainer
	cnt1.Initialize(page,"cnt1","cardtheme2")
	cnt1.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,10,5,0,0,"celda2")
	'cnt1.AddRows(5,False,"").AddCellsOSMP(1,0,0,0,12,3,3,5,0,0,0,"").AddCellsOSMP(1,0,1,1,12,3,3,5,0,0,0,"").AddCellsOSMP(1,0,1,1,12,3,3,5,0,0,0,"")
	cnt1.AddRows(4,True, "").AddCellsOSMP(4,0,0,0,12,6,3,5,0,0,10,"")
	cnt1.AddRows(3,True,"").AddCellsOSMP(3,0,0,0,12,4,4,0,0,0,0,"")
	
	
	cnt1.BuildGrid
	page.Cell(2,1).SetOffsetSize(0,0,0,12,11,11)
	page.Cell(2,1).AddComponent(cnt1)

	Dim lblT1 As ABMLabel
	lblT1.Initialize(page,"lblT1","{B}Documentation{/B}",ABM.SIZE_H6,False,"etiq")
	cnt1.Cell(1,1).UseTheme("celda2")
	cnt1.Cell(1,1).AddComponent(lblT1)

	Dim chk1 As ABMCheckbox	
	chk1.Initialize(page,"chk1","Birth certificate",False,"chk1")
	chk1.IsFilled=True
	cnt1.Cell(2,1).AddComponent(chk1)
	
	Dim chk2 As ABMCheckbox	
	chk2.Initialize(page,"chk2","Marriage certificate",False,"chk1")
	chk2.IsFilled=True
	cnt1.Cell(2,2).AddComponent(chk2)
	
	Dim chk3 As ABMCheckbox	
	chk3.Initialize(page,"chk3","Birth certificate of children",False,"chk1")
	chk3.IsFilled=True
	cnt1.Cell(2,3).AddComponent(chk3)
	
	Dim chk4 As ABMCheckbox	
	chk4.Initialize(page,"chk4","Proof of address",False,"chk1")
	chk4.IsFilled=True
	cnt1.Cell(2,4).AddComponent(chk4)
	
	Dim chk5 As ABMCheckbox	
	chk5.Initialize(page,"chk5","Official ID",False,"chk1")
	chk5.IsFilled=True
	cnt1.Cell(3,1).AddComponent(chk5)
	
	'Dim chk6 As ABMCheckbox	
	'chk6.Initialize(page,"chk6","Marriage certificate",False,"chk1")
	'chk6.IsFilled=True
	'cnt1.Cell(3,2).AddComponent(chk6)
	
	'Dim chk7 As ABMCheckbox	
	'chk7.Initialize(page,"chk7","Birth certificate of children",False,"chk1")
	'chk7.IsFilled=True
	'cnt1.Cell(3,3).AddComponent(chk7)
	
	'Dim chk8 As ABMCheckbox	
	'chk8.Initialize(page,"chk8","Proof of address",False,"chk1")
	'chk8.IsFilled=True
	'cnt1.Cell(4,4).AddComponent(chk8)

	Dim btnSave,btnSend,btnCancel As ABMButton
	btnSave.InitializeFlat(page,"btnSave","","","Save","btn")
	cnt1.Cell(7,1).UseTheme("celdacenter")
	cnt1.Cell(7,1).AddComponent(btnSave)

	btnSend.InitializeFlat(page,"btnSend","","","Send","btn")
	cnt1.Cell(7,2).UseTheme("celdacenter")
	cnt1.Cell(7,2).AddComponent(btnSend)

	btnCancel.InitializeFlat(page,"btnCancel","","","Cancel","btn")
	cnt1.Cell(7,3).UseTheme("celdacenter")
	cnt1.Cell(7,3).AddComponent(btnCancel)
	
	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub

Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then 
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub masuno (idtoast As Int) As Int
		myToastId=myToastId+1
		Return myToastId
End Sub
Sub btnSave_Clicked(Target As String)
		
	
End Sub
Sub btnSend_Clicked(Target As String)
	
End Sub
Sub btnCancel_Clicked(Target As String)
	
End Sub
Sub cmbDepartamento_Clicked(itemId As String)
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	Dim contenedora As ABMContainer = page.Component("contenedora")
	 Dim c1 As ABMCombo = contenedora.cell(2,1).Component("cmbDivision")
	 Dim t1 As String = c1.GetActiveItemId
	 Dim l1 As ABMLabel = c1.GetComponent(t1)
	  If c1.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","You must select the division",3000)
		Return
	  End If
	 Dim c2 As ABMCombo = contenedora.cell(2,2).Component("cmbSubDivision")
	 Dim t2 As String = c2.GetActiveItemId
	 Dim l2 As ABMLabel = c2.GetComponent(t2)
	  If c2.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","You must select the division",3000)
		Return
	  End If 
	 Dim c3 As ABMCombo = contenedora.cell(2,3).Component("cmbLocation")
	 Dim t3 As String = c3.GetActiveItemId
	 Dim l3 As ABMLabel = c3.GetComponent(t3)
	  If c3.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","You must select the location",3000)
		Return
	  End If 
	 Dim c4 As ABMCombo = contenedora.cell(2,4).Component("cmbAreas")
	 Dim t4 As String = c4.GetActiveItemId
	 Dim l4 As ABMLabel = c4.GetComponent(t4)
	 If c4.GetActiveItemId ="-1" Then 
	 	page.ShowToast(masuno(myToastId),"toast1","You must select the division",3000)
		Return
	 End If
	Dim cmbx As ABMCombo=contenedora.Cell(3,2).Component("cmbVacancy")
	Dim cmby As ABMCombo=contenedora.Cell(3,2).Component("cmbVacancy")
	cmby.Clear
	cmby.Refresh
	Dim nom_departamento As String 
	nom_departamento = sql1.ExecQuerySingleResult("Select Dept_Name from RH_Dept where Dept_Key='" & itemId & "' order by 1")
	rst=sql1.ExecQuery("Select * from RH_PersonnelRequisition where nom_division='"&t1&"' and nom_country='"&t2&"' and nom_location='"&t3&"' and nom_area='"&t4&"' and nom_department='" & nom_departamento & "' order by 1")
	Do While rst.NextRow
		cmbx.AddItem(rst.GetString2(0),rst.GetString2(6),BuildSimpleItem("P"& pos,"",rst.GetString2(6)))
		pos=pos+1
	Loop
	rst.Close
	cmbx.Refresh
End Sub
Sub cmbVacancy_Clicked(itemId As String)
	Dim contenedora As ABMContainer = page.Component("contenedora")
	Dim cmby As ABMCombo=contenedora.Cell(3,3).Component("cmbApplicant")
	cmby.Clear
	cmby.Refresh
	Dim sql1, sql2 As SQL
	Dim rst, rst2 As ResultSet
	Dim pos , pos2 As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	sql2.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
'	Dim posname As String
'	posname =sql1.ExecQuerySingleResult("Select nom_position from RH_PersonnelRequisition where id_personnelreq = '"&itemId&"'")
'	Dim poskey As String
'	poskey = sql1.ExecQuerySingleResult("Select Pos_Key from RH_Positons where Pos_Name='" & posname & "' order by 1")
'	rst=sql1.ExecQuery("Select * from RH_Application where Pos_Key='" &poskey& "'")
															
	rst=sql1.ExecQuery("Select * from RH_Application where Pos_Key='" &itemId& "'")	
	Do While rst.NextRow
		rst2 = 	sql2.ExecQuery("Select * from RH_Applicant where id_Applicant='" &rst.GetString2(7)& "'")	
		Do While rst2.NextRow
			Dim Name As String = rst2.GetString2(4)&" "&rst2.GetString2(5)&" "&rst2.GetString2(2)&" "&rst2.GetString2(3)
			 cmby.AddItem(rst2.GetString2(1),Name,BuildSimpleItem("P"& pos,"",Name))
			pos2 = pos2+1	
		Loop
		rst2.Close
		cmby.Refresh
		pos=pos+1
	Loop
	rst.Close
End Sub

Sub cmbSubDivision_Clicked(itemId As String)
		Dim contenedora As ABMContainer = page.Component("contenedora")
	Dim cmbx As ABMCombo=contenedora.Cell(2,3).Component("cmbLocation")
	Dim c1 As ABMCombo=contenedora.Cell(2,2).Component("cmbSubDivision")
	Dim id1 As String=c1.GetActiveItemId
	Dim lb1 As ABMLabel=c1.GetComponent(id1)
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select distinct nom_propiedad from RH_SubDivision where nom_pais='" & lb1.Text & "'")
	cmbx.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbx.AddItem(rst.GetString2(0) ,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	cmbx.Refresh

End Sub


Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
'	If icon <> "" Then
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
'	Else
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int 
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
	     combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next 
	
	'page.Refresh
	'combo1.Refresh	   
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid 
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)	
	Return myModal
End Sub

Sub Page_Ready()
	Log("Lista Check List")
	ConnectPage
	page.RestoreNavigationBarPosition
End Sub

