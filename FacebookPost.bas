﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Public myCard As ABMContainer
	Public myID As String
	Dim ABM As ABMaterial
	Dim myLike As LikeComponent
	Public IsOpen As Boolean
	
	Public photosFolder As String = "/www/" & ABMShared.AppName & "/photos/"
	Public DownloadFolder As String = "/www/" & ABMShared.AppName & "/perfiles/"
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	
End Sub

public Sub Build(page As ABMPage, ID As String) As ABMContainer
	myID = "post" & ID
	myCard.Initialize(page, ID, "zdepth")
	myCard.AddRowsM(1,False,0,0,"").AddCellsOSMP(1,0,0,0,2,2,1,10,0,20,0,"").AddCellsOSMP(1,0,0,0,10,10,11,0,0,50,0,"")
	myCard.AddRowsM(1,True,0,0,"").AddCells12MP(1,0,0,0,0,"")
	myCard.AddRowsM(1,True,0,0,"").AddCells12MP(1,0,0,11,11,"border")
	myCard.AddRowsM(1,True,10,0,"").AddCellsOSMP(1,0,0,0,6,6,6,0,10,0,0,"").AddCellsOSMP(1,0,0,0,6,6,6,0,10,0,0,"right")
	myCard.AddRowsM(1,True,0,0,"").AddCells12MP(1,0,0,0,0,"lightgrey")
	myCard.AddRowsM(1,False,0,10,"").AddCells12MP(1,0,10,0,0,"")
	myCard.BuildGrid
	
	Dim fotouser As String 
	
	If File.Exists(File.Combine(File.DirApp, DownloadFolder),Main.idsesion&"f.jpg") Then
		fotouser = "../perfiles/"&Main.idsesion&"f.jpg"
	Else
		
		If File.Exists(File.Combine(File.DirApp,photosFolder &page.ws.Session.GetAttribute("DBUse") ),page.ws.Session.GetAttribute("idsesion")&".jpg") Then
			fotouser = "../photos/"&page.ws.Session.GetAttribute("DBUse")&"/"&page.ws.Session.GetAttribute("idsesion")&".jpg"
		Else
			fotouser = "../perfiles/user.png"
		End If
	End If
	
	
	Dim sql1 As SQL
	Dim valores(), valores2()  As Object
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	Dim rst As ResultSet
	
	rst = sql1.ExecQuery("Select top 100 * from RH_POST where id='"&ID&"'")
	
	Do While rst.NextRow
		
		Dim Poster As ABMImage											'".jpg?"
		Poster.Initialize(page, ID & "Poster", "../post/" & fotouser & "?" & ABMShared.AppVersion,1)
		Poster.SetFixedSize(100,100)
		myCard.Cell(1,1).AddComponent(Poster)
	
		Dim lblTitle As ABMLabel
		lblTitle.Initialize(page, ID & "Title", "{B}{C:#3658b2}" & rst.GetString("nom_titulo") & "{/C}{/B}{BR}{C:#90949c}" & Rnd(1,60) & " minutos", ABM.SIZE_PARAGRAPH, False, "")
		myCard.Cell(1,2).AddComponent(lblTitle)
	
		Dim lblPost As ABMLabel
		lblPost.Initialize(page, ID & "Post", rst.GetString("nom_post"), ABM.SIZE_PARAGRAPH, False, "")
		myCard.Cell(2,1).AddComponent(lblPost)
	
		myCard.Cell(3,1).AddComponent(CardContent(page, ID, rst.GetString("nom_foto"), "Texto detalle de la foto" ))
		
		
		Dim cont As Int

		cont = sql1.ExecQuerySingleResult("SELECT count(*) FROM RH_REACTIONPOST WHERE id = '"&ID&"'")
		
		myLike.initialize(page, ID & "Like", cont)
		
		myCard.Cell(4,1).AddComponent(myLike.ABMComp)
	
		Dim reactionNumber As Int = Rnd(1,10)
	
		Dim lblReactionNum As ABMLabel
		lblReactionNum.Initialize(page, ID, reactionNumber & " reacciones", ABM.SIZE_PARAGRAPH, False, "")
		lblReactionNum.Clickable = True
		myCard.Cell(4,2).AddArrayComponent(lblReactionNum, "Reactions")
	
		myCard.Cell(5,1).AddComponent(CardReactions(page, ID, reactionNumber))
		
	Loop

	rst.Close
	Return myCard
End Sub

private Sub CardContent(page As ABMPage, ID As String, fotopost As String, textfoto As String) As ABMContainer
	Dim CardCont As ABMContainer
	CardCont.Initialize(page, ID & "Contents", "")
	CardCont.AddRowsM(1, False, 0,0, "").AddCells12(1,"")
	CardCont.AddRowsM(1, False, 0,0, "").AddCells12MP(1,0,0,10,10,"")
	CardCont.AddRowsM(1, False, 0,0, "").AddCells12MP(1,0,0,10,10,"")
	CardCont.BuildGrid
	
	Dim image As ABMImage
	image.Initialize(page, ID & "image", "../post/" & fotopost & "?" & ABMShared.AppVersion,1)
	image.IsResponsive = True
	CardCont.Cell(1,1).AddComponent(image)
	
	Dim imageTitle As ABMLabel
	imageTitle.Initialize(page, ID & "imageTitle", "{B}" & textfoto & "{/B}", ABM.SIZE_H5, False, "")
	imageTitle.IsFlowText = True
	CardCont.Cell(2,1).AddComponent(imageTitle)
	
	Dim divider As ABMDivider
	divider.Initialize(page, ID & "Divider", "divider")
	CardCont.Cell(3,1).AddComponent(divider)
	
	Dim imageBy As ABMLabel
	imageBy.Initialize(page, ID & "imageBy", "{C:#90949c}COPYRIGHT BY | {/C}{C:#5f72c1}" & "Karisma HC" & "{/C}", ABM.SIZE_PARAGRAPH, True, "")
	CardCont.Cell(3,1).AddComponent(imageBy)
	
	Return CardCont
End Sub

private Sub CardReactions(page As ABMPage, ID As String, reactionNumber As Int) As ABMContainer
	Dim CardReacts As ABMContainer
	CardReacts.Initialize(page, ID & "reactions", "")
	CardReacts.AddRowsM(1, False, 0,0, "").AddCells12MP(1,0,0,10,10,"")
	CardReacts.BuildGrid
	CardReacts.SetContentIsOpen(False)
	For i = 1 To reactionNumber
		CardReacts.Cell(1,1).AddComponent(Reaction(page, ID, i))
	Next
	Return CardReacts
End Sub

private Sub Reaction(page As ABMPage, ID As String, counter As Int) As ABMContainer
	Dim React As ABMContainer
	React.Initialize(page, ID & "reaction" & counter, "")
	React.AddRowsM(1, False, 0,0, "").AddCellsOSMP(1,0,0,0,2,2,1,10,0,20,0,"").AddCellsOSMP(1,0,0,0,10,10,11,0,0,50,50,"")
	React.BuildGrid
	
	Dim PosterReact As ABMImage
	PosterReact.Initialize(page, ID & "PosterReact" & counter, "../images/download" & Rnd(1,21) & ".jpg?" & ABMShared.AppVersion,1)
	React.Cell(1,1).AddComponent(PosterReact)
	
	Dim PosterText As ABMLabel
	PosterText.Initialize(page, ID & "PosterText" & counter, "{B}{C:#5f72c1}" & "Nombre usuario sistema" & "{/C}{/B} " & "Comentario usuario sistema", ABM.SIZE_PARAGRAPH, True, "")
	React.Cell(1,2).AddComponent(PosterText)
	
	Return React
End Sub