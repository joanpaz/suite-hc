﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "TimeLinePage"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = ""
	' your own variables
	Private postCounter As Int
	Private Posts As Map
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------
	Log("Connected")
	ws = WebSocket1
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
		' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
		page.Refresh
		page.FinishedLoading
	Else
		If page.WebsocketReconnected Then
			Log("Websocket reconnected")
			' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
			' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
			ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
		Else
			' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
			Log("Websocket first connection")
			page.Prepare
			ConnectPage
		End If
	End If
	Log(ABMPageId)
	'----------------------MODIFICATION-------------------------------
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")
End Sub

Sub Page_ParseEvent(Params As Map)
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)
		End Select
	End If
End Sub

public Sub BuildTheme()
	' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	
	theme.Page.BackColor = ABM.COLOR_BLUEGREY
	theme.Page.BackColorIntensity = ABM.INTENSITY_LIGHTEN5
	
	theme.AddCellTheme("border")
	theme.Cell("border").BorderColor = ABM.COLOR_GREY
	theme.Cell("border").BorderColorIntensity = ABM.INTENSITY_LIGHTEN3
	theme.Cell("border").BorderWidth = 1
	
	theme.AddContainerTheme("zdepth")
	theme.Container("zdepth").BackColor = ABM.COLOR_WHITE
	theme.container("zdepth").ZDepth = ABM.ZDEPTH_1
	
	theme.AddCellTheme("lightgrey")
	theme.Cell("lightgrey").BackColor = ABM.COLOR_GREY
	theme.Cell("lightgrey").BackColorIntensity = ABM.INTENSITY_LIGHTEN5
	
	theme.AddDividerTheme("divider")
	theme.Divider("divider").ForeColor = ABM.COLOR_GREY
	theme.Divider("divider").ForeColorIntensity = ABM.INTENSITY_LIGHTEN3
	
	theme.AddCellTheme("right")
	theme.Cell("right").Align = ABM.CELL_ALIGN_RIGHT
	
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "TimeLinePage.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	'page.ShowConnectedIndicator = True
	'page.AlwaysShowVerticalScrollBar = True
	
	page.AddExtraCSSFile("custom/reaction.css")
	page.AddExtraJavaScriptFile("custom/reaction.js")
	
		
	' adding a navigation bar
	'ABMShared.BuildNavigationBar(page, "Title","../images/logo.png", "", "", "")
			
	' create the page grid
	page.AddRowsM(1,True,0,0, "").AddCellsOSMP(1,0,2,3,12,8,6,20,0,0,0,"")
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	
		
End Sub

public Sub ConnectPage()
	Posts.Initialize
	'	connecting the navigation bar
	'ABMShared.ConnectNavigationBar(page)
	For i = 1 To 3
		addNewCard(False)
	Next
				
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
	
	' add one that will start the NextContents, must be done AFTEr the page has finished loading!
	addNewCard(True)
End Sub

Sub addNewCard(RaiseNext As Boolean)
	Dim NewPost As FacebookPost
	NewPost.Initialize
	postCounter = postCounter + 1
	page.Cell(1,1).AddComponent(NewPost.Build(page, "post" & postCounter))
	
	Posts.put(postCounter, NewPost)
	
	If RaiseNext Then
		page.Cell(1,1).Refresh
		page.RaiseNextContentOnComponent(NewPost.myCard,200)
	End If
End Sub

Sub page_NextContent(TriggerComponent As String)
	addNewCard(True)
End Sub

public Sub LikeComponent_Liked(value As Map)	
	Log(value.Get("like") & " ----> " & value.Get("target"))
End Sub

public Sub LikeComponent_UnLiked(value As Map)
	Log("Unliked ----> " & value.Get("target"))
End Sub

public Sub Reactions_Clicked(Target As String)
	Dim postNum As Int = Target.SubString(13)
	Dim NewPost As FacebookPost = Posts.Get(postNum)
	Dim CardReacts As ABMContainer = NewPost.myCard.Component("post" & postNum & "reactions")
	If NewPost.IsOpen Then
		CardReacts.CloseContent
	Else
		CardReacts.OpenContent
	End If
	NewPost.IsOpen = Not(NewPost.IsOpen)
End Sub

' clicked on the navigation bar
Sub Page_NavigationbarClicked(Action As String, Value As String)
	' saving the navigation bar position
	page.SaveNavigationBarPosition
	If Action = "LogOff" Then
		ABMShared.LogOff(page)
		Return
	End If

	ABMShared.NavigateToPage(ws, ABMPageId, Value)
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

