﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "calendar"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "19"
	' your own variables		
	Dim myToastId As Int = 1
	Dim myToastId As Int = 1
	Dim BackDate1 As Long
	Dim BackDate2 As Long
	Dim BackDate3 As Long
	Dim BackDate4 As Long
	Dim rdbP As ABMRadioGroup
	Dim lstAR As List
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	ConnectPage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------			
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")	
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
End Sub

public Sub BuildTheme()
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	theme.AddContainerTheme("conte")
	theme.Container("conte").ZDepth=ABM.ZDEPTH_3
	theme.AddDividerTheme("div1")
	theme.Divider("div1").ForeColor=ABM.COLOR_BLUE
	theme.Divider("div1").ForeColorIntensity=ABM.INTENSITY_LIGHTEN4
	theme.AddImageSliderTheme("sl1")
	theme.ImageSlider("sl1").Height=200
	theme.ImageSlider("sl1").Indicators=False
	theme.ImageSlider("sl1").FullWidth=True
	theme.ImageSlider("sl1").ZDepth=ABM.ZDEPTH_3
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "calendar.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' Añadir Navigation Bar
	ABMShared.BuildNavigationBar(page,"Mis Actividades","../images/klogo3.png","","calendar","")
	page.AddRows(1,True,"").AddCellsOS(10,0,0,0,12,8,8,"")
	page.AddRows(3,True, "").AddCells12(1,"")
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
	
	lstAR.Initialize
	Dim cal1 As ABMCalendar
	DateTime.DateFormat=("yyyy-MM-dd")
	cal1.Initialize(page, "cal1", DateTime.Date(DateTime.Now), ABM.FIRSTDAYOFWEEK_MONDAY, "en", ABM.CALENDAR_DEFAULTVIEW_WEEK,"calT1")
	cal1.Editable = True
	cal1.MinTime = "07:00"
	cal1.MaxTime = "24:00"
	page.Cell(1,1).AddComponent(cal1)
	Dim cnt1 As ABMContainer
	cnt1.Initialize(page,"cnt1","cardtheme2")
	cnt1.AddRows(1,True,"").AddCells12MP(4,20,-10,0,0,"")
	cnt1.BuildGrid
	page.Cell(2,1).AddComponent(cnt1)
	Dim btnnewapp As ABMButton
	btnnewapp.InitializeFlat(page,"btnnewapp","","","Entrevistas pendientes","btn")
	cnt1.Cell(1,1).UseTheme("celdacenter")
	cnt1.Cell(1,1).SetOffsetSize(0,0,0,12,3,3)
	cnt1.Cell(1,1).AddComponent(btnnewapp)
	Dim btnnewactivity As ABMButton
	btnnewactivity.InitializeFlat(page,"btnnewactivity","","","Nueva Actividad","btn")
	cnt1.Cell(1,2).UseTheme("celdacenter")
	cnt1.Cell(1,2).SetOffsetSize(0,0,0,12,3,3)
	cnt1.Cell(1,2).AddComponent(btnnewactivity)
	
	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub

Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then 
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub btncancel_Clicked(Target As String)
	Dim cntB As ABMContainer=page.Component("cnt1")
	Dim btn1 As ABMButton=cntB.Component("btnnewapp")
	Dim btn2 As ABMButton=cntB.Component("btnnewactivity")
	btn1.Enabled=True
	btn2.Enabled=True	
	btn1.Refresh
	btn2.Refresh
	rdbP.RemoveRadioButton(0)
	rdbP.RemoveRadioButton(0)
	rdbP.RemoveRadioButton(0)

	page.Cell(3,1).RemoveAllComponents
	page.Cell(3,1).Refresh

End Sub
Sub btncancel2_Clicked(Target As String)
	Dim cntB As ABMContainer=page.Component("cnt1")
	Dim btn1 As ABMButton=cntB.Component("btnnewapp")
	Dim btn2 As ABMButton=cntB.Component("btnnewactivity")
	btn1.Enabled=True
	btn2.Enabled=True	
	btn1.Refresh
	btn2.Refresh
	page.Cell(3,1).RemoveAllComponents
	page.Cell(3,1).Refresh
End Sub
Sub crear2
	Dim cntlist As ABMContainer
	cntlist.Initialize(page,"cntlist","cardtheme2")
	cntlist.AddRows(1,True,"").AddCells12MP(1,10,0,0,0,"")
	cntlist.AddRows(5,True,"").AddCells12MP(3,-15,0,0,0,"")
	cntlist.BuildGrid
	page.Cell(3,1).AddComponent(cntlist)
	page.Cell(3,1).SetFixedHeight(1,False)

	Dim tbl1 As ABMTable
	tbl1.Initialize(page,"tbl1",False,False,True,"tablas")
	tbl1.SetHeaders(Array As String("División","Sub-división","Locación","Área","Departamento","Posición","Fecha de captura"))
	tbl1.SetHeaderThemes(Array As String("headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter"))
	tbl1.SetFooter("Tabla informativa", 12,"headerfooter")
	cntlist.Cell(1,1).AddComponent(tbl1)

	Dim lblD2 As ABMLabel
	lblD2.Initialize(page,"lblD2","Fecha inicial",ABM.SIZE_SPAN,False,"etiq")
	cntlist.Cell(2,1).UseTheme("celda2")
	cntlist.Cell(2,1).SetOffsetSize(0,0,0,12,4,4)
	cntlist.Cell(2,1).AddComponent(lblD2)

	Dim NewDate As Long = DateTime.Now  
	Dim fecInc2 As ABMDateTimeScroller
	fecInc2.Initialize(page, "fecinc2", ABM.DATETIMESCROLLER_TYPE_DATETIME, ABM.DATETIMESCROLLER_MODE_MIXED, NewDate, "Seleccione fecha y hora", "")
	fecInc2.DateDayNames = "['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado']"
	fecInc2.DateMonthNamesShort = "['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']"
	fecInc2.TitleDateFormat = "DDD"
	fecInc2.ReturnDateFormat = "mm/dd/yy" '// <--- see demo source code For settings
	fecInc2.ReturnTimeFormat = "HH:ii"  '  // <--- see demo source code For settings  
	fecInc2.DateOrder = "Mddyy"           '// <--- see demo source code For settings
	fecInc2.TimeShowAMPM = False
	fecInc2.CancelText = "Close"
	fecInc2.PickText = "OK"




	cntlist.Cell(3,1).SetOffsetSize(0,0,0,12,4,4)
	cntlist.Cell(3,1).AddComponent(fecInc2)	
	
	Dim lblF2 As ABMLabel
	lblF2.Initialize(page,"lblF2","Fecha final",ABM.SIZE_SPAN,False,"etiq")
	cntlist.Cell(2,2).UseTheme("celda2")
	cntlist.Cell(2,2).SetOffsetSize(0,0,1,12,4,4)
	cntlist.Cell(2,2).AddComponent(lblF2)

	Dim fecfin2 As ABMDateTimeScroller
	fecfin2.Initialize(page, "fecfin2", ABM.DATETIMESCROLLER_TYPE_DATETIME, ABM.DATETIMESCROLLER_MODE_MIXED, NewDate, "Seleccione la fecha y hora", "")
	fecfin2.DateDayNames = "['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado']"
	fecfin2.DateMonthNamesShort = "['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']"
	fecfin2.TitleDateFormat = "DDD"
	fecfin2.ReturnDateFormat = "mm/dd/yy" '// <--- see demo source code For settings
	fecfin2.ReturnTimeFormat = "HH:ii"  '  // <--- see demo source code For settings  
	fecfin2.DateOrder = "Mddyy"           '// <--- see demo source code For settings
	fecfin2.TimeShowAMPM = False
	fecfin2.CancelText = "Close"
	fecfin2.PickText = "OK"
	cntlist.Cell(3,2).SetOffsetSize(0,0,1,12,4,4)
	cntlist.Cell(3,2).AddComponent(fecfin2)
	
	Dim btnsave2 As ABMButton
	btnsave2.InitializeFlat(page,"btnsave2","","","Salvar","btn")
	cntlist.Cell(2,3).UseTheme("celdacenter")
	cntlist.Cell(2,3).SetOffsetSize(0,0,1,12,2,2)
	cntlist.Cell(2,3).AddComponent(btnsave2)

	Dim btncancel2 As ABMButton
	btncancel2.InitializeFlat(page,"btncancel2","","","Cancelar","btn")
	cntlist.Cell(3,3).UseTheme("celdacenter")
	cntlist.Cell(3,3).SetOffsetSize(0,0,1,12,2,2)
	cntlist.Cell(3,3).MarginTop=50
	cntlist.Cell(3,3).AddComponent(btncancel2)

	
End Sub

Sub cargatabla
	Dim cntX As ABMContainer=page.Component("cntlist")
	Dim tblx As ABMTable=cntX.Component("tbl1")
	Dim sql1 As SQL
	Dim rst As ResultSet
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from VW_CITAS")
	Do While rst.NextRow
		Dim valor() As String
		Dim Datex As String
		Datex=rst.GetString("fec_capture")
		Datex=Datex.SubString2(0,10).trim
		Datex=Datex.SubString2(5,7) & "/" & Datex.SubString2(8,10) & "/" & Datex.SubString2(1,4)
		valor=Array As String(rst.GetString(	"nom_division"),rst.GetString("nom_country"),rst.GetString("nom_location"),rst.GetString("nom_area"),rst.GetString("nom_department"),rst.GetString("nom_position"),Datex)
		tblx.AddRow("P" & rst.GetString("id_applicant"),	valor)
	Loop
	
	
End Sub

Sub crear1 
	Dim cntapp As ABMContainer
	cntapp.Initialize(page,"cntapp","cardtheme2")
	cntapp.AddRows(1,True,"").AddCells12MP(1,5,0,0,0,"")
	cntapp.AddRows(5,True,"").AddCells12MP(3,-15,0,0,0,"")
	cntapp.BuildGrid
	page.Cell(3,1).AddComponent(cntapp)
	
	Dim lblTt As ABMLabel
	lblTt.Initialize(page,"lblTt","Nueva Actividad",ABM.SIZE_H6,False,"etiq")
	cntapp.Cell(1,1).UseTheme("celda2")
	cntapp.Cell(1,1).AddComponent(lblTt)

	Dim lblAc As ABMLabel
	lblAc.Initialize(page,"lblAc","Descripción",ABM.SIZE_SPAN,False,"etiq")
	cntapp.Cell(2,1).UseTheme("celda2")
	cntapp.Cell(2,1).SetOffsetSize(0,0,0,12,6,6)
	cntapp.Cell(2,1).AddComponent(lblAc)
	Dim txtNa As ABMInput
	txtNa.Initialize(page,"txtNa",ABM.INPUT_TEXT,"",True,"")
	cntapp.Cell(3,1).SetOffsetSize(0,0,0,12,6,6)
	cntapp.Cell(3,1).AddComponent(txtNa)
	
	Dim lblP As ABMLabel
	lblP.Initialize(page,"lblP","Prioridad",ABM.SIZE_SPAN,False,"etiq")
	cntapp.Cell(2,2).UseTheme("celda2")
	cntapp.Cell(2,2).SetOffsetSize(0,0,1,12,5,5)
	cntapp.Cell(2,2).AddComponent(lblP)
	rdbP.Initialize(page,"rdbP" ,"radio")
	rdbP.AddRadioButton("HIGH",True)
	rdbP.AddRadioButton("NORMAL",True)
	rdbP.AddRadioButton("LOW",True)
	cntapp.Cell(3,2).SetOffsetSize(0,0,1,12,5,5)
	cntapp.Cell(3,2).AddComponent(rdbP)
	
	Dim lblD As ABMLabel
	lblD.Initialize(page,"lblD","Fecha de inicio",ABM.SIZE_SPAN,False,"etiq")
	cntapp.Cell(4,1).UseTheme("celda2")
	cntapp.Cell(4,1).SetOffsetSize(0,0,0,12,4,4)
	cntapp.Cell(4,1).AddComponent(lblD)

	Dim NewDate As Long = DateTime.Now  
	Dim fecInc As ABMDateTimeScroller
	fecInc.Initialize(page, "fecinc", ABM.DATETIMESCROLLER_TYPE_DATETIME, ABM.DATETIMESCROLLER_MODE_MIXED, NewDate, "Seleccione una fecha y hora", "")
	fecInc.DateDayNames = "['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado']"
	fecInc.DateMonthNamesShort = "['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']"
	fecInc.TitleDateFormat = "DDD"
	fecInc.ReturnDateFormat = "mm/dd/yy" '// <--- see demo source code For settings
	fecInc.ReturnTimeFormat = "HH:ii"  '  // <--- see demo source code For settings  
	fecInc.DateOrder = "Mddyy"           '// <--- see demo source code For settings
	fecInc.TimeShowAMPM = False
	fecInc.CancelText = "Close"
	fecInc.PickText = "OK"
	cntapp.Cell(5,1).SetOffsetSize(0,0,0,12,4,4)
	cntapp.Cell(5,1).AddComponent(fecInc)	
	
	Dim lblF As ABMLabel
	lblF.Initialize(page,"lblF","Finish date",ABM.SIZE_SPAN,False,"etiq")
	cntapp.Cell(4,2).UseTheme("celda2")
	cntapp.Cell(4,2).SetOffsetSize(0,0,1,12,4,4)
	cntapp.Cell(4,2).AddComponent(lblF)

	Dim fecfin As ABMDateTimeScroller
	fecfin.Initialize(page, "fecfin", ABM.DATETIMESCROLLER_TYPE_DATETIME, ABM.DATETIMESCROLLER_MODE_MIXED, NewDate, "Seleccione una fecha y hora", "")
	fecfin.DateDayNames = "['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado']"
	fecfin.DateMonthNamesShort = "['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']"
	fecfin.TitleDateFormat = "DDD"
	fecfin.ReturnDateFormat = "mm/dd/yy" '// <--- see demo source code For settings
	fecfin.ReturnTimeFormat = "HH:ii"  '  // <--- see demo source code For settings  
	fecfin.DateOrder = "Mddyy"           '// <--- see demo source code For settings
	fecfin.TimeShowAMPM = False
	fecfin.CancelText = "Close"
	fecfin.PickText = "OK"
	cntapp.Cell(5,2).SetOffsetSize(0,0,1,12,4,4)
	cntapp.Cell(5,2).AddComponent(fecfin)
	
	Dim btnsave As ABMButton
	btnsave.InitializeFlat(page,"btnsave","","","Salvar","btn")
	cntapp.Cell(4,3).UseTheme("celdacenter")
	cntapp.Cell(4,3).SetOffsetSize(0,0,1,12,2,2)
	cntapp.Cell(4,3).AddComponent(btnsave)

	Dim btncancel As ABMButton
	btncancel.InitializeFlat(page,"btncancel","","","Cancelar","btn")
	cntapp.Cell(5,3).UseTheme("celdacenter")
	cntapp.Cell(5,3).SetOffsetSize(0,0,1,12,2,2)
	cntapp.Cell(5,3).MarginTop=50
	cntapp.Cell(5,3).AddComponent(btncancel)
	 page.Cell(3,1).Refresh
End Sub

Sub btnnewapp_Clicked(Target As String)
	
	Dim cntB As ABMContainer=page.Component("cnt1")
	Dim btn1 As ABMButton=cntB.Component("btnnewapp")
	Dim btn2 As ABMButton=cntB.Component("btnnewactivity")
	btn1.Enabled=False
	btn2.Enabled=False	
	btn1.Refresh
	btn2.Refresh
	crear2
	Dim cntM As ABMContainer=page.Component("cntlist")
	cntM.RunAnimation("unblur")
	cargatabla
	page.Cell(3,1).SetFixedHeight(650,False)
	page.Cell(3,1).Refresh
	
End Sub
Sub btnnewactivity_Clicked(Target As String)
	Dim cntB As ABMContainer=page.Component("cnt1")
	Dim btn1 As ABMButton=cntB.Component("btnnewapp")
	Dim btn2 As ABMButton=cntB.Component("btnnewactivity")
	btn1.Enabled=False
	btn2.Enabled=False	
	btn1.Refresh
	btn2.Refresh
	crear1
	page.Cell(3,1).SetFixedHeight(320,False)
	page.Cell(3,1).Refresh
End Sub

Sub fecInc_Changed(dateMilliseconds As String)
	BackDate1 = dateMilliseconds
End Sub
Sub fecfin_Changed(dateMilliseconds As String)
	BackDate2=dateMilliseconds
End Sub
Sub fecInc2_Changed(dateMilliseconds As String)
	BackDate3 = dateMilliseconds
End Sub
Sub fecfin2_Changed(dateMilliseconds As String)
	BackDate4=dateMilliseconds
End Sub

Sub btnsave_Clicked(Target As String)
	'salvar
	DateTime.DateFormat="MM/dd/yyyy"	
	Dim fecha1, fecha2 As String
	fecha1=DateTime.Date(BackDate1) & " " & DateTime.Time(BackDate1)
	fecha2=DateTime.Date(BackDate2) & " " & DateTime.Time(BackDate2)
		
	Log(DateTime.TimeFormat)
	
	Dim cntx1 As ABMContainer=page.Component("cntapp")
	Dim inpT1 As ABMInput=cntx1.Component("txtNa")
	If inpT1.Text="" Then 
		Return
	End If
	If fecha1.Contains("/1969") Then
		'fecha1=DateTime.Date(DateTime.Now) & " " &DateTime.Time(DateTime.Now)		
		page.Msgbox("msg1", "Seleccione otra fecha inicial","Error","ok","")
		Return
	End If

	If fecha2.Contains("/1969") Then
		'fecha2=DateTime.Date(DateTime.Now) & " " &DateTime.Time(DateTime.Now)		
		page.Msgbox("msg1", "Seleccione otra fecha final","Error","ok","")
		Return
	End If

	Dim per As Period=(ABMShared.Periodo(fecha1,fecha2))
	
	Log(per.Minutes)
	
	
	
	Dim sql1 As SQL
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	Dim valores() As String
	valores = Array As String(Main.idsesion,inpT1.Text.ToUpperCase,fecha1,fecha2,rdbP.GetActive,"ACTIVITY","PENDING",inpT1.Text.ToUpperCase)
	sql1.ExecNonQuery2("INSERT INTO RH_Calendar VALUES (?,?,?,?,?,?,?,?)", valores)
	Dim calend As ABMCalendar=page.Component("cal1")
	calend.RefetchData
	inpT1.Text=""
	inpT1.Refresh
	btncancel_Clicked("btncancel")
End Sub

Sub masuno (idtoast As Int) As Int
		myToastId=myToastId+1
		Return myToastId
End Sub

Sub btnsave2_Clicked(Target As String)
	'salvar
	DateTime.DateFormat="MM/dd/yyyy"'DateTime.DeviceDefaultDateFormat
	
	Dim fecha1, fecha2,FechaHora  As String
	fecha1=DateTime.Date(BackDate3) & " " & DateTime.Time(BackDate3)
	fecha2=DateTime.Date(BackDate4) & " " & DateTime.Time(BackDate4)
	
	Log(fecha1)
	If fecha1.Contains("/1969") Then
		fecha1=DateTime.Date(DateTime.Now) & " " &DateTime.Time(DateTime.Now)		
	End If

	If fecha2.Contains("/1969") Then
		fecha2=DateTime.Date(DateTime.Now) & " " &DateTime.Time(DateTime.Now)		
	End If
	
	Dim cntx2 As ABMContainer=page.Component("cntlist")
	Dim tblx As ABMTable=cntx2.Component("tbl1")
	Dim sql1 As SQL
	Dim rstApp As ResultSet
	Dim strCom As StringBuilder
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	Log("Select * from RH_applicant where id_applicant='" & tblx.GetActiveRow.Replace("P","") & "'")
	rstApp=sql1.ExecQuery("Select * from RH_applicant where id_applicant='" & tblx.GetActiveRow.Replace("P","") & "'") 
	Dim nombre As String
	Dim mailapplicant As String
	Do While rstApp.NextRow
		nombre=rstApp.GetString("nom_name") & " " & rstApp.GetString("nom_secondn") & " " & rstApp.GetString("nom_last") & " " &  rstApp.GetString("nom_Secondl")
		mailapplicant=rstApp.GetString("nom_email")
	Loop
	
	If lstAR.Size<1 Then 
		page.ShowToast(masuno(myToastId),"toast1","Select one row",3000)
		Return
	End If
	
	strCom.Initialize
	strCom.Append("¬").Append(nombre)
	strCom.Append("¬").Append(lstAR.Get(0))
	strCom.Append("¬").Append(lstAR.Get(1))
	strCom.Append("¬").Append(lstAR.Get(2))
	strCom.Append("¬").Append(lstAR.Get(3))
	strCom.Append("¬").Append(lstAR.Get(4))
	strCom.Append("¬").Append(lstAR.Get(5))
	Log (strCom.ToString)
	Dim valores() As String
	valores = Array As String(Main.idsesion,"Nueva cita",fecha1,fecha2,0,"APPOINTMENT","PENDING",strCom.ToString)
	sql1.ExecNonQuery2("INSERT INTO RH_Calendar VALUES (?,?,?,?,?,?,?,?)", valores)
	Dim calend As ABMCalendar=page.Component("cal1")
	calend.RefetchData

	DateTime.DateFormat="dd MMMM yyyy" 
	FechaHora=DateTime.Date(BackDate3) & " a las " & DateTime.Time(BackDate4)


	Dim cuerpo() As String 
	Dim subjet As String = "Cita en Capital Humano"
	Dim tittle As String = "Karisma lo espera a usted para entrevista el dia " & FechaHora
	Dim body As String ="Estimado " & nombre &  " Karisma lo espera a usted para entrevista el dia " & FechaHora & " esperamos con su presencia. " & CRLF & "no es necesario contestar este correo"
	Dim img As String ="http://34.193.69.179:8099/suite/images/klogo.png"
	
	cuerpo= Array As String (subjet,tittle,body,img)
	
	Dim destino() As String = Array As String (mailapplicant.ToLowerCase)
	
	CallSubDelayed3(emailutils,"emailsend",destino,cuerpo)

	
	btncancel2_Clicked("btncancel2")
End Sub
Sub tbl1_Clicked(PassedRowsAndColumns As List)
	Dim tblCell As ABMTableCell=PassedRowsAndColumns.Get(0)
	Dim cntx2 As ABMContainer=page.Component("cntlist")
	Dim tblx As ABMTable=cntx2.Component("tbl1")
	Dim c0 As String=tblx.GetString(tblCell.Row,0)
	Dim c1 As String=tblx.GetString(tblCell.Row,1)
	Dim c2 As String=tblx.GetString(tblCell.Row,2)
	Dim c3 As String=tblx.GetString(tblCell.Row,3)
	Dim c4 As String=tblx.GetString(tblCell.Row,4)
	Dim c5 As String=tblx.GetString(tblCell.Row,5)
	lstAR.Clear
	lstAR.Add(c0)
	lstAR.Add(c1)
	lstAR.Add(c2)
	lstAR.Add(c3)
	lstAR.Add(c4)
	lstAR.Add(c5)


End Sub


Sub cal1_FetchData(dateStart As String, dateEnd As String)
	Log("FECH")
	Dim sql2 As SQL
	Dim rst As ResultSet 
	Dim lstT As List
	lstT.Initialize

	sql2.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql2.ExecQuery("Select * from RH_Calendar")
	Do While rst.NextRow
			Dim ev As ABMCalendarEvent
			ev.Initialize(rst.GetInt("id_date"), "CITA",rst.GetString("Fec_start").Replace(" ","T"))
			ev.EndTime=(rst.GetString("Fec_end").Replace(" ","T"))
			If rst.GetInt("st_priority")=0 Then
				ev.BackgroundColor=ABM.COLOR_RED
			else if rst.GetInt("st_priority")=1 Then
				ev.BackgroundColor=ABM.COLOR_YELLOW
				ev.TextColor=ABM.COLOR_BLACK
			Else
				ev.BackgroundColor=ABM.COLOR_GREEN
			End If
			ev.Title=rst.GetString("nom_description")
			lstT.Add(ev)			
	Loop	
	Dim calend As ABMCalendar=page.Component("cal1")
	calend.SetEvents(lstT)
	calend.Refresh
End Sub
Sub cal1_EventStartChanged(params As Map)
	For i = 0 To params.Size - 1
		Log("Key: " & params.GetKeyAt(i) & " Value: " & params.GetValueAt(i))
	Next
	Dim fecha As String
	Dim valor As String= params.Get("newstart")
	
	
	fecha =ABMShared.ConvertDate(valor.Replace("T"," "),"yyyy-MM-dd hh:mm:ss","MM/dd/yyyy hh:mm:ss")
	Log("Nueva " & fecha)
	
	
	CallSubDelayed3( databaseutils,"changestartcal",  params.Get("eventid"),Array As String(params.Get("newstart"),params.Get("newend")))
End Sub
Sub cal1_EventClicked(eventId As String)
	page.AddModalSheetTemplate(DetalleCita(eventId))
	page.Refresh
	page.ShowModalSheet("mycita")
	Log(eventId)	
End Sub
Sub DetalleCita(id As Int) As ABMModalSheet
	Dim myCita As ABMModalSheet
	myCita.Initialize(page, "mycita", True, False, "")
	myCita.Content.UseTheme("mypagem")
	myCita.Footer.UseTheme("modalfooter")
	myCita.IsDismissible = False
	
	Dim sqlC As SQL
	Dim rstC As ResultSet
	sqlC.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rstC=sqlC.ExecQuery("Select * from RH_Calendar where id_date=" & id )
	rstC.NextRow
	Log(rstC.GetString("nom_type"))
	If rstC.GetString("nom_type")="APPOINTMENT" Then
		' create the grid for the content
		myCita.Content.AddRows(3,True, "").AddCells12(1,"")
		myCita.Content.AddRows(2,True, "").AddCells12(3,"")
		myCita.Content.AddRows(4,True, "").AddCells12(3,"")
		myCita.Content.BuildGrid
		Dim lblT As ABMLabel
		lblT.Initialize(page,"lblt","Detail of appointment",ABM.SIZE_H6,False,"etiq")
		myCita.Content.Cell(1,1).UseTheme("celda2")
		myCita.Content.Cell(1,1).AddComponent(lblT)
				
		Dim strVa() As String
		strVa=Regex.Split("¬",rstC.GetString("nom_notes"))
		
		Dim lbl1 As ABMLabel
		lbl1.Initialize(page,"lbl1","Name",ABM.SIZE_H6,False,"etiq")
		myCita.Content.Cell(2,1).UseTheme("celda2")
		myCita.Content.Cell(2,1).AddComponent(lbl1)
		Dim lbl2 As ABMLabel
		lbl2.Initialize(page,"lbl2",strVa(1),ABM.SIZE_H6,False,"")
		myCita.Content.Cell(3,1).UseTheme("celda")
		myCita.Content.Cell(3,1).AddComponent(lbl2)
		
		Dim lbl3 As ABMLabel
		lbl3.Initialize(page,"lbl3","Start date",ABM.SIZE_H6,False,"etiq")
		myCita.Content.Cell(4,1).UseTheme("celda2")
		myCita.Content.Cell(4,1).SetOffsetSize(0,0,0,4,4,4)
		myCita.Content.Cell(4,1).AddComponent(lbl3)
		Dim lbl4 As ABMLabel
		lbl4.Initialize(page,"lbl4",ABMShared.ConvertDate(rstC.GetString("fec_start"),"yyyy-mm-dd HH:mm:ss.0","dd MMM yyyy HH:mm"),ABM.SIZE_H6,False,"")
		myCita.Content.Cell(5,1).UseTheme("celda")
		myCita.Content.Cell(5,1).SetOffsetSize(0,0,0,4,4,4)
		myCita.Content.Cell(5,1).AddComponent(lbl4)
		
		Dim lbl5 As ABMLabel
		lbl5.Initialize(page,"lbl5","End date",ABM.SIZE_H6,False,"etiq")
		myCita.Content.Cell(4,2).UseTheme("celda2")
		myCita.Content.Cell(4,2).SetOffsetSize(0,2,2,4,4,4)
		myCita.Content.Cell(4,2).AddComponent(lbl5)
		Dim lbl6 As ABMLabel
		lbl6.Initialize(page,"lbl6",ABMShared.ConvertDate(rstC.GetString("fec_end"),"yyyy-mm-dd HH:mm:ss.0","dd MMM yyyy HH:mm"),ABM.SIZE_H6,False,"")
		myCita.Content.Cell(5,2).UseTheme("celda")
		myCita.Content.Cell(5,2).SetOffsetSize(0,2,2,4,4,4)
		myCita.Content.Cell(5,2).AddComponent(lbl6)

		Dim lbl7,lbl8,lbl9 As ABMLabel
		lbl7.Initialize(page,"lbl7","Division",ABM.SIZE_H6,False,"etiq")
		myCita.Content.Cell(6,1).UseTheme("celda2")
		myCita.Content.Cell(6,1).SetOffsetSize(0,0,0,3,3,3)
		myCita.Content.Cell(6,1).AddComponent(lbl7)
		
		lbl8.Initialize(page,"lbl8","Sub-division",ABM.SIZE_H6,False,"etiq")
		myCita.Content.Cell(6,2).UseTheme("celda2")
		myCita.Content.Cell(6,2).SetOffsetSize(0,0,1,3,3,3)
		myCita.Content.Cell(6,2).AddComponent(lbl8)

		lbl9.Initialize(page,"lbl9","Location",ABM.SIZE_H6,False,"etiq")
		myCita.Content.Cell(6,3).UseTheme("celda2")
		myCita.Content.Cell(6,3).SetOffsetSize(0,0,1,3,3,3)
		myCita.Content.Cell(6,3).AddComponent(lbl9)

		Dim lbl10,lbl11,lbl12 As ABMLabel
		lbl10.Initialize(page,"lbl10",strVa(2),ABM.SIZE_H6,False,"")
		myCita.Content.Cell(7,1).UseTheme("celda")
		myCita.Content.Cell(7,1).SetOffsetSize(0,0,0,3,3,3)
		myCita.Content.Cell(7,1).AddComponent(lbl10)

		lbl11.Initialize(page,"lbl11",strVa(3),ABM.SIZE_H6,False,"")
		myCita.Content.Cell(7,2).UseTheme("celda")
		myCita.Content.Cell(7,2).SetOffsetSize(0,0,1,3,3,3)
		myCita.Content.Cell(7,2).AddComponent(lbl11)

		lbl12.Initialize(page,"lbl12",strVa(4),ABM.SIZE_H6,False,"")
		myCita.Content.Cell(7,3).UseTheme("celda")
		myCita.Content.Cell(7,3).SetOffsetSize(0,0,1,3,3,3)
		myCita.Content.Cell(7,3).AddComponent(lbl12)

		Dim lbl13,lbl14,lbl15 As ABMLabel
		lbl13.Initialize(page,"lbl13","Area",ABM.SIZE_H6,False,"etiq")
		myCita.Content.Cell(8,1).UseTheme("celda2")
		myCita.Content.Cell(8,1).SetOffsetSize(0,0,0,3,3,3)
		myCita.Content.Cell(8,1).AddComponent(lbl13)
		
		lbl14.Initialize(page,"lbl14","Department",ABM.SIZE_H6,False,"etiq")
		myCita.Content.Cell(8,2).UseTheme("celda2")
		myCita.Content.Cell(8,2).SetOffsetSize(0,0,1,3,3,3)
		myCita.Content.Cell(8,2).AddComponent(lbl14)

		lbl15.Initialize(page,"lbl15","Position",ABM.SIZE_H6,False,"etiq")
		myCita.Content.Cell(8,3).UseTheme("celda2")
		myCita.Content.Cell(8,3).SetOffsetSize(0,0,1,3,3,3)
		myCita.Content.Cell(8,3).AddComponent(lbl15)

		Dim lbl16,lbl17,lbl18 As ABMLabel
		lbl16.Initialize(page,"lbl16",strVa(5),ABM.SIZE_H6,False,"")
		myCita.Content.Cell(9,1).UseTheme("celda")
		myCita.Content.Cell(9,1).SetOffsetSize(0,0,0,3,3,3)
		myCita.Content.Cell(9,1).AddComponent(lbl16)

		lbl17.Initialize(page,"lbl17",strVa(6),ABM.SIZE_H6,False,"")
		myCita.Content.Cell(9,2).UseTheme("celda")
		myCita.Content.Cell(9,2).SetOffsetSize(0,0,1,3,3,3)
		myCita.Content.Cell(9,2).AddComponent(lbl17)

		lbl18.Initialize(page,"lbl18",strVa(7),ABM.SIZE_H6,False,"")
		myCita.Content.Cell(9,3).UseTheme("celda")
		myCita.Content.Cell(9,3).SetOffsetSize(0,0,1,3,3,3)
		myCita.Content.Cell(9,3).AddComponent(lbl18)




'¬Joan Alexander Paz Acero¬DWT¬México¬AZUL BEACH¬F&B¬AYB¬ASISTENTE

	Else	
		
		
	End If
	
	
	
	myCita.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,6,6,6,3,3,3,"").AddCellsOS(1,0,0,0,3,3,3, "")
	myCita.Footer.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	Dim btn2 As ABMButton
	btn2.InitializeFlat(page, "btn2", "", "", "Close", "btn")
	myCita.Footer.Cell(1,2).SetOffsetSize(0,0,1,1,1,1)
	myCita.Footer.Cell(1,2).AddComponent(btn2)
	
	
	Return myCita
	
End Sub
Sub btn2_Clicked(Target As String)
		page.CloseModalSheet("mycita")
End Sub

Sub cal1_EventEndChanged(eventId As String, NewEnd As String)
	Log("end")
	CallSubDelayed3(databaseutils,"changeEndcal",eventId,NewEnd)
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
'	If icon <> "" Then
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
'	Else
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int 
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
	     combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next 
	
	'page.Refresh
	'combo1.Refresh	   
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid 
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)	
	Return myModal
End Sub

Sub Page_Ready()
	Log("Ready Calendar")
	ConnectPage
	page.RestoreNavigationBarPosition
End Sub
