﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'I have just created an excel importer for my ABMaterial App and just decided that I should ADOPT this class to meet my ExcelImporter code for my app so that I can share it in the forum
'My database information is saved as JSON maps inside a table. This eliminates the need to have a million columns for my tables.
'So one defines the class properties, these gets saved in the same structure as read from my db and then the code is generated.
'The assumption is that you have a table that stores your templates so that your app reads the structure from there.
Sub Class_Globals
	Private fx As JFX
	Private TemplateName As String
	Private TargetTable As String
	Private WorksheetName As String
	Private StartingRow As String
	Private EndingRow As String
	Private ClearTargetTable As String
	Private RefreshPage As String
	Private Columns As List
	Private TemplateTable As String
	Private TemplateWorkSheetName As String
	Private TemplateStartingRow As String
	Private TemplateEndingRow As String
End Sub

'Initializes the object. This is to set the default parameters
'Step 1
'sTemplateName: the name of the template
'sTargetTable: the target table to save the records to
'iStartingRow: the starting row of the excel workbook
'iEndingRow: the ending row of the excel workbook
'bClearTargetTable: the target table should be cleared
'bRefreshPage: refresh the ABMPage, I use this when I have a table to display the imported data in my page
Public Sub Initialize(sTemplateName As String,sTargetTable As String,sWorksheetName As String,iStartingRow As Int, iEndingRow As Int, bClearTargetTable As Boolean, bRefreshPage As Boolean)
	TemplateName = sTemplateName
	TargetTable = sTargetTable
	WorksheetName = sWorksheetName
	StartingRow = iStartingRow
	EndingRow = iEndingRow
	ClearTargetTable = iif(bClearTargetTable=True,1,0)
	RefreshPage = iif(bRefreshPage=True,1,0)
	Columns.Initialize 
End Sub


Sub iif(Text As String, Text1 As String, Text2 As String) As String
	If Text = True Then Return Text1 Else Return Text2
End Sub

'set the table that holds the templates and the fields holding this information
'Step 2
'sTemplateTable: the name of the database table storing the templates
'sTemplateWorkSheetName: the field that stores the worksheet name
'sTemplateStartingRow: the field that stores the starting row
'sTemplateEndingRow: the field that stores the ending row, 
public Sub SetTemplate(sTemplateTable As String, sTemplateWorkSheetName As String, sTemplateStartingRow As String, sTemplateEndingRow As String)
	TemplateTable = sTemplateTable
	TemplateWorkSheetName = sTemplateWorkSheetName
	TemplateStartingRow = sTemplateStartingRow
	TemplateEndingRow = sTemplateEndingRow
End Sub

'add a column to import, for each column to import add a column
'sfieldname: the target field name to save the data on the target table
'ssourcefieldname: the field name in the templates table that stores the column position
'srowposition: the row position where to get the content, leave blank for all rows to import
'scolumnposition: the column to import from A - AZ
public Sub AddColumn(sfieldname As String, ssourcefieldname As String, srowposition As String, scolumnposition As String)
	Dim m As Map
	m.Initialize 
	m.Put("fieldname", sfieldname)
	m.Put("sourcefieldname",ssourcefieldname)
	m.Put("rowposition", srowposition)
	m.Put("columnposition", scolumnposition)
	Columns.Add(m)
End Sub

'Step 4
Sub Build(dir As String, fil As String)
	Dim m As Map
	m.Initialize
	m.Put("templatename", TemplateName)
	m.Put("targettable", TargetTable)
	m.Put("worksheetname", WorksheetName)
	m.Put("startingrow", StartingRow)
	m.Put("endingrow", EndingRow)
	m.Put("templatetable", TemplateTable)
	m.Put("templateworksheetname", TemplateWorkSheetName)
	m.Put("templatestartingrow", TemplateStartingRow)
	m.Put("templateendingrow", TemplateEndingRow)
	
	'the adoption, this is not necessary 
	Dim json As String = Map2Json(m)
	Dim extras As String = List2Json(Columns)
	Dim mm As Map
	mm.Initialize 
	mm.Put("componentproperties", json)
	mm.Put("extras", extras)
	Dim code As String = ExcelImporterCode(mm)
	File.WriteString(dir,fil,code)
	OpenFile(File.Combine(dir,fil))
End Sub


Sub OpenFile(sPath As String)
	If File.Exists("",sPath) = True Then
		fx.ShowExternalDocument(File.GetUri("",sPath))
	End If
End Sub


'convert a map to json
Sub Map2Json(m As Map) As String
	Dim gen As JSONGenerator
	Dim outJSON As String
	gen.Initialize(m)
	outJSON = gen.ToString
	Return outJSON
End Sub

'convert a list to json
Sub List2Json(lst As List) As String
	Dim sOut As String
	Dim jsonGen As JSONGenerator
	jsonGen.Initialize2(lst)
	sOut = jsonGen.ToString
	Return sOut
End Sub

'convert json to a list
Sub Json2List(strValue As String) As List
	Dim lst As List
	lst.Initialize
	If strValue.Length = 0 Then
		Return lst
	End If
	Try
		Dim parser As JSONParser
		parser.Initialize(strValue)
		Return parser.NextArray
	Catch
		Return lst
	End Try
End Sub

'convert json2map
Sub Json2Map(jsonText As String) As Map
	Dim json As JSONParser
	Dim Map1 As Map
	Map1.Initialize
	Try
		If jsonText.length > 0 Then
			json.Initialize(jsonText)
			Map1 = json.NextObject
		End If
		Return Map1
	Catch
		Return Map1
	End Try
End Sub


Sub AddComment(sb As StringBuilder, comment As String)
	sb.Append("'").Append(comment).Append(CRLF)
End Sub

'create the importer code needed
private Sub ExcelImporterCode(comp As Map) As String
	Dim props As Map
	Dim propsJSON As String
	
	' read the contents
	propsJSON = comp.Get("componentproperties")
	Dim extras As String = comp.GetDefault("extras","")
	props = Json2Map(propsJSON)
	TemplateName = props.Getdefault("templatename","")
	TargetTable = props.Getdefault("targettable","")
	WorksheetName = props.Getdefault("worksheetname","")
	StartingRow = props.Getdefault("startingrow","")
	EndingRow = props.Getdefault("endingrow","")
	TemplateTable = props.getdefault("templatetable","")
	TemplateWorkSheetName  = props.getdefault("templateworksheetname","")
	TemplateStartingRow  = props.getdefault("templatestartingrow","")
	TemplateEndingRow  = props.getdefault("templateendingrow","")
	RefreshPage = props.getdefault("refreshpage","0")
	ClearTargetTable = props.GetDefault("cleartargettable","0")
	
	Dim sb As String = $"Sub ExcelImport${TargetTable}(xlsFileName As String)
	'get the iym template to use
    Dim cboTemplate As ABMCombo = page.Component("cboTemplate")
    Dim tempID As Int = cboTemplate.GetActiveItemId
    If tempID <= 0 Then
        myToastId = myToastId + 1
        page.ShowToast("toast" & myToastId, "toastred", "The ${TemplateName} should be selected first!", 3000)
        Return
    End If
    If File.Exists("",xlsFileName) = False Then Return


    Dim jsql As SQL = ABMShared.sqlget
	'read the template from file and set the needed properties
    Dim temp As Map = ABMShared.SQLRecordRead(jsql,"${TemplateTable}","id",tempID)
    If temp.IsInitialized = False Then
        myToastId = myToastId + 1
        page.ShowToast("toast" & myToastId, "toastred", "The ${TemplateName} could not be read, please correct this!", 3000)
        Return
    End If
	Dim worksheetname As String = temp.GetDefault("${TemplateWorkSheetName}","${WorksheetName}")
	Dim startrow As Long = temp.GetDefault("${TemplateStartingRow}",${StartingRow})
	startrow = startrow - 1"$
	Dim sbAll As StringBuilder
	sbAll.initialize
	sbAll.append(sb).append(CRLF)
	If EndingRow.length > 0 Then
		Dim sb As String = $"Dim endrow As Long = temp.GetDefault("${TemplateEndingRow}",${EndingRow})"$
		sbAll.append(sb).Append(CRLF)
	Else
		Dim sb As String = $"Dim endrow As Long = -1"$
		sbAll.append(sb).append(CRLF)
	End If
	Dim sb As String = $"'start processing the workbook
    Dim wb As PoiWorkbook
	'initialize an existing workbook
    wb.InitializeExisting("",xlsFileName,"")
	'get the worksheet names
    Dim wsl As List = wb.GetSheetNames
	'find the summary work sheet position
    Dim summaryPos As Int = wsl.IndexOf(worksheetname)
    If summaryPos = -1 Then Return
	'get the workbook
    Dim wSheet As PoiSheet = wb.GetSheet(summaryPos)
	Dim rCnt As Long
	Dim Records as List
	Records.initialize"$
	sbAll.append(sb).append(CRLF)
	'addchildcomponents
	If extras.Length > 0 Then
		Dim ExtrasList As List = Json2List(extras)
		AddComment(sbAll,"Get the row positions for the fields")
		For Each fld As Map In ExtrasList
			Dim rowposition As String = fld.GetDefault("rowposition","")
			Dim SourceFieldName As String = fld.GetDefault("sourcefieldname","")
			If rowposition.Length > 0 Then
				Dim sb As String = $"Dim ${SourceFieldName}Row As Int = temp.GetDefault("${SourceFieldName.tolowercase}","${rowposition}")"$
				sbAll.append(sb).Append(CRLF)
			End If
		Next
		AddComment(sbAll,"Get the column names (A-Z) for the fields")
		'process the columns
		For Each fld As Map In ExtrasList
			Dim columnposition As String = fld.GetDefault("columnposition","")
			Dim SourceFieldName As String = fld.GetDefault("sourcefieldname","")
			Dim sb As String = $"Dim ${SourceFieldName}Col As String = temp.GetDefault("${SourceFieldName.tolowercase}","${columnposition}")"$
			sbAll.append(sb).Append(CRLF)
		Next
		AddComment(sbAll,"Get the column positions (1..n) for the fields")
		'process the columns
		For Each fld As Map In ExtrasList
			Dim SourceFieldName As String = fld.GetDefault("sourcefieldname","")
			Dim sb As String = $"Dim ${SourceFieldName}ColPos As Int = ABMShared.GetColumnPos(${SourceFieldName}Col)"$
			sbAll.append(sb).Append(CRLF)
		Next
		'get any row columns provided
		For Each fld As Map In ExtrasList
			Dim SourceFieldName As String = fld.GetDefault("sourcefieldname","")
			Dim rowposition As String = fld.GetDefault("rowposition","")
			Dim columnposition As String = fld.GetDefault("columnposition","")
			If rowposition.length > 0 And columnposition.length > 0 Then
				Dim sb As String = $"Dim ${SourceFieldName} As String = ABMShared.GetRowCellValue(wSheet,${SourceFieldName}Row, ${SourceFieldName}Col)"$
				sbAll.append(sb).Append(CRLF)
			End If
		Next
		Dim sb As String = $"For rowCnt = startrow To endrow"$
		sbAll.append(sb).Append(CRLF)
		For Each fld As Map In ExtrasList
			Dim SourceFieldName As String = fld.GetDefault("sourcefieldname","")
			Dim rowposition As String = fld.GetDefault("rowposition","")
			Dim columnposition As String = fld.GetDefault("columnposition","")
			If rowposition.length = 0 And columnposition.length > 0 Then
				Dim sb As String = $"Dim ${SourceFieldName} As String = ABMShared.GetRowCellValue(wSheet,rowCnt, ${SourceFieldName}Col)"$
				sbAll.append(sb).Append(CRLF)
			End If
		Next
		AddComment(sbAll,"Create map To hold the record To add")
		Dim sb As String = $"Dim record As Map
		record.initialize"$
		sbAll.append(sb).append(CRLF)
		For Each fld As Map In ExtrasList
			Dim fieldname As String = fld.GetDefault("fieldname","")
			Dim SourceFieldName As String = fld.GetDefault("sourcefieldname","")
			Dim rowposition As String = fld.GetDefault("rowposition","")
			Dim columnposition As String = fld.GetDefault("columnposition","")
			If rowposition.length = 0 And columnposition.length > 0 Then
				Dim sb As String = $"record.put("${fieldname.tolowercase}",${SourceFieldName})"$
				sbAll.append(sb).Append(CRLF)
			End If
		Next
		Dim sb As String = $"Records.Add(record)
		Next"$
		sbAll.append(sb).Append(CRLF)
	End If
	
	Dim sb As String = $"'Open the connection to the database
	Dim jsql As SQL = ABMShared.sqlget"$
	If ClearTargetTable = "1" Then
		AddComment(sbAll,"Clear all records in target table")
		Dim sb As String = $"ABMShared.SQLClearTable(jsql,"${TargetTable}","id",True)"$
		sbAll.append(sb).append(CRLF)
	End If
	AddComment(sbAll,"Add all records to the table")
	Dim sb As String = $"ABMShared.SQLRecordInsertMaps(jSQL,"${TargetTable}",Records)
	ABMShared.SQLClose(jsql)
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastgreen", "${TemplateName} data imported successfully!", 3000)"$
	sbAll.append(sb).append(CRLF)
	If RefreshPage = "1" Then
		Dim sb As String = $"ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)"$
		sbAll.append(sb).Append(CRLF)
	End If
	sbAll.append("End Sub").append(CRLF)
	Return sbAll.tostring
End Sub