﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "idioma360"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "40"
	' your own variables
	Dim myToastId As Int = 1
	
	Dim langshow As Boolean = False
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------
	Log("Connected")
	ws = WebSocket1
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
		' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
		page.Refresh
		page.FinishedLoading
	Else
		If page.WebsocketReconnected Then
			Log("Websocket reconnected")
			' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
			' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
			ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
		Else
			' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
			Log("Websocket first connection")
			page.Prepare
			ConnectPage
		End If
	End If
	Log(ABMPageId)
	'----------------------MODIFICATION-------------------------------
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")
End Sub

Sub Page_ParseEvent(Params As Map)
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)
		End Select
	End If
End Sub

public Sub BuildTheme()
	' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	theme.AddPDFViewerTheme("pdf")
	theme.PDFViewer("pdf").ToolbarColor=ABM.COLOR_BROWN
	theme.PDFViewer("pdf").ToolbarColorIntensity=ABM.INTENSITY_DARKEN4

	' add additional themes specific for this page
	
	theme.AddMsgBoxTheme("image")
	theme.MsgBox("image").ConfirmButtonColor = ABM.COLOR_TEAL
	theme.MsgBox("image").ConfirmButtonColorIntensity = ABM.INTENSITY_DARKEN1
	theme.MsgBox("image").CancelButtonColor = ABM.COLOR_TEAL
	theme.MsgBox("image").CancelButtonColorIntensity = ABM.INTENSITY_DARKEN1
	theme.MsgBox("image").SetImage("../images/idiomas5.jpg", 200, 200)
	theme.MsgBox("image").BackColor = ABM.COLOR_WHITE
	'theme.MsgBox("image").BackColorIntensity = ABM.INTENSITY_LIGHTEN5
	
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "index.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' adding a navigation bar
	ABMShared.BuildNavigationBar(page,"Idiomas 360°","../images/klogo3.png","","idioma360","")
	
			
	' create the page grid
	page.AddRows(1,True, "").AddCells12(1,"")
	page.AddRows(1,False,"").AddCellsOSMP(1,0,1,1,12,2,2,0,0,0,0,"").AddCellsOSMP(1,0,0,0,12,6,6,0,0,50,0,"").AddCellsOSMP(1,0,0,0,12,2,2,0,0,50,0,"")

	page.AddRows(1,True, "").AddCellsOSMP(1,0,0,0,12,12,12,-20,0,0,0,"")
	page.AddRows(5,True, "").AddCells12(12,"")

	'	page.AddRows(4,True, "").AddCellsOS(5,0,0,0,12,2,2,"")
	'	page.AddRows(1,True, "").AddCells12(1,"")
	'	page.AddRows(1,True, "").AddCells12(1,"")
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
	
	page.AddModalSheetTemplate(idiomas)
	
	Dim imglogo As ABMImage
	imglogo.Initialize(page,"iml","../images/feedback.png",3)
	'page.Cell(2,1).AddComponent(imglogo)
	'page.Cell(2,1).UseTheme("imgctr")
	
	Dim cntt2 As ABMContainer
	cntt2.Initialize(page,"cntt2","")
	cntt2.AddRows(4,False,"").AddCells12(1,"")
	cntt2.BuildGrid
	page.Cell(2,2).AddComponent(cntt2)
	

		
	cntt2.Cell(2,1).AddComponent(ABMShared.BuildParagraphc(page,"e","{B}Seleccione el Idioma / Select language{B}" ,ABM.SIZE_H4,"#795548"))
	cntt2.Cell(2,1).PaddingLeft = "17%"
	
	
	
	
	
	
	Dim cntt1 As ABMContainer
	cntt1.Initialize(page,"cntt2","")
	cntt1.AddRows(3,True,"").AddCellsOSMP(3,0,0,0,12,4,4,0,0,10,10,"")
	cntt1.BuildGrid
	page.Cell(3,1).AddComponent(cntt1)
	
			
	Dim presentacion As ABMImage
	presentacion.Initialize(page,"presentacion","../images/presentacion.png",3)
	cntt1.Cell(1,2).AddComponent(presentacion)
	cntt1.Cell(1,2).SetOffsetSize(0,0,4,4,4,4)
	
	Dim pizarraesp As ABMImage
	pizarraesp.Initialize(page,"pizarraesp","../images/esp2.jpg",3)
	pizarraesp.SetFixedSize(250,150)
	cntt1.Cell(2,1).AddComponent(pizarraesp)

	Dim pizarraing As ABMImage
	pizarraing.Initialize(page,"pizarraing","../images/ing.jpg",3)
	pizarraing.SetFixedSize(250,150)
	cntt1.Cell(2,3).AddComponent(pizarraing)
	cntt1.Cell(2,3).SetOffsetSize(0,0,4,4,4,4)
	
	
	Dim anterior As ABMButton
	anterior.InitializeFlat(page,"anterior","fa fa-arrow-circle-left",ABM.ICONALIGN_LEFT,"Anterior","btngreen")
	page.Cell(5,1).AddComponent(anterior)
	page.Cell(5,1).SetOffsetSize(0,0,4,3,3,3)
	
	Dim siguiente As ABMButton
	siguiente.InitializeFlat(page,"siguiente","fa fa-arrow-circle-right",ABM.ICONALIGN_RIGHT,"Siguiente","btngreen")
	page.Cell(5,2).AddComponent(siguiente)
	page.Cell(5,2).SetOffsetSize(0,0,0,3,3,3)
	page.Cell(5,2).MarginLeft = "-10%"
		
	
	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
	
	Dim script As String = $"
		$('button').css('border-radius', '10px');
			 							"$
	
	page.ws.Eval(script,Null)
End Sub

Sub pasos_Clicked(Target As String)
	
	
	Log(Target)
	
	
End Sub



Sub botones_divisiones
	page.pause
	
	Dim sql1 As SQL
	Dim text As ABMLabel
	Dim cursor As ResultSet
	Dim pos , i , j, cant As Int=0
	
	Dim cont1 As ABMContainer
	cont1.Initialize(page,"cont1","")
	
	
	
	
	
	sql1.Initialize(Main.strDrv,Main.strUrl)
	
	cant =  sql1.ExecQuerySingleResult("SELECT count(*) FROM [karismaHC].[dbo].[RH_Division]")
	
	If cant > 3 Then
		
		If ((cant Mod 3)>0) Then
			cant =Round(cant / 3)+ 1
		Else
			cant =Round(cant / 3)
		End If
		
	
		cont1.AddRows(cant,True,"").AddCellsOSMP(1,0,0,0,12,6,3,0,0,0,0,"").AddCellsOSMP(2,0,0,1,12,6,3,0,0,0,0,"")
		cont1.BuildGrid
	
	Else
		cant = 3
		cont1.AddRows(cant,True,"").AddCellsOSMP(1,0,0,0,12,6,6,0,0,0,0,"")
		cont1.BuildGrid
				
	End If
	
	
	cursor = sql1.ExecQuery("SELECT * FROM [karismaHC].[dbo].[RH_Division]")
	
	i = 1
	j = 1
	
	Do While cursor.NextRow
	
		If i > cant Then
			i = 1
			j = j + 1
		End If
		
		
		
		'Dim button As ABMButton
		'button.InitializeRaised(page,cursor.GetString2(0),"","",cursor.GetString2(0),"btngreen")
		''button.Size = ABM.ICONSIZE_LARGE
		''button.UseFullCellWidth = True
		
		Dim divcontainer As ABMContainer
		divcontainer.Initialize(page,cursor.GetString2(0),"")
		divcontainer.SetBackgroundImage(ABM.COLOR_WHITE,ABM.INTENSITY_LIGHTEN1,"../images/div1.png",ABM.CONTAINERIMAGE_REPEAT_NOREPEAT,ABM.CONTAINERIMAGE_POSITION_CENTERCENTER)
		
		
		
		cont1.Cell(i,j).AddArrayComponent(divcontainer,"divs")
	
		
		i= 1 + i
	Loop
	
	cursor.Close
	
	page.Cell(3,1).AddComponent(cont1)
	page.Cell(3,1).SetOffsetSize(0,0,2,12,10,10)
	page.Cell(3,1).MarginTop = "5%"

	
	page.Refresh
	
	Dim script As String = $"
	$('button').css('border-radius', '20px');
		 							"$
	page.ws.Eval(script,Null)
	
	
	page.Resume
End Sub

Sub divs_Clicked(Target As String)
	
	
	page.Msgbox2("idioma", "Seleccione el Idioma / Select language", "", "Español","English",False, ABM.MSGBOX_TYPE_WARNING, "image")
		
	

	'page.ShowModalSheet("idiomas")
	
	Log(Target)
	
End Sub


Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub manual_Clicked(Target As String)
	Dim pdf As ABMPDFViewer
	pdf.Initialize(page, "pdf", 800, "../files/360.pdf","pdf")
	pdf.PreparePrintingText = "Preparing to print..."
	pdf.ReadDirection = ABM.PDF_READDIRECTION_LTR
	pdf.AllowDownload = True
	pdf.AllowOpen = False
	pdf.AllowPrint = True
	pdf.AllowSideBar = True
	pdf.AllowViewBookmark = False
	page.Cell(7,1).PaddingTop = "5%"
	page.Cell(7,1).AddComponent(pdf)
	page.Refresh
End Sub

Sub resumen_Clicked(Target As String)
	ABMShared.NavigateToPage(page.ws, page.GetPageID, "../resumen360/resumen360.html")
End Sub

Sub auto_Clicked(Target As String)
	ABMShared.NavigateToPage(page.ws, page.GetPageID, "../evaluacion360/evaluacion360.html")
End Sub

Sub periodos_Clicked(Target As String)
	ABMShared.NavigateToPage(page.ws, page.GetPageID, "../periodos360/")
End Sub

Sub objetivos_Clicked(Target As String)
	ABMShared.NavigateToPage(page.ws, page.GetPageID, "../generaretro/generaretro.html")
End Sub

Sub resultados_Clicked(Target As String)
	ABMShared.NavigateToPage(page.ws, page.GetPageID, "../resultados360/resultados360.html")
End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	If Action = "idioma" Then
		
		If Not(langshow) Then
			page.NavigationBar.OpenExtraContent
			langshow = True
		Else
			langshow = False
			page.NavigationBar.CloseExtraContent
		End If
	
		Return
	End If
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
	'	If icon <> "" Then
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	'	Else
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
		combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next
	
	'page.Refresh
	'combo1.Refresh
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)
	Return myModal
End Sub

Sub idiomas() As ABMModalSheet
	
	
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "idiomas", True, ABM.MODALSHEET_TYPE_TOP, "")
	myModal.Content.UseTheme("")
	''myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = True
	myModal.Content.AddRows(1,True, "").AddCellsOSMP(1,0,0,1,12,12,12,20,0,60,0,"")
	myModal.Content.AddRows(1,True, "").AddCellsOSMP(2,0,0,0,12,12,12,20,0,0,0,"")

	myModal.Content.BuildGrid
	
	
	myModal.Content.Cell(1,1).AddComponent(ABMShared.BuildParagraphc(page,"e","{B}Seleccione el Idioma / Select language{/B} " ,ABM.SIZE_H4,"#795548"))
	
	Dim esp As ABMImage
	esp.Initialize(page,"esp","../images/esp.jpg",1.0)
	esp.Caption="Español"
	esp.SetFixedSize(100,100)
	esp.Tag=1
	esp.IsCircular = False
	esp.IsClickable=True
	''esp.IsMaterialBoxed=True
	esp.IsResponsive=True
	myModal.Content.Cell(2,1).AddArrayComponent(esp,"idioma")
	myModal.Content.Cell(2,1).SetOffsetSize(0,0,1,12,3,3)
	
	Dim ing As ABMImage
	ing.Initialize(page,"ing","../images/ing.jpg",1.0)
	ing.Caption="English"
	ing.SetFixedSize(100,100)
	ing.Tag=1
	ing.IsCircular = False
	ing.IsClickable=True
	''ing.IsMaterialBoxed=True
	ing.IsResponsive=True
	myModal.Content.Cell(2,2).AddArrayComponent(ing,"idioma")
	myModal.Content.Cell(2,2).SetOffsetSize(0,0,1,12,3,3)
	
	myModal.Refresh
	page.Refresh
	
	Dim script As String = $"
		$('div').css('border-radius', '20px');
		$('button').css('border-radius', '20px');
			 							"$
	
	page.ws.Eval(script,Null)
	
	Return myModal
	
End Sub

Sub idioma_Clicked(Target As String)
	
	Log(Target)
	ABMShared.NavigateToPage(page.ws, page.GetPageID, "../generaretro/generaretro.html")
End Sub

Sub page_MsgboxResult(returnName As String, result As String)
	
	If result = "abmok" Then
		Main.generaretro_lang = "esp"
	Else
		Main.generaretro_lang = "ing"
	End If
	
	
	Log(returnName)
	Log(result)
	ABMShared.NavigateToPage(page.ws, page.GetPageID, "../generaretro/generaretro.html")
End Sub

