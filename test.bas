﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "test"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "25"
	' your own variables		
	Dim myToastId As Int = 1
	
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	ConnectPage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------	
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")	
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
End Sub

public Sub BuildTheme()
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	theme.AddContainerTheme("conte")
	theme.Container("conte").ZDepth=ABM.ZDEPTH_3
	theme.AddDividerTheme("div1")
	theme.Divider("div1").ForeColor=ABM.COLOR_BLUE
	theme.Divider("div1").ForeColorIntensity=ABM.INTENSITY_LIGHTEN4
	theme.AddImageSliderTheme("sl1")
	theme.ImageSlider("sl1").Height=200
	theme.ImageSlider("sl1").Indicators=False
	theme.ImageSlider("sl1").FullWidth=True
	theme.ImageSlider("sl1").ZDepth=ABM.ZDEPTH_3
	
	'Combos
	theme.AddComboTheme("combo")
	theme.Combo("combo").ZDepth=ABM.ZDEPTH_3
	
	
	
		'label
	theme.AddlabelTheme("label1")
	theme.Label("label1").ForeColor = ABM.COLOR_WHITE	
	
		' add additional themes specific for this page
	theme.AddTableTheme("tbl1theme")
	theme.Table("tbl1theme").ZDepth = ABM.ZDEPTH_1
	
	theme.Table("tbl1theme").AddCellTheme("headerfooter")
	theme.Table("tbl1theme").Cell("headerfooter").BackColor = ABM.COLOR_BROWN
	theme.Table("tbl1theme").Cell("headerfooter").ActiveBackColorIntensity = ABM.INTENSITY_DARKEN4
	theme.Table("tbl1theme").Cell("headerfooter").ForeColor = ABM.COLOR_WHITE	
	theme.Table("tbl1theme").Cell("headerfooter").Align = ABM.TABLECELL_HORIZONTALALIGN_CENTER
	
	
	theme.Table("tbl1theme").AddCellTheme("headerfooterright")
	theme.Table("tbl1theme").Cell("headerfooterright").BackColor = ABM.COLOR_BLUEGREY
	theme.Table("tbl1theme").Cell("headerfooterright").ForeColor = ABM.COLOR_WHITE
	theme.Table("tbl1theme").Cell("headerfooterright").Align = ABM.TABLECELL_HORIZONTALALIGN_RIGHT
	
	theme.Table("tbl1theme").AddCellTheme("nocolor")
	theme.Table("tbl1theme").Cell("nocolor").ActiveBackColor = ABM.COLOR_BLUEGREY
	theme.Table("tbl1theme").Cell("nocolor").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN2
	
	theme.Table("tbl1theme").AddCellTheme("nocoloredit")
	theme.Table("tbl1theme").Cell("nocoloredit").ActiveBackColor = ABM.COLOR_BLUEGREY
	theme.Table("tbl1theme").Cell("nocoloredit").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN2
	theme.Table("tbl1theme").Cell("nocoloredit").IsEditable = True
	theme.Table("tbl1theme").Cell("nocoloredit").InputMask = "'alias':'decimal', 'digits': 2"
	theme.Table("tbl1theme").Cell("nocoloredit").AllowEnterKey = False
	
	theme.Table("tbl1theme").AddCellTheme("positive")
	theme.Table("tbl1theme").Cell("positive").BackColor = ABM.COLOR_GREEN
	theme.Table("tbl1theme").Cell("positive").BackColorIntensity = ABM.INTENSITY_LIGHTEN3
	theme.Table("tbl1theme").Cell("positive").ActiveBackColor = ABM.COLOR_GREEN
	theme.Table("tbl1theme").Cell("positive").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN1
	theme.Table("tbl1theme").Cell("positive").Align = ABM.TABLECELL_HORIZONTALALIGN_RIGHT
	
	theme.Table("tbl1theme").AddCellTheme("negative")
	theme.Table("tbl1theme").Cell("negative").BackColor = ABM.COLOR_RED
	theme.Table("tbl1theme").Cell("negative").BackColorIntensity = ABM.INTENSITY_LIGHTEN3
	theme.Table("tbl1theme").Cell("negative").ActiveBackColor = ABM.COLOR_RED
	theme.Table("tbl1theme").Cell("negative").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN1
	theme.Table("tbl1theme").Cell("negative").Align = ABM.TABLECELL_HORIZONTALALIGN_RIGHT
	
	theme.Table("tbl1theme").AddCellTheme("aligntop")
	theme.Table("tbl1theme").Cell("aligntop").VerticalAlign = ABM.TABLECELL_VERTICALALIGN_TOP
	theme.Table("tbl1theme").Cell("aligntop").BorderColor = ABM.COLOR_BLACK
	theme.Table("tbl1theme").Cell("aligntop").BorderWidth = 1
	'theme.Table("tbl1theme").Cell("aligntop").ZDepth = ABM.ZDEPTH_1
	theme.Table("tbl1theme").Cell("aligntop").Align = ABM.TABLECELL_HORIZONTALALIGN_CENTER
	
		'tema de celda titulo
	theme.AddCellTheme("titulo1")
 	theme.Cell("titulo1").Align=ABM.CELL_ALIGN_CENTER
 	theme.Cell("titulo1").BackColor=ABM.COLOR_BROWN
 	theme.Cell("titulo1").BackColorIntensity=ABM.INTENSITY_LIGHTEN1
	
	'tema de celda titulo
	theme.AddInputTheme("input1")
	theme.Input("input1").FocusForeColor = ABM.COLOR_WHITE
	theme.Input("input1").InputColor = ABM.COLOR_black
	theme.Input("input1").ValidColor = ABM.COLOR_WHITE
	theme.Input("input1").InvalidColor = ABM.COLOR_WHITE 
	theme.Input("input1").AutoCompleteBackColor = ABM.COLOR_WHITE 
	theme.Input("input1").AutoCompleteHoverBackColor = ABM.COLOR_WHITE 
 	'theme.Input("input1").   '=ABM.INPUT_TEXTALIGN_CENTER
 	'theme.Input("input1").
 	'theme.Input("input1").BackColorIntensity=ABM.INTENSITY_LIGHTEN1
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "test.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' adding a navigation bar
	
	' Añadir Navigation Bar
	ABMShared.BuildNavigationBar(page,"test","../images/klogo3.png","","test","")

	page.AddRows(16,True,"").AddCells12(2,"")
	
	
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()
	
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
	
	page.AddModalSheetTemplate(Mensajes)
		

			Dim correo As ABMButton
			correo.InitializeFlat(page,"correo","","","Correo","")

			page.Cell(1,1).AddComponent(correo)




'	' TABLA PARA PROBAR ERROR
'	
'	Dim tbl2 As ABMTable
'	tbl2.InitializeScrollable(page, "tbl2", False, False, False, Array As Int(300, 150,250,250,250,150), "tbl1theme")
'	tbl2.SetHeaders(Array As String("OBJETIVES TO BE EVALUATED", "POINTS %", "HOW TO BE MEASURED? (Cuantitative data)", "PERFORMANCE SELF-EVALUATION","(Supervisor) ENTER COMMENTS ON RESULTS"," LETTER GRADE PER SCALE" ))
'	tbl2.SetHeaderThemes(Array As String("headerfooter", "headerfooter", "headerfooter", "headerfooter", "headerfooter","headerfooter"))
'	
'		tbl2.SetFooter("Totales.", 12,"headerfooter")
'	
'	' HABILITAR PARA PRUEBA
'		page.Cell(2,1).SetFixedHeight(1000,False)
'		'page.Cell(2,1).AddComponent(tbl2)
'		
'		
'		
'		'page.Cell(1,1).MarginTop = "5%"
'	
'	
'	Dim r As List
'	Dim rCellThemes As List
'		
'		r.Initialize
'		rCellThemes.Initialize
'	
'	
'			Dim inputp As ABMInput
'		
'			inputp.Initialize(page,"inputp",ABM.INPUT_TEXT,"INGRESE TEXTO PRUEBA",True, "")
'			
'			
'			Dim  pruebacont As ABMContainer
'			pruebacont.Initialize(page,"pruebacont","")
'			pruebacont.AddRows(1,False,"").AddCells12(1,"")
'			pruebacont.BuildGrid
'			
'			'pruebacont.Cell(1,1).AddArrayComponent(inputp,"inputp1")
'			
'			
'			r.Add(inputp)
'			rCellThemes.Add("aligntop")	
'				
'			
'			
'			tbl2.AddRow("uid1", r)
'			tbl2.SetRowThemes(rCellThemes)
'			tbl2.Refresh
'			
'	
'		    Dim prueba As ABMButton
'			prueba.InitializeFlat(page,"prueba","","","JALAR","")
'			
'			
'				' HABILITAR PARA PRUEBA
'			page.Cell(3,1).AddComponent(prueba)
'	
'	
'	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub
Sub correo_Clicked(Target As String)
	Log("correo")	
	'Dim  user = "suitehc.karisma@gmail.com", password = "karisma$" As String
	'Dim smtpServer = "smtp.gmail.com" As String
	'Dim smtpPort = 465 As Int
	'Dim SMTP As SMTP
	
	Dim cuerpo() As String 
	Dim subjet As String = "Cita en Capital Humano"
	Dim tittle As String = "Karisma lo espera a usted para entrevista el dia " 
	Dim body As String ="Estimado " &   " Karisma lo espera a usted para entrevista el dia " & " esperamos con su presencia. " & CRLF & "no es necesario contestar este correo"
	Dim img As String ="http://34.193.69.179:8099/suite/images/klogo.png" 
	
	
	Dim destino As String="ricardo_tm@hotmail.com"
	 Dim  user = "HCsupport@karismahotels.com", password = "6Cjs4z4C" As String
	 Dim smtpServer = "smtp.office365.com" As String
	 Dim smtpPort As Int
	 Dim SMTP As SMTP
	smtpPort = 587
	
	SMTP.AuthMethod=SMTP.AUTH_LOGIN
	'SMTP.UseSSL=True
	SMTP.StartTLSMode=True
	Try
	 SMTP.Initialize(smtpServer, smtpPort, user, password,"smtp")
	 SMTP.UseSSL = True
	 
	 'Destino="joanpaz@gmail.com"
	 
	 SMTP.To.Add(destino)
	 
	 SMTP.Subject="Subject mio"
	cuerpo= Array As String (subjet,tittle,body,img)

	 
	 SMTP.HtmlBody = True
		 SMTP.Body=$"<html>
		<body>
		<img src=\"${cuerpo(3)} alt=\"Karisma><br>
		<b>${cuerpo(1)}</b><br>
		<font color=\"black>${cuerpo(2)}</font>
		</body>
		</html>"$
	 SMTP.Send
	 Log("ENVIADO")
	Catch

	Log("ERROR AL ENVIAR EL CORREO")
	End Try
	 
		 
	
	
	
	
End Sub

Sub SMTP_MessageSent(Success As Boolean)
	If Success Then Log("salio")
End Sub

Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then 
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub prueba_Clicked(Target As String)
	
	'Dim container As ABMContainer = page.Component("tbl2")
	
	'Dim tx As ABMInput = container.getcomponent("inputp")
	
	'Dim containera As ABMContainer = container.Component("tbl2_0_0__")
	
	'Log (tx)
	
	'Dim tbl As ABMTable = page.Component("tbl2")
	
'	Dim pruebacont As ABMContainer = tbl.GetComponent("0","pruebacont") 'tbl2_0_0__
	
	'Dim tx As ABMInput = tbl.GetComponent("0","inputp")
	
	
	'Dim test As String = tx.Text
	
	'Log(test)
	
	Dim valores(), valores2()  As Object
	Dim sql1 , sql2 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	
	sql2.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
	
	'page.ShowLoader
	sql2.ExecNonQuery("EXEC [GIN]." & ws.Session.GetAttribute("DBUse") & ".dbo.PT_POR_CATEGORIA '456465', 'epaz7', '2017-01-01', '2017-01-12' ")
	
	
	
	Dim consulta As String = "Select * FROM OPENQUERY(GIN, 'select * from " & ws.Session.GetAttribute("DBUse") & ".dbo.vw_PT_POR_CATEGORIAepaz7')"
	
	rst= sql2.ExecQuery(consulta)
	
	
	
	Do While rst.NextRow
		Dim a As String = rst.GetString2(1)		
		Dim b As String = rst.GetString2(2)
		Dim c As String = rst.GetString2(3)
		Dim d As String = rst.GetString2(4)	
			
			Log(a&""&b&""&c&""&d)
			
	Loop	


	Log("paso")
	
	
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
'	If icon <> "" Then
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
'	Else
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int 
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
	     combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next 
	
	'page.Refresh
	'combo1.Refresh	   
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid 
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)	
	Return myModal
End Sub

Sub Page_Ready()
	Log("Test ready")
	ConnectPage
	page.RestoreNavigationBarPosition
End Sub