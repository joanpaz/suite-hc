﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "altapersonal"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = ""
	' your own variables
	Dim myToastId As Int = 1
	
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------
	Log("Connected")
	ws = WebSocket1
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
		' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
		page.Refresh
		page.FinishedLoading
	Else
		If page.WebsocketReconnected Then
			Log("Websocket reconnected")
			' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
			' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
			ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
		Else
			' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
			Log("Websocket first connection")
			page.Prepare
			ConnectPage
		End If
	End If
	Log(ABMPageId)
	'----------------------MODIFICATION-------------------------------
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")
End Sub

Sub Page_ParseEvent(Params As Map)
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)
		End Select
	End If
End Sub

public Sub BuildTheme()
	' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	
	' add additional themes specific for this page
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "altapersonal.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' adding a navigation bar
	
			
	' create the page grid
	page.AddRows(4,True, "").AddCells12(1,"")
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()
	'	connecting the navigation bar
		
	'ABMShared.ConnectNavigationBar(page)
	'	init all your own variables (like a List, Map) and add your components

	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim consulta As String
	sql1.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")


	Dim cnt2 As ABMContainer
	cnt2.Initialize(page,"cnt2","cnt")
	cnt2.AddRow(1,True,"","")
	cnt2.AddRows(7,False,"").AddCellsOSMP(4,0,0,0,12,12,3,0,0,20,20,"")'.AddCellsOS(4,0,0,1,12,12,2,"")
	cnt2.AddRows(1,False,"").AddCellsOSMP(6,0,0,0,12,12,2,0,0,20,5,"")'.AddCellsOS(4,0,0,1,12,12,2,"")
	cnt2.AddRows(1,False,"").AddCellsOSMP(4,0,0,0,12,12,3,0,0,20,20,"")'.AddCellsOS(4,0,0,1,12,12,2,"")
	cnt2.AddRow(1,True,"","")
	cnt2.AddRows(5,False,"").AddCellsOSMP(4,0,0,0,12,12,3,0,0,20,20,"")'.AddCellsOS(4,0,0,1,12,12,2,"")
	cnt2.AddRow(1,True,"","")
	cnt2.AddRows(3,False,"").AddCellsOSMP(4,0,0,0,12,12,3,0,0,20,20,"")'.AddCellsOS(4,0,0,1,12,12,2,"")
	cnt2.AddRow(1,True,"","")
	cnt2.AddRows(3,False,"").AddCellsOSMP(4,0,0,0,12,12,3,0,0,20,20,"")'.AddCellsOS(4,0,0,1,12,12,2,"")

	cnt2.BuildGrid
	page.Cell(2,1).AddComponent(cnt2)

	Dim lbl2 As ABMLabel
	lbl2.Initialize(page,"lbl2","Datos Personales",ABM.SIZE_A,False,"etiq")
	cnt2.Cell(1,1).MarginTop="1%"
	cnt2.Cell(1,1).UseTheme("celda2")
	cnt2.Cell(1,1).AddComponent(lbl2)
	
	Dim txtnombre As ABMInput
	txtnombre.Initialize(page,"txtnombre",ABM.INPUT_TEXT,"Nombre",False,"")
	cnt2.Cell(2,1).AddComponent(txtnombre)
	
	Dim txtnombre2 As ABMInput
	txtnombre2.Initialize(page,"txtnombre2",ABM.INPUT_TEXT,"Segundo nombre",False,"")
	cnt2.Cell(2,2).AddComponent(txtnombre2)
	
	Dim txtap As ABMInput
	txtap.Initialize(page,"txtap",ABM.INPUT_TEXT,"Apellido paterno",False,"")
	cnt2.Cell(2,3).AddComponent(txtap)
	
	Dim txtam As ABMInput
	txtam.Initialize(page,"txtam",ABM.INPUT_TEXT,"Apellido materno",False,"")
	cnt2.Cell(2,4).AddComponent(txtam)
	
	Dim cmbG As ABMCombo
	cmbG.Initialize(page,"cmbg","Genero",150,"")
	cmbG.AddItem("hombre","Hombre",BuildSimpleItem(1,"","Hombre"))
	cmbG.AddItem("mujer","Mujer",BuildSimpleItem(1,"","Mujer"))
	cnt2.Cell(3,1).AddComponent(cmbG)
	
	Dim NewDate As Long = DateTime.Now  'Date(DateTime.Now)
	Dim mydate1 As ABMDateTimePicker
	mydate1.Initialize(page, "mydate1", ABM.DATETIMEPICKER_TYPE_DATE, NewDate, "Fecha de nacimiento", "")
	mydate1.CancelText = "Cancelar"
	mydate1.PickText = "OK"
	mydate1.TodayText = ""
	mydate1.Language = "en"
	mydate1.ReturnDateFormat = "DD/MM/YYYY" '  // <--- see demo source code For settings
	mydate1.ReturnTimeFormat = ""'"HH:mm"  '      // <--- see demo source code For settings
	mydate1.FirstDayOfWeek = 0
	mydate1.ClickThrough = True ' <--- set To True If, when the user makes a selection, the ok action should be triggered.
	'cnt2.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
	cnt2.Cell(3,2).AddComponent(mydate1)
	
	Dim txtdom As ABMInput
	txtdom.Initialize(page,"txtdom",ABM.INPUT_TEXT,"Domicilio",False,"")
	cnt2.Cell(3,3).AddComponent(txtdom)
		
	Dim cmbP As ABMCombo
	cmbP.Initialize(page,"cmbp","Pais",150,"")
	'consulta="Select 
	cmbP.AddItem("hombre","Hombre",BuildSimpleItem(1,"","Hombre"))
	cmbP.AddItem("mujer","Mujer",BuildSimpleItem(1,"","Mujer"))
	cnt2.Cell(3,4).AddComponent(cmbP)

	Dim cmbE As ABMCombo
	cmbE.Initialize(page,"cmbe","Estado",150,"")
	cmbE.AddItem("hombre","Hombre",BuildSimpleItem(1,"","Hombre"))
	cmbE.AddItem("mujer","Mujer",BuildSimpleItem(1,"","Mujer"))
	cnt2.Cell(4,1).AddComponent(cmbE)

	Dim cmbC As ABMCombo
	cmbC.Initialize(page,"cmbc","Ciudad",150,"")
	cmbC.AddItem("hombre","Hombre",BuildSimpleItem(1,"","Hombre"))
	cmbC.AddItem("mujer","Mujer",BuildSimpleItem(1,"","Mujer"))
	cnt2.Cell(4,2).AddComponent(cmbC)

	Dim txtmun As ABMInput
	txtmun.Initialize(page,"txtmun",ABM.INPUT_TEXT,"Municipio",False,"")
	cnt2.Cell(4,3).AddComponent(txtmun)

	Dim cmbNc As ABMCombo
	cmbNc.Initialize(page,"cmbnc","Nacionalidad",150,"")
	
	'Select * from openquery(GIN,'Select * from RH_DoradoSensimar1.dbo.RH_nationality')

	cmbNc.AddItem("hombre","Hombre",BuildSimpleItem(1,"","Hombre"))
	cmbNc.AddItem("mujer","Mujer",BuildSimpleItem(1,"","Mujer"))
	cnt2.Cell(4,4).AddComponent(cmbNc)

	Dim cmbCv As ABMCombo
	cmbCv.Initialize(page,"cmbcv","Estado civil",150,"")
	cmbCv.AddItem("hombre","Hombre",BuildSimpleItem(1,"","Hombre"))
	cmbCv.AddItem("mujer","Mujer",BuildSimpleItem(1,"","Mujer"))
	cnt2.Cell(5,1).AddComponent(cmbCv)

	Dim txtemail As ABMInput
	txtemail.Initialize(page,"txtemail",ABM.INPUT_EMAIL,"Correo electrónico",False,"")
	cnt2.Cell(5,2).AddComponent(txtemail)

	Dim cmbEM As ABMCombo
	cmbEM.Initialize(page,"cmbem","Estado de nacimiento",150,"")
	cmbEM.AddItem("hombre","Hombre",BuildSimpleItem(1,"","Hombre"))
	cmbEM.AddItem("mujer","Mujer",BuildSimpleItem(1,"","Mujer"))
	cnt2.Cell(5,3).AddComponent(cmbEM)

	Dim txtTC As ABMInput
	txtTC.Initialize(page,"txttc",ABM.INPUT_TEL,"Teléfono casa",False,"")
	cnt2.Cell(5,4).AddComponent(txtTC)

	Dim txtTCl As ABMInput
	txtTCl.Initialize(page,"txttcl",ABM.INPUT_TEL,"Teléfono celular",False,"")
	cnt2.Cell(6,1).AddComponent(txtTCl)

	Dim txtTEm As ABMInput
	txtTEm.Initialize(page,"txttem",ABM.INPUT_TEL,"Teléfono de emergencia",False,"")
	cnt2.Cell(6,2).AddComponent(txtTEm)

	Dim txtCEm As ABMInput
	txtCEm.Initialize(page,"txtcem",ABM.INPUT_TEXT,"Contacto de emergencia",False,"")
	cnt2.Cell(6,3).AddComponent(txtCEm)

	Dim txtImss As ABMInput
	txtImss.Initialize(page,"txtimss",ABM.INPUT_TEXT,"Número de IMSS",False,"")
	cnt2.Cell(6,4).AddComponent(txtImss)

	Dim txtRFC As ABMInput
	txtRFC.Initialize(page,"txtrfc",ABM.INPUT_TEXT,"R.F.C.",False,"")
	cnt2.Cell(7,1).AddComponent(txtRFC)

	Dim txtCURP As ABMInput
	txtCURP.Initialize(page,"txtcurp",ABM.INPUT_TEXT,"C.U.R.P.",False,"")
	cnt2.Cell(7,2).AddComponent(txtCURP)

	Dim txtNH As ABMInput
	txtNH.Initialize(page,"txtnh",ABM.INPUT_TEXT,"Número de hijos",False,"")
	cnt2.Cell(7,3).AddComponent(txtNH)

	Dim txtNoH1 As ABMInput
	txtNoH1.Initialize(page,"txtnoh1",ABM.INPUT_TEXT,"Nombre hijo 1",False,"")
	cnt2.Cell(8,1).AddComponent(txtNoH1)

	Dim fechijo1 As ABMDateTimePicker
	fechijo1.Initialize(page, "fechijo1", ABM.DATETIMEPICKER_TYPE_DATE, NewDate, "Fecha de nacimiento", "")
	fechijo1.CancelText = "Cancelar"
	fechijo1.PickText = "OK"
	fechijo1.TodayText = ""
	fechijo1.Language = "en"
	fechijo1.ReturnDateFormat = "DD/MM/YYYY" '  // <--- see demo source code For settings
	fechijo1.ReturnTimeFormat = ""'"HH:mm"  '      // <--- see demo source code For settings
	fechijo1.FirstDayOfWeek = 0
	fechijo1.ClickThrough = True ' <--- set To True If, when the user makes a selection, the ok action should be triggered.
	'cnt2.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
	cnt2.Cell(8,2).AddComponent(fechijo1)

	Dim txtNoH2 As ABMInput
	txtNoH2.Initialize(page,"txtnoh2",ABM.INPUT_TEXT,"Nombre hijo 2",False,"")
	cnt2.Cell(8,3).AddComponent(txtNoH2)

	Dim fechijo2 As ABMDateTimePicker
	fechijo2.Initialize(page, "fechijo2", ABM.DATETIMEPICKER_TYPE_DATE, NewDate, "Fecha de nacimiento", "")
	fechijo2.CancelText = "Cancelar"
	fechijo2.PickText = "OK"
	fechijo2.TodayText = ""
	fechijo2.Language = "en"
	fechijo2.ReturnDateFormat = "DD/MM/YYYY" '  // <--- see demo source code For settings
	fechijo2.ReturnTimeFormat = ""'"HH:mm"  '      // <--- see demo source code For settings
	fechijo2.FirstDayOfWeek = 0
	fechijo2.ClickThrough = True ' <--- set To True If, when the user makes a selection, the ok action should be triggered.
	'cnt2.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
	cnt2.Cell(8,4).AddComponent(fechijo2)

	Dim cmbID1 As ABMCombo
	cmbID1.Initialize(page,"cmbID1","Idioma 1",150,"")
	cmbID1.AddItem("ingles","Ingles",BuildSimpleItem(1,"","Ingles"))
	cmbID1.AddItem("frances","Frances",BuildSimpleItem(1,"","Frances"))
	cnt2.Cell(9,1).AddComponent(cmbID1)
	Dim cmbIDN1 As ABMCombo
	cmbIDN1.Initialize(page,"cmbIDN1","Nivel 1",150,"")
	For i=10 To 100
		cmbIDN1.AddItem(i,i&"%",BuildSimpleItem(1,"",i&"%"))
	Next
	cnt2.Cell(9,2).AddComponent(cmbIDN1)
	Dim cmbNT1 As ABMCombo
	cmbNT1.Initialize(page,"cmbNT1","Nativo 1",150,"")
	cmbNT1.AddItem("si","SI",BuildSimpleItem(1,"","SI"))
	cmbNT1.AddItem("no","NO",BuildSimpleItem(1,"","NO"))
	cnt2.Cell(9,3).AddComponent(cmbNT1)

	Dim cmbID2 As ABMCombo
	cmbID2.Initialize(page,"cmbID2","Idioma 2",150,"")
	cmbID2.AddItem("ingles","Ingles",BuildSimpleItem(1,"","Ingles"))
	cmbID2.AddItem("frances","Frances",BuildSimpleItem(1,"","Frances"))
	cnt2.Cell(9,4).AddComponent(cmbID2)
	Dim cmbIDN2 As ABMCombo
	cmbIDN2.Initialize(page,"cmbIDN2","Nivel 2",150,"")
	For i=10 To 100
		cmbIDN2.AddItem(i,i&"%",BuildSimpleItem(1,"",i&"%"))
	Next
	cnt2.Cell(9,5).AddComponent(cmbIDN2)
	Dim cmbNT2 As ABMCombo
	cmbNT2.Initialize(page,"cmbNT2","Nativo 2",150,"")
	cmbNT2.AddItem("si","SI",BuildSimpleItem(1,"","SI"))
	cmbNT2.AddItem("no","NO",BuildSimpleItem(1,"","NO"))
	cnt2.Cell(9,6).AddComponent(cmbNT2)

	Dim cmbEs As ABMCombo
	cmbEs.Initialize(page,"cmbEs","Escolaridad",150,"")
	cmbEs.AddItem("ingles","Ingles",BuildSimpleItem(1,"","Ingles"))
	cmbEs.AddItem("frances","Frances",BuildSimpleItem(1,"","Frances"))
	cnt2.Cell(10,1).AddComponent(cmbEs)

	Dim txtDis As ABMInput
	txtDis.Initialize(page,"txtDis",ABM.INPUT_TEXT,"¿Cuenta con alguna discapacidad?",False,"")
	cnt2.Cell(10,3).AddComponent(txtDis)

	Dim lbl3 As ABMLabel
	lbl3.Initialize(page,"lbl3","Datos de nomina",ABM.SIZE_A,False,"etiq")
	cnt2.Cell(11,1).MarginTop="1%"
	cnt2.Cell(11,1).UseTheme("celda2")
	cnt2.Cell(11,1).AddComponent(lbl3)

	Dim txtNoCol As ABMInput
	txtNoCol.Initialize(page,"txtNoCol",ABM.INPUT_TEXT,"Número de colaborador",False,"")
	cnt2.Cell(12,1).AddComponent(txtNoCol)

	Dim cmbCto As ABMCombo
	cmbCto.Initialize(page,"cmbCto","Centro de Costos",150,"")
	cmbCto.AddItem("ingles","Ingles",BuildSimpleItem(1,"","Ingles"))
	cmbCto.AddItem("frances","Frances",BuildSimpleItem(1,"","Frances"))
	cnt2.Cell(12,2).AddComponent(cmbCto)
	
	Dim cmbCat As ABMCombo
	cmbCat.Initialize(page,"cmbCat","Categoria",150,"")
	cmbCat.AddItem("ingles","Ingles",BuildSimpleItem(1,"","Ingles"))
	cmbCat.AddItem("frances","Frances",BuildSimpleItem(1,"","Frances"))
	cnt2.Cell(12,3).AddComponent(cmbCat)
	
	Dim txtSalD As ABMInput
	txtSalD.Initialize(page,"txtSalD",ABM.INPUT_TEXT,"Salario Diario",False,"")
	cnt2.Cell(13,1).AddComponent(txtSalD)
	
	Dim txtSalDI As ABMInput
	txtSalDI.Initialize(page,"txtSalDI",ABM.INPUT_TEXT,"Salario Diario Integrado",False,"")
	cnt2.Cell(13,2).AddComponent(txtSalDI)
	
	Dim txtFact As ABMInput
	txtFact.Initialize(page,"txtFact",ABM.INPUT_TEXT,"Factor de integración",False,"")
	cnt2.Cell(13,3).AddComponent(txtFact)
	
	Dim cmbTipo As ABMCombo
	cmbTipo.Initialize(page,"cmbTipo","Categoria",150,"")
	cmbTipo.AddItem("ingles","Ingles",BuildSimpleItem(1,"","Ingles"))
	cmbTipo.AddItem("frances","Frances",BuildSimpleItem(1,"","Frances"))
	cnt2.Cell(14,1).AddComponent(cmbTipo)
	
	Dim cmbRep As ABMCombo
	cmbRep.Initialize(page,"cmbRep","Reportará a :",150,"")
	cmbRep.AddItem("ingles","Ingles",BuildSimpleItem(1,"","Ingles"))
	cmbRep.AddItem("frances","Frances",BuildSimpleItem(1,"","Frances"))
	cnt2.Cell(14,2).AddComponent(cmbRep)
	
	Dim cmbTipoNom As ABMCombo
	cmbTipoNom.Initialize(page,"cmbTipoNom","Tipo de nómina",150,"")
	cmbTipoNom.AddItem("ingles","Ingles",BuildSimpleItem(1,"","Ingles"))
	cmbTipoNom.AddItem("frances","Frances",BuildSimpleItem(1,"","Frances"))
	cnt2.Cell(14,3).AddComponent(cmbTipoNom)
	
	Dim chkDll As ABMCheckbox
	chkDll.Initialize(page,"chkDll","Activar Dolares",False,"")
	cnt2.Cell(14,4).MarginTop="50"
	cnt2.Cell(14,4).AddComponent(chkDll)
	
	Dim fecinic2 As ABMDateTimePicker
	fecinic2.Initialize(page, "fecinic2", ABM.DATETIMEPICKER_TYPE_DATE, NewDate, "Inicio del contrato", "")
	fecinic2.CancelText = "Cancelar"
	fecinic2.PickText = "OK"
	fecinic2.TodayText = ""
	fecinic2.Language = "en"
	fecinic2.ReturnDateFormat = "DD/MM/YYYY" '  // <--- see demo source code For settings
	fecinic2.ReturnTimeFormat = ""'"HH:mm"  '      // <--- see demo source code For settings
	fecinic2.FirstDayOfWeek = 0
	fecinic2.ClickThrough = True ' <--- set To True If, when the user makes a selection, the ok action should be triggered.
	'cnt2.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
	cnt2.Cell(15,1).AddComponent(fecinic2)
	
	Dim txtDura As ABMInput
	txtDura.Initialize(page,"txtDura",ABM.INPUT_TEXT,"Duración del contrato",False,"")
	cnt2.Cell(15,2).AddComponent(txtDura)
	
	Dim fecfin2 As ABMDateTimePicker
	fecfin2.Initialize(page, "fecfin2", ABM.DATETIMEPICKER_TYPE_DATE, NewDate, "Fin de contrato", "")
	fecfin2.CancelText = "Cancelar"
	fecfin2.PickText = "OK"
	fecfin2.TodayText = ""
	fecfin2.Language = "en"
	fecfin2.ReturnDateFormat = "DD/MM/YYYY" '  // <--- see demo source code For settings
	fecfin2.ReturnTimeFormat = ""'"HH:mm"  '      // <--- see demo source code For settings
	fecfin2.FirstDayOfWeek = 0
	fecfin2.ClickThrough = True ' <--- set To True If, when the user makes a selection, the ok action should be triggered.
	'cnt2.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
	cnt2.Cell(15,3).AddComponent(fecfin2)
	
	Dim cmbCrInf As ABMCombo
	cmbCrInf.Initialize(page,"cmbCrInf","Crédito INFONAVIT",150,"")
	cmbCrInf.AddItem("SI","SI",BuildSimpleItem(1,"","SI"))
	cmbCrInf.AddItem("NO","NO",BuildSimpleItem(1,"","NO"))
	cnt2.Cell(16,1).AddComponent(cmbCrInf)
	
	Dim txtNoInf As ABMInput
	txtNoInf.Initialize(page,"txtNoInf",ABM.INPUT_TEXT,"Número de Crédito INFONAVIT",False,"")
	cnt2.Cell(16,2).AddComponent(txtNoInf)
	
	Dim cmbCrFon As ABMCombo
	cmbCrFon.Initialize(page,"cmbCrFon","Crédito FONACOT",150,"")
	cmbCrFon.AddItem("SI","SI",BuildSimpleItem(1,"","SI"))
	cmbCrFon.AddItem("NO","NO",BuildSimpleItem(1,"","NO"))
	cnt2.Cell(16,3).AddComponent(cmbCrFon)
	
	Dim txtNoFon As ABMInput
	txtNoFon.Initialize(page,"txtNoFon",ABM.INPUT_TEXT,"Número de Crédito FONACOT",False,"")
	cnt2.Cell(16,4).AddComponent(txtNoFon)
	
	Dim lbl4 As ABMLabel
	lbl4.Initialize(page,"lbl4","Datos de empresa",ABM.SIZE_A,False,"etiq")
	cnt2.Cell(17,1).MarginTop="1%"
	cnt2.Cell(17,1).UseTheme("celda2")
	cnt2.Cell(17,1).AddComponent(lbl4)
	
	Dim fecE1 As ABMDateTimePicker
	fecE1.Initialize(page, "fecE1", ABM.DATETIMEPICKER_TYPE_DATE, NewDate, "Inicio tarjeta salud", "")
	fecE1.CancelText = "Cancelar"
	fecE1.PickText = "OK"
	fecE1.TodayText = ""
	fecE1.Language = "en"
	fecE1.ReturnDateFormat = "DD/MM/YYYY" '  // <--- see demo source code For settings
	fecE1.ReturnTimeFormat = ""'"HH:mm"  '      // <--- see demo source code For settings
	fecE1.FirstDayOfWeek = 0
	fecE1.ClickThrough = True ' <--- set To True If, when the user makes a selection, the ok action should be triggered.
	'cnt2.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
	cnt2.Cell(18,1).AddComponent(fecE1)
	
	Dim fecE2 As ABMDateTimePicker
	fecE2.Initialize(page, "fecE2", ABM.DATETIMEPICKER_TYPE_DATE, NewDate, "Vencimiento tarjeta salud", "")
	fecE2.CancelText = "Cancelar"
	fecE2.PickText = "OK"
	fecE2.TodayText = ""
	fecE2.Language = "en"
	fecE2.ReturnDateFormat = "DD/MM/YYYY" '  // <--- see demo source code For settings
	fecE2.ReturnTimeFormat = ""'"HH:mm"  '      // <--- see demo source code For settings
	fecE2.FirstDayOfWeek = 0
	fecE2.ClickThrough = True ' <--- set To True If, when the user makes a selection, the ok action should be triggered.
	'cnt2.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
	cnt2.Cell(18,2).AddComponent(fecE2)

	Dim fecE3 As ABMDateTimePicker
	fecE3.Initialize(page, "fecE3", ABM.DATETIMEPICKER_TYPE_DATE, NewDate, "Inicio de embarazo", "")
	fecE3.CancelText = "Cancelar"
	fecE3.PickText = "OK"
	fecE3.TodayText = ""
	fecE3.Language = "en"
	fecE3.ReturnDateFormat = "DD/MM/YYYY" '  // <--- see demo source code For settings
	fecE3.ReturnTimeFormat = ""'"HH:mm"  '      // <--- see demo source code For settings
	fecE3.FirstDayOfWeek = 0
	fecE3.ClickThrough = True ' <--- set To True If, when the user makes a selection, the ok action should be triggered.
	'cnt2.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
	cnt2.Cell(18,3).AddComponent(fecE3)
	
	Dim fecE4 As ABMDateTimePicker
	fecE4.Initialize(page, "fecE4", ABM.DATETIMEPICKER_TYPE_DATE, NewDate, "Termino de embarazo", "")
	fecE4.CancelText = "Cancelar"
	fecE4.PickText = "OK"
	fecE4.TodayText = ""
	fecE4.Language = "en"
	fecE4.ReturnDateFormat = "DD/MM/YYYY" '  // <--- see demo source code For settings
	fecE4.ReturnTimeFormat = ""'"HH:mm"  '      // <--- see demo source code For settings
	fecE4.FirstDayOfWeek = 0
	fecE4.ClickThrough = True ' <--- set To True If, when the user makes a selection, the ok action should be triggered.
	'cnt2.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
	cnt2.Cell(18,4).AddComponent(fecE4)
	
	Dim txtLooker As ABMInput
	txtLooker.Initialize(page,"txtLooker",ABM.INPUT_TEXT,"Número de looker",False,"")
	cnt2.Cell(19,1).AddComponent(txtLooker)
	
	Dim chkPobl As ABMCheckbox
	chkPobl.Initialize(page,"chkPobl","Activar Poblado",False,"")
	chkPobl.SetTooltip("Activar",ABM.POSITION_TOPLEFT,1000)
	cnt2.Cell(19,2).MarginTop="50"
	cnt2.Cell(19,2).AddComponent(chkPobl)
	
	Dim txtNoHab As ABMInput
	txtNoHab.Initialize(page,"txtNoHab",ABM.INPUT_TEXT,"Número y tipo de habitación",False,"")
	cnt2.Cell(19,3).AddComponent(txtNoHab)

	Dim fecE5 As ABMDateTimePicker
	fecE5.Initialize(page, "fecE5", ABM.DATETIMEPICKER_TYPE_DATE, NewDate, "Expedición de antecedentes no penales", "")
	fecE5.CancelText = "Cancelar"
	fecE5.PickText = "OK"
	fecE5.TodayText = ""
	fecE5.Language = "en"
	fecE5.ReturnDateFormat = "DD/MM/YYYY" '  // <--- see demo source code For settings
	fecE5.ReturnTimeFormat = ""'"HH:mm"  '      // <--- see demo source code For settings
	fecE5.FirstDayOfWeek = 0
	fecE5.ClickThrough = True ' <--- set To True If, when the user makes a selection, the ok action should be triggered.
	'cnt2.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
	cnt2.Cell(20,1).AddComponent(fecE5)
	
	Dim fecE6 As ABMDateTimePicker
	fecE6.Initialize(page, "fecE6", ABM.DATETIMEPICKER_TYPE_DATE, NewDate, "Expedición antidoping", "")
	fecE6.CancelText = "Cancelar"
	fecE6.PickText = "OK"
	fecE6.TodayText = ""
	fecE6.Language = "en"
	fecE6.ReturnDateFormat = "DD/MM/YYYY" '  // <--- see demo source code For settings
	fecE6.ReturnTimeFormat = ""'"HH:mm"  '      // <--- see demo source code For settings
	fecE6.FirstDayOfWeek = 0
	fecE6.ClickThrough = True ' <--- set To True If, when the user makes a selection, the ok action should be triggered.
	'cnt2.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
	cnt2.Cell(20,2).AddComponent(fecE6)

	Dim txtTransporte As ABMInput
	txtTransporte.Initialize(page,"txtTransporte",ABM.INPUT_TEXT,"Número de transporte",False,"")
	cnt2.Cell(20,3).AddComponent(txtTransporte)
	
	Dim txtVisa As ABMInput
	txtVisa.Initialize(page,"txtVisa",ABM.INPUT_TEXT,"Número de visa Americana",False,"")
	cnt2.Cell(20,4).AddComponent(txtVisa)
	
	Dim txtNotas As ABMInput
	txtNotas.Initialize(page,"txtNotas",ABM.INPUT_TEXT,"Notas",False,"")
	cnt2.Cell(21,1).AddComponent(txtNotas)

	Dim btnA As ABMButton
	btnA.InitializeFlat(page,"btnA","","","Aceptar","btn")
	cnt2.Cell(23,2).AddComponent(btnA)

	Dim btnL As ABMButton
	btnL.InitializeFlat(page,"btnL","","","Limpiar","btn")
	cnt2.Cell(23,3).AddComponent(btnL)
	
	
	If  Not(Main.fondoe=Null) And Not(Main.fondoe="") And  Not(Main.fondoe="null") And Not(Main.fondoe="no") And Not(Main.fondoe="#") Then
		If Main.fondoe.Length<10 Then
			Dim script As String = $"$('body').css('background-color', '${Main.fondoe}');
													 $('body').attr('class','');	 
											"$
			page.ws.Eval(script,Null)
		Else
			Dim su As StringUtils
			Dim BT2() As Byte= su.DecodeBase64(Main.fondoe)
			Dim imagen As Image = BytesToImage(BT2)
			Dim carpeta As String = File.Combine(File.DirApp, Main.DownloadFolder)
						
			Dim nomfondo As String =  ws.Session.GetAttribute("idsesion")&"b.jpg"
			If File.Exists (carpeta,  ws.Session.GetAttribute("idsesion")&"b.jpg") Then
			Dim borro As Boolean = False
				borro = File.Delete(carpeta,  ws.Session.GetAttribute("idsesion")&"b.jpg")
			End If
			
			Dim Out As OutputStream = File.OpenOutput(carpeta,  ws.Session.GetAttribute("idsesion")&"b.jpg" , True)
			imagen.WriteToStream(Out)
			Out.Close
						
			Dim script As String = $"$('body').css('background-image', 'url(../perfiles/${nomfondo})');
						
						 $('body').css('background-size', 'cover');
						 
							"$
			page.ws.Eval(script,Null)
			
		End If
	End If
	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub
Sub btnA_Clicked(Target As String)
	'Validaciones
	Dim cntX As ABMContainer=page.Component("cnt2")
	Dim txtnombre As ABMInput=cntX.Component("txtnombre")
	If txtnombre.Text.Length<=0 Then
		page.Msgbox("","Deberá escribir el nombre","Error de Captura","Aceptar","mb")
		Return
	End If
	Dim txtnombre2 As ABMInput=cntX.Component("txtnombre2")
	If txtnombre2.Text.Length<0 Then txtnombre2.Text=" "
	Dim txtap As ABMInput=cntX.Component("txtap")
	If txtap.Text.Length<=0 Then
		page.Msgbox("","Deberá escribir el apellido paterno","Error de Captura","Aceptar","mb")
		Return
	End If
	Dim txtam As ABMInput=cntX.Component("txtam")
	If txtam.Text.Length<=0 Then
		page.Msgbox("","Deberá escribir el apellido materno","Error de Captura","Aceptar","mb")
		Return
	End If
	Dim cmbG As ABMCombo=cntX.Component("cmbg")
	If cmbG.GetActiveItemId="-1" Then
		page.Msgbox("","Deberá seleccionar el genero","Error de Captura","Aceptar","mb")
		Return
	End If
	Dim mydate1 As ABMDateTimePicker=cntX.Component("mydate1")
	If DateTime.Date(mydate1.GetDate)=DateTime.Date(DateTime.Now) Then
		page.Msgbox("","La fecha de nacimiento no puede ser hoy","Error de Captura","Aceptar","mb")
		Return
	End If
	Dim txtdom As ABMInput=cntX.Component("txtdom")
	If txtdom.Text.Length<=0 Then
		page.Msgbox("","Deberá escribir el domicilio","Error de Captura","Aceptar","mb")
		Return
	End If
	Dim cmbP As ABMCombo=cntX.Component("cmbp")
	If cmbP.GetActiveItemId="-1" Then
		page.Msgbox("","Deberá seleccionar el pais","Error de Captura","Aceptar","mb")
		Return
	End If
	Dim cmbE As ABMCombo=cntX.Component("cmbe")
	If cmbE.GetActiveItemId="-1" Then
		page.Msgbox("","Deberá seleccionar el estado","Error de Captura","Aceptar","mb")
		Return
	End If
	Dim cmbC As ABMCombo=cntX.Component("cmbc")
	If cmbC.GetActiveItemId="-1" Then
		page.Msgbox("","Deberá seleccionar la ciudad","Error de Captura","Aceptar","mb")
		Return
	End If
	Dim txtmun	As ABMInput=cntX.Component("txtmun")
	If txtmun.Text.Length<=0 Then
		page.Msgbox("","Deberá escribir el municipio","Error de Captura","Aceptar","mb")
		Return
	End If
	Dim cmbnc As ABMCombo=cntX.Component("cmbnc")
	If cmbnc.GetActiveItemId=-1 Then
		page.Msgbox("","Deberá seleccionar la nacionalidad","Error de Captura","Aceptar","mb")
		Return	
	End If

	
	
	
	
End Sub
Sub BytesToImage(bytes() As Byte) As Image
	Dim In As InputStream
	In.InitializeFromBytesArray(bytes, 0, bytes.Length)
	Dim bmp As Image
	bmp.Initialize2(In)
   
	Return bmp
End Sub

Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
	'	If icon <> "" Then
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	'	Else
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
		combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next
	
	'page.Refresh
	'combo1.Refresh
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)
	Return myModal
End Sub

