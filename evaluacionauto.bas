﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'estas 
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "evaluacionauto"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = ""
	' your own variables
	
	Public calif As Int = 5
	Public notas As Map
	Dim contenedores As List = Array As String ("A","B","C","D","E","F","G","H","I","J")
	
	Dim Idencuesta ,IdEntrevistado,nombreEnt,Relacion As String
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------
	Log("Connected")
	ws = WebSocket1
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
		' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
		page.Refresh
		page.FinishedLoading
	Else
		If page.WebsocketReconnected Then
			Log("Websocket reconnected")
			' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
			' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
			ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
		Else
			' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
			Log("Websocket first connection")
			page.Prepare
			ConnectPage
		End If
	End If
	Log(ABMPageId)
	'----------------------MODIFICATION-------------------------------
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")
End Sub

Sub Page_ParseEvent(Params As Map)
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)
		End Select
	End If
End Sub

public Sub BuildTheme()
	' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	
	theme.AddContainerTheme("t1")
	theme.Container("t1").BackColor=ABM.COLOR_TEAL
	theme.Container("t1").BackColorIntensity=ABM.INTENSITY_DARKEN1
	
	theme.AddCellTheme("t2")
	theme.cell("t2").BackColor=ABM.COLOR_TEAL
	theme.cell("t2").BackColorIntensity=ABM.INTENSITY_LIGHTEN3
	theme.Cell("t2").Align=ABM.CELL_ALIGN_CENTER
	theme.AddCellTheme("t3")
	theme.cell("t3").BackColor=ABM.COLOR_TEAL
	theme.cell("t3").BackColorIntensity=ABM.INTENSITY_LIGHTEN3
	
	theme.AddCellTheme("claro")
	theme.Cell("claro").BackColor=ABM.COLOR_CYAN
	theme.Cell("claro").BackColorIntensity=ABM.INTENSITY_LIGHTEN5
	
	theme.AddCellTheme("claro2")
	theme.Cell("claro2").BackColor=ABM.COLOR_BLUEGREY
	theme.Cell("claro2").BackColorIntensity=ABM.INTENSITY_LIGHTEN5
	
	
	
	
	
		
	
	'	theme.AddContainerTheme("azul")
	'	theme.Container("azul").BackColor=ABM.COLOR_LIGHTBLUE
	'	theme.Container("azul").BackColorIntensity=ABM.INTENSITY_ACCENT3
	'
	'	theme.AddLabelTheme("azullbl")
	'	theme.Label("azullbl").BackColor=ABM.COLOR_LIGHTBLUE
	'	theme.Label("azullbl").BackColorIntensity=ABM.INTENSITY_ACCENT3
	'
	'
	'	theme.AddCellTheme("azulcell")
	'	theme.Cell("azulcell").BackColor=ABM.COLOR_LIGHTBLUE
	'	theme.Cell("azulcell").BackColorIntensity=ABM.INTENSITY_ACCENT3
	'	'theme.Cell("azulcell").Align=ABM.CELL_ALIGN_CENTER
	'
	'	theme.AddCellTheme("azulcell2")
	'	theme.Cell("azulcell2").BackColor=ABM.COLOR_LIGHTBLUE
	'	theme.Cell("azulcell2").BackColorIntensity=ABM.INTENSITY_ACCENT3
	'	theme.Cell("azulcell2").Align=ABM.CELL_ALIGN_CENTER
	'
	'
	'
	'	theme.AddLabelTheme("clarolbl")
	'	theme.Label("clarolbl").BackColor=ABM.COLOR_CYAN
	'	theme.Label("clarolbl").BackColorIntensity=ABM.INTENSITY_LIGHTEN5
	'
	'	theme.AddCellTheme("db")
	'	theme.Cell("db").BorderColor=ABM.COLOR_BLACK
	'	theme.Cell("db").BorderWidth=1


	theme.AddButtonTheme("b1")
	theme.Button("b1").BackColor=ABM.COLOR_RED
	theme.Button("b1").BackColorIntensity=ABM.INTENSITY_LIGHTEN1
	theme.Button("b1").ZDepth=ABM.ZDEPTH_REMOVE
	theme.AddButtonTheme("b1c")
	theme.Button("b1c").BackColor=ABM.COLOR_RED
	theme.Button("b1c").BackColorIntensity=ABM.INTENSITY_DARKEN4
	theme.Button("b1c").ZDepth=ABM.ZDEPTH_5

	theme.AddButtonTheme("b2")
	theme.Button("b2").BackColor=ABM.COLOR_ORANGE
	theme.Button("b2").BackColorIntensity=ABM.INTENSITY_LIGHTEN1
	theme.AddButtonTheme("b2c")
	theme.Button("b2c").BackColor=ABM.COLOR_ORANGE
	theme.Button("b2c").BackColorIntensity=ABM.INTENSITY_DARKEN2
	theme.Button("b2c").ZDepth=ABM.ZDEPTH_5

	theme.AddButtonTheme("b3")
	theme.Button("b3").BackColor=ABM.COLOR_YELLOW
	theme.Button("b3").BackColorIntensity=ABM.INTENSITY_LIGHTEN1
	theme.AddButtonTheme("b3c")
	theme.Button("b3c").BackColor=ABM.COLOR_YELLOW
	theme.Button("b3c").BackColorIntensity=ABM.INTENSITY_DARKEN2
	theme.Button("b3c").ZDepth=ABM.ZDEPTH_5

	theme.AddButtonTheme("b4")
	theme.Button("b4").BackColor=ABM.COLOR_LIME
	theme.Button("b4").BackColorIntensity=ABM.INTENSITY_LIGHTEN1
	theme.AddButtonTheme("b4c")
	theme.Button("b4c").BackColor=ABM.COLOR_LIME
	theme.Button("b4c").BackColorIntensity=ABM.INTENSITY_DARKEN4
	theme.Button("b4c").ZDepth=ABM.ZDEPTH_5

	theme.AddButtonTheme("b5")
	theme.Button("b5").BackColor=ABM.COLOR_GREEN
	theme.Button("b5").BackColorIntensity=ABM.INTENSITY_LIGHTEN1
	theme.AddButtonTheme("b5c")
	theme.Button("b5c").BackColor=ABM.COLOR_GREEN
	theme.Button("b5c").BackColorIntensity=ABM.INTENSITY_DARKEN4
	theme.Button("b5c").ZDepth=ABM.ZDEPTH_5


	theme.AddButtonTheme("b6")
	theme.Button("b6").BackColor=ABM.COLOR_GREY
	theme.Button("b6").BackColorIntensity=ABM.INTENSITY_LIGHTEN1
	theme.AddButtonTheme("b6c")
	theme.Button("b6c").BackColor=ABM.COLOR_GREY
	theme.Button("b6c").BackColorIntensity=ABM.INTENSITY_DARKEN4
	theme.Button("b6c").ZDepth=ABM.ZDEPTH_5



End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "evaluacionpub.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
	'page.AlwaysShowVerticalScrollBar = True
	
		
	'adding a navigation bar
	ABMShared.BuildNavigationBar(page, "Auto Evaluación 360°","../images/klogo3.png", "", "Auto Evaluación 360°", "")
			
	' create the page grid
	page.AddRows(1,False,"").AddCells12(1,"")'.AddCellsOSMP(1,0,0,0,12,6,4,0,0,0,0,"").AddCellsOSMP(1,0,0,0,12,6,8,0,0,0,0,"")
	page.AddRows(1,False,"").AddCellsOSMP(1,0,0,0,2,2,2,0,0,0,0,"")'.AddCellsOSMP(1,0,0,0,10,10,10,0,0,0,0,"")
	page.AddRows(1,False,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	'page.AddRows(3,False,"").AddCells12(1,"")
	page.AddRows(10,False,"").AddCellsOSMP(1,0,0,0,8,8,12,10,0,20,20,"")
	page.AddRows(1,False,"").AddCells12(1,"")
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	
		
End Sub

public Sub ConnectPage()
	'	connecting the navigation bar
	'ABMShared.ConnectNavigationBar(page)
	Idencuesta=ws.UpgradeRequest.GetParameter("id")
	Log ("id encuesta " & Idencuesta)
	Dim sql1,sql2 As SQL
	Dim rst As ResultSet
	sql1.Initialize(Main.strDrv,"jdbc:sqlserver://25.9.192.248:1433;databaseName=KarismaHC;user=karismapass;password=SuiteHC$;")
	sql2.Initialize(Main.strDrv,"jdbc:sqlserver://25.9.192.248:1433;databaseName=KarismaHC;user=karismapass;password=SuiteHC$;")

	
	rst=sql1.ExecQuery("Select * from RH_lista360 where id_gui='" & Idencuesta & "'")
	rst.NextRow
	IdEntrevistado=rst.GetString("id_user")
	nombreEnt=sql2.ExecQuerySingleResult("Select nom_nombre from RH_UserConf where id_user='" & IdEntrevistado & "'")
	Relacion=rst.GetString("nom_tipo")
	
	Dim cntT1 As ABMContainer
	cntT1.Initialize(page,"cntt1","t1")
	cntT1.AddRows(1,False,"").AddCells12(1,"")'.AddCellsOSMP(1,0,0,0,12,6,2,0,0,0,0,"").AddCellsOSMP(1,0,0,0,12,6,9,0,0,0,0,"")
	cntT1.BuildGrid
	page.Cell(1,1).AddComponent(cntT1)
	
	'	Dim img As ABMImage
	'	img.Initialize(page,"im","../images/klogo2.png",3)
	'	cntT1.Cell(1,1).SetFixedHeight(84,False)
	'	cntT1.Cell(1,1).AddComponent(img)
	
	Dim lbl As ABMLabel
	lbl.Initialize(page,"lbl","{B}Evaluación de Retroalimentación 360°{/B}",ABM.SIZE_H4,False,"etiq")'etiq
	'	cntT1.Cell(1,2).UseTheme("celda2")
	'cntT1.Cell(1,1).MarginLeft = "10.5%"
	cntT1.Cell(1,1).SetFixedHeight(84,False)
	cntT1.Cell(1,1).UseTheme("celda3")
	cntT1.Cell(1,1).AddComponent(lbl)

	Dim imglogo As ABMImage
	imglogo.Initialize(page,"iml","../images/feedback.png",3)
	page.Cell(2,1).AddComponent(imglogo)
	'page.Cell(2,1).UseTheme("imgctr")
	
	
	'page.Cell(3,1).SetFixedHeight(0,False)
	'page.Cell(4,1).SetFixedHeight(0,False)
	'page.Cell(5,1).SetFixedHeight(0,False)
	
	Dim cntt2 As ABMContainer
	cntt2.Initialize(page,"cntt2","")
	cntt2.AddRows(2,False,"").AddCells12(1,"")
	cntt2.BuildGrid
	page.Cell(3,1).AddComponent(cntt2)
		
	cntt2.Cell(1,1).AddComponent(ABMShared.BuildParagraph(page,"e","Bienvenid@ " & rst.GetString("nom_nombre") ,ABM.SIZE_H4))
	cntt2.Cell(2,1).AddComponent(ABMShared.BuildParagraph(page,"p","Agradecemos su tiempo para realizar esta evaluación a " & nombreEnt,ABM.SIZE_H4  ))
	cntt2.Cell(1,1).SetFixedHeight(30,False)
	cntt2.Cell(2,1).SetFixedHeight(30,False)
	cntt2.Cell(1,1).PaddingLeft = "2%"
	cntt2.Cell(2,1).PaddingLeft = "2%"

	
	comunicacion ("A",4,1,"COMUNICACIÓN","A")'6
	comunicacion ("B",5,1,"PENSAMIENTO ESTRATÉGICO","B")
	comunicacion("C",6,1,"ORIENTACIÓN A RESULTADOS","C")
	comunicacion("D",7,1,"ACEPTAR EL CAMBIO","D")
	comunicacion("E",8,1,"TOMA DE DECISIONES","E")
	comunicacion("F",9,1,"LIDERAZGO","F")
	comunicacion("G",10,1,"OPERAR CON DISCIPLINA","G")
	comunicacion("H",11,1,"AMBIENTE DE TRABAJO","H")
	comunicacion("I",12,1,"INNOVAR Y REAPLICAR ","I")
	comunicacion("J",13,1,"DESARROLLO DE PERSONAL","J")
	
	Dim salvar As ABMButton
	salvar.InitializeFlat(page,"salvar","","","Salvar","")'btn
	page.Cell(14,1).AddComponent(salvar)'16
	page.Cell(14,1).SetOffsetSize(0,0,5,12,6,6)
	page.Cell(14,1).PaddingTop = "2%"
	
	notas.Initialize
	Dim script As String = $"
		document.body.style.fontFamily = 'Open Sans';
	"$
	
	page.ws.Eval(script,Null)
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	'page.RestoreNavigationBarPosition
End Sub

Sub comunicacion(conteiner As String,Rx As Int,Cx As Int,titulo As String,archivo As String)
	Dim cntA As ABMContainer
	cntA.Initialize(page,conteiner,"")'cnt
	Dim lst As List
	lst.Initialize
	lst.Clear
	lst=File.ReadList(Main.dirfiles,archivo & ".txt")
	
	cntA.AddRows(12,False,"").AddCellsOS(1,0,0,0,6,6,6,"").AddCellsOS(6,0,0,0,1,1,1,"") ' COLOCAR 8 SI SE HABILITA EL TOTAL
	cntA.BuildGrid
	Dim lbltA1 As ABMLabel
	lbltA1.Initialize(page,"lbltA1","{B}" & titulo & "{/B}",ABM.SIZE_H5,False,"etiq")'
	cntA.Cell(1,1).SetFixedHeight(50,False)
	cntA.Cell(1,1).UseTheme("t2")
	cntA.Cell(1,1).AddComponent(lbltA1)
	Dim lbltA2 As ABMLabel
	lbltA2.Initialize(page,"lbltA2","{B}Nunca{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,2).SetFixedHeight(50,False)
	cntA.Cell(1,2).UseTheme("t3")
	cntA.Cell(1,2).AddComponent(lbltA2)
	Dim lbltA3 As ABMLabel
	lbltA3.Initialize(page,"lbltA3","{B}Rara vez{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,3).SetFixedHeight(50,False)
	cntA.Cell(1,3).UseTheme("t3")
	cntA.Cell(1,3).AddComponent(lbltA3)
	Dim lbltA4 As ABMLabel
	lbltA4.Initialize(page,"lbltA4","{B}Algunas veces{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,4).SetFixedHeight(50,False)
	cntA.Cell(1,4).UseTheme("t3")
	cntA.Cell(1,4).AddComponent(lbltA4)
	Dim lbltA5 As ABMLabel
	lbltA5.Initialize(page,"lbltA5","{B}Casi siempre{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,5).SetFixedHeight(50,False)
	cntA.Cell(1,5).UseTheme("t3")
	cntA.Cell(1,5).AddComponent(lbltA5)
	Dim lbltA6 As ABMLabel
	lbltA6.Initialize(page,"lbltA6","{B}Siempre{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,6).SetFixedHeight(50,False)
	cntA.Cell(1,6).UseTheme("t3")
	cntA.Cell(1,6).AddComponent(lbltA6)
	Dim lbltA7 As ABMLabel
	lbltA7.Initialize(page,"lbltA7","{B}NS/NC{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,7).SetFixedHeight(50,False)
	cntA.Cell(1,7).UseTheme("t3")
	cntA.Cell(1,7).AddComponent(lbltA7)
	'Dim lbltA8 As ABMLabel
	
	'	lbltA8.Initialize(page,"lbltA8","{B}NOTA{/B}",ABM.SIZE_SMALL,False,"etiq")
	'	cntA.Cell(1,8).SetFixedHeight(50,False)
	'	cntA.Cell(1,8).UseTheme("celda2")
	'	cntA.Cell(1,8).AddComponent(lbltA8)
	
	Dim temaC As Boolean=False
	
	Dim c As Int = 1
	
	For Rn=0 To lst.size-1
		Dim rnmas1 As Int = Rn +1
		Dim lblcA1 As ABMLabel
		lblcA1.Initialize(page,"O"&conteiner&rnmas1 ,"{B}" & lst.Get(Rn)& "{/B}",ABM.SIZE_A,False,"")
		
		If temaC=False Then
			cntA.Cell(2+Rn,1).UseTheme("claro2")
			
		Else
			cntA.Cell(2+Rn,1).UseTheme("claro")
			lblcA1.UseTheme("clarolbl")
		End If
		cntA.Cell(2+Rn,1).AddComponent(lblcA1)
		cntA.Cell(2+Rn,1).PaddingLeft = "1%"
		'cntA.Cell(2+Rn+1,1).AddComponent(ABMShared.Buildline(page,"1"))
		
		
		For c= 1 To calif
			Dim btn1 As ABMButton
			btn1.InitializeFloating(page,conteiner&rnmas1&c,"","b6")
			btn1.Size=ABM.BUTTONSIZE_SMALL
			If temaC=False Then
				cntA.Cell(2+Rn,c+1).UseTheme("claro2")
			Else
				cntA.Cell(2+Rn,c+1).UseTheme("claro")
			End If
			If c=1 Then
				btn1.UseTheme("b1")
			else if c=2 Then
				btn1.UseTheme("b2")
			else if c=3 Then
				btn1.UseTheme("b3")
			else if c=4 Then
				btn1.UseTheme("b4")
			else if c=5 Then
				btn1.UseTheme("b5")
			End If
			cntA.Cell(2+Rn,c+1).AddArrayComponent(btn1,"C")
				
			''			Dim inp1 As ABMCheckbox
			''			inp1.Initialize(page,conteiner&rnmas1&c,c,False,"")
			''			inp1.IsFilled = True
			''			If temaC=False Then
			''				cntA.Cell(2+Rn,c+1).UseTheme("db")
			''			Else
			''				cntA.Cell(2+Rn,c+1).UseTheme("claro")
			''			End If
			''			cntA.Cell(2+Rn,c+1).AddArrayComponent(inp1,"C")
			'cntA.Cell(2+Rn+1,c+1).AddComponent(ABMShared.Buildline(page,"1"))
		Next
		
		Dim btn1 As ABMButton
		btn1.InitializeFloating(page,conteiner&rnmas1&6,"","")
		btn1.Size=ABM.BUTTONSIZE_SMALL
		If temaC=False Then
			cntA.Cell(2+Rn,7).UseTheme("claro2")
		Else
			cntA.Cell(2+Rn,7).UseTheme("claro")
		End If
		btn1.UseTheme("b6")
		cntA.Cell(2+Rn,7).AddArrayComponent(btn1,"C")
		If temaC=False Then
			temaC=True
		Else
			temaC=False
		End If
		
		''		Dim inp1 As ABMCheckbox
		''		inp1.Initialize(page,conteiner&rnmas1&6,"-",False,"")
		''		inp1.IsFilled = True
		''		If temaC=False Then
		''			cntA.Cell(2+Rn,7).UseTheme("")
		''			temaC=True
		''		Else
		''			cntA.Cell(2+Rn,7).UseTheme("claro")
		''			temaC=False
		''		End If
		''		cntA.Cell(2+Rn,7).AddArrayComponent(inp1,"C")
		'cntA.Cell(2+Rn+1,7).AddComponent(ABMShared.Buildline(page,"1"))
				
		'	Dim lblAR1 As ABMLabel
		'	lblAR1.Initialize(page,"lblAR1","-",ABM.SIZE_A,False,"")
		'	cntA.Cell(2+Rn,8).UseTheme("celda4")
		'	cntA.Cell(2+Rn,8).AddComponent(lblAR1)
		'	cntA.Cell(2+Rn+1,8).AddComponent(ABMShared.Buildline(page,"1"))
	
		'	Dim lblAR As ABMLabel
		'	lblAR.Initialize(page,"T"&conteiner&rnmas1,"0",ABM.SIZE_A,False,"")
		'	cntA.Cell(2+Rn,9).UseTheme("celda4")
		'	cntA.Cell(2+Rn,9).AddComponent(lblAR)
		'	cntA.Cell(2+Rn+1,9).AddComponent(ABMShared.Buildline(page,"1"))
		
	Next
	page.Cell(Rx ,Cx).AddComponent(cntA)
End Sub


Sub cmbPuesto_Clicked(itemId As String)
	'Dim cmbx As ABMCombo=page.Component("cmbPuesto")
	'Dim cmby As ABMCombo=page.Component("cmbDepartamento")
	Dim cmbz As ABMCombo=page.Component("cmbempleados")
	cmbz.Clear
	cmbz.refresh
	Dim query As String
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	page.Pause
	sql1.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
	query ="Select * FROM OPENQUERY(GIN, 'select * from "&ws.Session.GetAttribute("DBUse")&".dbo.vw_lgen_altasybajas where Pkey_Pos_Key=''"&itemId&"''')"
	rst=sql1.ExecQuery(query)
	pos=0
	'cmbempleados.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbz.AddItem( rst.GetString2(0),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
		pos=pos+1
	Loop
	rst.close
	page.resume
	cmbz.Refresh
End Sub



Sub cmbDepartamento_Clicked(itemId As String)
	Dim cmbx As ABMCombo=page.Component("cmbPuesto")
	Dim cmby As ABMCombo=page.Component("cmbPuesto")
	cmby.Clear
	cmby.Refresh
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_Positons where Pos_Dep_Key='" & itemId & "' order by 3")
	cmbx.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbx.AddItem(rst.GetString2(0),rst.GetString2(2),BuildSimpleItem("P"& pos,"",rst.GetString2(2)))
		pos=pos+1
	Loop
	rst.Close
	cmbx.Refresh
End Sub

Sub salvar_Clicked(Target As String)
	
	
	Dim sql1 As SQL
	Dim valores(), valores2()  As Object
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	Dim rst As ResultSet
	Dim pos As Int=0
	Dim idobj As Int
	Dim sqlt As String
		
	
	Dim i,j As Int = 1
	For i =1 To contenedores.Size
		
		Dim lst As List
		lst.Initialize
		lst.Clear
		lst=File.ReadList(Main.dirfiles,contenedores.Get(i-1) & ".txt")
		Dim contenedor As ABMContainer =page.Component(contenedores.Get(i-1))
		Dim letra As String = contenedores.Get(i-1)
		Dim sumar As Int = 0
		
		For j = 1 To lst.Size
			
			'Dim total As ABMLabel = contenedor.Cell(1+j,9).Component("T"&contenedores.Get(i-1)&j)
			Dim obj As ABMLabel = contenedor.Cell(1+j,1).Component("O"&letra&j)
			
			Dim keynotas As String  = letra.ToLowerCase&j
			If notas.ContainsKey(keynotas) Then
				Dim nota As Int = notas.Get(keynotas)
			Else
				Dim nota As Int = 0
			End If
				
			Log (nota)
			sumar  = sumar + nota
			' AQUI SE SALVAN LAS NOTAS EN LA BASE DE DATOS, "NOTA" ES EL TOTAL DE CADA LINEA DEL CONTENEDOR
			' contenedores.Get(i-1) trae la letra del contenedor
			' j trae la linea
			
			Dim valores()  As Object

			valores= Array As Object (letra.ToUpperCase,j,obj.Text,Idencuesta,IdEntrevistado,Relacion,nota,Null,Null,DateTime.Date(DateTime.Now),Null,Null, Null, ws.Session.GetAttribute("DBUse") )
			sql1.ExecNonQuery2("INSERT INTO RH_EVALUACION360 VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,? )", valores)
			
			
						
		Next
		
		Dim notasno0 As Int = 0  ' CALCULO DEL PROMEDIO SIN LAS NOTAS VALOR 0
			
		For Each k As String In notas.Keys
			Dim letramap As String = k.SubString2(0,1).ToLowerCase
				
			If letramap = letra.ToLowerCase Then
				notasno0 = notasno0 + 1
			End If
				
		Next
		
		
		' VARIABLE DEL PROMEDIO DEL TOTAL SI SE DESEA MOSTRAR
		If Not(notasno0 = 0) Then
			Dim promedio As Float = sumar / notasno0
		End If
	   
		Log ("el promedio del contenedor "&contenedores.Get(i-1)&" es "&promedio)
	   
	   
	Next
	ABMShared.NavigateToPage(page.ws, page.GetPageID, "../acceso/acceso.html")
	
End Sub

Sub  C_Clicked(Target As String)
	
	Dim a As String = Target
	Log (Target)
	Dim cont As String = a.SubString2(1,2).ToLowerCase
	Dim fila As Int = a.SubString2(2,3)
	Dim pos As Int =  a.SubString2(3,4)
	
	If notas.ContainsKey(cont&fila) Then
		notas.Remove(cont&fila)
	End If
	Dim contenedor As ABMContainer =page.Component(cont)
	
	
	Dim i As Int
	
	For i = 1 To calif+1
		Dim btnx As ABMButton = contenedor.Cell(1+fila,i+1).Component("c"&cont&fila&i)
		
		If Not(i = pos) Then
			'check.State = False
			btnx.IconName=""
			If i=1 Then
				btnx.UseTheme("b1")
			else if i=2 Then
				btnx.UseTheme("b2")
			else if i=3 Then
				btnx.UseTheme("b3")
			else if i=4 Then
				btnx.UseTheme("b4")
			else if i=5 Then
				btnx.UseTheme("b5")
			End If
		Else
			btnx.IconName="done"
			If i=1 Then
				btnx.UseTheme("b1c")
			else if i=2 Then
				btnx.UseTheme("b2c")
			else if i=3 Then
				btnx.UseTheme("b3c")
			else if i=4 Then
				btnx.UseTheme("b4c")
			else if i=5 Then
				btnx.UseTheme("b5c")
			End If
		End If
		btnx.Refresh
	Next
	
	If Not(pos = 6) Then
		notas.Put(cont&fila,pos)
	End If
	
	
	'Dim total As ABMLabel = contenedor.Cell(1+fila,9).Component("T"&cont&fila)
	'total.Text = pos
	'total.Refresh
End Sub





' clicked on the navigation bar
Sub Page_NavigationbarClicked(Action As String, Value As String)
	' saving the navigation bar position
	page.SaveNavigationBarPosition
	If Action = "LogOff" Then
		ABMShared.LogOff(page)
		Return
	End If

	ABMShared.NavigateToPage(ws, ABMPageId, Value)
End Sub
'	Dim inpR1C1 As ABMInput
'	inpR1C1.Narrow=True
'	inpR1C1.Initialize(page,"inpR1C1",ABM.INPUT_text,"",False,"")
'	cntA.Cell(2,2).AddComponent(inpR1C1)
'	Dim inpR1C2 As ABMInput
'	inpR1C2.Narrow=True
'	inpR1C2.Initialize(page,"inpR1C2",ABM.INPUT_text,"",False,"")
'	cntA.Cell(2,3).AddComponent(inpR1C2)
'	Dim inpR1C3	 As ABMInput
'	inpR1C3.Narrow=True
'	inpR1C3.Initialize(page,"inpR1C3",ABM.INPUT_text,"",False,"")
'	cntA.Cell(2,4).AddComponent(inpR1C3)
'	Dim inpR1C4 As ABMInput
'	inpR1C4.Narrow=True
'	inpR1C4.Initialize(page,"inpR1C4",ABM.INPUT_text,"",False,"")
'	cntA.Cell(2,5).AddComponent(inpR1C4)
'	Dim inpR1C5 As ABMInput
'	inpR1C5.Narrow=True
'	inpR1C5.Initialize(page,"inpR1C5",ABM.INPUT_text,"",False,"")
'	cntA.Cell(2,6).AddComponent(inpR1C5)
'	Dim inpR1C6	 As ABMInput
'	inpR1C6.Narrow=True
'	inpR1C6.Initialize(page,"inpR1C6",ABM.INPUT_text,"",False,"")
'	cntA.Cell(2,7).AddComponent(inpR1C6)

Sub Page_FileUploaded(FileName As String, success As Boolean)
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
	'	If icon <> "" Then
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	'	Else
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
		combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next
	
	'page.Refresh
	'combo1.Refresh
	Return combo1
	

End Sub
