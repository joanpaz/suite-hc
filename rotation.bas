﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@

'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "rotation"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = ""
	' your own variables		
	Dim myToastId As Int = 1
	Dim BackDate1 As Long
	Dim BackDate2 As Long
	'Dim years As List
	'Dim months As List
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	ConnectPage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------	
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")	
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
End Sub

public Sub BuildTheme()
	' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	
	theme.AddCellTheme("c1")
	theme.Cell("c1").BackColor=ABM.COLOR_LIME

	theme.AddLabelTheme("tlbl")
	theme.Label("tlbl").ForeColor=ABM.COLOR_RED
	' add additional themes specific for this page
	' add additional themes specific for this page
	 theme.AddTableTheme("tbl1theme")
	 theme.Table("tbl1theme").ZDepth = ABM.ZDEPTH_1
	 
	 theme.Table("tbl1theme").AddCellTheme("headerfooter")
	 theme.Table("tbl1theme").Cell("headerfooter").BackColor = ABM.COLOR_BROWN
	 theme.Table("tbl1theme").Cell("headerfooter").BackColorIntensity=ABM.INTENSITY_DARKEN4
	 theme.Table("tbl1theme").Cell("headerfooter").ActiveBackColorIntensity = ABM.INTENSITY_DARKEN4
	 theme.Table("tbl1theme").Cell("headerfooter").ForeColor = ABM.COLOR_WHITE 
	 theme.Table("tbl1theme").Cell("headerfooter").Align = ABM.TABLECELL_HORIZONTALALIGN_CENTER
	 
	 
	 theme.Table("tbl1theme").AddCellTheme("headerfooterright")
	 theme.Table("tbl1theme").Cell("headerfooterright").BackColor = ABM.COLOR_BLUEGREY
	 theme.Table("tbl1theme").Cell("headerfooterright").ForeColor = ABM.COLOR_WHITE
	 theme.Table("tbl1theme").Cell("headerfooterright").Align = ABM.TABLECELL_HORIZONTALALIGN_RIGHT
	 
	 theme.Table("tbl1theme").AddCellTheme("nocolor")
	 theme.Table("tbl1theme").Cell("nocolor").ActiveBackColor = ABM.COLOR_BLUEGREY
	 theme.Table("tbl1theme").Cell("nocolor").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN2
	 
	 theme.Table("tbl1theme").AddCellTheme("nocoloredit")
	 theme.Table("tbl1theme").Cell("nocoloredit").ActiveBackColor = ABM.COLOR_BLUEGREY
	 theme.Table("tbl1theme").Cell("nocoloredit").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN2
	 theme.Table("tbl1theme").Cell("nocoloredit").IsEditable = True
	 theme.Table("tbl1theme").Cell("nocoloredit").InputMask = "'alias':'decimal', 'digits': 2"
	 theme.Table("tbl1theme").Cell("nocoloredit").AllowEnterKey = False
	 
	 theme.Table("tbl1theme").AddCellTheme("positive")
	 theme.Table("tbl1theme").Cell("positive").BackColor = ABM.COLOR_GREEN
	 theme.Table("tbl1theme").Cell("positive").BackColorIntensity = ABM.INTENSITY_LIGHTEN3
	 theme.Table("tbl1theme").Cell("positive").ActiveBackColor = ABM.COLOR_GREEN
	 theme.Table("tbl1theme").Cell("positive").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN1
	 theme.Table("tbl1theme").Cell("positive").Align = ABM.TABLECELL_HORIZONTALALIGN_RIGHT
	 
	 theme.Table("tbl1theme").AddCellTheme("negative")
	 theme.Table("tbl1theme").Cell("negative").BackColor = ABM.COLOR_RED
	 theme.Table("tbl1theme").Cell("negative").BackColorIntensity = ABM.INTENSITY_LIGHTEN3
	 theme.Table("tbl1theme").Cell("negative").ActiveBackColor = ABM.COLOR_RED
	 theme.Table("tbl1theme").Cell("negative").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN1
	 theme.Table("tbl1theme").Cell("negative").Align = ABM.TABLECELL_HORIZONTALALIGN_RIGHT
	 
	 theme.Table("tbl1theme").AddCellTheme("aligntop")
	 theme.Table("tbl1theme").Cell("aligntop").VerticalAlign = ABM.TABLECELL_VERTICALALIGN_TOP
	 theme.Table("tbl1theme").Cell("aligntop").BorderColor = ABM.COLOR_BLACK
	 theme.Table("tbl1theme").Cell("aligntop").BorderWidth = 1
	 'theme.Table("tbl1theme").Cell("aligntop").ZDepth = ABM.ZDEPTH_1
	 theme.Table("tbl1theme").Cell("aligntop").Align = ABM.TABLECELL_HORIZONTALALIGN_CENTER

End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "rotation.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' adding a navigation bar
	ABMShared.BuildNavigationBar(page,"Indicadores por rotación","../images/klogo3.png","","rotation","")
	
			
	' create the page grid
	'page.AddRows(6,True, "").AddCellsOSMP(1,0,0,0,12,12,12,0,0,-10,-5,"")
	
	page.AddRows(2,False, "").AddCellsOSMP(2,0,1,1,12,10,10,0,0,0,0,"")
	
	page.AddRows(4,False, "").AddCellsOSMP(1,0,0,0,12,12,12,0,0,-5,-10,"")
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()
'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)

'	init all your own variables (like a List, Map) and add your components
	Dim cnt As ABMContainer
	cnt.Initialize(page,"cnt","cnt")
	cnt.AddRows(1,True,"").AddCells12(1,"")
	cnt.AddRows(2,True,"").AddCellsOSMP(1,0,0,0,6,2,2,0,0,0,0,"").AddCellsOSMP(3,0,1,1,6,2,2,0,0,0,0,"")
	cnt.AddRow(1,True,"","")
	cnt.AddRow(2,True,"","")
	cnt.BuildGrid
	page.Cell(2,1).AddComponent(cnt)
	Dim lblt As ABMLabel
	lblt.Initialize(page,"lblt","Filtros",ABM.SIZE_A,False,"etiq")
	cnt.Cell(1,1).MarginTop="1%"
	cnt.Cell(1,1).UseTheme("celda2")
	cnt.Cell(1,1).AddComponent(lblt)
	cnt.Cell(2,1).AddComponent(buildcombo("monthin","","Mes inicial", getmonths))
	cnt.Cell(2,2).AddComponent(buildcombo("yearin","","Año inicial", getyears))
	cnt.Cell(2,3).AddComponent(buildcombo("monthfin","","Mes final", getmonths))
	cnt.Cell(2,4).AddComponent(buildcombo("yearfin","","Año final", getyears))
	Dim btna As ABMButton
	btna.InitializeFlat(page,"btna","","","Ok","btn")
	cnt.Cell(4,1).UseTheme("celda3")
	cnt.Cell(4,1).AddComponent(btna)
	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub
Sub fecin_Changed(dateMilliseconds As String)
	BackDate1=dateMilliseconds
End Sub
Sub fecfin_Changed(dateMilliseconds As String)
	BackDate2=dateMilliseconds
End Sub
Sub btna_Clicked(Target As String)
	'crea	
	'-----inicio
	Dim cnt As ABMContainer = page.Component("cnt")
	Dim monthin, monthfin, yearin, yearfin As ABMCombo
	monthin = cnt.Cell(2,1).Component("monthin") 
	yearin = cnt.Cell(2,2).Component("yearin") 
	monthfin = cnt.Cell(2,3).Component("monthfin") 
	yearfin = cnt.Cell(2,4).Component("yearfin") 
	If monthin.GetActiveItemId = -1 Then
		page.Msgbox("msg1", "Seleccione fecha inicial","Error","ok","")
		Return
	End If
	If monthfin.GetActiveItemId = -1 Then
		page.Msgbox("msg1", "Seleccione fecha final","Error","ok","")
		Return
	End If
	If yearin.GetActiveItemId = -1 Then
		page.Msgbox("msg1", "Seleccione fecha inicial","Error","ok","")
		Return
	End If
	If yearfin.GetActiveItemId = -1 Then
		page.Msgbox("msg1", "Seleccione fecha final","Error","ok","")
		Return
	End If
	DateTime.DateFormat="MM/dd/yyyy"	
	Dim fecha1, fecha2, year As String
	Dim y,m1, m2,d As Int
	Dim l As ABMLabel 
	l=yearin.GetComponent(yearin.GetActiveItemId)
	y=l.ID
	year = y
	m1=monthin.GetActiveItemId+1
	fecha1 = DateTime.Date(DateUtils.SetDate( y,m1,1))
	l=yearfin.GetComponent(yearfin.GetActiveItemId)
	y=l.id
	m2=monthfin.GetActiveItemId+1
	d=DateUtils.NumberOfDaysInMonth(m2,y)
	fecha2 = DateTime.Date(DateUtils.SetDate( y,m2,d))
	Dim tam As Int = (m2 - m1) + 3
	'---Fin
	page.Pause
	Dim cnt2 As ABMContainer
	cnt2.Initialize(page,"cnt2","cnt")
	cnt2.AddRow(1,False,"","")
	cnt2.AddRows(3,False,"").AddCellsOS(2,0,1,1,12,3,3,"")
	cnt2.AddRows(1,False,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,20,60,"") '.AddCellsOSMP(1,0,0,0,12,6,6,0,0,10,0,"")
	cnt2.AddRows(4,False,"").AddCells12(1,"")
	cnt2.BuildGrid
	Dim img As ABMImage
	img.Initialize(page,"img","../images/klogo2.jpg",3)
	cnt2.Cell(2,1).SetOffsetSize(1,1,1,12,12,12)
	cnt2.Cell(2,1).AddComponent(img)
	cnt2.Cell(3,1).UseTheme("celda3")
	
	Dim Text1 As String
	Text1="HOTEL " & ws.Session.GetAttribute("location") & "{BR}Indicadores por rotación"

	cnt2.Cell(3,1).AddComponent(ABMShared.BuildParagraph(page,"p",Text1,ABM.SIZE_H5))
	

		

	Dim tbl1 As ABMTable	
	tbl1.Initialize(page,"tbl1",False,False,True,"tbl1theme")
	
	
	Dim i As Int
	Dim listah, listaht, listart, listar As List
	listah.Initialize
	listaht.Initialize
	listar.Initialize
	listart.Initialize
	'vacios.Initialize
	
	listah.add("Concepto")
	listaht.add("headerfooter")
	listart.add("aligntop")
	'vacios.add(" ")
	
	
	For i=m1-1 To m2-1
		listah.Add(getmonths.Get(i))
		listaht.add("headerfooter")
		listart.add("aligntop")	
	'	vacios.add(" ")
	Next
	
	listah.add(year)
	listaht.add("headerfooter")
	listart.add("aligntop")
	'vacios.add(" ")
	
	
	
'	tbl1.SetHeaders(Array As String("Concept","January","February","March","April","May","June","July","August","September","October","November","December",year))
'	tbl1.SetHeaderThemes(Array As String("headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter"))
	tbl1.SetHeaders(listah)
	tbl1.SetHeaderThemes(listaht)

	Dim sql1 As SQL
	Dim rst As ResultSet
	'sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
	Log("Antes " & DateTime.Time(DateTime.Now))
	
	'Log(fecha1)
	sql1.ExecNonQuery("EXEC [GIN]." & ws.Session.GetAttribute("DBUse") & ".dbo.PT_rotacion '456465', 'epaz','"  & fecha1 & "','" & fecha2& "'")
	
	rst=sql1.ExecQuery("Select * FROM OPENQUERY(GIN,'select * from " & ws.Session.GetAttribute("DBUse") & ".dbo.vw_PT_rotacionepaz')")
	Log("despues " & DateTime.Time(DateTime.Now))
	Do While rst.NextRow
		Dim valor() As String
		'valor=Array As String(rst.GetString("CONCEPTOS"),rst.GetString("Enero"),rst.GetString("Febrero"),rst.GetString("Marzo"),rst.GetString("Abril"),rst.GetString("Mayo"),rst.GetString("Junio"),rst.GetString("Julio"),rst.GetString("Agosto"),rst.GetString("Septiembre"),rst.GetString("Octubre"),rst.GetString("Noviembre"),rst.GetString("Diciembre"),rst.GetString("2016"))
		If rst.GetString("CONCEPTOS").Trim="" Then
			'valor=Array As String("  "," "," "," "," "," "," "," "," "," "," "," "," "," ")			
			listar.Initialize
			For i=0 To tam-1
				
				listar.Add(" ")
			Next
		
		Else  
		
			listar.Initialize
			listar.Add(rst.GetString("CONCEPTOS"))
			'Dim col As Int = 3
			For i=0 To tam-2
				
				listar.Add(Round2(rst.GetString2(i+3),2)&" ")
				'col = col+1
			Next
			
			'listar.Add(rst.GetString(year))
			
			'valor=Array As String(rst.GetString("CONCEPTOS"),Round2(rst.GetString("Enero"),2),Round2(rst.GetString("Febrero"),2),Round2(rst.GetString("Marzo"),2),Round2(rst.GetString("Abril"),2),Round2(rst.GetString("Mayo"),2),Round2(rst.GetString("Junio"),2),Round2(rst.GetString("Julio"),2),Round2(rst.GetString("Agosto"),2),Round2(rst.GetString("Septiembre"),2),Round2(rst.GetString("Octubre"),2),Round2(rst.GetString("Nombiembre"),2),Round2(rst.GetString("Diciembre"),2),rst.GetString(year))
		
		
		End If
		'tbl1.AddRow("P" & rst.GetInt("idciunter"),	valor)
		tbl1.AddRow("P" & rst.GetInt("idciunter"),	listar)
		'tbl1.SetRowThemes(Array As String("aligntop","aligntop","aligntop","aligntop","aligntop","aligntop","aligntop","aligntop","aligntop","aligntop","aligntop","aligntop","aligntop","aligntop"))
		 
		 tbl1.SetRowThemes(listart)
	Loop
	tbl1.SetFooter("Tabla informativa", 14,"headerfooter")
	cnt2.Cell(4,1).AddComponent(ABMShared.BuildParagraph(page,"p1","Periodo consultado desde " & ABMShared.ConvertDate(fecha1,"MM/dd/yyy","MMMM dd yyyy") & " hasta " & ABMShared.ConvertDate(fecha2,"MM/dd/yyy","MMMM dd yyyy"),ABM.SIZE_A))



	cnt2.Cell(5,1).AddComponent(tbl1)
	page.Cell(6,1).RemoveAllComponents
	page.Cell(6,1).AddComponent(cnt2)
	page.Cell(6,1).SetOffsetSize(0,0,0,12,12,12)
	page.Refresh
	page.Resume
End Sub




Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then 
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
'	If icon <> "" Then
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
'	Else
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,200,"combo")
	
	Dim i As Int 
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
	     combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next 
	
	'page.Refresh
	'combo1.Refresh	   
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid 
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)	
	Return myModal
End Sub


Sub getyears() As List
	Dim year As List
	year.Initialize
	For i=DateTime.GetYear(DateTime.Now) To 1990 Step -1
		year.Add(i)
	Next
	Return year
End Sub
Sub getmonths() As List
	Dim months As List
	months.Initialize
	months.Add("Enero")
	months.Add("Febrero")
	months.Add("Marzo")
	months.Add("Abril")
	months.Add("Mayo")
	months.Add("Junio")
	months.Add("Julio")
	months.Add("Agosto")
	months.Add("Septiembre")
	months.Add("Octubre")
	months.Add("Noviembre")
	months.Add("Diciembre")
	Return months
End Sub




