﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "configuracion"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "3"
	' your own variables
	Dim myToastId As Int = 1
	
	'variables de uplload
	Public DownloadFolder As String = "/www/" & ABMShared.AppName & "/perfiles/"
	Public DownloadMaxSize As String = 1000*1024
	'Dim myToastId As Int = 1
	Private sql1 As SQL
	Public fondoe, fotoe As String
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------
	Log("Connected")
	ws = WebSocket1
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
		' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
		page.Refresh
		page.FinishedLoading
	Else
		If page.WebsocketReconnected Then
			Log("Websocket reconnected")
			' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
			' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
			ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
		Else
			' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
			Log("Websocket first connection")
			page.Prepare
			ConnectPage
		End If
	End If
	Log(ABMPageId)
	'----------------------MODIFICATION-------------------------------
		
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")
	' and remove them again
	Try
		ws.Session.RemoveAttribute("abmcallback")
		ws.Session.RemoveAttribute("abmdownloadfolder")
		ws.Session.RemoveAttribute("abmmaxsize")
	Catch
		Log(LastException.Message)
	End Try
End Sub



Sub Page_ParseEvent(Params As Map)
	
	
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)
		End Select
	End If
End Sub

public Sub BuildTheme()
	
	
	
	' start with the base theme defined in comunes
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	
	' add additional themes specific for this page
	theme.AddUploadTheme("upload")
	theme.Upload("upload").ErrorColor = ABM.COLOR_RED
	theme.Upload("upload").BackColor = ABM.COLOR_BROWN
	theme.Upload("upload").BackColorIntensity = ABM.INTENSITY_DARKEN4
	theme.Upload("upload").DropZoneBackColor =  ABM.COLOR_WHITE
	'theme.Upload("upload").DropZoneBackColorIntensity =
	theme.Upload("upload").ButtonHoverBackColor = ABM.COLOR_ORANGE
	theme.Upload("upload").ButtonHoverBackColorIntensity = ABM.INTENSITY_DARKEN4
	theme.Upload("upload").ButtonBackColor = ABM.COLOR_BROWN
	theme.Upload("upload").ButtonBackColorIntensity = ABM.INTENSITY_DARKEN4
	theme.Upload("upload").ForeColor = ABM.COLOR_BLACK
	theme.Upload("upload").ForeColorIntensity = ABM.INTENSITY_DARKEN4
	theme.Upload("upload").UploadProgressColor = ABM.COLOR_ORANGE
	theme.Upload("upload").UploadProgressColorIntensity = ABM.INTENSITY_DARKEN4
	theme.Upload("upload").ButtonForeColor = ABM.COLOR_WHITE
	theme.Upload("upload").ZDepth = ABM.ZDEPTH_4
	
	'theme.Upload("upload").
	
	theme.AddInputTheme("inp1")
	theme.Input("inp1").ZDepth = ABM.ZDEPTH_3
	
	
	
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	page.SetLoaderJUMPINGBALL(ABM.COLOR_BLACK, ABM.INTENSITY_NORMAL, ABM.COLOR_WHITE, ABM.INTENSITY_NORMAL, "Por favor espere")
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "configuracion.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' adding a navigation bar
	
	ABMShared.BuildNavigationBar(page, "Configuración ","../images/klogo3.png","","configuracion","")
			
	' create the page grid
	page.AddRows(1,True, "").AddCells12MP(1,20,0,0,0,"") '1
	page.AddRows(1,True, "").AddCells12MP(1,20,0,0,0,"") '2
	page.AddRows(1,True, "").AddCells12MP(2,20,0,0,0,"") '3
	page.AddRows(1,True, "").AddCells12MP(4,20,0,0,0,"") '4
	page.AddRows(1,True, "").AddCells12MP(2,20,0,0,0,"") '5
	page.AddRows(1,True, "").AddCells12(2,"") '4
	page.AddRows(1,True, "").AddCells12(1,"") '5
	page.AddRows(1,True, "").AddCells12(3,"") '6
	page.AddRows(1,True, "").AddCells12(1,"") '7
	page.AddRows(1,True, "").AddCells12(1,"") '8
	page.AddRows(1,True, "").AddCells12(3,"") '9
	page.AddRows(1,True, "").AddCells12(1,"") '10
	page.AddRows(1,True, "").AddCells12(3,"") '11
	page.AddRows(1,True, "").AddCells12(1,"") '12
	page.AddRows(1,True, "").AddCells12(1,"") '13
	page.AddRows(1,True, "").AddCells12(1,"") '14   idioma
	page.AddRows(1,True, "").AddCells12(2,"") '15
	page.AddRows(1,True, "").AddCells12(1,"") '16
	page.AddRows(1,True, "").AddCells12(1,"") '17
	page.AddRows(1,True, "").AddCells12(1,"") '18
	page.AddRows(1,True, "").AddCells12(1,"") '19
	page.AddRows(1,True, "").AddCells12(1,"") '20
	page.AddRows(1,True, "").AddCells12(1,"") '21
	page.AddRows(1,True, "").AddCells12(1,"") '22
	page.AddRows(1,True, "").AddCells12(1,"") '23
	page.AddRows(1,True, "").AddCells12(1,"") '24
	page.AddRows(1,True, "").AddCells12(1,"") '25
	page.AddRows(1,True, "").AddCells12(1,"") '26
	page.AddRows(1,True, "").AddCells12(1,"") '27
	page.AddRows(1,True, "").AddCells12(1,"") ' 28 EL BOTON ACEPTAR PARA GUARDAR
	page.AddRows(1,True, "").AddCells12(1,"") '29
			
	
	
		
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	
	page.AddExtraCSSFile("custom/spectrum.css")
	page.AddExtraJavaScriptFile("custom/spectrum.js")
	
	'	page.AddExtraJavaScriptFile("custom/jquerry.js")
	'	page.AddExtraJavaScriptFile("custom/colorpicker.js")
	'	page.AddExtraJavaScriptFile("custom/eye.js")
	'	page.AddExtraJavaScriptFile("custom/utils.js")
	'	page.AddExtraJavaScriptFile("custom/layout.js?ver=1.0.2")
	'
	'	page.AddExtraCSSFile("custom/colorpicker.css")
	'	page.AddExtraCSSFile("custom/layout.css")
	
	
		
End Sub

public Sub ConnectPage()
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
		
	
	ABMShared.coloreafondo(page)
	
	'	init all your own variables (like a List, Map) and add your components
	Dim fila As Int = 2
	Dim columna As Int = 1
	
	page.AddModalSheetTemplate(Mensajes)
	
	Dim contenedora As ABMContainer
	contenedora.Initialize(page,"contenedora","cardtheme2")
	'contenedora.AddRows
	
	contenedora.AddRows(3,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	'contenedora.AddRows(2,True,"").AddCellsOSMP(1,0,0,0,12,6,4,20,0,0,0,"").AddCellsOSMP(1,0,0,0,12,2,1,20,0,10,0,"").AddCellsOSMP(1,0,0,0,12,6,5,20,0,20,0,"").AddCellsOSMP(1,0,0,0,12,6,1,20,0,15,0,"")
	
	contenedora.BuildGrid
	page.Cell(1,1).SetOffsetSize(0,0,0,12,6,6)
	page.Cell(1,1).AddComponent(contenedora)
	
	
	Dim lbl As ABMLabel
	lbl.Initialize(page,"lbl","Escoja una opción a personalizar:",ABM.SIZE_H6,False,"etiq")
	contenedora.Cell(1,1).AddComponent(lbl)
	contenedora.Cell(1,1).UseTheme("celda2")
	
	Dim radioup As ABMRadioGroup
	radioup.Initialize(page, "radioup", "")
	
	radioup.AddRadioButton("Cargar foto de perfil", True)
	radioup.AddRadioButton("Cargar foto del fondo", True)
	
	contenedora.Cell(2,1).AddComponent(radioup)
	
	
	
	
	Dim contenedorb As ABMContainer
	contenedorb.Initialize(page,"contenedorb","cardtheme2")
	'contenedora.AddRows
	
	contenedorb.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	contenedorb.AddRows(1,True,"").AddCellsOSMP(1,0,0,3,12,12,6,0,0,0,0,"")
	contenedorb.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,6,4,20,0,0,0,"").AddCellsOSMP(1,0,0,0,12,2,1,20,0,10,0,"").AddCellsOSMP(1,0,0,0,12,6,5,20,0,20,0,"").AddCellsOSMP(1,0,0,0,12,6,1,20,0,15,0,"")
	contenedorb.AddRows(1,True,"").AddCellsOSMP(1,0,0,4,12,12,6,20,0,0,0,"")
	contenedorb.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	
	contenedorb.BuildGrid
	page.Cell(4,1).SetOffsetSize(0,0,0,12,6,6)
	page.Cell(4,1).AddComponent(contenedorb)
	
	
	Dim lbl As ABMLabel
	lbl.Initialize(page,"lbl","Otras opciones para el fondo:",ABM.SIZE_H6,False,"etiq")
	contenedorb.Cell(1,1).AddComponent(lbl)
	contenedorb.Cell(1,1).UseTheme("celda2")
	
	Dim remove As ABMButton
	remove.InitializeFlat(page,"remove","","","Limpiar fondo","btn")
	contenedorb.Cell(4,1).AddComponent(remove)
	
	
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page,"lbl1","Seleccionar color de fondo",ABM.SIZE_h6,False,"")
	contenedorb.Cell(3,1).AddComponent(lbl1)
	
	
	
	Dim btnbg As ABMButton
	btnbg.InitializeFlat(page,"btnbg","","","Aplicar","btn")
	contenedorb.Cell(3,4).AddComponent(btnbg)
	
	Dim inpbg As ABMInput
	inpbg.Initialize(page,"inpbg","","",False,"")
	inpbg.Enabled=False
	contenedorb.Cell(3,3).AddComponent(inpbg)
	
	
	
	'contenedora.Cell(4,1).SetOffsetSize(0,0,0,12,6,2)
	'contenedora.Cell(4,2).SetOffsetSize(0,0,0,12,6,3)
	'contenedora.Cell(4,3).SetOffsetSize(0,0,0,12,6,3)
	'contenedora.Cell(4,4).SetOffsetSize(0,0,0,12,6,3)
	
	
	'--------------------------------
	
	'Dim contenedorb As ABMContainer
	'contenedorb.Initialize(page,"contenedorb","cardtheme2")
	'contenedorb.AddRows
	
	'contenedorb.AddRows(3,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	
	'contenedorb.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	'contenedorb.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	
	'contenedorb.BuildGrid
	'page.Cell(2,1).SetOffsetSize(0,0,0,12,6,6)
	'page.Cell(2,1).AddComponent(contenedorb)
	'contenedorb.Cell(1,1).UseTheme("celda2")
	
	
	
	
	
	Dim lbl As ABMLabel
	lbl.Initialize(page,"lbl","Seleccione el archivo:",ABM.SIZE_H6,False,"etiq")
	page.Cell(2,1).UseTheme("celda2")
	'contenedorb.Cell(1,1).AddComponent(lbl)
	
	page.Cell(2,1).SetOffsetSize(0,0,0,12,6,6)
	page.Cell(2,1).AddComponent(lbl)
	
	
	Dim upload1 As ABMUpload
	upload1.Initialize(page, "upload1", "Arrastre aquí", "Buscar", "upload")
	page.Cell(3,1).SetOffsetSize(0,0,0,12,6,6)
	page.Cell(3,1).AddComponent(upload1)
	
	
	'page.resume

	'	Dim inp1 As ABMFileInput
	'	inp1.Initialize(page, "inp1", "Select a file", "Open", True, "", "btn")
	'	page.Cell(3,1).SetOffsetSize(0,0,0,12,6,6)
	'	page.Cell(3,1).AddComponent(inp1)


	
	
	'	Dim inpbg As ABMInput
	'	inpbg.Initialize(page,"inpbg",ABM.INPUT_TEXT,"",False,"")
	'	page.Cell(4,2).AddComponent(inpbg)
	
	
	
	
	
	
	'ABM.InsertHTMLAfter(page,"r5c2",$"<link rel="stylesheet" type="text/css" href="spectrum.css">"$)
	'ABM.InsertHTMLAfter(page,"r5c2",$"<script type="text/javascript" src="spectrum.js"></script>"$)
	
	'ABM.InsertHTMLAfter(page,"r4c2",$"<div><input type='text' id="custom" /></div>"$)

	
	'preferredFormat: "hex",
	If  Not(fondoe=Null) And Not(fondoe="") And  Not(fondoe="null") And Not(fondoe="no") And Not(fondoe="#") Then
		If fondoe.Length<10 Then
			Dim script As String = $"$('body').css('background-color', '${fondoe}');
													 $('body').attr('class','');	 
											"$
			page.ws.Eval(script,Null)
		Else
			Dim su As StringUtils
			Dim BT2() As Byte= su.DecodeBase64(fondoe)
			Dim imagen As Image = BytesToImage(BT2)
			Dim carpeta As String = File.Combine(File.DirApp, DownloadFolder)
						
			Dim nomfondo As String =  ws.Session.GetAttribute("idsesion")&"b.jpg"
			Dim borro As Boolean = False
			If File.Exists (carpeta,  ws.Session.GetAttribute("idsesion")&"b.jpg") Then
				borro = File.Delete(carpeta,  ws.Session.GetAttribute("idsesion")&"b.jpg")
			End If
			
			Dim Out As OutputStream = File.OpenOutput(carpeta,  ws.Session.GetAttribute("idsesion")&"b.jpg" , True)
			imagen.WriteToStream(Out)
			Out.Close
						
			Dim script As String = $"$('body').css('background-image', 'url(../perfiles/${nomfondo})');
						
						 $('body').css('background-size', 'cover');
						 
							"$
			page.ws.Eval(script,Null)
			
		End If
	End If

	
	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
	
	
	ABM.AddHTML(page,"contenedorb-r3c2",$"<input type='text' id="custom" />"$)
	Dim script As String = $"$("#custom").spectrum({
	
    allowEmpty:true,
    showInitial: true,
    showInput: true,
	
	change: function(color) {
  	$("#contenedorb-inpbg").attr("value",color.toHexString());
	}
	
	
});


	

		 							"$
	
	page.ws.Eval(script,Null)
	
	
	Dim script As String = $"$("#contenedorb-inpbg").attr("value","#");
												"$
	page.ws.Eval(script,Null)
	
	
	'	Dim script As String = $"$("#inpbg").attr("class","jscolor");
	'							$("#inpbg").attr("value","ab2567");
	'							$("#inpbg").removeAttr("evname");
	'							$("#inpbg").removeAttr("style");
	'							$("#inpbg").removeAttr("type");
	'							$("#inpbg").removeAttr("id");
	'		 							"$
	'
	'
	'           				page.ws.Eval(script,Null)

					
	'	ABM.AddHTMLPageComponent(page,$"<p id="colorpickerHolder"></p>"$)
	'page.ws.Flush
			
		
	page.Refresh
End Sub


Sub btnbg_Clicked(Target As String)
	
	'
	'var t = $("#custon").spectrum("get");
	't = t.toHex()
	
	Dim contenedor As ABMContainer = page.Component("contenedorb")
	Dim input As ABMInput = contenedor.Cell(3,3).Component("inpbg")
	
	
	If input.Text="" Or input.Text="#" Then
		page.ShowToast(masuno(myToastId),"toast1","Background color no selected!",3000)
		Return
	Else
		sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")

	
		sql1.ExecNonQuery("Update RH_UserConf set nom_fondo ='"&input.Text&"' where nom_user='"&Main.username&"'")
		Main.fondoe=input.Text
		myToastId = myToastId + 1
		page.ShowToast(masuno(myToastId),"toast1","Background color changed!",3000)
	
	
		Dim script As String = $"$('body').css('background-color','${input.Text}');
								$('body').attr('class','');			 
										"$

					
		page.ws.Eval(script,Null)
	

	End If
	
	
End Sub

Sub remove_Clicked(Target As String)
	'Dim contenedor As ABMContainer = page.Component("contenedora")
	'Dim remove As ABMButton = contenedor.Cell(3,1).Component("remove")
	
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")

	sql1.ExecNonQuery("Update RH_UserConf set nom_fondo ='no' where nom_user='"&Main.username&"'")
	Main.fondoe="no"
	myToastId = myToastId + 1
	page.ShowToast(masuno(myToastId),"toast1","Background image deleted!",3000)
	
	Dim script As String = $"$('body').attr('class','white loaded');
										"$
	page.ws.Eval(script,Null)
End Sub


Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	
	
	If SubTarget="" Then
		Log("CLICK EN CONF")
		Return
	End If
	
	If SubTarget="sms" Then
		
		Dim script As String = $"$('body').css('background-color','#ff0000');
								$('body').attr('class','');			 
										"$

					
		page.ws.Eval(script,Null)
						
						
		'page.Refresh
	
	End If
	
	
	If SubTarget="ayuda" Then
		
		Dim script As String = $"$('body').css('background-image', 'url(../perfiles/default.jpg)');
		   
		   							$('body').css('background-size', 'cover');
		   "$

		page.ws.Eval(script,Null)
			
		'page.Refresh
		page.ws.Flush
		
	
	End If
	
	
	If SubTarget="conf" Then
	
		'page.Pause
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
		'page.Resume
	End If
	
	'	Dim myTexts, myReturns As List
	'	myTexts.Initialize
	'	myReturns.Initialize
	'	myToastId = myToastId + 1
	'	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	'Return
	
	
	
End Sub

Sub inp1_Changed(value As String)
	Log("value : " & value)
	
	If Not(value.Contains(".jpg")) Then
		page.ShowToast("toast" & masuno(myToastId), "toastgreen", "Archivo debe ser .jpg ", 5000)
		Return
	End If
	
	'Dim contenedorb As ABMContainer = page.Component("contenedorb")
	Dim inp1 As ABMFileInput  = page.Cell(3,1).Component("inp1")
	
	
	Dim carpeta As String = File.Combine(File.DirApp, DownloadFolder)
	Dim borro As Boolean = False
	If File.Exists (carpeta,value) Then
		borro = File.Delete(carpeta,value)
	End If
	
	
	
	page.Pause
	inp1.UploadToServer 'Here the File Is uploading
	
End Sub




Sub Page_FileUploaded(FileName As String, success As Boolean)
	myToastId = myToastId + 1
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	'Dim contenedorb As ABMContainer = page.Component("contenedorb")
	' Dim inp1 As ABMFileInput  = contenedorb.Cell(2,1).Component("inp1")
  
	If success Then
   	
	
	
		Dim su As StringUtils
		Dim folder As String = File.Combine(File.DirApp, DownloadFolder)
		Dim BT2() As Byte
		Dim ser2 As ByteConverter
		Dim imagen As Image
		Dim imagenenc As String = ""
	
		If File.Exists(folder,FileName) Then
			imagen.Initialize(folder,FileName)
			BT2=ImageToBytes(imagen)
			Log(BT2.Length)
			imagenenc=su.EncodeBase64(BT2)
			File.WriteString(folder,FileName,imagenenc)
		
			File.Delete(folder,FileName)
		
		Else
			page.ShowToast(masuno(myToastId),"toast1","Upload Error",3000)
			Return
		End If
	
		Dim contenedor As ABMContainer = page.Component("contenedora")
		Dim radio As ABMRadioGroup = contenedor.Cell(2,1).Component("radioup")
	
	
		
	
		Select Case radio.GetActive
			Case 0
		
				sql1.ExecNonQuery("Update RH_UserConf set nom_foto ='"&imagenenc&"' where nom_user='"&Main.username&"'")
				Main.fotoe=imagenenc
				page.ShowToast("toast" & masuno(myToastId), "toastgreen", "File " & FileName & " is the profile picture now!", 5000)
			Case 1
		
				sql1.ExecNonQuery("Update RH_UserConf set nom_fondo ='"&imagenenc&"' where nom_user='"&Main.username&"'")
				Main.fondoe = imagenenc
				page.ShowToast("toast" & masuno(myToastId), "toastgreen", "File " & FileName & " is the background picture now!", 5000)
			Case Else
		
		End Select
	
	
	
		'Main.sustfondo(FileName,Main.fondo)
	
		page.ShowToast("toast" & myToastId, "toastgreen", "File " & FileName & " uploaded success!", 5000)
	Else
		page.ShowToast("toast" & myToastId, "toastred", "File " & FileName & " not uploaded!", 5000)
	End If
   
	'Dim inp1 As ABMFileInput  = page.Cell(3,1).Component("inp1")
	'inp1.Clear
   
	page.Resume
	page.ws.Flush
	page.Refresh
End Sub

Sub Page_Ready()
	Log("miperfil!")
	ConnectPage
	page.RestoreNavigationBarPosition
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub



Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
	'	If icon <> "" Then
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	'	Else
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
		combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next
	
	'page.Refresh
	'combo1.Refresh
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)
	Return myModal
End Sub


Sub masuno (idtoast As Int) As Int
	myToastId=myToastId+1
	Return myToastId
End Sub


Sub ImageToBytes(Image As Image) As Byte()
	Dim out As OutputStream
	out.InitializeToBytesArray(0)
	Image.WriteToStream(out)', 100, "JPEG")
	out.Close
	Return out.ToBytesArray
End Sub

Sub BytesToImage(bytes() As Byte) As Image
	Dim In As InputStream
	In.InitializeFromBytesArray(bytes, 0, bytes.Length)
	Dim bmp As Image
	bmp.Initialize2(In)
   
	Return bmp
End Sub

Sub coloreafondo (mypage As ABMPage)
	If  Not(Main.fondoe=Null) And Not(Main.fondoe="") And Not(Main.fondoe="null") And Not(Main.fondoe="no") And Not(Main.fondoe="#") And Main.fondoe.Length<10 Then
		Dim script As String = $"$('body').css('background-color', '${Main.fondoe}');
                 $('body').attr('class','');     
    	       "$
		mypage.ws.Eval(script,Null)
	End If
End Sub

