﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "periodos360"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = ""
	' your own variables
	Dim myToastId As Int = 1
	
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------
	Log("Connected")
	ws = WebSocket1
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
		' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
		page.Refresh
		page.FinishedLoading
	Else
		If page.WebsocketReconnected Then
			Log("Websocket reconnected")
			' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
			' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
			ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
		Else
			' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
			Log("Websocket first connection")
			page.Prepare
			ConnectPage
		End If
	End If
	Log(ABMPageId)
	'----------------------MODIFICATION-------------------------------
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")
End Sub

Sub Page_ParseEvent(Params As Map)
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)
		End Select
	End If
End Sub

public Sub BuildTheme()
	' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	
	' add additional themes specific for this page
	
	theme.AddCellTheme("encabezadocell")
	theme.Cell("encabezadocell").Align=ABM.CELL_ALIGN_CENTER
	theme.Cell("encabezadocell").BackColor=ABM.COLOR_BROWN
	theme.Cell("encabezadocell").BackColorIntensity=ABM.INTENSITY_LIGHTEN1
	theme.Cell("encabezadocell").BorderColor = ABM.COLOR_BLACK
	theme.Cell("encabezadocell").BorderWidth = 1
	
	theme.AddCellTheme("cuerpocell")
	theme.Cell("cuerpocell").Align= ABM.CELL_ALIGN_JUSTIFY
	theme.Cell("cuerpocell").BorderColor = ABM.COLOR_BLACK
	theme.Cell("cuerpocell").BorderWidth = 1
	theme.Cell("cuerpocell").VerticalAlign = True
	
	
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "index.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' adding a navigation bar
	'adding a navigation bar
	ABMShared.BuildNavigationBar(page, "Periodos 360°","../images/klogo3.png", "", "Periodos 360°", "")
			
	' create the page grid
	page.AddRows(4,True, "").AddCells12(2,"")
	
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()
	'	connecting the navigation bar
		
	ABMShared.ConnectNavigationBar(page)
	'	init all your own variables (like a List, Map) and add your components
	
	' Variables de la bd
	
	
	
	Dim tittle As ABMLabel
	tittle.Initialize(page,"tittle","Períodos de evaluación 360", ABM.SIZE_H5,False,"")
	page.Cell(1,1).addComponent(tittle)
	
	nuevoperiodo
	
	llenarperiodos
	
	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub

Sub nuevoperiodo
	
	page.Cell(2,1).RemoveAllComponents
	
	Dim fec_inicio As ABMDateTimePicker
	Dim fec_fin As ABMDateTimePicker
	Dim nom_descripcion As ABMInput
	Dim st_status As ABMCombo
	Dim NewDate As Long = DateTime.Now
	Dim guardar As ABMButton
	
	Dim cont1 As ABMContainer
	cont1.Initialize(page,"cont1","")
	cont1.AddRows(10,False,"").AddCells12(4,"")
	cont1.BuildGrid
	page.Cell(2,1).AddComponent(cont1)
	page.Cell(2,1).SetOffsetSize(0,0,0,12,6,6)
	
	fec_inicio.Initialize(page, "fec_inicio", ABM.DATETIMEPICKER_TYPE_DATE, NewDate , "Requerido en esta fecha", "date1")
	fec_inicio.CancelText = "Cancel"
	fec_inicio.PickText = "OK"
	fec_inicio.TodayText = ""
	fec_inicio.Language = "en"
	fec_inicio.ReturnDateFormat = "DD/MM/YYYY" '  // <--- see demo source code For settings
	fec_inicio.ReturnTimeFormat = ""'"HH:mm"  '      // <--- see demo source code For settings
	fec_inicio.FirstDayOfWeek = 0
	fec_inicio.ClickThrough = True ' <--- set To True If, when the user makes a selection, the ok action should be triggered.
	cont1.Cell(1,1).SetOffsetSize(0,0,0,12,6,3)
	cont1.Cell(1,1).AddComponent(fec_inicio)
	
	
	fec_fin.Initialize(page, "fec_fin", ABM.DATETIMEPICKER_TYPE_DATE, NewDate, "Requerido en esta fecha", "date1")
	fec_fin.CancelText = "Cancel"
	fec_fin.PickText = "OK"
	fec_fin.TodayText = ""
	fec_fin.Language = "en"
	fec_fin.ReturnDateFormat = "DD/MM/YYYY" '  // <--- see demo source code For settings
	fec_fin.ReturnTimeFormat = ""'"HH:mm"  '      // <--- see demo source code For settings
	fec_fin.FirstDayOfWeek = 0
	fec_fin.ClickThrough = True ' <--- set To True If, when the user makes a selection, the ok action should be triggered.
	cont1.Cell(2,1).SetOffsetSize(0,0,0,12,6,3)
	cont1.Cell(2,1).AddComponent(fec_fin)
	
	
	nom_descripcion.Initialize(page,"nom_descripcion",ABM.INPUT_TEXT, "Descripción", False, "")
	cont1.Cell(3,1).SetOffsetSize(0,0,0,12,6,4)
	cont1.Cell(3,1).AddComponent(nom_descripcion)
	
	
	st_status.Initialize(page,"st_status","Estatus", 250,"")
	st_status.AddItem("0","Desabilitado",BuildSimpleItem("est0","","Desabilitado"))
	st_status.AddItem("1","Habilitado",BuildSimpleItem("est1","","Habilitado"))
	cont1.Cell(4,1).AddComponent(st_status)
	cont1.Cell(4,1).SetOffsetSize(0,0,0,12,6,4)
	cont1.Cell(4,1).Refresh
	st_status.Refresh
	
	
	guardar.InitializeFlat(page,"guardar","","","Guardar","btn")
	cont1.Cell(5,1).AddComponent(guardar)
	
	
	page.Refresh
	
End Sub



Sub llenarperiodos
	page.pause
	
	Dim sql1 As SQL
	Dim text As ABMLabel
	Dim cursor As ResultSet
	Dim pos , i , cant As Int=0
	
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	
	page.Cell(2,2).RemoveAllComponents
	cant = sql1.ExecQuerySingleResult("SELECT count(*) FROM [karismaHC].[dbo].[periodos360] where nom_db='"&page.ws.Session.GetAttribute("DBUse")&"'")
	
	Dim cont2 As ABMContainer
	cont2.Initialize(page,"cont2","")
	cont2.AddRows(1,False,"").AddCells12MP(6,0,0,0,0,"encabezadocell")
	
	cont2.AddRows(cant,False,"").AddCells12MP(6,0,0,0,0,"cuerpocell")
	cont2.BuildGrid
	page.Cell(2,2).AddComponent(cont2)
	page.Cell(2,2).SetOffsetSize(0,0,0,12,6,6)
	
	
	i=i+1
	cont2.Cell(i,1).AddComponent(BuildSimpleItem("tittle1","","Fecha de inicio"))
	cont2.Cell(i,2).AddComponent(BuildSimpleItem("tittle2","","Fecha de fin."))
	cont2.Cell(i,3).AddComponent(BuildSimpleItem("tittle3","","Descripción"))
	cont2.Cell(i,4).AddComponent(BuildSimpleItem("tittle4","","Año"))
	cont2.Cell(i,5).AddComponent(BuildSimpleItem("tittle5","","Status"))
	cont2.Cell(i,6).AddComponent(BuildSimpleItem("tittle6","","Opciones"))
	cont2.Cell(i,1).SetOffsetSize(0,0,0,12,6,2)
	cont2.Cell(i,2).SetOffsetSize(0,0,0,12,6,2)
	cont2.Cell(i,3).SetOffsetSize(0,0,0,12,6,2)
	cont2.Cell(i,4).SetOffsetSize(0,0,0,12,6,2)
	cont2.Cell(i,5).SetOffsetSize(0,0,0,12,6,2)
	cont2.Cell(i,6).SetOffsetSize(0,0,0,12,6,2)
	
	
	
	
	
	cursor = sql1.ExecQuery("SELECT * FROM [karismaHC].[dbo].[periodos360] where nom_db='"&page.ws.Session.GetAttribute("DBUse")&"'")
	
	i=i+1
	Do While cursor.NextRow
		
		
		cont2.Cell(i,1).AddComponent(BuildSimpleItem("value1"&i,"",ABMShared.ConvertDate(cursor.GetString("fec_inicio"),"yyyy-mm-dd","MMMM dd yyyy")))
		
		cont2.Cell(i,2).AddComponent(BuildSimpleItem("value2"&i,"",ABMShared.ConvertDate(cursor.GetString("fec_fin"),"yyyy-mm-dd","MMMM dd yyyy")))
		
		cont2.Cell(i,3).AddComponent(BuildSimpleItem("value3"&i,"",cursor.GetString("nom_descripcion")))
		
		cont2.Cell(i,4).AddComponent(BuildSimpleItem("value4"&i,"",cursor.GetString("fec_ano")))
		
		cont2.Cell(i,5).AddComponent(BuildSimpleItem("value5"&i,"",cursor.GetString("st_status")))
		
		Dim option As ABMButton
		option.InitializeFloating(page,cursor.GetString("id")&"e","edit","btn")
		option.Size = ABM.BUTTONSIZE_NORMAL
		cont2.Cell(i,6).AddArrayComponent(option,"edit")
		
		Dim option As ABMButton
		option.InitializeFloating(page,cursor.GetString("id")&"d","delete","btn")
		option.Size = ABM.BUTTONSIZE_NORMAL
		cont2.Cell(i,6).AddArrayComponent(option,"delete")
		
		cont2.Cell(i,1).SetOffsetSize(0,0,0,12,6,2)
		cont2.Cell(i,2).SetOffsetSize(0,0,0,12,6,2)
		cont2.Cell(i,3).SetOffsetSize(0,0,0,12,6,2)
		cont2.Cell(i,4).SetOffsetSize(0,0,0,12,6,2)
		cont2.Cell(i,5).SetOffsetSize(0,0,0,12,6,2)
		 cont2.Cell(i,6).SetOffsetSize(0,0,0,12,6,2)
		 
		cont2.Cell(i,1).SetFixedHeight(100,False)
		cont2.Cell(i,2).SetFixedHeight(100,False)
		cont2.Cell(i,3).SetFixedHeight(100,False)
		cont2.Cell(i,4).SetFixedHeight(100,False)
		cont2.Cell(i,5).SetFixedHeight(100,False)
		cont2.Cell(i,6).SetFixedHeight(100,False)
		
		cont2.Cell(i,1).MarginTop = "-3%"
		cont2.Cell(i,2).MarginTop = "-3%"
		cont2.Cell(i,3).MarginTop = "-3%"
		cont2.Cell(i,4).MarginTop = "-3%"
		cont2.Cell(i,5).MarginTop = "-3%"
		cont2.Cell(i,6).MarginTop = "-3%"
		
		cont2.Cell(i,1).MarginBottom = "0%"
		cont2.Cell(i,2).MarginBottom = "0%"
		cont2.Cell(i,3).MarginBottom = "0%"
		cont2.Cell(i,4).MarginBottom = "0%"
		cont2.Cell(i,5).MarginBottom = "0%"
		cont2.Cell(i,6).MarginBottom= "0%"
		
		
		cont2.Cell(i,1).PaddingTop = "0"
		cont2.Cell(i,2).PaddingTop = "0"
		cont2.Cell(i,3).PaddingTop = "0"
		cont2.Cell(i,4).PaddingTop = "0"
		cont2.Cell(i,5).PaddingTop = "0"
		cont2.Cell(i,6).PaddingTop = "0"
		
		cont2.Cell(i,1).PaddingBottom = "0"
		cont2.Cell(i,2).PaddingBottom = "0"
		cont2.Cell(i,3).PaddingBottom = "0"
		cont2.Cell(i,4).PaddingBottom = "0"
		cont2.Cell(i,5).PaddingBottom = "0"
		cont2.Cell(i,6).PaddingBottom = "0"
		
		
		
			
		''Dim divend As ABMDivider
		''divend.Initialize(page, "divend", "divider")
		''cont2.Cell(i+2,1).AddComponent(divend)
		
		Log(cursor.GetString("fec_inicio"))
		Log(cursor.GetString("fec_fin"))
		Log(cursor.GetString("nom_descripcion"))
		Log(cursor.GetString("fec_ano"))
		Log(cursor.GetString("fec_periodo"))
		Log(cursor.GetString("st_status"))
		
		i=i+1
	Loop
	
	
	page.Refresh
	
	page.Resume
End Sub

Sub guardar_Clicked(Target As String)
	page.pause
	
	Dim sql1 As SQL
	Dim text As ABMLabel
	Dim cursor As ResultSet
	Dim pos , i , cant As Int=0
	Dim q As String
	
	
	
	Dim cont1 As ABMContainer = page.Component("cont1")
	' Variables de la bd
	Dim fec_inicio As ABMDateTimePicker = cont1.Cell(1,1).Component("fec_inicio")
	Dim fec_fin As ABMDateTimePicker = cont1.Cell(2,1).Component("fec_fin")
	Dim nom_descripcion As ABMInput = cont1.Cell(3,1).Component("nom_descripcion")
	Dim st_status As ABMCombo = cont1.Cell(4,1).Component("st_status")
	Dim guardar As ABMButton = cont1.Cell(5,1).Component("guardar")
	
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	
	
	If guardar.Text = "Editar" Then
		
		q = "UPDATE [karismaHC].[dbo].[periodos360] SET nom_descripcion='"&nom_descripcion.Text&"' , fec_ano='"&DateTime.GetYear(fec_inicio.GetDate)&"' , fec_inicio='"&DateTime.Date(fec_inicio.GetDate)&"' , fec_fin='"&DateTime.Date(fec_fin.GetDate)&"' , st_status='"&st_status.GetActiveItemId&"' where id='"&guardar.tag&"'"
		sql1.ExecNonQuery(q)
		
		myToastId = myToastId + 1
		page.ShowToast("toast" & myToastId, "toastred", "Editó el período " & DateTime.GetYear(fec_inicio.GetDate), 5000)
		
		guardar.Text = "Guardar"
		guardar.Tag = 0
		
		nuevoperiodo
	
	Else
		
		Dim values() As Object
		values = Array As Object(nom_descripcion.Text, DateTime.GetYear(fec_inicio.GetDate),DateTime.Date(fec_inicio.GetDate), DateTime.Date(fec_fin.GetDate),page.ws.Session.GetAttribute("DBUse"),st_status.GetActiveItemId)
	
		sql1.ExecNonQuery2("INSERT INTO [karismaHC].[dbo].[periodos360] VALUES (?, ?, 0, ?, ?,?,?)", values)
		
		
		myToastId = myToastId + 1
		page.ShowToast("toast" & myToastId, "toastred", "Insertó el período " & DateTime.GetYear(fec_inicio.GetDate), 5000)
		
	End If
	
	page.Resume	
	
	llenarperiodos
End Sub

Sub llenarcampos(id As String)
	page.pause
	
	Dim sql1 As SQL
	Dim text As ABMLabel
	Dim cursor As ResultSet
	Dim pos , i , cant As Int=0
	
	
	
	Dim cont1 As ABMContainer = page.Component("cont1")
	' Variables de la bd
	Dim fec_inicio As ABMDateTimePicker = cont1.Cell(1,1).Component("fec_inicio")
	Dim fec_fin As ABMDateTimePicker = cont1.Cell(2,1).Component("fec_fin")
	Dim nom_descripcion As ABMInput = cont1.Cell(3,1).Component("nom_descripcion")
	Dim st_status As ABMCombo = cont1.Cell(4,1).Component("st_status")

	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	
	cursor = sql1.ExecQuery("SELECT * FROM [karismaHC].[dbo].[periodos360] where id='"&id&"'")
	
	Do While cursor.NextRow
		
		
		DateTime.DateFormat = "yyyy-MM-dd 00:00:00"
		Dim t As Long = DateTime.DateParse(cursor.GetString("fec_inicio"))
		fec_inicio.SetDate(t)
		
		DateTime.DateFormat = "yyyy-MM-dd 00:00:00"
		Dim t As Long = DateTime.DateParse(cursor.GetString("fec_fin"))
		fec_fin.SetDate(t)
		
		nom_descripcion.Text = cursor.GetString("nom_descripcion")
		st_status.SetActiveItemId(cursor.GetString("st_status"))
		
	Loop
	
	
	page.Refresh
	
	page.Resume
End Sub


Sub edit_Clicked(Target As String)
	
	Log(Target)
	Dim cont1 As ABMContainer = page.Component("cont1")
	Dim guardar As ABMButton = cont1.Cell(5,1).Component("guardar")
	
	guardar.Text = "Editar"
	guardar.tag = Target.Replace("edit","").Replace("e","")
	
	llenarcampos(Target.Replace("edit","").Replace("e",""))
	
End Sub

Sub delete_Clicked(Target As String)
	Log(Target)
	
	borrarperiodo(Target.Replace("delete","").Replace("d",""))
	
	
	
End Sub

Sub borrarperiodo(id As Int)
	page.Pause	
	
	Dim sql1 As SQL
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	sql1.ExecNonQuery("DELETE FROM [karismaHC].[dbo].[periodos360] where id='"&id&"'")
	llenarperiodos
	
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Borrado periodo " & id, 5000)
	
	page.Resume
End Sub


Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
	'	If icon <> "" Then
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	'	Else
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
		combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next
	
	'page.Refresh
	'combo1.Refresh
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)
	Return myModal
End Sub

