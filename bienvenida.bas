﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "bienvenida"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "6"
	' your own variables		
	Dim myToastId As Int = 1
	Public ChatName As String
	'Private tmr As Timer
	Dim lst As List
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	ConnectPage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------			
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")	
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
End Sub

public Sub BuildTheme()
		' start with the base theme defined in Comunes
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	
	' add additional themes specific for this page
	
	theme.AddImageSliderTheme ("sl1")
	theme.ImageSlider ("sl1").Indicators=False
    theme.ImageSlider ("sl1").Colorize(ABM.COLOR_TRANSPARENT)
	theme.ImageSlider ("sl1").TitleForeColor=ABM.COLOR_BROWN
	theme.ImageSlider ("sl1").SubTitleForeColor=ABM.COLOR_BROWN
	
	
	'add additional themes specific For this page    AGREGAR A ABMSHARED
	theme.AddChatTheme("myChat") 
	theme.Chat("myChat").AddBubble("server")
	theme.Chat("myChat").Bubble("server").Color = ABM.COLOR_BLACK
	theme.Chat("myChat").Bubble("server").TextColor = ABM.COLOR_WHITE
	
	theme.Chat("myChat").AddBubble("me")
	theme.Chat("myChat").Bubble("me").Color = ABM.COLOR_LIGHTBLUE
	theme.Chat("myChat").Bubble("me").TextColor = ABM.COLOR_WHITE
	
	theme.Chat("myChat").AddBubble("them")
	theme.Chat("myChat").Bubble("them").Color = ABM.COLOR_GREY
	theme.Chat("myChat").Bubble("them").ColorIntensity = ABM.INTENSITY_LIGHTEN2
	theme.Chat("myChat").Bubble("them").TextColor = ABM.COLOR_BLACK
	
	theme.AddImageSliderTheme("sld")
	theme.ImageSlider("sld").ActiveBulletColor=ABM.COLOR_BROWN	
	theme.ImageSlider("sld").ActiveBulletColorIntensity=ABM.INTENSITY_DARKEN4
	theme.ImageSlider("sld").InactiveBulletColor=ABM.COLOR_LIME
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	page.SetLoaderJUMPINGBALL(ABM.COLOR_BLACK, ABM.INTENSITY_NORMAL, ABM.COLOR_WHITE, ABM.INTENSITY_NORMAL, "Por favor espere")
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "bienvenida.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' adding a navigation bar
	
	' adding a navigation bar
	ABMShared.BuildNavigationBar(page,"Bienvenid@ "&Main.nombrecompleto,"../images/klogo3.png","","bienvenida","")
			
	' create the page grid
	page.AddRows(1,True, "").AddCellsos(1,0,2,2,12,8,8,"") '1
	page.AddRows(1,True, "").AddCellsOS(1,0,0,0,12,12,12, "")
	page.AddRows(1,True, "").AddCellsOS(3,0,4,4,12,1,1, "")
	page.AddRows(2,True, "").AddCellsOSMP(1,0,0,0,12,12,12,50,0,1,5,"") '
	
	'page.SetBackgroundImage("../images/bienvenida.jpg")	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	
End Sub

public Sub ConnectPage()
'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
		
	page.AddModalSheetTemplate(modalchat)
'	init all your own variables (like a List, Map) and add your components

	lst.Initialize
	lst.Add("Suite para el capital humano")
	lst.Add("Bienvenido")
	lst.Add("Suite HC")

	Dim lbl1 As ABMLabel

	lbl1.Initialize(page,"lbl1","{B}Suite para el Capital Humano{/B}{BR}La Experiencia Karisma comienza en el momento de solicitar un trabajo en una de nuestras locaciones.{BR}Esta herramienta maximiza el valor del recurso más importante de nuestra compañía, su fuerza de trabajo." _
			& "{BR}La Suite ofrece una ventaja única de abordar los desafíos a través de una solución integrada, intuitiva, flexible y completa.",ABM.SIZE_H5,False,"")
	page.Cell(1,1).AddComponent(lbl1)	
	lbl1.IsFlowText=True	
	lbl1.VerticalAlign=True
	'lbl1.IsBlockQuote=True
	

	'en 5 poner slide
	Dim sld As ABMImageSlider
	sld.Initialize(page,"sld","sld")
	sld.AddSlideImage("../slides/about1.png","","",ABM.IMAGESLIDER_CENTER)
	sld.AddSlideImage("../slides/about2.png","","",ABM.IMAGESLIDER_LEFT)
	'sld.AddSlideImage("../slides/about3.png","","",ABM.IMAGESLIDER_RIGHT)
	'sld.AddSlideImage("../slides/about4.png","Titulo 4","subtitulo 4",ABM.IMAGESLIDER_CENTER)
	'page.Cell(2,1).SetFixedHeight(1500,False)
	page.Cell(2,1).AddComponent(sld)
	
	Dim imgv2 As ABMImage
	imgv2.Initialize(page,"imgv2","../images/reclutamiento.png",3)
	imgv2.IsCircular=True
	imgv2.IsClickable=True
	
	page.Cell(3,2).AddComponent(imgv2)
	
	Dim containervideo As ABMContainer
	containervideo.InitializeAnimated(page,"containervideo","initialblur","")
	containervideo.AddRows(2,False,"").AddCells12(1,"")
	containervideo.BuildGrid
	
	'' add HTML5 video
	Dim video As ABMVideo
	video.InitializeHTML5(page, "video", "", False, "video/mp4")
	containervideo.Cell(1,1).AddComponent(video)
				
	' add youtube controls
	Dim btnPlay As ABMButton
	btnPlay.Initializefloating(page, "btnPlay", "mdi-av-play-arrow", "darkred")
	containervideo.Cell(2,1).AddComponent(btnPlay)
			
	Dim btnStop As ABMButton
	btnStop.Initializefloating(page, "btnStop", "mdi-av-stop", "darkred")
	containervideo.Cell(2,1).AddComponent(btnStop)
			
	Dim btnMute As ABMButton
	btnMute.Initializefloating(page, "btnMute", "mdi-av-volume-mute", "darkred")
	containervideo.Cell(2,1).AddComponent(btnMute)
			
	Dim btnVolumeDown As ABMButton
	btnVolumeDown.Initializefloating(page, "btnVolumeDown", "mdi-av-volume-down", "darkred")
	containervideo.Cell(2,1).AddComponent(btnVolumeDown)
			
	Dim btnVolumeUp As ABMButton
	btnVolumeUp.Initializefloating(page, "btnVolumeUp", "mdi-av-volume-up", "darkred")
	containervideo.Cell(2,1).AddComponent(btnVolumeUp)
	
	
	page.Cell(4,1).AddComponent(containervideo)
	
	
	creaflotante
	
	
	Dim script As String = $"
		document.body.style.fontFamily = 'Oswald';
		
	"$
	
	page.ws.Eval(script,Null)
	
	
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub
Sub imgv2_Clicked(Target As String)
	Log("sipdjw")
End Sub
Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	
	
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	If SubTarget="sms" Then
		
		
		
		If ws.Session.HasAttribute("nombre") Then
			ChatName = ws.Session.GetAttribute("nombre")
		Else
			ChatName = "Invitado"
		End If
		
			DateTime.TimeFormat = "HH:mm"
	
	
			Dim bubble As ABMChatBubble
			bubble.Initialize(ChatName, ChatName, ChatName & " " & DateTime.Time(DateTime.Now), "me", "them")
			CallSubDelayed3(ChatShared, "NewMessage", ABMPageId, bubble)
			CallSubDelayed3(ChatShared, "IsTyping", ABMPageId, "")
			
		'ChatShared.AvoidDuplicates.Put(ChatName.ToLowerCase, ChatName)
		Dim m As ABMModalSheet = page.ModalSheet("chat")
		Dim Chat As ABMChat = m.Content.Component("conversation")
		Chat.SetMyFrom(ChatName)
		Chat.AddBubble("Server", "Welcome to ABMChat " & ChatName & "!", "The Server", "server", "server")
		Chat.Refresh
		CallSubDelayed3(chatshared, "NewConnection", Me , ABMPageId)
		
		
		page.ShowModalSheet("chat")
		
		
		
	End If
	
	If SubTarget="conf" Then 
	
	'page.Pause
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	'page.Resume	
	End If
	
	
End Sub


Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub


Sub modalchat() As ABMModalSheet
	
	Dim Chat As ABMChat
	Dim ChatInput As ABMInput
	Dim ChatClear As ABMButton
	Dim myModal As ABMModalSheet
	

	myModal.Initialize(page, "chat", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("")
	myModal.IsDismissible = False

	' create the grid for the content
	myModal.Content.AddRows(2,True, "").AddCells12(1,"")
	myModal.Content.AddRows(1,True, "").AddCells12(12,"")
	myModal.Content.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	Chat.Initialize2(page, "conversation", 450, 200, "mychat")
	Chat.AddBubble("Server", "Bienvenido al chat público de karisma rrhh suite", "El Servidor", "server", "server")
	myModal.Content.Cell(1,1).AddComponent(Chat)
	myModal.Content.Cell(1,1).SetFixedHeight(450,False)
	
	
	ChatInput.Initialize(page, "ChatInput", ABM.INPUT_TEXT, "", False, "")
	ChatInput.PlaceHolderText = "Type your message here...."
	ChatInput.RaiseChangedEvent = True
	myModal.Content.Cell(2,1).AddComponent(ChatInput)
	
	
	ChatClear.InitializeFloating(page, "chatClear", "chat_bubble_outline", "")
	myModal.Content.Cell(2,1).AddComponent(ChatClear)
	
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "SALIR", "transparent")
	myModal.content.Cell(3,1).AddComponent(msbtn3)
	myModal.content.Cell(3,1).SetOffsetSize(0,0,8,12,12,4)
	
	
	
	Return myModal
End Sub

Sub msbtn3_Clicked(Target As String)
	page.CloseModalSheet("chat")
	CallSubDelayed3(ChatShared, "Disconnected", Me, ABMPageId)
End Sub

'FUNCIONES DEL CHAT

Sub ChatClear_Clicked(Target As String)
	Dim m As ABMModalSheet = page.ModalSheet("chat")
	Dim Chat As ABMChat = m.Content.Component("conversation")
	Chat.ClearConversation
End Sub

Sub page_MsgboxResult(returnName As String, result As String)
	Log(returnName & ": " & result)
End Sub

Sub ChatInput_Changed(value As String)
	Dim m As ABMModalSheet = page.ModalSheet("chat")
	If chatshared.HasIdentified(ABMPageId) Then
		CallSubDelayed3(chatshared, "IsTyping", ABMPageId, value)
	End If
End Sub

Sub ChatInput_EnterPressed(value As String)
	DateTime.TimeFormat = "HH:mm"
	Dim m As ABMModalSheet = page.ModalSheet("chat")
	
'	If ChatShared.HasIdentified(ABMPageId) Then
		Dim bubble As ABMChatBubble
		bubble.Initialize(ChatName, value, ChatName & " " & DateTime.Time(DateTime.Now), "me", "them")
		CallSubDelayed3(chatshared, "NewMessage", ABMPageId, bubble)
		CallSubDelayed3(chatshared, "IsTyping", ABMPageId, "")
'	Else
'		If value = "" Then Return
'		If ChatShared.AvoidDuplicates.ContainsKey(value.ToLowerCase) Then
'			Dim Chat As ABMChat = m.Content.Component("conversation")
'			Dim Chat As ABMChat = m.Content.Component("conversation")
'			Chat.AddBubble("Server", "Sorry, but " & value & " is already taken!", "The Server", "server", "server")
'			Chat.Refresh
'		Else
'			ChatName = value
'			ChatShared.AvoidDuplicates.Put(value.ToLowerCase, value)
'			Dim Chat As ABMChat = m.Content.Component("conversation")
'			Chat.SetMyFrom(ChatName)
'			Chat.AddBubble("Server", "Welcome to ABMChat " & ChatName & "!", "The Server", "server", "server")
'			Chat.Refresh
'			CallSubDelayed3(ChatShared, "NewConnection", Me, ABMPageId)
'		End If
'	End If
	
	Dim ChatInput As ABMInput = m.content.Component("ChatInput")
	ChatInput.Text = ""
	ChatInput.Refresh
End Sub

public Sub NewMessage(bubble As ABMChatBubble)
	Dim m As ABMModalSheet = page.ModalSheet("chat")
	Dim Chat As ABMChat = m.Content.Component("conversation")
	Chat.AddBubble2(bubble)
	Chat.Refresh
	Chat.ScrollToBottom	' scroll to the end of the chat
End Sub

public Sub IsTyping(pageID As String, typeBool As Boolean)
	Dim m As ABMModalSheet = page.ModalSheet("chat")
	
	Dim Chat As ABMChat = m.Content.Component("conversation")
	If pageID <> ABMPageId Then
		If typeBool = False Then
			Chat.ThemAreTyping(False, "") 'hide the 'is typing' bubble
		Else
			Chat.ThemAreTyping(True, "") ' show the 'is typing' bubble if not yet shown
		End If
		Chat.Refresh
		Chat.ScrollToBottom	' scroll to the end of the chat
	End If
End Sub

public Sub CurrentConversation(LastMessages As List)
	Dim m As ABMModalSheet = page.ModalSheet("chat")
	If LastMessages.Size > 0 Then
		Dim Chat As ABMChat = m.Content.Component("conversation")
		Chat.SetConversation(LastMessages)
		Chat.Refresh
		Chat.ScrollToBottom	' scroll to the end of the chat
	End If
End Sub





'Sub Page_Ready()
'	Log("configuracion!")
'	ConnectPage
'	page.RestoreNavigationBarPosition
'End Sub
