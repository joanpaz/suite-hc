﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "personnelreq"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "17"
	' your own variables		
	Dim myToastId As Int = 1
	Dim cmbDivision As ABMCombo
	Public cmbSubDivision,cmbLocation,cmbAreas,cmbDepartamento,cmbPuesto,cmbNum As ABMCombo

	Dim rdb1 As ABMRadioGroup
	Dim rdb2 As ABMRadioGroup

	Dim txtRepl As ABMInput
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
		'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	ConnectPage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------	
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")	
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
End Sub

public Sub BuildTheme()
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	theme.AddContainerTheme("conte")
	theme.Container("conte").ZDepth=ABM.ZDEPTH_3
	theme.AddDividerTheme("div1")
	theme.Divider("div1").ForeColor=ABM.COLOR_BLUE
	theme.Divider("div1").ForeColorIntensity=ABM.INTENSITY_LIGHTEN4
	theme.AddImageSliderTheme("sl1")
	theme.ImageSlider("sl1").Height=200
	theme.ImageSlider("sl1").Indicators=False
	theme.ImageSlider("sl1").FullWidth=True
	theme.ImageSlider("sl1").ZDepth=ABM.ZDEPTH_3
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "personnelreq.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
		' Añadir Navigation Bar
	ABMShared.BuildNavigationBar(page,"Personal requerido","../images/klogo3.png","","personnelreq","")

	page.AddRows(1,True, "").AddCells12(1,"")
	page.AddRows(3,True, "").AddCells12(4,"")
	page.AddRows(3,True, "").AddCellsOSMP(1,0,0,0,12,11,11,5,0,0,0,"")
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()
	
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)

	page.Cell(1,1).AddComponent(ABMShared.BuildParagraph(page,"inst","En esta sección puede requerir un nuevo empleado",ABM.SIZE_H5))
	Dim lblPais As ABMLabel
	lblPais.Initialize(page,"lblpais","Select country ",ABM.SIZE_H5,False,"field")
	'Division
	cmbDivision.Initialize(page,"cmbDivision",  "División",150,"combo")
	cmbDivision.AddItem("P","",BuildSimpleItem("P","",""))
	page.Cell(2,1).SetOffsetSize(0,0,0,12,6,3)
	page.Cell(2,1).AddComponent(cmbDivision)
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_dIVISION order by 1")
	Do While rst.NextRow
		cmbDivision.AddItem("P"& pos,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	'SubDivision	
	cmbSubDivision.Initialize(page,"cmbSubDivision","Seleccione el país",250,"combo")
	page.Cell(2,2).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(2,2).AddComponent(cmbSubDivision)
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_Country order by 1")
	cmbSubDivision.AddItem("P","",BuildSimpleItem("P","",""))
	pos=0
	Do While rst.NextRow
		cmbSubDivision.AddItem("P"& pos,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	'Location
	cmbLocation.Initialize(page,"cmbLocation","Seleccione la locación",250,"combo")
	page.Cell(2,3).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(2,3).AddComponent(cmbLocation)
	'Areas
	cmbAreas.Initialize(page,"cmbAreas","Seleccione el Área",250,"combo")
	page.Cell(3,1).SetOffsetSize(0,0,0,12,6,3)
	page.Cell(3,1).AddComponent(cmbAreas)
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_Area order by 1")
	Do While rst.NextRow
		cmbAreas.AddItem("P"& pos,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	'Depatamentos
	cmbDepartamento.Initialize(page,"cmbDepartamento","Seleccione el departamento",250,"combo")
	page.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(3,2).AddComponent(cmbDepartamento)
	rst=sql1.ExecQuery("Select * from RH_Dept")
	pos=0
	cmbDepartamento.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbDepartamento.AddItem( rst.GetString2(0),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
		pos=pos+1
	Loop
	rst.close
	'Puesto
	cmbPuesto.Initialize(page,"cmbPuesto","Seleccione la posición",250,"combo")
	page.Cell(3,3).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(3,3).AddComponent(cmbPuesto)
	'cantidad
	cmbNum.Initialize(page,"cmbNum","Seleccione el número de posiciones requeridas",250,"combo")
	page.Cell(4,1).SetOffsetSize(0,0,0,12,6,3)
	page.Cell(4,1).AddComponent(cmbNum)
	cmbNum.AddItem("P","",BuildSimpleItem("P","",""))
	For i=1 To 5
		cmbNum.AddItem("N" & i, i,BuildSimpleItem("N"&i,"",i))
	Next

	Dim NewDate As Long = DateTime.Now  'Date(DateTime.Now)
	Dim mydate1 As ABMDateTimePicker
	mydate1.Initialize(page, "mydate1", ABM.DATETIMEPICKER_TYPE_DATE, NewDate, "Fecha de emisión", "date1")
	mydate1.CancelText = "Cancelar"
	mydate1.PickText = "OK"
	mydate1.TodayText = ""
	mydate1.Language = "en"
	mydate1.ReturnDateFormat = "DD/MM/YYYY" '  // <--- see demo source code For settings
	mydate1.ReturnTimeFormat = ""'"HH:mm"  '      // <--- see demo source code For settings
	mydate1.FirstDayOfWeek = 0
	mydate1.ClickThrough = True ' <--- set To True If, when the user makes a selection, the ok action should be triggered.
	page.Cell(4,2).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(4,2).AddComponent(mydate1)	

	Dim mydate2 As ABMDateTimePicker
	mydate2.Initialize(page, "mydate2", ABM.DATETIMEPICKER_TYPE_DATE, NewDate, "Requerido en esta fecha", "date1")
	mydate2.CancelText = "Cancel"
	mydate2.PickText = "OK"
	mydate2.TodayText = ""
	mydate2.Language = "en"
	mydate2.ReturnDateFormat = "DD/MM/YYYY" '  // <--- see demo source code For settings
	mydate2.ReturnTimeFormat = ""'"HH:mm"  '      // <--- see demo source code For settings
	mydate2.FirstDayOfWeek = 0
	mydate2.ClickThrough = True ' <--- set To True If, when the user makes a selection, the ok action should be triggered.
	page.Cell(4,3).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(4,3).AddComponent(mydate2)	

	Dim cnt1 As ABMContainer
	cnt1.Initialize(page,"cnt1","cardtheme2")
	'cnt1.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	cnt1.AddRows(1,True, "").AddCells12MP(1,20,0,0,0,"").AddCells12MP(1,20,0,0,0,"")
	cnt1.AddRows(1,True, "").AddCells12(1,"").AddCells12(1,"")
	cnt1.AddRows(1,True, "").AddCells12(1,"").AddCells12(1,"")
	
	cnt1.AddRows(1,True,"").AddCells12MP(1,20,0,0,0,"").AddCells12MP(1,20,0,0,0,"")
	cnt1.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	
	cnt1.BuildGrid
	page.Cell(5,1).SetOffsetSize(0,0,0,12,11,11)
	page.Cell(5,1).AddComponent(cnt1)
	
	Dim lblT1,lblT2 As ABMLabel
	lblT1.Initialize(page,"","Requisición de personal a ser llenada",ABM.SIZE_H6,False,"etiq")
	cnt1.Cell(1,1).UseTheme("celda2")
	cnt1.Cell(1,1).SetOffsetSize(0,0,0,12,5,5)
	cnt1.Cell(1,1).AddComponent(lblT1)

	lblT2.Initialize(page,"","Tipo de contrato",ABM.SIZE_H6,False,"etiq")
	cnt1.Cell(1,2).UseTheme("celda2")
	cnt1.Cell(1,2).SetOffsetSize(0,1,1,12,5,5)
	cnt1.Cell(1,2).AddComponent(lblT2)

	Dim contr As ABMContainer
	contr.Initialize(page,"contr","cardtheme2")
	contr.AddRows(1,False,"").AddCells12(1,"")
	contr.BuildGrid
	cnt1.Cell(2,1).SetOffsetSize(0,0,0,12,5,5)
	cnt1.Cell(2,1).AddComponent(contr)	
	
	rdb1.Initialize(page,"rdb1","")
	rdb1.AddRadioButton("Reemplazo de vacante",True)
	rdb1.AddRadioButton("Incremento de personal",True)
	rdb1.AddRadioButton("Nueva creación",True)
	contr.Cell(1,1).SetOffsetSize(1,1,1,7,7,7)
	contr.Cell(1,1).AddComponent(rdb1)

	Dim contr2 As ABMContainer
	contr2.Initialize(page,"contr","cardtheme2")
	contr2.AddRows(1,False,"").AddCells12(1,"")
	contr2.AddRowsM(1,False,40,0,"").AddCells12(1,"")
	contr2.BuildGrid
	cnt1.Cell(2,2).SetOffsetSize(1,1,1,5,5,5)
	cnt1.Cell(2,2).AddComponent(contr2)	
	
	rdb2.Initialize(page,"rdb2","")
	rdb2.AddRadioButton("Temporal",True)
	rdb2.AddRadioButton("Indeterminado",True)
	contr2.Cell(1,1).SetOffsetSize(1,1,1,10,10,10)
	contr2.Cell(1,1).AddComponent(rdb2)


	Dim contex As ABMContainer
	contex.Initialize(page,"contex","cardtheme2")
	contex.AddRows(2,False,"").AddCells12(1,"")
	contex.BuildGrid
	cnt1.Cell(3,1).SetOffsetSize(0,0,0,12,5,5)
	cnt1.Cell(3,1).AddComponent(contex)	
	
	Dim lblex As ABMLabel
	lblex.Initialize(page,"lblex","En caso del reemplazo de una vacante, ¿quien será reemplazado?",ABM.SIZE_H6,False,"")
	contex.Cell(1,1).SetOffsetSize(1,1,1,10,10,10)
	contex.Cell(1,1).AddComponent(lblex)

	
	txtRepl.Initialize(page,"txtrepl",ABM.INPUT_TEXT,"",False,"")
	contex.Cell(2,1).SetOffsetSize(1,1,1,10,10,10)
	contex.Cell(2,1).AddComponent(txtRepl)

	Dim cntcode As ABMContainer
	cntcode.Initialize(page,"cntcode","cardtheme2")
	cntcode.AddRows(2,False,"").AddCells12MP(1,5,0,10,0,"")
	cntcode.BuildGrid
	cnt1.Cell(3,2).SetOffsetSize(0,1,1,12,5,5)
	cnt1.Cell(3,2).AddComponent(cntcode)
	
	Dim lblcode As ABMLabel
	lblcode.Initialize(page,"", "Code",ABM.SIZE_H6,False,"etiq")
	cntcode.Cell(1,1).UseTheme("celda2")
	cntcode.Cell(1,1).SetOffsetSize(0,0,1,10,10,10)
	cntcode.Cell(1,1).AddComponent(lblcode)
	
	Dim txtcode As ABMInput
	txtcode.Initialize(page,"txtcode",ABM.INPUT_TEXT,"",False,"")
	cntcode.Cell(2,1).AddComponent(txtcode)

	Dim lblT2 As ABMLabel
	lblT2.Initialize(page,"","En caso de aumento de personal o nueva creación, indique la razón de la requisición",ABM.SIZE_H6,False,"etiq")
	cnt1.Cell(4,1).UseTheme("celda2")
	cnt1.Cell(4,1).AddComponent(lblT2)
		
	Dim txtReason As ABMInput
	txtReason.Initialize(page,"txtReason",ABM.INPUT_TEXT,"",False,"")
	cnt1.Cell(5,1).SetOffsetSize(0,0,0,12,12,12)
	cnt1.Cell(5,1).AddComponent(txtReason)
	
'--	----------- mas
	Dim cnt2 As ABMContainer
	cnt2.Initialize(page,"cnt2","cardtheme2")
	'cnt2.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	cnt2.AddRows(1,True, "").AddCellsOSMP(1,0,0,0,6,4,4,5,0,0,0,"").AddCellsOSMP(1,0,0,0,6,4,4,5,0,-100,0,"").AddCellsOSMP(1,0,0,0,6,4,4,5,0,-100,0,"")
	cnt2.AddRows(1,True, "").AddCellsOSMP(1,0,0,0,6,3,3,5,0,0,0,"").AddCellsOSMP(1,0,0,0,6,3,3,5,0,-50,0,"").AddCellsOSMP(1,0,0,0,6,3,3,5,0,-50,0,"").AddCellsOSMP(1,0,0,0,6,3,3,5,0,-50,0,"")
	cnt2.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,-15,10,0,0,"")

	
	cnt2.BuildGrid
	page.Cell(6,1).SetOffsetSize(0,0,0,12,11,11)
	page.Cell(6,1).AddComponent(cnt2)
	Dim cntf1,cntf2,cntf3 As ABMContainer
	cntf1.Initialize(page,"cntf1","cardtheme2")
	cntf1.AddRows(2,True,"").AddCells12(2,"")	
	cntf1.BuildGrid
	cnt2.Cell(1,1).addComponent(cntf1)
	
	Dim lblf1,lblf2,lblf3,lblf4,lblf5,lblf6,lblf7 As ABMLabel
	Dim txtN1,txtN2,txtN3,txtN4,txtN5,txtN6,txtN7 As ABMInput

	lblf1.Initialize(page,"","Supervisor del aplicante",ABM.SIZE_SPAN,False,"etiq")
	cntf1.Cell(1,1).UseTheme("celda2")
	cntf1.Cell(1,1).AddComponent(lblf1)
	txtN1.Initialize(page,"txtN1",ABM.INPUT_TEXT,"NOMBRE",False,"")
	cntf1.Cell(2,1).AddComponent(txtN1)
		
	cntf2.Initialize(page,"cntf2","cardtheme2")
	cntf2.AddRows(2,True,"").AddCells12(2,"")	
	cntf2.BuildGrid
	cnt2.Cell(1,2).addComponent(cntf2)
	
	lblf2.Initialize(page,"","Gerente de capital humano",ABM.SIZE_SPAN,False,"etiq")
	cntf2.Cell(1,1).UseTheme("celda2")
	cntf2.Cell(1,1).AddComponent(lblf2)
	txtN2.Initialize(page,"txtN2",ABM.INPUT_TEXT,"NOMBRE",False,"")
	cntf2.Cell(2,1).AddComponent(txtN2)

	
	cntf3.Initialize(page,"cntf3","cardtheme2")
	cntf3.AddRows(2,True,"").AddCells12(2,"")	
	cntf3.BuildGrid
	cnt2.Cell(1,3).addComponent(cntf3)
	
	lblf3.Initialize(page,"","Gerente general / Aprovación",ABM.SIZE_SPAN,False,"etiq")
	cntf3.Cell(1,1).UseTheme("celda2")
	cntf3.Cell(1,1).AddComponent(lblf3)
	txtN3.Initialize(page,"txtN3",ABM.INPUT_TEXT,"",False,"")
	cntf3.Cell(2,1).AddComponent(txtN3)

	'renglon2
	Dim cntf4,cntf5,cntf6,cntf7 As ABMContainer
	cntf4.Initialize(page,"cntf4","cardtheme2")
	cntf4.AddRows(2,True,"").AddCells12(2,"")	
	cntf4.BuildGrid
	cnt2.Cell(2,1).addComponent(cntf4)
	lblf4.Initialize(page,"lbl","Controlador",ABM.SIZE_SPAN,False,"etiq")
	cntf4.Cell(1,1).UseTheme("celda2")
	cntf4.Cell(1,1).AddComponent(lblf4)
	txtN4.Initialize(page,"txtN4",ABM.INPUT_TEXT,"NAME",False,"")
	cntf4.Cell(2,1).AddComponent(txtN4)
	
	cntf5.Initialize(page,"cntf5","cardtheme2")
	cntf5.AddRows(2,True,"").AddCells12(2,"")	
	cntf5.BuildGrid
	cnt2.Cell(2,2).addComponent(cntf5)
	lblf5.Initialize(page,"","Corp. Finanzas",ABM.SIZE_SPAN,False,"etiq")
	cntf5.Cell(1,1).UseTheme("celda2")
	cntf5.Cell(1,1).AddComponent(lblf5)
	txtN5.Initialize(page,"txtN5",ABM.INPUT_TEXT,"NAME",False,"")
	cntf5.Cell(2,1).AddComponent(txtN5)

	cntf6.Initialize(page,"cntf6","cardtheme2")
	cntf6.AddRows(2,True,"").AddCells12(2,"")	
	cntf6.BuildGrid
	cnt2.Cell(2,3).addComponent(cntf6)
	lblf6.Initialize(page,"","Corp. Capital Humano",ABM.SIZE_SPAN,False,"etiq")
	cntf6.Cell(1,1).UseTheme("celda2")
	cntf6.Cell(1,1).AddComponent(lblf6)
	txtN6.Initialize(page,"txtN6",ABM.INPUT_TEXT,"NAME",False,"")
	cntf6.Cell(2,1).AddComponent(txtN6)

	cntf7.Initialize(page,"cntf7","cardtheme2")
	cntf7.AddRows(2,True,"").AddCells12(2,"")	
	cntf7.BuildGrid
	cnt2.Cell(2,4).addComponent(cntf7)
	lblf7.Initialize(page,"","Dir. o VP de Operaciones",ABM.SIZE_SPAN,False,"etiq")
	cntf7.Cell(1,1).UseTheme("celda2")
	cntf7.Cell(1,1).AddComponent(lblf7)
	txtN7.Initialize(page,"txtN7",ABM.INPUT_TEXT,"NAME",False,"")
	cntf7.Cell(2,1).AddComponent(txtN7)

	Dim lblF As ABMLabel
	lblF.Initialize(page,"","Nota :Sólo se necesitan firmas de nivel corporativo en caso de nueva creación y / o Aumento de personal (últimos 3 cuadros)",ABM.SIZE_SPAN,False,"etiq")
	cnt2.Cell(3,1).UseTheme("celda2")
	cnt2.Cell(3,1).AddComponent(lblF)
	
	Dim cntB As ABMContainer
	cntB.Initialize(page,"cntB","cardtheme2")
	cntB.AddRows(1,False,"").AddCellsOSMP(3,1,1,1,3,3,3,20,20,0,0,"")
	cntB.BuildGrid
	page.Cell(7,1).AddComponent(cntB)
	
	Dim btn1,btn2,btn3 As ABMButton
	btn1.InitializeFlat(page,"btn1","","","Salvar","btn")
	btn2.InitializeFlat(page,"btn2","","","Salvar e imprimir","btn")
	
	btn3.InitializeFlat(page,"btn3","","","Cancelar","btn")
	
	cntB.Cell(1,1).AddComponent(btn1)
	'cntB.Cell(1,2).AddComponent(btn2)
	cntB.Cell(1,3).AddComponent(btn3)
	
	If  Not(Main.fondoe=Null) And Not(Main.fondoe="") And  Not(Main.fondoe="null") And Not(Main.fondoe="no") And Not(Main.fondoe="#") Then
		If Main.fondoe.Length<10 Then
			Dim script As String = $"$('body').css('background-color', '${Main.fondoe}');
													 $('body').attr('class','');	 
											"$
			page.ws.Eval(script,Null)
		Else
			Dim su As StringUtils
			Dim BT2() As Byte= su.DecodeBase64(Main.fondoe)
			Dim imagen As Image = BytesToImage(BT2)
			Dim carpeta As String = File.Combine(File.DirApp, Main.DownloadFolder)
						
			Dim nomfondo As String =  ws.Session.GetAttribute("idsesion")&"b.jpg"
			Dim borro As Boolean = False
			If File.Exists (carpeta,  ws.Session.GetAttribute("idsesion")&"b.jpg") Then
				borro = File.Delete(carpeta,  ws.Session.GetAttribute("idsesion")&"b.jpg")
			End If
			
			Dim Out As OutputStream = File.OpenOutput(carpeta,  ws.Session.GetAttribute("idsesion")&"b.jpg" , True)
			imagen.WriteToStream(Out)
			Out.Close
						
			Dim script As String = $"$('body').css('background-image', 'url(../perfiles/${nomfondo})');
						
						 $('body').css('background-size', 'cover');
						 
							"$
			page.ws.Eval(script,Null)
			
		End If
	End If
	
	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub
Sub BytesToImage(bytes() As Byte) As Image
	Dim In As InputStream
	In.InitializeFromBytesArray(bytes, 0, bytes.Length)
	Dim bmp As Image
	bmp.Initialize2(In)
   
	Return bmp
End Sub

Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then 
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub btn1_Clicked(Target As String)
	myToastId=1
		
	Dim cmb1 As ABMCombo=page.Component("cmbDivision")	
	Dim CL1 As ABMLabel=cmb1.GetComponent(cmb1.GetActiveItemId)
	Dim cmb2 As ABMCombo=page.Component("cmbSubDivision")	
	Dim CL2 As ABMLabel=cmb2.GetComponent(cmb2.GetActiveItemId)
	Dim cmb3 As ABMCombo=page.Component("cmbLocation")	
	Dim CL3 As ABMLabel=cmb3.GetComponent(cmb3.GetActiveItemId)
	Dim cmb4 As ABMCombo=page.Component("cmbAreas")	
	Dim CL4 As ABMLabel=cmb4.GetComponent(cmb4.GetActiveItemId)
	Dim cmb5 As ABMCombo=page.Component("cmbDepartamento")	
	Dim CL5 As ABMLabel=cmb5.GetComponent(cmb5.GetActiveItemId)
	Dim cmb6 As ABMCombo=page.Component("cmbPuesto")	
	Dim CL6 As ABMLabel=cmb6.GetComponent(cmb6.GetActiveItemId)
	Dim cmb7 As ABMCombo=page.Component("cmbNum")	
	Dim CL7 As ABMLabel=cmb7.GetComponent(cmb7.GetActiveItemId)
	'valido combos
	If cmb1.GetActiveItemId ="-1" Then page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar la división",3000)
	If cmb2.GetActiveItemId ="-1" Then page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar el país",3000)
	If cmb3.GetActiveItemId ="-1" Then page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar la locación",3000)
	If cmb4.GetActiveItemId ="-1" Then page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar el área",3000)
	If cmb5.GetActiveItemId ="-1" Then page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar el departamento",3000)
	If cmb6.GetActiveItemId ="-1" Then page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar la posición",3000)
	If cmb7.GetActiveItemId ="-1" Then page.ShowToast(masuno(myToastId),"toast1","Debe seleccionar la candidad de posiciones",3000)

	Dim date1 As ABMDateTimePicker=page.Component("mydate1")
	Dim date2 As ABMDateTimePicker=page.Component("mydate2")
	'valido fecha	
	
	Dim cntx1 As ABMContainer=page.Component("cnt1")
	Dim cntxx1 As ABMContainer=cntx1.ComponentRowCell(3,1,"contex")
	Dim tx1 As ABMInput=cntxx1.cell(2,1).Component("txtRepl")	
	'If Comunes.validatxt(tx1.Text) Then 	page.ShowToast(masuno(myToastId),"toast1","xxxx",3000)
	Dim cntxx1 As ABMContainer=cntx1.ComponentRowCell(3,2,"cntcode")
	Dim tx2 As ABMInput=cntxx1.cell(2,1).Component("txtcode")	
	'If Comunes.validatxt(tx2.Text) Then 	page.ShowToast(masuno(myToastId),"toast1","xxxxyyyyy",3000)
	Dim tx3 As ABMInput=cntx1.ComponentRowCell(5,1,"txtReason")

	Dim cntx2 As ABMContainer=page.Component("cnt2")
	Dim cntfx1 As ABMContainer=cntx2.ComponentRowCell(1,1,"cntf1")
	Dim txtNX1 As ABMInput=cntfx1.ComponentRowCell(2,1,"txtN1")
	Dim cntfx2 As ABMContainer=cntx2.ComponentRowCell(1,2,"cntf2")
	Dim txtNX2 As ABMInput=cntfx2.ComponentRowCell(2,1,"txtN2")
	Dim cntfx3 As ABMContainer=cntx2.ComponentRowCell(1,3,"cntf3")
	Dim txtNX3 As ABMInput=cntfx3.ComponentRowCell(2,1,"txtN3")
	Dim cntfx4 As ABMContainer=cntx2.ComponentRowCell(2,1,"cntf4")
	Dim txtNX4 As ABMInput=cntfx4.ComponentRowCell(2,1,"txtN4")
	Dim cntfx5 As ABMContainer=cntx2.ComponentRowCell(2,2,"cntf5")
	Dim txtNX5 As ABMInput=cntfx5.ComponentRowCell(2,1,"txtN5")
    Dim cntfx6 As ABMContainer=cntx2.ComponentRowCell(2,3,"cntf6")
	Dim txtNX6 As ABMInput=cntfx6.ComponentRowCell(2,1,"txtN6")
	Dim cntfx7 As ABMContainer=cntx2.ComponentRowCell(2,4,"cntf7")
	Dim txtNX7 As ABMInput=cntfx7.ComponentRowCell(2,1,"txtN7")
	
	'Radio 1
	Dim rdbT1,rdbT2 As String
	If rdb1.GetActive=0 Then
		rdbT1="REEMPLAZO DE VACANTE"
	else If rdb1.GetActive=1 Then
		rdbT1="INCREMENTO DE PERSONAL"
	else if rdb1.GetActive=2 Then
		rdbT1="NUEVA CREACIÓN"
	End If
	
	If rdb2.GetActive=0 Then
		rdbT2="TEMPORAL"	
	else if rdb2.GetActive=1 Then
		rdbT2="INDETERMINADO"	
	End If
	Dim sql2 As SQL
	sql2.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	Dim valores() As String
	valores = Array As String(CL1.Text.ToUpperCase,CL2.Text.ToUpperCase,CL3.Text.ToUpperCase,CL4.Text.ToUpperCase,CL5.Text.ToUpperCase,CL6.Text.ToUpperCase,CL7.Text.ToUpperCase,DateTime.Date(date1.GetDate),DateTime.Date(date2.GetDate), tx2.Text.ToUpperCase,rdbT2,rdbT1,tx1.Text.ToUpperCase,tx3.Text.ToUpperCase,txtNX1.Text.ToUpperCase,txtNX2.Text.ToUpperCase,txtNX3.Text.ToUpperCase,txtNX4.Text.ToUpperCase,txtNX5.Text.ToUpperCase,txtNX6.Text.ToUpperCase,txtNX7.Text.ToUpperCase)
	
	For i=0 To valores.Length-1
		Log(valores(i))
	Next
	sql2.ExecNonQuery2("INSERT INTO RH_PersonnelRequisition VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )", valores)
	page.ShowToast(masuno(myToastId),"toast1","Saved",3000)
	
	ABMShared.NavigateToPage(ws,ABMPageId,"../personnelreq/personnelreq.html")

	
End Sub
Sub btn3_Clicked(Target As String)
	Limpiar	
End Sub
Sub Limpiar
	Dim cmb1 As ABMCombo=page.Component("cmbDivision")	
	Dim cmb2 As ABMCombo=page.Component("cmbSubDivision")	
	Dim cmb3 As ABMCombo=page.Component("cmbLocation")	
	Dim cmb4 As ABMCombo=page.Component("cmbAreas")	
	Dim cmb5 As ABMCombo=page.Component("cmbDepartamento")	
	Dim cmb6 As ABMCombo=page.Component("cmbPuesto")	
	Dim cmb7 As ABMCombo=page.Component("cmbNum")	
	cmb1.SetActiveItemId(0)	
	cmb1.Refresh
	cmb2.SetActiveItemId(0)	
	cmb2.Refresh
	cmb3.SetActiveItemId(0)	
	cmb3.Refresh
	cmb4.SetActiveItemId(0)	
	cmb4.Refresh
	cmb5.SetActiveItemId(0)	
	cmb5.Refresh
	cmb6.SetActiveItemId(0)	
	cmb6.Refresh
	cmb7.SetActiveItemId(0)	
	cmb7.Refresh

	Dim cntx1 As ABMContainer=page.Component("cnt1")
	Dim cntxx1 As ABMContainer=cntx1.ComponentRowCell(3,1,"contex")
	Dim tx1 As ABMInput=cntxx1.cell(2,1).Component("txtRepl")	
	'If Comunes.validatxt(tx1.Text) Then 	page.ShowToast(masuno(myToastId),"toast1","xxxx",3000)
	Dim cntxx1 As ABMContainer=cntx1.ComponentRowCell(3,2,"cntcode")
	Dim tx2 As ABMInput=cntxx1.cell(2,1).Component("txtcode")	
	'If Comunes.validatxt(tx2.Text) Then 	page.ShowToast(masuno(myToastId),"toast1","xxxxyyyyy",3000)
	Dim tx3 As ABMInput=cntx1.ComponentRowCell(5,1,"txtReason")
	Dim cntx2 As ABMContainer=page.Component("cnt2")
	Dim cntfx1 As ABMContainer=cntx2.ComponentRowCell(1,1,"cntf1")
	Dim txtNX1 As ABMInput=cntfx1.ComponentRowCell(2,1,"txtN1")
	Dim cntfx2 As ABMContainer=cntx2.ComponentRowCell(1,2,"cntf2")
	Dim txtNX2 As ABMInput=cntfx2.ComponentRowCell(2,1,"txtN2")
	Dim cntfx3 As ABMContainer=cntx2.ComponentRowCell(1,3,"cntf3")
	Dim txtNX3 As ABMInput=cntfx3.ComponentRowCell(2,1,"txtN3")
	Dim cntfx4 As ABMContainer=cntx2.ComponentRowCell(2,1,"cntf4")
	Dim txtNX4 As ABMInput=cntfx4.ComponentRowCell(2,1,"txtN4")
	Dim cntfx5 As ABMContainer=cntx2.ComponentRowCell(2,2,"cntf5")
	Dim txtNX5 As ABMInput=cntfx5.ComponentRowCell(2,1,"txtN5")
    Dim cntfx6 As ABMContainer=cntx2.ComponentRowCell(2,3,"cntf6")
	Dim txtNX6 As ABMInput=cntfx6.ComponentRowCell(2,1,"txtN6")
	Dim cntfx7 As ABMContainer=cntx2.ComponentRowCell(2,4,"cntf7")
	Dim txtNX7 As ABMInput=cntfx7.ComponentRowCell(2,1,"txtN7")
	tx1.Text=""
	tx1.Refresh
	tx2.Text=""
	tx2.Refresh
	tx3.Text=""
	tx3.Refresh
	txtNX1.Text=""
	txtNX1.Refresh
	txtNX2.Text=""
	txtNX2.Refresh
	txtNX3.Text=""
	txtNX3.Refresh
	txtNX4.Text=""
	txtNX4.Refresh
	txtNX5.Text=""
	txtNX5.Refresh
	txtNX6.Text=""
	txtNX6.Refresh
	txtNX7.Text=""
	txtNX7.Refresh
	
End Sub

Sub masuno (idtoast As Int) As Int
		myToastId=myToastId+1
		Return myToastId
End Sub

Sub cmbDepartamento_Clicked(itemId As String)
	Dim cmbx As ABMCombo=page.Component("cmbPuesto")
	Dim cmby As ABMCombo=page.Component("cmbPuesto")
	cmby.Clear
	cmby.Refresh
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_Positons where Pos_Dep_Key='" & itemId & "' order by 3")
	cmbx.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbx.AddItem("P"& pos & "-" & rst.GetString2(5),rst.GetString2(2),BuildSimpleItem("P"& pos,"",rst.GetString2(2)))
		pos=pos+1
	Loop
	rst.Close
	cmbx.Refresh
End Sub

Sub cmbSubDivision_Clicked(itemId As String)
	Dim cmbx As ABMCombo=page.Component("cmbLocation")
	Dim c1 As ABMCombo=page.Component("cmbSubDivision")
	Dim id1 As String=c1.GetActiveItemId
	Dim lb1 As ABMLabel=c1.GetComponent(id1)
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select distinct nom_propiedad from RH_SubDivision where nom_pais='" & lb1.Text & "'")
	cmbx.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbx.AddItem("P"& pos ,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	cmbx.Refresh
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
'	If icon <> "" Then
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
'	Else
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int 
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
	     combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next 
	
	'page.Refresh
	'combo1.Refresh	   
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid 
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)	
	Return myModal
End Sub

Sub Page_Ready()
	Log("Lista personnel requisition")
	ConnectPage
	page.RestoreNavigationBarPosition
End Sub

