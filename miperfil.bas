﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "miperfil"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Public ABMPageId As String = "2"
	' your own variables
	Dim myToastId As Int = 1
	
	'--- TAMAÑO DE LA PAGINA
	Public ScrnSize As Int = 0
     
	
	Dim myToastId As Int = 1
	
	'Private sql1 As SQL
	
	
	' VARIABLES HOJA 1
	
	Public nombrecompleto As String = "" 'ws.Session.GetAttribute2("authName", "")
	Public numerotarjeta As String = "" 'ws.Session.GetAttribute2("idsesion", "")
	Public idusuario As String = ""
	Public username As String = ""
	
	
	Public numeronomina As String = "K02192731"
	Public empresanomina As String = "NOM_AQUINO"
	Public status1 As String = "Activo"
	Public status2 As String = "ALTA INICIAL"
	Public departamento As String = "RRHH"
	Public iddepto As String
	Public puesto As String = "JEFE DE PERSONAL"
	Public centrocosto As String = "ADMINISTRATIVOS"
	Public rfc As String = "PUCE5504064J7"
	Public imss As String = "82078529748"
	Public curp As String = "PUCE850406HYNCHL06"
	Public fechainicio As String = "11/25/2013"
	Public fechakarisma As String
	Public fechaterminacion As String = "N/A"
	
	' VARIABLES HOJA 2
	
	
	Public dummy2a As String = "value1"
	Public dummy2b As String = "value2"
	Public dummy2c As String = "value3"
	Public dummy2d As String = "value4"
	Public dummy2e As String = "value5"
	Public dummy2f As String = "value6"
	Public dummy2g As String = "value7"
	
	' VARIABLES HOJA 3
	Public dummy3a As String = "value1"
	Public dummy3b As String = "value2"
	Public dummy3c As String = "value3"
	Public dummy3d As String = "value4"
	Public dummy3e As String = "value5"
	Public dummy3f As String = "value6"
	Public dummy3g As String = "value7"
	
	
	
	' VARIABLES HOJA 5
	Public dummy4a As String = "value1"
	Public dummy4b As String = "value2"
	Public dummy4c As String = "value4"
	Public dummy4d As String = "value4"
	Public dummy4e As String = "value5"
	Public dummy4f As String = "value6"
	Public dummy4g As String = "value7"
	' VARIABLES HOJA 5
	
	Public dummy5a As String = "value1"
	Public dummy5b As String = "value2"
	Public dummy5c As String = "value5"
	Public dummy5d As String = "value4"
	Public dummy5e As String = "value5"
	Public dummy5f As String = "value6"
	Public dummy5g As String = "value7"
	
	'VARIABLES HOJA 6
	
	Public dummy6a As String = "value1"
	Public dummy6b As String = "value2"
	Public dummy6c As String = "value6"
	Public dummy6d As String = "value4"
	Public dummy6e As String = "value5"
	Public dummy6f As String = "value6"
	Public dummy6g As String = "value7"
	
	' VARIABLES HOJA 7
	
	Public dummy7a As String = "value1"
	Public dummy7b As String = "value2"
	Public dummy7c As String = "value7"
	Public dummy7d As String = "value4"
	Public dummy7e As String = "value5"
	Public dummy7f As String = "value6"
	Public dummy7g As String = "value7"
	
	'VARIABLES HOJA 8
	
	Public dummy8a As String = "value1"
	Public dummy8b As String = "value2"
	Public dummy8c As String = "value8"
	Public dummy8d As String = "value4"
	Public dummy8e As String = "value5"
	Public dummy8f As String = "value6"
	Public dummy8g As String = "value7"
	
	'VARIABLES HOJA 9
	
	Public dummy9a As String = "value1"
	Public dummy9b As String = "value2"
	Public dummy9c As String = "value9"
	Public dummy9d As String = "value4"
	Public dummy9e As String = "value5"
	Public dummy9f As String = "value6"
	Public dummy9g As String = "value7"
	
	Public fondoe, fotoe As String
	
	Public DownloadFolder As String = "/www/" & ABMShared.AppName & "/perfiles/"
	Public photosFolder As String = "/www/" & ABMShared.AppName & "/photos/"
	
	Public DownloadMaxSize As String = 1024*1024
	
	
	Dim fila As Int = 0
	Dim columna As Int = 0
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------
	Log("Connected")
	ws = WebSocket1
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
		' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
		page.Refresh
		page.FinishedLoading
	Else
		If page.WebsocketReconnected Then
			Log("Websocket reconnected")
			' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
			' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
			ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
		Else
			' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
			Log("Websocket first connection")
			page.Prepare
			ConnectPage
		End If
	End If
	Log(ABMPageId)
	'----------------------MODIFICATION-------------------------------
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")
End Sub

Sub Page_ParseEvent(Params As Map)
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)
		End Select
	End If
End Sub

public Sub BuildTheme()
	' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	

	
	'theme.Label("Tittle").ForeColorIntensity = ABM.INTENSITY_DARKEN1
	'pie de modal

	
	
	' add additional themes specific for this page
	
	'tema de las listas que contienen los contenedores
	
	theme.AddListTheme("listatheme")
	

		
	theme.List("listatheme").BackColor = ABM.COLOR_BROWN
	theme.List("listatheme").BackColorIntensity = ABM.INTENSITY_LIGHTEN4
	
	theme.List("listatheme").ItemBackColor = ABM.COLOR_BROWN
	theme.List("listatheme").ItemBackColorIntensity = ABM.INTENSITY_LIGHTEN4
		
	theme.List("listatheme").ItemActiveColor = ABM.COLOR_BROWN
	theme.List("listatheme").ItemActiveColorIntensity = ABM.INTENSITY_LIGHTEN4
	
	theme.List("listatheme").ItemHoverColor = ABM.COLOR_BROWN
	theme.List("listatheme").ItemHoverColorIntensity = ABM.INTENSITY_LIGHTEN4
	
	theme.List("listatheme").ItemDividerColor = ABM.COLOR_BROWN
	theme.List("listatheme").ItemDividerColorIntensity = ABM.INTENSITY_LIGHTEN4
	
	
	
	theme.List("listatheme").SubItemBackColor = ABM.COLOR_BROWN
	theme.List("listatheme").SubItemBackColorIntensity = ABM.INTENSITY_LIGHTEN4
	
	theme.List("listatheme").subItemActiveColor = ABM.COLOR_BROWN
	theme.List("listatheme").subItemActiveColorIntensity = ABM.INTENSITY_LIGHTEN4
	
	theme.List("listatheme").subItemHoverColor = ABM.COLOR_BROWN
	theme.List("listatheme").subItemHoverColorIntensity = ABM.INTENSITY_LIGHTEN4
	
	theme.List("listatheme").subItemDividerColor = ABM.COLOR_BROWN
	theme.List("listatheme").subItemDividerColorIntensity = ABM.INTENSITY_LIGHTEN4
	
	
	theme.List("listatheme").AddItemTheme("itemtheme")
	theme.List("listatheme").Item("itemtheme").ActiveColor = ABM.COLOR_BROWN
	theme.List("listatheme").Item("itemtheme").ActiveColorIntensity = ABM.INTENSITY_LIGHTEN4
	theme.List("listatheme").Item("itemtheme").BackColor = ABM.COLOR_BROWN
	theme.List("listatheme").Item("itemtheme").BackColorIntensity = ABM.INTENSITY_LIGHTEN4
	theme.List("listatheme").Item("itemtheme").HoverColor = ABM.COLOR_BROWN
	theme.List("listatheme").Item("itemtheme").HoverColorIntensity = ABM.INTENSITY_LIGHTEN4
	
	

	theme.List("listatheme").ZDepth = ABM.ZDEPTH_5
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "miperfil.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' adding a navigation bar
	
			
	' create the page grid
	' adding a navigation bar
	
	'	If Main.username = page.ws.Session.GetAttribute2("authName", "") Then
	If  Main.subordinado = False Then
		ABMShared.BuildNavigationBar(page,"Mi Página ","../images/klogo3.png","","miperfil","")
	Else
		ABMShared.BuildNavigationBar(page,"Sub. Página ","../images/klogo3.png","","miperfil","")
	End If
			
	' create the page grid
	page.AddRows(1,True, "").AddCells12(2,"") ' LINEA EN BLANCO (1)
	
	'CAJA 1	(1)		                'CAJA2	(2)				 	'CAJA 3  (3)
	page.AddRows(1,True, "").AddCellsOSMP(1,0,0,0,12,4,4,0,1,0,50,"").AddCellsOSMP(1,0,0,0,12,4,4,0,1,0,50,"").AddCellsOSMP(1,0,0,0,12,4,4,0,1,0,0,"") ' LC1 (2)
	
	page.AddRows(1,True, "").AddCellsOSMP(1,0,0,0,12,4,4,0,1,0,50,"").AddCellsOSMP(1,0,0,0,12,4,4,0,1,0,50,"").AddCellsOSMP(1,0,0,0,12,4,4,0,1,0,0,"") ' LC1 (4)
	
	page.AddRows(1,True, "").AddCellsOSMP(1,0,0,0,12,4,4,0,1,0,50,"").AddCellsOSMP(1,0,0,0,12,4,4,0,1,0,50,"").AddCellsOSMP(1,0,0,0,12,4,4,0,1,0,0,"") ' LC1 (4)
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	
	'Dim carpeta As String = File.Combine(File.DirApp, DownloadFolder)
	'Dim borro As Boolean = False
	'If File.Exists (carpeta,"default.jpg") Then
	'  		borro = File.Delete(carpeta,"default.jpg")
	'  	End If
	
	'page.AddExtraJavaScriptFile("custom/jquery.min.js")
	
	'page.SetBackgroundImage("../perfiles/default.jpg")
	

	
	' page.ws.Flush
	
		
End Sub

public Sub ConnectPage()
	
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)

	'page.ComesFromPageCache = False
	' Dim script As String = $" $('body').css('background-image', 'url(../images/backgrounds/header-top.jpg)');"$

	'	page.ws.Eval(script,Null)
	
	'page.ws.Flush	  'url(../perfiles/default.jpg)
		
		
		
		
	'	Dim script As String = $"$('body').css('background-image', '');
									
	'							 $('body').css('background-size', 'cover');
									 
	'	"$

	'Dim script As String =  $"   "$
	'    page.ws.Eval(script,Null)


	'page.AddExtraCSSFile()
	'	Dim script As String = $"$('body').css('background-attachment', 'fixed');"$

	'   page.ws.Eval(script,Null)
	
	'	Dim script As String = $"$('body').css('background-repeat', 'no-repeat');"$

	'  page.ws.Eval(script,Null)
	
	
	
	page.Pause

	
	
	Dim sql1 As SQL
	' TRAER INFORMACION DEL USUARIO SELECCIONADO
	sql1.Initialize(Main.strDrv,Main.strUrl)
	
	If Main.idsesion <> "" And Main.subordinado =True Then
		idusuario = Main.idsesion
			
	Else
			
		If ws.Session.GetAttribute2("idsesion", "") <> "" Then
			idusuario= ws.Session.GetAttribute2("idsesion", "")
		End If
		
	End If
	fondoe = ""
	fotoe = ""
		
		
	If Main.subordinado = True Then

		Dim rst As ResultSet
		rst=sql1.ExecQuery("Select * from RH_UserConf where id_user='"&idusuario&"'")

		Do While rst.NextRow
			username = rst.GetString("nom_user")
			numerotarjeta =  rst.GetString("id_user")
			idusuario =  rst.GetString("id_user")
			nombrecompleto = rst.GetString("nom_nombre")
			'fondoe = rst.GetString("nom_fondo")
			fotoe = rst.GetString("nom_foto")
		Loop
		If nombrecompleto = "" Then
			nombrecompleto = username
		End If
	Else
		username = ws.Session.GetAttribute("authName")
		numerotarjeta =  ws.Session.GetAttribute("idsesion")
		idusuario = ws.Session.GetAttribute("idsesion") 'rst.GetString("id_user")
		nombrecompleto = ws.Session.GetAttribute("nombre") 'rst.GetString("nom_nombre")
		fondoe = Main.fondoe 'rst.GetString("nom_fondo")
		fotoe = Main.fotoe 'rst.GetString("nom_foto")
	End If

	If  Not(fondoe=Null) And Not(fondoe="") And Not(Main.subordinado) And Not(fondoe="null") And Not(fondoe="no") And Not(fondoe="#") Then
		If fondoe.Length<10 Then
			Dim script As String = $"$('body').css('background-color', '${fondoe}');
													 $('body').attr('class','');	 
											"$
			page.ws.Eval(script,Null)
		Else
			Dim su As StringUtils
			Dim BT2() As Byte= su.DecodeBase64(fondoe)
			Dim imagen As Image = BytesToImage(BT2)
			Dim carpeta As String = File.Combine(File.DirApp, DownloadFolder)
						
			Dim nomfondo As String = idusuario&"b.jpg"
			Dim borro As Boolean = False
			If File.Exists (carpeta, idusuario&"b.jpg") Then
				borro = File.Delete(carpeta, idusuario&"b.jpg")
			End If
			
			Dim Out As OutputStream = File.OpenOutput(carpeta, idusuario&"b.jpg" , True)
			imagen.WriteToStream(Out)
			Out.Close
						
			Dim script As String = $"$('body').css('background-image', 'url(../perfiles/${nomfondo})');
						
						 $('body').css('background-size', 'cover');
						 
							"$
			page.ws.Eval(script,Null)
			
		End If
	End If
	If Not(fotoe=Null) And Not(fotoe="") And Not(fotoe="null") Then
		Dim su As StringUtils
		Dim BT2() As Byte= su.DecodeBase64(fotoe)
		Dim imagen As Image = BytesToImage(BT2)
		Dim carpeta As String = File.Combine(File.DirApp, DownloadFolder)
					
		Dim borro As Boolean = False
		If File.Exists (carpeta,idusuario&"f.jpg") Then
			borro = File.Delete(carpeta,idusuario&"f.jpg")
		End If
					
					
		Dim Out As OutputStream = File.OpenOutput(carpeta, idusuario&"f.jpg", True)
		imagen.WriteToStream(Out)
		Out.Close
				 
	End If
	
	' BOTON DE ACCION PARA CONF
	

	creaflotante


	'-------------------------------------- CONTENEDOR 1 (DATOS PERSONALES MI PERFIL)
	If Not(idusuario ="null") And Not(idusuario="") Then
		creacont1
	End If
	'-------------------------------------- CONTENEDOR 2 (DATOS MI CARRERA -- MIS CURSOS )
	
	creacont2
	
	
	
	'-------------------------------------- CONTENEDOR 3 (DATOS ATRACCION DE TALENTOS)
	
	creacont3
	
	
	
	'-------------------------------------- CONTENEDOR 4 (DATOS JAM)
	
	creacont4
	
	
	'-------------------------------------- CONTENEDOR 5 (DATOS NOMINA)
	
	creacont5
	
	
	'-------------------------------------- CONTENEDOR 6 (DATOS MI EQUIPO)   
	
	creacont6
	
	
	'-------------------------------------- CONTENEDOR 7 (DATOS ENTRENAMIENTO Y DESARROLLO)
	
	creacont7
	
	
	'-------------------------------------- CONTENEDOR 8 (DATOS EVAL DESEMPEÑO)
	
	creacont8
	
	
	
	'-------------------------------------- CONTENEDOR 9 (DATOS KPI)
	
	creacont9

		
	page.Resume
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
	
	
'	Dim script As String = $"
'	$('div').css('border-radius', '20px');
'		 							"$
'	
'	page.ws.Eval(script,Null)
'	
End Sub

Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	
	
	If SubTarget="" Then
		Log("CLICK EN CONF")
		Return
	End If
	
	If SubTarget="sms" Then
		
		Dim script As String = $"$('body').css('background-color', '#8ff442 !important');
										"$

						
		page.ws.Eval(script,Null)
	
	End If
	
	
	If SubTarget="ayuda" Then
		
		Dim script As String = $"$('body').css('background-image', 'url(../perfiles/default.jpg)');
		   
		   							$('body').css('background-size', 'cover');
		   "$

		page.ws.Eval(script,Null)
			
		'page.Refresh
		page.ws.Flush
		
	
	End If
	
	
	If SubTarget="conf" Then
	
		'page.Pause
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
		'page.Resume
	End If
	
	'	Dim myTexts, myReturns As List
	'	myTexts.Initialize
	'	myReturns.Initialize
	'	myToastId = myToastId + 1
	'	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	'Return
	
	
	
End Sub

Sub creacont1
	
	
	Try
		Dim res As ResultSet
		Dim sql1, sql2 As SQL
	
		Dim db As String =ws.Session.GetAttribute("DBUse")
		Dim personIdX As String
		sql1.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
		sql2.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
		If Main.subordinado=False Then
			Dim consulta As String = "Select * FROM OPENQUERY(GIN, 'select * from "&db&".dbo.vw_lgen_altasybajas where PKey_Key1=''"&idusuario&"''')"
		Else
			Dim consulta As String = "Select * FROM OPENQUERY(GIN, 'select * from "&db&".dbo.vw_lgen_altasybajas where PKey_Key1=''"&Main.idsesion&"''')"
		End If
		res = sql2.ExecQuery(consulta)
		Log("la primera" & consulta)
		Do While res.NextRow
			nombrecompleto=res.GetString("nombrecompleto") '----ojo
			numeronomina  = res.GetString("PKey_Key1")
			empresanomina  = res.GetString("PPay_Pay_DB")
			status1  = res.GetString("Stat_Descripcion")
			status2  = res.GetString("PKey_Status1")
			departamento  = res.GetString("Dept_Name")  '     sql2.ExecQuerySingleResult("Select Dept_Name from RH_Dept where Dept_Key='"&res.GetString("Dept_Name")&"'")
			iddepto=res.GetString("pkey_dept_key")
			puesto  = res.GetString("Pos_Name")
			centrocosto  = sql2.ExecQuerySingleResult("Select * FROM OPENQUERY(GIN, 'select Coc_Description from  "&ws.Session.GetAttribute("DBUse")&".dbo.RH_Costcenter  where Coc_Key = ''" & res.GetString("Pkey_Coc_Key") & "''')")
			rfc  = res.GetString("PNum_FiscalNo")
			imss  = res.GetString("PNum_SocialSecNo")
			curp  = res.GetString("PKey_Security_No") 'res.GetString("")
			fechaterminacion  = res.GetString("PDate_End")
			fechainicio=res.GetString("Pdate_contract")			
			personIdX=res.GetString("Person_id")
			Log("Select pdate_date9 FROM OPENQUERY(GIN, 'select pdate_date9 from  "&ws.Session.GetAttribute("DBUse")&".dbo.rh_pers_dates where person_id=''" & personIdX & "''')")
			fechakarisma=sql1.ExecQuerySingleResult("Select pdate_date9 FROM OPENQUERY(GIN, 'select pdate_date9 from  "&ws.Session.GetAttribute("DBUse")&".dbo.rh_pers_dates where person_id=''" & personIdX & "''')")

		Loop
		res.Close

		If fechaterminacion=Null Then fechaterminacion="-"
	
		res.Close
			 
		fila = 0
		columna = 0

		Dim contenedor1 As ABMContainer
		contenedor1.Initialize(page, "contenedor1", "cardtheme")
		contenedor1.AddRows(1,True,"").AddCellsOS(1,0,0,0,12,4,4,"cardtheme") ' ESPACIO LIBRE ARRIBA  1-2
		contenedor1.AddRows(1,False,"").AddCellsOSMP(1,0,0,0,12,4,4,20,0,0,0,"cardtheme")
		contenedor1.AddRowsM(1,True,0,0,"").AddCellsOS(1,0,0,0,9,9,9,"cardtheme").AddCellsOS(1,0,0,0,3,3,3,"cardtheme")   'NOMBRE Y FOTO 3
		contenedor1.AddRowsM(1,True,0,0,"").AddCellsOS(3,0,0,0,12,12,12,"cardtheme") ' ESPACIO LIBRE DEBAJO DEL NOMBRE 4
	
		'contenedor1.AddRowsM(16,True,6,6,"").AddCellsOSMP(1,0,0,0,6,6,6,0,0,0,0,"cardtheme").AddCellsOSMP(1,0,0,0,4,4,4,0,0,0,0,"cardtheme") ' CAMPOS DE DATOS 5-20
		contenedor1.AddRowsM(16,True,6,6,"").AddCellsOSMP(1,0,0,0,6,6,6,0,0,0,0,"cardtheme").AddCellsOSMP(1,0,0,0,4,4,4,0,0,0,0,"cardtheme") ' CAMPOS DE DATOS 5-20
		contenedor1.AddRows(2,True,"").AddCellsOS(1,0,0,0,12,4,4,"cardtheme") ' ESPACIO LIBRE ABAJO 21-22
		contenedor1.BuildGrid
		contenedor1.Cell(2,1).SetOffsetSize(0,0,0,12,12,12)
		contenedor1.Cell(2,1).UseTheme("TittleCell")
	
	
		fila = 7
		columna = 1
	
		Dim Tittle1 As ABMLabel
	
		If Not(Main.subordinado) Then
			Tittle1.Initialize(page,"Tittle1","MI PERFIL",ABM.SIZE_H6,False,"Tittle")
		Else
			Tittle1.Initialize(page,"Tittle1","SUB-PERFIL",ABM.SIZE_H6,False,"Tittle")
		End If
	
		contenedor1.Cell(2,1).AddComponent(Tittle1)
	
		Dim contenedor1a As ABMContainer
		contenedor1a.Initialize(page, "contenedor1a", "")
		contenedor1a.AddRows(2,True,"").AddCellsOS(1,0,0,0,12,8,8,"cardtheme")
		contenedor1a.BuildGrid
		contenedor1.cell(3,1).AddComponent(contenedor1a)
		Dim img As ABMImage
		If File.Exists(File.Combine(File.DirApp, DownloadFolder),idusuario&"f.jpg") Then
			img.Initialize(page,"1", "../perfiles/"&idusuario&"f.jpg",1.0)
		Else
		
			If File.Exists(File.Combine(File.DirApp,photosFolder &ws.Session.GetAttribute("DBUse") ),personIdX&".jpg") Then
				img.Initialize(page,"1","../photos/"&ws.Session.GetAttribute("DBUse")&"/"&personIdX&".jpg",1)
			Else
				img.Initialize(page,"1", "../perfiles/user.png",1.0)
			End If
		End If
		img.Caption="Num.1"
		img.Tag=1
		img.IsCircular = False
		img.IsClickable=True
		img.IsMaterialBoxed=True
		img.IsResponsive=True
		contenedor1.cell(3,2).AddArrayComponent(img,"myimg")
	

		Dim nombrecompletov As ABMLabel

		nombrecompletov.Initialize(page,"nombrecompletov",nombrecompleto,ABM.SIZE_H5,False,"cardtheme")
		contenedor1a.Cell(1,1).AddComponent(nombrecompletov)


		'-------------------------
		Dim puestot As ABMLabel
		puestot.Initialize(page,"puestot","Posición",ABM.SIZE_H6,False,"cardtheme")
		contenedor1.cell(fila,columna).AddComponent(puestot)
	
		Dim puestov As ABMLabel
		puestov.Initialize(page,"puestov",puesto,ABM.SIZE_H6,False,"cardtheme")
		contenedor1.cell(fila,columna+1).AddComponent(puestov)
		
		'-------------------------
		Dim departamentot As ABMLabel
		departamentot.Initialize(page,"departamentot","Departamento",ABM.SIZE_H6,False,"cardtheme")
		contenedor1.cell(fila+1,columna).AddComponent(departamentot)
	
		Dim departamentov As ABMLabel
		departamentov.Initialize(page,"departamentov",departamento,ABM.SIZE_H6,False,"cardtheme")
		contenedor1.cell(fila+1,columna+1).AddComponent(departamentov)
		
		'-------------------------
		Dim centrocostot As ABMLabel
		centrocostot.Initialize(page,"centrocostot","Centro de costo",ABM.SIZE_H6,False,"cardtheme")
		contenedor1.cell(fila+2,columna).AddComponent(centrocostot)
	
		Dim centrocostov As ABMLabel
		centrocostov.Initialize(page,"centrocostov",centrocosto,ABM.SIZE_H6,False,"cardtheme")
		contenedor1.cell(fila+2,columna+1).AddComponent(centrocostov)
		
		'-------------------------
		Dim status1t As ABMLabel
		status1t.Initialize(page,"status1t","Estatus",ABM.SIZE_H6,False,"cardtheme")
		contenedor1.cell(fila+3,columna).AddComponent(status1t)
	
		Dim status1v As ABMLabel
		status1v.Initialize(page,"status1v",status1,ABM.SIZE_H6,False,"cardtheme")
		contenedor1.cell(fila+3,columna+1).AddComponent(status1v)
		
		'-------------------------
		Dim curpt As ABMLabel
		curpt.Initialize(page,"curpt","CURP",ABM.SIZE_H6,False,"cardtheme")
		contenedor1.cell(fila+4,columna).AddComponent(curpt)
	
		Dim curpv As ABMLabel
		curpv.Initialize(page,"curpv",curp,ABM.SIZE_H6,False,"cardtheme")
		contenedor1.cell(fila+4,columna+1).AddComponent(curpv)
		
		'-------------------------
		Dim rfct As ABMLabel
		rfct.Initialize(page,"rfct","RFC",ABM.SIZE_H6,False,"cardtheme")
		contenedor1.cell(fila+5,columna).AddComponent(rfct)
	
		Dim rfcv As ABMLabel
		rfcv.Initialize(page,"rfcv",rfc,ABM.SIZE_H6,False,"cardtheme")
		contenedor1.cell(fila+5,columna+1).AddComponent(rfcv)
		
		'-------------------------
		Dim imsst As ABMLabel
		imsst.Initialize(page,"imsst","IMSS",ABM.SIZE_H6,False,"cardtheme")
		contenedor1.cell(fila+6,columna).AddComponent(imsst)
	
		Dim imssv As ABMLabel
		imssv.Initialize(page,"imssv",imss,ABM.SIZE_H6,False,"cardtheme")
		contenedor1.cell(fila+6,columna+1).AddComponent(imssv)
		
		'-------------------------
		Dim fechainiciot As ABMLabel
		fechainiciot.Initialize(page,"fechainiciot","Fecha de ingreso",ABM.SIZE_H6,False,"cardtheme")
		contenedor1.cell(fila+7,columna).AddComponent(fechainiciot)
	
		If fechainicio.Length < 7 Then
			fechainicio = "N/A"
	
		Else
			fechainicio = ABMShared.ConvertDate(fechainicio,"yyyy-MM-dd","MMMM dd yyyy")
		End If
	
	
	
		Dim fechainiciov As ABMLabel
		fechainiciov.Initialize(page,"fechainiciov",fechainicio,ABM.SIZE_H6,False,"cardtheme")
		contenedor1.cell(fila+7,columna+1).AddComponent(fechainiciov)
	
	
	
		'-------------------------
		Dim fechaterminaciont As ABMLabel
		fechaterminaciont.Initialize(page,"fechaterminaciont","Fecha de antiguedad",ABM.SIZE_H6,False,"cardtheme")
		contenedor1.cell(fila+8,columna).AddComponent(fechaterminaciont)
	
	
		If fechakarisma.Length < 7 Then
			fechakarisma = "N/A"
	
		Else
			fechakarisma = ABMShared.ConvertDate(fechakarisma,"yyyy-mm-dd","MMMM dd yyyy")
		End If
	
	
		Dim fechaterminacionv As ABMLabel
		fechaterminacionv.Initialize(page,"fechaterminacionv",fechakarisma,ABM.SIZE_H6,False,"cardtheme")
		contenedor1.cell(fila+8,columna+1).AddComponent(fechaterminacionv)
	

'--------------------------------------------	
		numerotarjeta = ws.Session.GetAttribute2("idsesion", "")
	
		Dim numerotarjetas As ABMLabel
		numerotarjetas.Initialize(page,"numerotarjetas",numerotarjeta,ABM.SIZE_H6,False,"cardtheme")
		'contenedor1a.Cell(2,1).AddComponent(numerotarjetas)
	
	
		' add a divider on the row
		Dim div1 As ABMDivider
		div1.Initialize(page, "div1", "")
	
		'contenedor1.Row(5).SetDivider(div1)
	
		'-------------------------
		Dim numerotarjetat As ABMLabel
		numerotarjetat.Initialize(page,"numerotarjetat","Tarjeta Num.",ABM.SIZE_H6,False,"cardtheme")
		'contenedor1.cell(fila,columna).AddComponent(numerotarjetat)
	
		Dim numerotarjetav As ABMLabel
		If Main.subordinado=False Then
			numerotarjetav.Initialize(page,"numerotarjetav",numerotarjeta,ABM.SIZE_H6,False,"cardtheme")
		Else
			numerotarjetav.Initialize(page,"numerotarjetav",Main.idsesion,ABM.SIZE_H6,False,"cardtheme")
		End If
		'contenedor1.cell(fila,columna+1).AddComponent(numerotarjetav)
	
	
		'-------------------------
		Dim numeronominat As ABMLabel
		numeronominat.Initialize(page,"numeronominat","Roster Num.",ABM.SIZE_H6,False,"cardtheme")
		'contenedor1.cell(fila,columna).AddComponent(numeronominat)
	
		Dim numeronominav As ABMLabel
		numeronominav.Initialize(page,"numeronominav",numeronomina,ABM.SIZE_H6,False,"cardtheme")
		'contenedor1.cell(fila,columna+1).AddComponent(numeronominav)
	
		'-------------------------
		Dim empresanominat As ABMLabel
		empresanominat.Initialize(page,"empresanominat","División",ABM.SIZE_H6,False,"cardtheme")
		'contenedor1.cell(fila+2,columna).AddComponent(empresanominat)
	
		Dim empresanominav As ABMLabel
		empresanominav.Initialize(page,"empresanominav",empresanomina,ABM.SIZE_H6,False,"cardtheme")
		'contenedor1.cell(fila+2,columna+1).AddComponent(empresanominav)
	

		'-------------------------
		Dim status2t As ABMLabel
		status2t.Initialize(page,"status2t","Estatus 2",ABM.SIZE_H6,False,"cardtheme")
		'contenedor1.cell(fila+4,columna).AddComponent(status2t)
	
		Dim status2v As ABMLabel
		status2v.Initialize(page,"status2v",status2,ABM.SIZE_H6,False,"cardtheme")
		'contenedor1.cell(fila+4,columna+1).AddComponent(status2v)
	

	
		
	

		Dim lista1 As ABMList
		'list1.Initialize(page, "list1", ABM.COLLAPSE_ACCORDION, "")
	
		lista1.InitializeWithMaxHeight(page,"lista1",ABM.COLLAPSE_ACCORDION,400, "listatheme")

		lista1.AddItemWithTheme("L1", contenedor1, "itemtheme")
		
		
		page.Cell(2,1).AddComponent(lista1)
	Catch
		Log(LastException.Message)
	End Try
End Sub

Sub creacont2
	
	fila = 0
	columna = 0
	'cursos
	Dim sqlC As SQL
	Dim rstC As ResultSet
	sqlC.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
	Dim consulta As String
	Dim person As String
	Dim ctax As ResultSet
	Dim CAx,CBx,CEx,CMx,CTx,OTx As String
	If Main.subordinado=False Then
		consulta="Select * from Openquery(GIN,'Select Person_id from "&  ws.Session.GetAttribute("DBUse") & ".dbo.vw_Person_Diagrama where Pkey_key1=''" & idusuario &"''')"
		person=sqlC.ExecQuerySingleResult(consulta)
	Else
		consulta="Select * from Openquery(GIN,'Select Person_id from "&  ws.Session.GetAttribute("DBUse") & ".dbo.vw_Person_Diagrama where Pkey_key1=''" & Main.idsesion  &"''')"
		person=sqlC.ExecQuerySingleResult(consulta)
	End If
	consulta="Select * from openquery(GIN,'select count(*) as ctaX from " &  ws.Session.GetAttribute("DBUse") &".dbo.VW_MASTER_LGEN_TRAINNING1 where Person_id="& person  &"')"
	Log("ANTES " &consulta)
	ctax=sqlC.ExecQuery(consulta)
	Dim resuta As String
	Do While ctax.NextRow
		resuta=ctax.GetString("ctax")
	Loop
	If resuta<>"0" Then
		consulta="Select * from Openquery (GIN,'select distinct SUBSTRING(train_name,0,3) as ''Sub'',Count(*) as ''Cta'' from " &  ws.Session.GetAttribute("DBUse") &".dbo.VW_MASTER_LGEN_TRAINNING1 where Person_id="& person  &" Group by SUBSTRING(train_name,0,3)') "
		rstC=sqlC.ExecQuery(consulta)
		Do While rstC.NextRow
			If rstC.GetString("Sub")="CA" Then
				CAx=rstC.GetString("Cta")
			else If rstC.GetString("Sub")="CB" Then
				CBx=rstC.GetString("Cta")
			else If rstC.GetString("Sub")="CE" Then
				CEx=rstC.GetString("Cta")
			else If rstC.GetString("Sub")="CM" Then
				CMx=rstC.GetString("Cta")
			else If rstC.GetString("Sub")="CT" Then
				CTx=rstC.GetString("Cta")
			Else
				OTx=rstC.GetString("Cta")
			End If
		Loop
	End If
	
	Dim contenedor2 As ABMContainer
	contenedor2.Initialize(page, "contenedor2", "cardtheme")
	contenedor2.AddRowsM(2,False,10,20,"").AddCellsOSMP(3,0,0,0,12,4,4,5,1,5,10,"cardtheme") ' Tittle  1
	contenedor2.AddRowsM(7,True,6,6,"").AddCellsOSMP(1,0,0,0,1,1,1,0,0,0,0,"cardtheme").AddCellsOSMP(1,0,0,0,5,5,5,0,0,10,0,"cardtheme").AddCellsOSMP(1,0,0,0,5,5,5,0,0,50,0,"cardtheme").AddCellsOSMP(1,0,0,0,1,1,1,0,0,0,0,"cardtheme") ' CAMPOS DE DATOS 2-7
	fila = 3
	columna = 2
	contenedor2.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,4,4,0,10,10,0,"cardtheme") ' ESPACIO LIBRE ABAJO 8
	contenedor2.BuildGrid
	contenedor2.Cell(2,1).SetOffsetSize(0,0,0,12,12,12)
	contenedor2.Cell(2,1).UseTheme("TittleCell")
	
	'-----------------------------
	Dim Tittle2 As ABMLabel
	Tittle2.Initialize(page,"Tittle2","MIS CURSOS",ABM.SIZE_H6,False,"Tittle")
	Tittle2.Clickable=True
	contenedor2.Cell(2,1).AddComponent(Tittle2)
	
	'Tittle2.IconName = "mdi-action-account-balance"
	
	'-------------------------
	Dim dummy2at As ABMLabel
	dummy2at.Initialize(page,"value2at","Certificación básica",ABM.SIZE_H6,False,"cardtheme")
	contenedor2.Cell(fila,columna).AddComponent(dummy2at)
	
	Dim dummy2av As ABMLabel
	dummy2av.Initialize(page,"value2av",CBx,ABM.SIZE_H6,False,"cardtheme")
	contenedor2.Cell(fila,columna+1).AddComponent(dummy2av)
	
	'-------------------------
	Dim dummy2bt As ABMLabel
	dummy2bt.Initialize(page,"value2bt","Certificación intermedia",ABM.SIZE_H6,False,"cardtheme")
	contenedor2.Cell(fila+1,columna).AddComponent(dummy2bt)
	
	Dim dummy2bv As ABMLabel
	dummy2bv.Initialize(page,"value2bv",CMx,ABM.SIZE_H6,False,"cardtheme")
	contenedor2.Cell(fila+1,columna+1).AddComponent(dummy2bv)
	
	'-------------------------
	Dim dummy2ct As ABMLabel
	dummy2ct.Initialize(page,"value2ct","Certificación avanzada",ABM.SIZE_H6,False,"cardtheme")
	contenedor2.Cell(fila+2,columna).AddComponent(dummy2ct)
	
	Dim dummy2cv As ABMLabel
	dummy2cv.Initialize(page,"value2cv",CAx,ABM.SIZE_H6,False,"cardtheme")
	contenedor2.Cell(fila+2,columna+1).AddComponent(dummy2cv)
	
	
	'-------------------------
	Dim dummy2dt As ABMLabel
	dummy2dt.Initialize(page,"value2et","Certificación técnica",ABM.SIZE_H6,False,"cardtheme")
	
	contenedor2.Cell(fila+3,columna).AddComponent(dummy2dt)
	
	
	Dim dummy2dv As ABMLabel
	dummy2dv.Initialize(page,"value2ev",CTx,ABM.SIZE_H6,False,"cardtheme")
	contenedor2.Cell(fila+3,columna+1).AddComponent(dummy2dv)
	
	'-------------------------
	Dim dummy2et As ABMLabel
	dummy2et.Initialize(page,"value2et","Certificación especializada",ABM.SIZE_H6,False,"cardtheme")
	contenedor2.Cell(fila+4,columna).AddComponent(dummy2et)
	
	Dim dummy2ev As ABMLabel
	dummy2ev.Initialize(page,"value2ev",CEx,ABM.SIZE_H6,False,"cardtheme")
	contenedor2.Cell(fila+4,columna+1).AddComponent(dummy2ev)
	
	'-------------------------
	Dim dummy2ft As ABMLabel
	dummy2ft.Initialize(page,"value2ft","Otros",ABM.SIZE_H6,False,"cardtheme")
	contenedor2.Cell(fila+5,columna).AddComponent(dummy2ft)
	
	Dim dummy2fv As ABMLabel
	dummy2fv.Initialize(page,"value2fv",OTx,ABM.SIZE_H6,False,"cardtheme")
	contenedor2.Cell(fila+5,columna+1).AddComponent(dummy2fv)
	
	'-------------------------
	Dim dummy2gt As ABMLabel
	dummy2gt.Initialize(page,"value2gt","Ago. 2014",ABM.SIZE_H6,False,"cardtheme")
	'contenedor2.Cell(fila+6,columna).AddComponent(dummy2gt)
	
	Dim dummy2gv As ABMLabel
	dummy2gv.Initialize(page,"value2gv","Manejo de alimentos",ABM.SIZE_H6,False,"cardtheme")
	'contenedor2.Cell(fila+6,columna+1).AddComponent(dummy2gv)
	
	
	Dim lista2 As ABMList
	'list1.Initialize(page, "list1", ABM.COLLAPSE_ACCORDION, "")
	
	lista2.InitializeWithMaxHeight(page,"lista2",ABM.COLLAPSE_ACCORDION,400, "listatheme")
		
	lista2.AddItemWithTheme("L2", contenedor2, "itemtheme")
		
	page.Cell(3,3).AddComponent(lista2)
	
End Sub
Sub Tittle2_Clicked(Target As String)
	Log("mi carrera")
	ABMShared.NavigateToPage(ws,ABMPageId,"../micarrera/micarrera.html")
End Sub






Sub creacont3
	fila = 0
	columna = 0
	
	Dim contenedor3 As ABMContainer
	contenedor3.Initialize(page, "contenedor3", "cardtheme")
	contenedor3.AddRowsM(2,False,10,20,"").AddCellsOSMP(3,0,0,0,12,4,4,5,1,5,10,"cardtheme") ' Tittle  1
	contenedor3.AddRowsM(7,True,6,6,"").AddCellsOSMP(1,0,0,0,1,1,1,0,0,0,0,"cardtheme").AddCellsOSMP(1,0,0,0,5,5,5,0,0,10,0,"cardtheme").AddCellsOSMP(1,0,0,0,5,5,5,0,0,50,0,"cardtheme").AddCellsOSMP(1,0,0,0,1,1,1,0,0,0,0,"cardtheme") ' CAMPOS DE DATOS 2-7
	fila = 3
	columna = 2
	contenedor3.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,4,4,0,10,10,0,"cardtheme") ' ESPACIO LIBRE ABAJO 8
	
	contenedor3.BuildGrid
	contenedor3.Cell(2,1).SetOffsetSize(0,0,0,12,12,12)
	contenedor3.Cell(2,1).UseTheme("TittleCell")
	
	
	'-----------------------------
	Dim Tittle3 As ABMLabel
	Tittle3.Initialize(page,"Tittle3","ATRACCIÓN DE TALENTOS",ABM.SIZE_H6,False,"Tittle")
	Tittle3.Clickable=True
	contenedor3.Cell(2,1).AddComponent(Tittle3)
	
	'-------------------------
	Dim dummy3at As ABMLabel
	dummy3at.Initialize(page,"value3at","Vacantes:",ABM.SIZE_H6,False,"cardtheme")
	contenedor3.Cell(fila,columna).AddComponent(dummy3at)
	
	Dim dummy3av As ABMLabel
	dummy3av.Initialize(page,"value3av","",ABM.SIZE_H6,False,"cardtheme")
	contenedor3.Cell(fila,columna+1).AddComponent(dummy3av)
	
	'-------------------------
	Dim dummy3bt As ABMLabel
	dummy3bt.Initialize(page,"value3bt","Cubiertos",ABM.SIZE_H6,False,"cardtheme")
	contenedor3.Cell(fila+1,columna).AddComponent(dummy3bt)
	
	Dim dummy3bv As ABMLabel
	dummy3bv.Initialize(page,"value3bv","",ABM.SIZE_H6,False,"cardtheme")
	contenedor3.Cell(fila+1,columna+1).AddComponent(dummy3bv)
	
	'-------------------------
	Dim dummy3ct As ABMLabel
	dummy3ct.Initialize(page,"value3ct","Revisión de CV",ABM.SIZE_H6,False,"cardtheme")
	contenedor3.Cell(fila+2,columna).AddComponent(dummy3ct)
	
	Dim dummy3cv As ABMLabel
	dummy3cv.Initialize(page,"value3cv","",ABM.SIZE_H6,False,"cardtheme")
	contenedor3.Cell(fila+2,columna+1).AddComponent(dummy3cv)
	
	
	'-------------------------
	Dim dummy3dt As ABMLabel
	dummy3dt.Initialize(page,"value3et","Entrevistas pendientes",ABM.SIZE_H6,False,"cardtheme")
	contenedor3.Cell(fila+3,columna).AddComponent(dummy3dt)
	
	Dim dummy3dv As ABMLabel
	dummy3dv.Initialize(page,"value3ev","",ABM.SIZE_H6,False,"cardtheme")
	contenedor3.Cell(fila+3,columna+1).AddComponent(dummy3dv)
	
	'-------------------------
	Dim dummy3et As ABMLabel
	dummy3et.Initialize(page,"value3et","Solicitud de personal",ABM.SIZE_H6,False,"cardtheme")
	contenedor3.Cell(fila+4,columna).AddComponent(dummy3et)
	
	Dim dummy3ev As ABMLabel
	dummy3ev.Initialize(page,"value3ev","",ABM.SIZE_H6,False,"cardtheme")
	contenedor3.Cell(fila+4,columna+1).AddComponent(dummy3ev)
	
	'-------------------------
	Dim dummy3ft As ABMLabel
	dummy3ft.Initialize(page,"value3ft","value3f",ABM.SIZE_H6,False,"cardtheme")
	'contenedor3.Cell(fila+5,columna).AddComponent(dummy3ft)
	
	Dim dummy3fv As ABMLabel
	dummy3fv.Initialize(page,"value3fv",dummy3f,ABM.SIZE_H6,False,"cardtheme")
	'contenedor3.Cell(fila+5,columna+1).AddComponent(dummy3fv)
	
	'-------------------------
	Dim dummy3gt As ABMLabel
	dummy3gt.Initialize(page,"value3gt","value3g",ABM.SIZE_H6,False,"cardtheme")
	'contenedor3.Cell(fila+6,columna).AddComponent(dummy3gt)
	
	Dim dummy3gv As ABMLabel
	dummy3gv.Initialize(page,"value3gv",dummy3g,ABM.SIZE_H6,False,"cardtheme")
	'contenedor3.Cell(fila+6,columna+1).AddComponent(dummy3gv)
	
	Dim lista3 As ABMList
	'list1.Initialize(page, "list1", ABM.COLLAPSE_ACCORDION, "")
	
	lista3.InitializeWithMaxHeight(page,"lista3",ABM.COLLAPSE_ACCORDION,400, "listatheme")
		
	lista3.AddItemWithTheme("L3", contenedor3,"itemtheme")
		
	page.Cell(3,2).AddComponent(lista3)
	
End Sub
Sub Tittle3_Clicked(Target As String)
	page.AddModalSheetTemplate(Descripcion("Titulo","AA"))
	page.Refresh
	page.ShowModalSheet("descripcion")
	
End Sub

Sub creacont4
	
	'consltar vacaciones pendientes
	Dim sql2 As SQL
	Dim consulta As String
	sql2.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
	
	Dim diasA,diasP,person  As String
	Dim consulta3 As String
	Dim rst As ResultSet
	If Main.subordinado=False Then
		consulta="Select * FROM OPENQUERY(GIN, 'select top 1 Vac_days_anniversary , Vac_date_period from  "&ws.Session.GetAttribute("DBUse")&".dbo.RH_PERS_VACATIONS  where Vac_Person_Id = ''" & ws.Session.GetAttribute("Person_id") & "'' order by Vac_period desc')"
	
	Else
		consulta="Select * from Openquery(GIN,'Select Person_id from "&  ws.Session.GetAttribute("DBUse") & ".dbo.vw_Person_Diagrama where Pkey_key1=''" & Main.idsesion  &"''')"
		person=sql2.ExecQuerySingleResult(consulta)
		consulta="Select * FROM OPENQUERY(GIN, 'select top 1 Vac_days_anniversary , Vac_date_period from  "&ws.Session.GetAttribute("DBUse")&".dbo.RH_PERS_VACATIONS  where Vac_Person_Id = ''" & person & "'' order by Vac_period desc')"
	End If
	Log(consulta)
	rst=sql2.ExecQuery(consulta)
	rst.NextRow
	diasA=ABMShared.ConvertDate(rst.GetString("Vac_date_period"),"yyyy-MM-dd","dd MMM yyyy")
	Log(diasA)
	Dim difDate As Period
	'diasP=DateTime.Date(DateTime.Now)
	'difDate=ABMShared.Periodo(ABMShared.ConvertDate(rst.GetString("Vac_date_period"),"yyyy-MM-dd HH:mm:ss","MM/dd/yyyy HH:mm:ss"),ABMShared.ConvertDate(DateTime.Date(DateTime.Now),"yyyy-MM-dd HH:mm:ss","MM/dd/yyyy HH:mm:ss"))
	difDate=ABMShared.Periodo("08/16/2016 00:00:00","04/26/2017 00:00:00")
	Log("PERIODO " & difDate.Days)
	
	Log(difDate.Years)
	Log("Meses " & difDate.Months)
	Log(DateTime.DateFormat)
	DateTime.DateFormat="MM/dd/yyyy"
	Dim f1 As Long=DateTime.DateParse("08/16/2016")
	Log(f1)
	Dim p As Period = DateUtils.PeriodBetweenInDays(DateTime.DateParse("08/16/2016"),DateTime.DateParse("04/26/2017"))
	
	Log("P  " & p.Days)
	
	
	fila = 0
	columna = 0
	
	Dim contenedor4 As ABMContainer
	contenedor4.Initialize(page, "contenedor4", "cardtheme")
	contenedor4.AddRowsM(2,False,10,20,"").AddCellsOSMP(3,0,0,0,12,4,4,5,1,5,10,"cardtheme") ' Tittle  1
	contenedor4.AddRowsM(7,True,6,6,"").AddCellsOSMP(1,0,0,0,1,1,1,0,0,0,0,"cardtheme").AddCellsOSMP(1,0,0,0,5,5,5,0,0,10,0,"cardtheme").AddCellsOSMP(1,0,0,0,5,5,5,0,0,50,0,"cardtheme").AddCellsOSMP(1,0,0,0,1,1,1,0,0,0,0,"cardtheme") ' CAMPOS DE DATOS 2-7
	fila = 3
	columna = 2
	contenedor4.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,4,4,0,10,10,0,"cardtheme") ' ESPACIO LIBRE ABAJO 8
	
	contenedor4.BuildGrid
	contenedor4.Cell(2,1).SetOffsetSize(0,0,0,12,12,12)
	contenedor4.Cell(2,1).UseTheme("TittleCell")
	'-----------------------------
	Dim Tittle4 As ABMLabel
	Tittle4.Initialize(page,"Tittle4","MIS BENEFICIOS",ABM.SIZE_H6,False,"Tittle")
	contenedor4.Cell(2,1).AddComponent(Tittle4)
	'-------------------------
	Dim dummy4at As ABMLabel
	dummy4at.Initialize(page,"value4at","Inicio de periodo vac.",ABM.SIZE_H6,False,"cardtheme")
	contenedor4.Cell(fila,columna).AddComponent(dummy4at)
	
	Dim dummy4av As ABMLabel
	dummy4av.Initialize(page,"value4av",diasA,ABM.SIZE_H6,False,"cardtheme")
	contenedor4.Cell(fila,columna+1).AddComponent(dummy4av)
	
	'-------------------------
	Dim dummy4bt As ABMLabel
	dummy4bt.Initialize(page,"value4bt","Días transcurridos",ABM.SIZE_H6,False,"cardtheme")
	contenedor4.Cell(fila+1,columna).AddComponent(dummy4bt)
	
	Dim dummy4bv As ABMLabel
	dummy4bv.Initialize(page,"value4bv",p.Days,ABM.SIZE_H6,False,"cardtheme")
	contenedor4.Cell(fila+1,columna+1).AddComponent(dummy4bv)
	
	'-------------------------
	Dim dummy4ct As ABMLabel
	dummy4ct.Initialize(page,"value4ct","Días de vacaciones",ABM.SIZE_H6,False,"cardtheme")
	contenedor4.Cell(fila+2,columna).AddComponent(dummy4ct)
	
	Dim dummy4cv As ABMLabel
	dummy4cv.Initialize(page,"value4cv","03",ABM.SIZE_H6,False,"cardtheme")
	'contenedor4.Cell(fila+2,columna+1).AddComponent(dummy4cv)
	
	
	'-------------------------
	Dim dummy4dt As ABMLabel
	dummy4dt.Initialize(page,"value4et","Días de aguinaldo ",ABM.SIZE_H6,False,"cardtheme")
	contenedor4.Cell(fila+3,columna).AddComponent(dummy4dt)
	
	Dim dummy4dv As ABMLabel
	dummy4dv.Initialize(page,"value4ev","02",ABM.SIZE_H6,False,"cardtheme")
	'contenedor4.Cell(fila+3,columna+1).AddComponent(dummy4dv)
	
	'-------------------------
	Dim dummy4et As ABMLabel
	dummy4et.Initialize(page,"value4et","% de prima vac.",ABM.SIZE_H6,False,"cardtheme")
	contenedor4.Cell(fila+4,columna).AddComponent(dummy4et)
	
	Dim dummy4ev As ABMLabel
	dummy4ev.Initialize(page,"value4ev","02",ABM.SIZE_H6,False,"cardtheme")
	'contenedor4.Cell(fila+4,columna+1).AddComponent(dummy4ev)
	
	'-------------------------
	Dim dummy4ft As ABMLabel
	dummy4ft.Initialize(page,"value4ft","value4f",ABM.SIZE_H6,False,"cardtheme")
	'contenedor4.Cell(fila+5,columna).AddComponent(dummy4ft)
	
	Dim dummy4fv As ABMLabel
	dummy4fv.Initialize(page,"value4fv",dummy4f,ABM.SIZE_H6,False,"cardtheme")
	'contenedor4.Cell(fila+5,columna+1).AddComponent(dummy4fv)
	
	'-------------------------
	Dim dummy4gt As ABMLabel
	dummy4gt.Initialize(page,"value4gt","value4g",ABM.SIZE_H6,False,"cardtheme")
	'contenedor4.Cell(fila+6,columna).AddComponent(dummy4gt)
	
	Dim dummy4gv As ABMLabel
	dummy4gv.Initialize(page,"value4gv",dummy4g,ABM.SIZE_H6,False,"cardtheme")
	'contenedor4.Cell(fila+6,columna+1).AddComponent(dummy4gv)
	
	Dim lista4 As ABMList
	
	lista4.InitializeWithMaxHeight(page,"lista4",ABM.COLLAPSE_ACCORDION,400, "listatheme")
	
	lista4.AddItemWithTheme("L4", contenedor4,"itemtheme")
	
	page.Cell(3,1).AddComponent(lista4)
	
End Sub

Sub creacont5
	fila = 0
	columna = 0
	Dim sql2 As SQL
	Dim res , res2 As ResultSet
	sql2.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
	If Main.subordinado=False Then
		Dim consulta As String = "Select * FROM OPENQUERY(GIN, 'select * from "&ws.Session.GetAttribute("DBUse")&".dbo.vw_lgen_altasybajas where PKey_Key1=''"&idusuario&"''')"
		Dim consulta2 As String = "Select * FROM OPENQUERY(SELECT * FROM "&ws.Session.GetAttribute("DBUse")&".[dbo].[RH_Pers_Payroll] inner join "&ws.Session.GetAttribute("DBUse")&".[dbo].[RH_Pers_Keys] on [PPay_Person_Id] = [Person_Id] And PKey_Pay_Db = PPay_Pay_DB where PKey_Pay_No = ''"&idusuario&"''')"
	Else
		Dim consulta As String = "Select * FROM OPENQUERY(GIN, 'select * from "&ws.Session.GetAttribute("DBUse")&".dbo.vw_lgen_altasybajas where PKey_Key1=''"&Main.idsesion&"''')"
		Dim consulta2 As String = "Select * FROM OPENQUERY(SELECT * FROM "&ws.Session.GetAttribute("DBUse")&".[dbo].[RH_Pers_Payroll] inner join "&ws.Session.GetAttribute("DBUse")&".[dbo].[RH_Pers_Keys] on [PPay_Person_Id] = [Person_Id] And PKey_Pay_Db = PPay_Pay_DB where PKey_Pay_No = ''"&Main.idsesion&"''')"
	End If
	Log("Salario" & consulta2)
	
	Dim tipoC,RFCx,Imssx, categoria, salmens, saldia, diasvac, primavac, fondoahorro As String
	
	res = sql2.ExecQuery(consulta)
	res2 =  sql2.ExecQuery(consulta)
	
	Do While res2.NextRow
		saldia = res2.GetString("PPay_Normal_Salary2")
		salmens = saldia * 30.4
	Loop
	
	
	res2.Close
	
	Do While res.NextRow
		
		categoria = sql2.ExecQuerySingleResult("Select * FROM OPENQUERY(GIN, 'select Cat_Description from  "&ws.Session.GetAttribute("DBUse")&".dbo.RH_Category  where Cat_Key = ''" & res.GetString("Pos_Status") & "''')")
		tipoC=res.GetString("PPay_contract_type")
		'CURPx=res.GetString("Pkey_security_no")
		RFCx=res.GetString("Pnum_fiscalno")
		Imssx=res.GetString("Pnum_socialsecno")
	Loop
	
	res.Close
	
	If tipoC="P" Then tipoC="INDETERMINADO"
	If tipoC="E" Then tipoC="TEMPORAL"
	
	
	Dim contenedor5 As ABMContainer
	contenedor5.Initialize(page, "contenedor5", "cardtheme")
	contenedor5.AddRowsM(2,False,10,20,"").AddCellsOSMP(3,0,0,0,12,4,4,5,1,5,10,"cardtheme") ' Tittle  1
	contenedor5.AddRowsM(7,True,6,6,"").AddCellsOSMP(1,0,0,0,1,1,1,0,0,0,0,"cardtheme").AddCellsOSMP(1,0,0,0,5,5,5,0,0,10,0,"cardtheme").AddCellsOSMP(1,0,0,0,5,5,5,0,0,20,0,"cardtheme").AddCellsOSMP(1,0,0,0,1,1,1,0,0,0,0,"cardtheme") ' CAMPOS DE DATOS 2-7
	fila = 3
	columna = 2
	contenedor5.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,4,4,0,10,10,0,"cardtheme") ' ESPACIO LIBRE ABAJO 8
	
	contenedor5.BuildGrid
	contenedor5.Cell(2,1).SetOffsetSize(0,0,0,12,12,12)
	contenedor5.Cell(2,1).UseTheme("TittleCell")
	
	'-----------------------------
	Dim Tittle5 As ABMLabel
	Tittle5.Initialize(page,"Tittle5","NÓMINA",ABM.SIZE_H6,False,"Tittle")
	contenedor5.Cell(2,1).AddComponent(Tittle5)
	'-------------------------
	Dim dummy5at As ABMLabel
	dummy5at.Initialize(page,"value5at","Tipo de contrato",ABM.SIZE_H6,False,"cardtheme")
	contenedor5.Cell(fila,columna).AddComponent(dummy5at)
	'tipo de contrato
	Dim dummy5av As ABMLabel
	dummy5av.Initialize(page,"value5av",tipoC,ABM.SIZE_H6,False,"cardtheme")
	contenedor5.Cell(fila,columna+1).AddComponent(dummy5av)
	
	'-------------------------
	
	
	Dim dummy5bt As ABMLabel
	dummy5bt.Initialize(page,"value5bt","Categoria",ABM.SIZE_H6,False,"cardtheme")
	contenedor5.Cell(fila+1,columna).AddComponent(dummy5bt)
	
	Dim dummy5bv As ABMLabel
	dummy5bv.Initialize(page,"value5bv",categoria,ABM.SIZE_H6,False,"cardtheme")
	contenedor5.Cell(fila+1,columna+1).AddComponent(dummy5bv)
	Log("-->>" & (fila+1) & " , " & (columna+1))
	'-------------------------
	
	Dim dummy5bt As ABMLabel
	dummy5bt.Initialize(page,"value5bt","Salario diario",ABM.SIZE_H6,False,"cardtheme")
	contenedor5.Cell(fila+2,columna).AddComponent(dummy5bt)
	
	Dim dummy5bv As ABMLabel
	dummy5bv.Initialize(page,"value5bv","$"&Round2(saldia,2),ABM.SIZE_H6,False,"cardtheme")
	contenedor5.Cell(fila+2,columna+1).AddComponent(dummy5bv)
	
	Log("-->>" & (fila+1) & " , " & (columna+1))
	'-------------------------
	
	Dim dummy5ct As ABMLabel
	dummy5ct.Initialize(page,"value5ct","Salario mensual",ABM.SIZE_H6,False,"cardtheme")
	contenedor5.Cell(fila+3,columna).AddComponent(dummy5ct)
	
	Dim dummy5cv As ABMLabel
	dummy5cv.Initialize(page,"value5cv","$"&Round2(salmens,2),ABM.SIZE_H6,False,"cardtheme")
	contenedor5.Cell(fila+3,columna+1).AddComponent(dummy5cv)
	
	'-------------------------
	'Vacaciones
	Dim Tittle51 As ABMLabel
	'Tittle51.Initialize(page,"Tittle51","VACACIONES",ABM.SIZE_H6,False,"Tittle")
	'contenedor5.Cell(fila+4,columna).AddComponent(Tittle51)

	'contenedor5.Cell(fila+4,columna).SetOffsetSize(0,0,0,12,12,12)
	'contenedor5.Cell(fila+4,columna).UseTheme("TittleCell")

	'Dim dummy5ct As ABMLabel
	'dummy5ct.Initialize(page,"value5ct","Inicio del periodo",ABM.SIZE_H6,False,"cardtheme")
	'contenedor5.Cell(fila+5,columna).AddComponent(dummy5ct)
	
'	'consltar vacaciones pendientes  
'	Dim diasA,diasP,person  As String
'	Dim consulta3 As String	
'	Dim rst As ResultSet  
'	If Main.subordinado=False Then
'		consulta="Select * FROM OPENQUERY(GIN, 'select top 1 Vac_days_anniversary , Vac_date_period from  "&ws.Session.GetAttribute("DBUse")&".dbo.RH_PERS_VACATIONS  where Vac_Person_Id = ''" & ws.Session.GetAttribute("Person_id") & "'' order by Vac_period desc')"
'	
'	Else
'		consulta="Select * from Openquery(GIN,'Select Person_id from "&  ws.Session.GetAttribute("DBUse") & ".dbo.vw_Person_Diagrama where Pkey_key1=''" & Main.idsesion  &"''')"
'		person=sql2.ExecQuerySingleResult(consulta)
'		consulta="Select * FROM OPENQUERY(GIN, 'select top 1 Vac_days_anniversary from  "&ws.Session.GetAttribute("DBUse")&".dbo.RH_PERS_VACATIONS  where Vac_Person_Id = ''" & person & "'' order by Vac_period desc')"
'	End If
'	Log(consulta)
'	rst=sql2.ExecQuery(consulta)
'	rst.NextRow
'	diasA=ABMShared.ConvertDate(rst.GetString("Vac_date_period"),"yyyy-MM-dd","dd MMM yyyy")
'	Log(diasA)
'	Dim difDate As Period
'	'diasP=DateTime.Date(DateTime.Now)
'	'difDate=ABMShared.Periodo(ABMShared.ConvertDate(rst.GetString("Vac_date_period"),"yyyy-MM-dd HH:mm:ss","MM/dd/yyyy HH:mm:ss"),ABMShared.ConvertDate(DateTime.Date(DateTime.Now),"yyyy-MM-dd HH:mm:ss","MM/dd/yyyy HH:mm:ss"))
'	difDate=ABMShared.Periodo("08/16/2016 00:00:00","04/26/2017 00:00:00")
'	Log("PERIODO " & difDate.Days)
'	
'	Log(difDate.Years)
'	Log("Meses " & difDate.Months)
'	Log(DateTime.DateFormat)
'	DateTime.DateFormat="MM/dd/yyyy"
'	Dim f1 As Long=DateTime.DateParse("08/16/2016")
'	Log(f1)
'	Dim p As Period = DateUtils.PeriodBetweenInDays(DateTime.DateParse("08/16/2016"),DateTime.DateParse("04/26/2017"))
'	
'	Log("P  " & p.Days)
	Dim dummy5cv As ABMLabel
	'dummy5cv.Initialize(page,"value5cv",diasA,ABM.SIZE_H6,False,"cardtheme")
	'contenedor5.Cell(fila+5,columna+1).AddComponent(dummy5cv)
	
	'-------------------------
	Dim dummy5dt As ABMLabel
	dummy5dt.Initialize(page,"value5et","Días trasncurridos",ABM.SIZE_H6,False,"cardtheme")
	'contenedor5.Cell(fila+6,columna).AddComponent(dummy5dt)
	
	Dim dummy5dv As ABMLabel
	'dummy5dv.Initialize(page,"value5ev",p.Days,ABM.SIZE_H6,False,"cardtheme")
	'contenedor5.Cell(fila+6,columna+1).AddComponent(dummy5dv)
	
	'-------------------------
	Dim dummy5et As ABMLabel
	dummy5et.Initialize(page,"value5et","value5e",ABM.SIZE_H6,False,"cardtheme")
	'contenedor5.Cell(fila+4,columna).AddComponent(dummy5et)
	
	Dim dummy5ev As ABMLabel
	dummy5ev.Initialize(page,"value5ev",dummy5e,ABM.SIZE_H6,False,"cardtheme")
	'contenedor5.Cell(fila+4,columna+1).AddComponent(dummy5ev)
	
	'-------------------------
	Dim dummy5ft As ABMLabel
	dummy5ft.Initialize(page,"value5ft","value5f",ABM.SIZE_H6,False,"cardtheme")
	'contenedor5.Cell(fila+5,columna).AddComponent(dummy5ft)
	
	Dim dummy5fv As ABMLabel
	dummy5fv.Initialize(page,"value5fv",dummy5f,ABM.SIZE_H6,False,"cardtheme")
	'contenedor5.Cell(fila+5,columna+1).AddComponent(dummy5fv)
	
	'-------------------------
	Dim dummy5gt As ABMLabel
	dummy5gt.Initialize(page,"value5gt","value5g",ABM.SIZE_H6,False,"cardtheme")
	'contenedor5.Cell(fila+6,columna).AddComponent(dummy5gt)
	
	Dim dummy5gv As ABMLabel
	dummy5gv.Initialize(page,"value5gv",dummy5g,ABM.SIZE_H6,False,"cardtheme")
	'contenedor5.Cell(fila+6,columna+1).AddComponent(dummy5gv)
	
	Dim lista5 As ABMList
	'list1.Initialize(page, "list1", ABM.COLLAPSE_ACCORDION, "")
	
	lista5.InitializeWithMaxHeight(page,"lista5",ABM.COLLAPSE_ACCORDION,400, "listatheme")
		
	lista5.AddItemWithTheme("L5", contenedor5,"itemtheme")
		
	page.Cell(2,3).AddComponent(lista5)
	
End Sub


Sub creacont6
	fila = 0
	columna = 0
	
	Dim contenedor6 As ABMContainer
	contenedor6.Initialize(page, "contenedor6", "cardtheme")
	contenedor6.AddRowsM(2,False,10,20,"").AddCellsOSMP(3,0,0,0,12,4,4,5,1,5,10,"cardtheme") ' Tittle  1
	contenedor6.AddRowsM(7,True,6,6,"").AddCellsOSMP(1,0,0,0,1,1,1,0,0,0,0,"cardtheme").AddCellsOSMP(1,0,0,0,5,5,5,0,0,10,0,"cardtheme").AddCellsOSMP(1,0,0,0,5,5,5,0,0,50,0,"cardtheme").AddCellsOSMP(1,0,0,0,1,1,1,0,0,0,0,"cardtheme") ' CAMPOS DE DATOS 2-7
	fila = 3
	columna = 2
	contenedor6.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,4,4,0,10,10,0,"cardtheme") ' ESPACIO LIBRE ABAJO 8
	
	contenedor6.BuildGrid
	contenedor6.Cell(2,1).SetOffsetSize(0,0,0,12,12,12)
	contenedor6.Cell(2,1).UseTheme("TittleCell")
	'-----------------------------
	Dim Tittle6 As ABMLabel
	Tittle6.Initialize(page,"Tittle6","KPI",ABM.SIZE_H6,False,"Tittle")
	contenedor6.Cell(2,1).AddComponent(Tittle6)
	'-------------------------
	'	Dim dummy6at As ABMLabel
	'	dummy6at.Initialize(page,"value6at","value6a",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor6.Cell(fila,columna).AddComponent(dummy6at)
	'
	'	Dim dummy6av As ABMLabel
	'	dummy6av.Initialize(page,"value6av",dummy6a,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor6.Cell(fila,columna+1).AddComponent(dummy6av)
	'
	'	'-------------------------
	'	Dim dummy6bt As ABMLabel
	'	dummy6bt.Initialize(page,"value6bt","value6b",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor6.Cell(fila+1,columna).AddComponent(dummy6bt)
	'
	'	Dim dummy6bv As ABMLabel
	'	dummy6bv.Initialize(page,"value6bv",dummy6b,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor6.Cell(fila+1,columna+1).AddComponent(dummy6bv)
	'
	'		'-------------------------
	'	Dim dummy6ct As ABMLabel
	'	dummy6ct.Initialize(page,"value6ct","value6c",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor6.Cell(fila+2,columna).AddComponent(dummy6ct)
	'
	'	Dim dummy6cv As ABMLabel
	'	dummy6cv.Initialize(page,"value6cv",dummy6c,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor6.Cell(fila+2,columna+1).AddComponent(dummy6cv)
	'
	'
	'	'-------------------------
	'	Dim dummy6dt As ABMLabel
	'	dummy6dt.Initialize(page,"value6et","value6e",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor6.Cell(fila+3,columna).AddComponent(dummy6dt)
	'
	'	Dim dummy6dv As ABMLabel
	'	dummy6dv.Initialize(page,"value6ev",dummy6d,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor6.Cell(fila+3,columna+1).AddComponent(dummy6dv)
	'
	'		'-------------------------
	'	Dim dummy6et As ABMLabel
	'	dummy6et.Initialize(page,"value6et","value6e",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor6.Cell(fila+4,columna).AddComponent(dummy6et)
	'
	'	Dim dummy6ev As ABMLabel
	'	dummy6ev.Initialize(page,"value6ev",dummy6e,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor6.Cell(fila+4,columna+1).AddComponent(dummy6ev)
	'
	'	'-------------------------
	'	Dim dummy6ft As ABMLabel
	'	dummy6ft.Initialize(page,"value6ft","value6f",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor6.Cell(fila+5,columna).AddComponent(dummy6ft)
	'
	'	Dim dummy6fv As ABMLabel
	'	dummy6fv.Initialize(page,"value6fv",dummy6f,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor6.Cell(fila+5,columna+1).AddComponent(dummy6fv)
	'
	'	'-------------------------
	'	Dim dummy6gt As ABMLabel
	'	dummy6gt.Initialize(page,"value6gt","value6g",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor6.Cell(fila+6,columna).AddComponent(dummy6gt)
	'
	'	Dim dummy6gv As ABMLabel
	'	dummy6gv.Initialize(page,"value6gv",dummy6g,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor6.Cell(fila+6,columna+1).AddComponent(dummy6gv)
	
	Dim lista6 As ABMList
	'list1.Initialize(page, "list1", ABM.COLLAPSE_ACCORDION, "")
	
	lista6.InitializeWithMaxHeight(page,"lista6",ABM.COLLAPSE_ACCORDION,400, "listatheme")
		
	lista6.AddItemWithTheme("L6", contenedor6,"itemtheme")
		
	page.Cell(4,3).AddComponent(lista6)
	
End Sub


Sub creacont7
	fila = 0
	columna = 0
	
	Dim contenedor7 As ABMContainer
	contenedor7.Initialize(page, "contenedor7", "cardtheme")
	contenedor7.AddRowsM(2,False,10,20,"").AddCellsOSMP(3,0,0,0,12,4,4,5,1,5,10,"cardtheme") ' Tittle  1
	contenedor7.AddRowsM(7,True,6,6,"").AddCellsOSMP(1,0,0,0,1,1,1,0,0,0,0,"cardtheme").AddCellsOSMP(1,0,0,0,5,5,5,0,0,10,0,"cardtheme").AddCellsOSMP(1,0,0,0,5,5,5,0,0,50,0,"cardtheme").AddCellsOSMP(1,0,0,0,1,1,1,0,0,0,0,"cardtheme") ' CAMPOS DE DATOS 2-7
	fila = 3
	columna = 2
	contenedor7.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,4,4,0,10,10,0,"cardtheme") ' ESPACIO LIBRE ABAJO 8
	
	contenedor7.BuildGrid
	contenedor7.Cell(2,1).SetOffsetSize(0,0,0,12,12,12)
	contenedor7.Cell(2,1).UseTheme("TittleCell")
	'-----------------------------
	Dim Tittle7 As ABMLabel
	Tittle7.Initialize(page,"Tittle7","ENTRENAMIENTO Y DESARROLLO ",ABM.SIZE_H6,False,"Tittle")
	contenedor7.Cell(2,1).AddComponent(Tittle7)
	'-------------------------
	'	Dim dummy7at As ABMLabel
	'	dummy7at.Initialize(page,"value7at","value7a",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor7.Cell(fila,columna).AddComponent(dummy7at)
	'
	'	Dim dummy7av As ABMLabel
	'	dummy7av.Initialize(page,"value7av",dummy7a,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor7.Cell(fila,columna+1).AddComponent(dummy7av)
	'
	'	'-------------------------
	'	Dim dummy7bt As ABMLabel
	'	dummy7bt.Initialize(page,"value7bt","value7b",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor7.Cell(fila+1,columna).AddComponent(dummy7bt)
	'
	'	Dim dummy7bv As ABMLabel
	'	dummy7bv.Initialize(page,"value7bv",dummy7b,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor7.Cell(fila+1,columna+1).AddComponent(dummy7bv)
	'
	'		'-------------------------
	'	Dim dummy7ct As ABMLabel
	'	dummy7ct.Initialize(page,"value7ct","value7c",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor7.Cell(fila+2,columna).AddComponent(dummy7ct)
	'
	'	Dim dummy7cv As ABMLabel
	'	dummy7cv.Initialize(page,"value7cv",dummy7c,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor7.Cell(fila+2,columna+1).AddComponent(dummy7cv)
	'
	'
	'	'-------------------------
	'	Dim dummy7dt As ABMLabel
	'	dummy7dt.Initialize(page,"value7et","value7e",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor7.Cell(fila+3,columna).AddComponent(dummy7dt)
	'
	'	Dim dummy7dv As ABMLabel
	'	dummy7dv.Initialize(page,"value7ev",dummy7d,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor7.Cell(fila+3,columna+1).AddComponent(dummy7dv)
	'
	'		'-------------------------
	'	Dim dummy7et As ABMLabel
	'	dummy7et.Initialize(page,"value7et","value7e",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor7.Cell(fila+4,columna).AddComponent(dummy7et)
	'
	'	Dim dummy7ev As ABMLabel
	'	dummy7ev.Initialize(page,"value7ev",dummy7e,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor7.Cell(fila+4,columna+1).AddComponent(dummy7ev)
	'
	'	'-------------------------
	'	Dim dummy7ft As ABMLabel
	'	dummy7ft.Initialize(page,"value7ft","value7f",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor7.Cell(fila+5,columna).AddComponent(dummy7ft)
	'
	'	Dim dummy7fv As ABMLabel
	'	dummy7fv.Initialize(page,"value7fv",dummy7f,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor7.Cell(fila+5,columna+1).AddComponent(dummy7fv)
	'
	'	'-------------------------
	'	Dim dummy7gt As ABMLabel
	'	dummy7gt.Initialize(page,"value7gt","value7g",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor7.Cell(fila+6,columna).AddComponent(dummy7gt)
	'
	'	Dim dummy7gv As ABMLabel
	'	dummy7gv.Initialize(page,"value7gv",dummy7g,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor7.Cell(fila+6,columna+1).AddComponent(dummy7gv)
	
	Dim lista7 As ABMList
		
	lista7.InitializeWithMaxHeight(page,"lista7",ABM.COLLAPSE_ACCORDION,400, "listatheme")
		
	lista7.AddItemWithTheme("L7", contenedor7,"itemtheme")
	
	
	page.Cell(4,2).AddComponent(lista7)
	
End Sub


Sub creacont8
	fila = 0
	columna = 0
	
	Dim contenedor8 As ABMContainer
	contenedor8.Initialize(page, "contenedor8", "cardtheme")
	contenedor8.AddRowsM(2,False,10,20,"").AddCellsOSMP(3,0,0,0,12,4,4,5,1,5,10,"cardtheme") ' Tittle  1
	contenedor8.AddRowsM(7,True,6,6,"").AddCellsOSMP(1,0,0,0,1,1,1,0,0,0,0,"cardtheme").AddCellsOSMP(1,0,0,0,5,5,5,0,0,10,0,"cardtheme").AddCellsOSMP(1,0,0,0,5,5,5,0,0,50,0,"cardtheme").AddCellsOSMP(1,0,0,0,1,1,1,0,0,0,0,"cardtheme") ' CAMPOS DE DATOS 2-7
	fila = 3
	columna = 2
	contenedor8.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,4,4,0,10,10,0,"cardtheme") ' ESPACIO LIBRE ABAJO 8
	
	contenedor8.BuildGrid
	contenedor8.Cell(2,1).SetOffsetSize(0,0,0,12,12,12)
	contenedor8.Cell(2,1).UseTheme("TittleCell")
	
	'-----------------------------
	Dim Tittle8 As ABMLabel
	Tittle8.Initialize(page,"Tittle8","EVALUACIÓN DEL DESEMPEÑO",ABM.SIZE_H6,False,"Tittle")
	contenedor8.Cell(2,1).AddComponent(Tittle8)
	'-------------------------
	'	Dim dummy8at As ABMLabel
	'	dummy8at.Initialize(page,"value8at","value8a",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor8.Cell(fila,columna).AddComponent(dummy8at)
	'
	'	Dim dummy8av As ABMLabel
	'	dummy8av.Initialize(page,"value8av",dummy8a,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor8.Cell(fila,columna+1).AddComponent(dummy8av)
	'
	'	'-------------------------
	'	Dim dummy8bt As ABMLabel
	'	dummy8bt.Initialize(page,"value8bt","value8b",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor8.Cell(fila+1,columna).AddComponent(dummy8bt)
	'
	'	Dim dummy8bv As ABMLabel
	'	dummy8bv.Initialize(page,"value8bv",dummy8b,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor8.Cell(fila+1,columna+1).AddComponent(dummy8bv)
	'
	'		'-------------------------
	'	Dim dummy8ct As ABMLabel
	'	dummy8ct.Initialize(page,"value8ct","value8c",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor8.Cell(fila+2,columna).AddComponent(dummy8ct)
	'
	'	Dim dummy8cv As ABMLabel
	'	dummy8cv.Initialize(page,"value8cv",dummy8c,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor8.Cell(fila+2,columna+1).AddComponent(dummy8cv)
	'
	'
	'	'-------------------------
	'	Dim dummy8dt As ABMLabel
	'	dummy8dt.Initialize(page,"value8et","value8e",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor8.Cell(fila+3,columna).AddComponent(dummy8dt)
	'
	'	Dim dummy8dv As ABMLabel
	'	dummy8dv.Initialize(page,"value8ev",dummy8d,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor8.Cell(fila+3,columna+1).AddComponent(dummy8dv)
	'
	'		'-------------------------
	'	Dim dummy8et As ABMLabel
	'	dummy8et.Initialize(page,"value8et","value8e",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor8.Cell(fila+4,columna).AddComponent(dummy8et)
	'
	'	Dim dummy8ev As ABMLabel
	'	dummy8ev.Initialize(page,"value8ev",dummy8e,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor8.Cell(fila+4,columna+1).AddComponent(dummy8ev)
	'
	'	'-------------------------
	'	Dim dummy8ft As ABMLabel
	'	dummy8ft.Initialize(page,"value8ft","value8f",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor8.Cell(fila+5,columna).AddComponent(dummy8ft)
	'
	'	Dim dummy8fv As ABMLabel
	'	dummy8fv.Initialize(page,"value8fv",dummy8f,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor8.Cell(fila+5,columna+1).AddComponent(dummy8fv)
	'
	'	'-------------------------
	'	Dim dummy8gt As ABMLabel
	'	dummy8gt.Initialize(page,"value8gt","value8g",ABM.SIZE_H6,False,"cardtheme")
	'	contenedor8.Cell(fila+6,columna).AddComponent(dummy8gt)
	'
	'	Dim dummy8gv As ABMLabel
	'	dummy8gv.Initialize(page,"value8gv",dummy8g,ABM.SIZE_H6,False,"cardtheme")
	'	contenedor8.Cell(fila+6,columna+1).AddComponent(dummy8gv)
	
	Dim lista8 As ABMList
	'list1.Initialize(page, "list1", ABM.COLLAPSE_ACCORDION, "")
	
	lista8.InitializeWithMaxHeight(page,"lista8",ABM.COLLAPSE_ACCORDION,400, "listatheme")
	
	lista8.AddItemWithTheme("L8", contenedor8,"itemtheme")
	
	page.Cell(4,1).AddComponent(lista8)
	
End Sub
Sub tittle9_Clicked(Target As String)
	ABMShared.NavigateToPage(ws,ABMPageId,"../PaginaTemplate/PaginaTemplate.html")
	
	
	
End Sub


Sub creacont9
	Dim contenedor9 As ABMContainer
	contenedor9.Initialize(page, "contenedor9", "cardtheme")
	contenedor9.AddRows(1,False,"").AddCellsOSMP(1,0,0,0,12,12,12,40,10,10,10,"cardtheme")
	contenedor9.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,10,10,10,10,"cardtheme")
	contenedor9.BuildGrid
	contenedor9.Cell(1,1).SetOffsetSize(0,0,0,12,12,12)
	contenedor9.Cell(1,1).UseTheme("TittleCell")
	Dim Tittle9 As ABMLabel
	Tittle9.Clickable=True
	Tittle9.Initialize(page,"tittle9","MI EQUIPO",ABM.SIZE_H6,False,"tittle")
	contenedor9.Cell(1,1).AddComponent(Tittle9)
	Dim list1 As ABMList
	list1.Initialize(page, "list1", ABM.COLLAPSE_ACCORDION, "")
	
	Dim categoria As String
	Dim sql As SQL
	Dim res As ResultSet
	Dim db As String=ws.Session.GetAttribute("DBUse")
	If Main.subordinado=False Then
		sql.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
		Dim consulta As String = "Select * FROM OPENQUERY(GIN, 'select Pkey_key1,nombrecompleto,Pos_name,Person_id, Pos_Status  from "&db&".dbo.vw_lgen_altasybajas where  person_id<>''" & ws.Session.GetAttribute("Person_id") & "'' and pkey_status1=''A'' and pkey_dept_key=''"&iddepto&"'' order by PPay_SSalary1 desc')"
		'PKey_Key1=''"&idusuario&"''')"
		res = sql.ExecQuery(consulta)
		Do While res.NextRow
			categoria = sql.ExecQuerySingleResult("Select * FROM OPENQUERY(GIN, 'select Cat_Description from  "&db&".dbo.RH_Category  where Cat_Key = ''" & res.GetString("Pos_Status") & "''')")
			list1.AddItem(res.GetString("Pkey_key1"), contenedorempleado(res.GetString("Pkey_key1"),"",res.GetString("nombrecompleto"),res.GetString("Pos_name"),res.GetString("Person_id"),categoria))
		Loop
		res.Close
	End If
	list1.InitializeWithMaxHeight(page, "list1", ABM.COLLAPSE_EXPANDABLE, 250, "list1theme")
	contenedor9.Cell(2,1).AddComponent(list1)
	Dim lista9 As ABMList
	lista9.InitializeWithMaxHeight(page,"lista9",ABM.COLLAPSE_ACCORDION,400, "listatheme")
	lista9.AddItemWithTheme("L9", contenedor9,"itemtheme")
	page.Cell(2,2).AddComponent(lista9)
	
End Sub


Sub Page_Ready()
	Log("miperfil!")
	ConnectPage
	page.RestoreNavigationBarPosition
End Sub


Sub contenedorempleado (id As String, icon As String, nombre As String,puestox As String,pesonid As String,categoria As String) As ABMContainer
	Dim rst As ResultSet
	Dim sql1 As SQL
	
	sql1.Initialize(Main.strDrv,Main.strUrl)
	
	
	Dim contenedoremp As ABMContainer
	contenedoremp.Initialize(page, "empleado"&id, "")
	contenedoremp.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,4,4,5,1,1,50,"").AddCellsOSMP(1,0,0,0,12,8,8,10,0,1,0,"")
	contenedoremp.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,4,4,0,0,0,0,"").AddCellsOSMP(1,0,0,0,12,8,8,0,0,1,0,"")
	contenedoremp.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,4,4,0,0,0,0,"").AddCellsOSMP(1,0,0,0,12,8,8,0,0,1,0,"")
	'contenedoremp.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,4,4,0,0,0,0,"").AddCellsOSMP(1,0,0,0,12,6,8,0,0,1,0,"")
	
	'contenedoremp.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,4,4,0,0,0,0,"").AddCellsOSMP(1,0,0,0,12,6,8,0,0,1,0,"")
	'contenedoremp.AddRow(1,False,"","")
	'contenedoremp.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	contenedoremp.BuildGrid

	rst=sql1.ExecQuery("Select * from RH_UserConf where id_user='"&id&"'")

	Do While rst.NextRow
		fotoe = rst.GetString("nom_foto")
		If Not(fotoe=Null) And Not(fotoe="") Then
			Dim su As StringUtils
			Dim BT2() As Byte= su.DecodeBase64(fotoe)
			Dim imagen As Image = BytesToImage(BT2)
			Dim carpeta As String = File.Combine(File.DirApp, DownloadFolder)
			
			Dim borro As Boolean 
			borro=False
			If File.Exists (carpeta,id&"sf.jpg") Then
				borro = File.Delete(carpeta,id&"sf.jpg")
			End If
			Dim Out As OutputStream = File.OpenOutput(carpeta, id&"sf.jpg", True)
			imagen.WriteToStream(Out)
			Out.Close
		Else
			
			
		End If
	Loop
	
	Dim foto As ABMImage
	If File.Exists(File.Combine(File.DirApp, DownloadFolder),id&"sf.jpg") Then
		foto.Initialize(page,"1", "../perfiles/"&id&"sf.jpg",1.0)
	Else
		'busco en otra carpeta
		
		If File.Exists(File.Combine(File.DirApp,photosFolder &ws.Session.GetAttribute("DBUse") ),pesonid&".jpg") Then
			foto.Initialize(page,"1","../photos/"&ws.Session.GetAttribute("DBUse")&"/"&pesonid&".jpg",2)
		Else
			foto.Initialize(page,"1", "../perfiles/user.png",1.0)
		End If
	End If
	' foto.Initialize(page,"1","../perfiles/"&id&"sf.jpg",1.0)


	foto.Caption="Num."&id
	foto.Tag=1
	foto.IsCircular = True
	foto.IsResponsive=True
	
	contenedoremp.Cell(1,1).SetOffsetSize(0,0,0,12,6,4)
	contenedoremp.Cell(1,1).AddArrayComponent(foto,"myfoto")

	
	Dim nombres(6) As String = Regex.Split(" ",nombre)
	Dim nombre1 , nombre2, cadena As String = ""
	Dim n As Int = 1
	
	If nombre.Length > 16 Then
		
		' SI QUEREMOS USAR SOLO UN NOMBRE Y UN APELLIDO		
		If nombres.Length >=3 Then
			cadena = nombres(0) &" "& nombres(2)		 
		Else
			If nombres.Length >=2 Then
				cadena = nombres(0) &" "& nombres(1)
			Else 	
				cadena = nombre
			End If
		End If
		
		' SI QUEREMOS ARMAR EL NOMBRE COMPLETO COMENTAMOS LO DE ARRIBA

		If cadena.Length < 17 And cadena.Length > 0 Then 

			nombre1 = cadena
		Else
			For n = 0 To nombres.Length-1
					
				Dim cadena As String = nombre1 & " " &nombres(n)
				If cadena.Length < 17 Then
					nombre1 = nombre1 & " " &nombres(n)
				Else
					nombre2 = nombre2 & " " &nombres(n)
				End If
					
					
			Next
		End If
		
	Else
		nombre1 = nombre
	End If
	'Log (nombre)
	
	Dim nombreempleado As ABMLabel
	nombreempleado.Initialize(page,"nombreempleado",nombre1,ABM.SIZE_SMALL,False,"")
	contenedoremp.Cell(1,2).AddComponent(nombreempleado)
	contenedoremp.Cell(1,2).SetOffsetSize(0,0,2,12,6,6)
	
	contenedoremp.Cell(1,2).SetFixedHeight(10,False)
	
	Dim puestoE As ABMLabel
	puestoE.Initialize(page,"puesto",puestox,ABM.SIZE_SMALL,False,"")
	contenedoremp.Cell(2,1).AddComponent(puestoE)
	contenedoremp.Cell(2,1).SetOffsetSize(0,0,0,12,12,12)
	
	Dim categoriaE As ABMLabel
	categoriaE.Initialize(page,"categoria",categoria,ABM.SIZE_SMALL,False,"")
	contenedoremp.Cell(3,1).AddComponent(categoriaE)
	contenedoremp.Cell(3,1).SetOffsetSize(0,0,0,12,12,12)
	

'	Dim nombreempleado2 As ABMLabel
'	nombreempleado2.Initialize(page,"nombreempleado2",nombre2,ABM.SIZE_SMALL,False,"")
'	contenedoremp.Cell(2,2).AddComponent(nombreempleado2)
'	contenedoremp.Cell(2,2).SetOffsetSize(0,0,0,12,6,6)
	contenedoremp.Cell(3,2).SetFixedHeight(10,False)
	
	
	

	'contenedoremp.Cell(2,2).SetFixedHeight(7,False)
	Return contenedoremp
		
End Sub

'   EL EVENTO DE CLICK
Sub List1_Clicked(ItemId As String)

	Main.idsesion = ItemId
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Click en "&ItemId , 5000) ', myTexts, myReturns)

	ABMShared.NavigateToPage(ws,ABMPageId,"../miperfil/miperfil.html")
	Main.subordinado = True

End Sub

' SI PISAN LA FOTO DEL USUARIO
Sub myimg_clicked(Target As String)
	Dim img As ABMImage = page.Component(Target)
	Dim title As String = img.Caption
	page.ShowToast("toast1", "toastgreen", "Clicked " & title, 5000)
End Sub


Sub msbtn3_Clicked(Target As String)
	page.CloseModalSheet("descripcion")
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
	'	If icon <> "" Then
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	'	Else
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
		combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next
	
	'page.Refresh
	'combo1.Refresh
	Return combo1
	

End Sub
Sub Descripcion (titulo As String,Archivo As String) As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "descripcion", True, False, "")
	myModal.Content.UseTheme("mypagem")
	myModal.Footer.UseTheme("modalfooter")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12MP(1,0,0,-1,-1,"")
	myModal.Content.AddRows(5,True,"").AddCells12(1,"")
	myModal.Content.BuildGrid
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", titulo,ABM.SIZE_A, False, "etiq")
	myModal.Content.Cell(1,1).UseTheme("celda2")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Content.Cell(3,1).AddComponent(ABMShared.BuildParagraph(page,"x","El reclutamiento es el proceso para atraer candidatos para cubrir vacantes. La selección es el proceso en el que se elige entre los candidatos reclutados a aquellos que reúnen las características idóneas para el puesto requerido.",ABM.SIZE_PARAGRAPH))
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "btn")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)
	Return myModal
	
End Sub

Sub setScrnSize(pg As ABMPage) ' in ABMShared...
   
	Dim ret As Int = 2 ' set initial as desktop
	Try
		Dim NowWH As String = ABM.GetBrowserWidthHeight( pg) ' returns a string "width;height"
   
		If NowWH <> "" And NowWH <> ";" Then ' check if we got something useful back
			Dim split() As String = Regex.Split(";", NowWH) ' split the string
      
			Dim NewH As Int = split(0)
			Log(" width in PX: "&NewH)
       
       
			If NewH < 600 Then ret = 0    ' phone
			If NewH > 600 And NewH <= 992 Then ret = 1  ' tablet landscape
			If NewH > 992 Then ret = 2 ' desktop


		End If
	Catch
		Log(" screen setting failed!")
	End Try

	Log(" screen width: " &ret)
	ScrnSize = ret
   
End Sub

Sub ImageToBytes(Image As Image) As Byte()
	Dim out As OutputStream
	out.InitializeToBytesArray(0)
	Image.WriteToStream(out)', 100, "JPEG")
	out.Close
	Return out.ToBytesArray
End Sub

Sub BytesToImage(bytes() As Byte) As Image
	Dim In As InputStream
	In.InitializeFromBytesArray(bytes, 0, bytes.Length)
	Dim bmp As Image
	bmp.Initialize2(In)
   
	Return bmp
End Sub


'document.body.style.backgroundImage = 'url('+getBG()+')';
'				 Dim imgdir As String = "../perfiles/"&Main.username&"b.jpg"
'				
'				
'				Dim cssdir As String = "/www/" & AppName & "/miperfil/"
'				' Dim cssdir As String = "../miperfil/"
'				' $('body').css('background-image', 'url(../images/backgrounds/header-top.jpg)');
'				' Dim script As String = $"document.body.style.backgroundImage = 'url(+${imgdir}+)';"$
'				Dim carpetacss As String = File.Combine(File.DirApp, cssdir)
'				Dim css As String
'				
'				css = File.ReadString(carpetacss,"miperfil.css")
'				 
'				 css = css.Replace("default.jpg",Main.username&"b.jpg")
'				 File.Delete(carpetacss,"miperfil.css")
'				 File.WriteString(carpetacss,"miperfil.css",css)
				 
	'			 Dim script As String = $"$('body').('background-image', 'url(../perfiles/aabreub.jpg)');"$

                ' page.ws.Eval(script, Null)
'				 page.ws.RunFunction(script,Null)
	'			 page.Refresh

