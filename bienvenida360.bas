﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "bienvenida360"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "40"
	' your own variables
	Dim myToastId As Int = 1
	
	Dim langshow As Boolean = False
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------
	Log("Connected")
	ws = WebSocket1
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
		' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
		page.Refresh
		page.FinishedLoading
	Else
		If page.WebsocketReconnected Then
			Log("Websocket reconnected")
			' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
			' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
			ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
		Else
			' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
			Log("Websocket first connection")
			page.Prepare
			ConnectPage
		End If
	End If
	Log(ABMPageId)
	'----------------------MODIFICATION-------------------------------
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")
End Sub

Sub Page_ParseEvent(Params As Map)
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)
		End Select
	End If
End Sub

public Sub BuildTheme()
	' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	theme.AddPDFViewerTheme("pdf")
	theme.PDFViewer("pdf").ToolbarColor=ABM.COLOR_BROWN
	theme.PDFViewer("pdf").ToolbarColorIntensity=ABM.INTENSITY_DARKEN4

	' add additional themes specific for this page
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "index.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' adding a navigation bar
	ABMShared.BuildNavigationBar(page,"Evaluación 360° FeedBack","../images/klogo3.png","","bienvenida360","")
	
			
	' create the page grid
	page.AddRows(1,True, "").AddCells12(1,"")
	page.AddRows(1,True, "").AddCells12(1,"")
	page.AddRows(1,True, "").AddCells12(1,"")
	page.AddRows(1,True, "").AddCells12(1,"")
	page.AddRows(1,True, "").AddCells12(12,"")

	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
	Dim cnt As ABMContainer
	cnt.Initialize(page,"cnt","")
	cnt.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,7,30,0,50,0,"").AddCellsOSMP(1,0,0,0,12,12,2,0,0,0,0,"")
	cnt.BuildGrid
	page.Cell(1,1).MarginTop="1%"
	page.Cell(1,1).AddComponent(cnt)

	Dim cnt1 As ABMContainer
	cnt1.Initialize(page,"cnt1", "")
	cnt1.AddRows(3,False,"").AddCells12(1,"")
	cnt1.BuildGrid
	cnt.Cell(1,1).AddComponent(cnt1)
	
	cnt1.Cell(3,1).AddComponent(ABMShared.BuildParagraphc(page,"id","Bienvenido (a) al sistema de evaluación",ABM.SIZE_H3,"#795548"))

	'Dim lblt1 As ABMLabel
	'lblt1.Initialize(page,"iblt1","Bienvenido (a) al sistema de evaluación",ABM.SIZE_H1,False,"")
	'lblt1.IsFlowText=True
	'cnt1.Cell(3,1).UseTheme("celda3")
	'cnt1.Cell(3,1).AddComponent(lblt1)

	Dim imglogo As ABMImage
	imglogo.Initialize(page,"iml","../images/feedback.png",3)
	cnt.Cell(1,2).AddComponent(imglogo)

	Dim cnt2 As ABMContainer
	cnt2.Initialize(page,"cnt2","")
	cnt2.AddRows(1,False,"").AddCells12MP(1,10,0,100,0,"")
	cnt2.BuildGrid
	page.Cell(2,1).AddComponent(cnt2)
	Dim lblt2 As ABMLabel
	lblt2.Initialize(page,"lblt2","{B}K360° - Karisma Feedback{/B} es una herramienta integral que permite medir el nivel de las competencias de los colaboradores de manera global." _
					& "{BR}{BR}Los principales usos que se le da a esta herramienta son los siguientes:",ABM.SIZE_H5,False,"lblbr")
	cnt2.Cell(1,1).AddComponent(lblt2)

	Dim cnt3 As ABMContainer
	cnt3.Initialize(page,"cnt3","")
	cnt3.AddRows(1,True,"").AddCells12(1,"")	
	cnt3.BuildGrid
	page.Cell(3,1).AddComponent(cnt3)
	Dim imgc As ABMImage
	imgc.Initialize(page,"imlc","../images/imgbienvenida.png",3)
	cnt3.Cell(1,1).AddComponent(imgc)
	
	Dim cnt4 As ABMContainer
	cnt4.Initialize(page,"cnt4","")
	cnt4.AddRows(1,True,"").AddCells12(1,"")
	cnt4.BuildGrid
	page.Cell(4,1).AddComponent(cnt4)
	
	Dim texto As String
	texto="Esta evaluación {B}brinda una perspectiva más amplia al obtener información y calificaciones desde todos los ángulos: jefe directo, compañeros, subordinados, clientes internos y externos, etc.{/B} quienes participan en el proceso y ayudan a construir una evaluación completa del colaborador según el tipo de interacción que tengan."
	
	
	Dim lblt4 As ABMLabel
	lblt4.Initialize(page,"lblt4",texto,ABM.SIZE_H5,False,"lblbr")
	cnt4.Cell(1,1).AddComponent(lblt4)
	
	Dim anterior As ABMButton
	anterior.InitializeFlat(page,"anterior","fa fa-arrow-circle-left",ABM.ICONALIGN_LEFT,"Anterior","btngreen")
	'page.Cell(5,1).AddComponent(anterior)
	'page.Cell(5,1).SetOffsetSize(0,0,4,3,3,3)
	
	Dim siguiente As ABMButton
	siguiente.InitializeFlat(page,"siguiente","fa fa-arrow-circle-right",ABM.ICONALIGN_RIGHT,"Siguiente","btngreen")
	page.Cell(5,2).AddComponent(siguiente)
	page.Cell(5,2).SetOffsetSize(0,0,6,3,3,3)
	'page.Cell(5,2).MarginLeft = "-10%"

	
	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
	
	Dim script As String = $"
		$('button').css('border-radius', '10px');
			 							"$
	
	page.ws.Eval(script,Null)
	
	
End Sub


Sub siguiente_Clicked(Target As String)
	ABMShared.NavigateToPage(page.ws, page.GetPageID, "../division360/")
End Sub

Sub anterior_Clicked(Target As String)
	ABMShared.NavigateToPage(page.ws, page.GetPageID, "../periodos360/")
End Sub

Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub manual_Clicked(Target As String)
	Dim pdf As ABMPDFViewer
	pdf.Initialize(page, "pdf", 800, "../files/360.pdf","pdf")
	pdf.PreparePrintingText = "Preparing to print..."
	pdf.ReadDirection = ABM.PDF_READDIRECTION_LTR
	pdf.AllowDownload = True
	pdf.AllowOpen = False
	pdf.AllowPrint = True
	pdf.AllowSideBar = True
	pdf.AllowViewBookmark = False
	page.Cell(7,1).PaddingTop = "5%"
	page.Cell(7,1).AddComponent(pdf)
	page.Refresh
End Sub

Sub resumen_Clicked(Target As String)
	ABMShared.NavigateToPage(page.ws, page.GetPageID, "../resumen360/resumen360.html")
End Sub

Sub auto_Clicked(Target As String)
	ABMShared.NavigateToPage(page.ws, page.GetPageID, "../evaluacion360/evaluacion360.html")
End Sub

Sub periodos_Clicked(Target As String)
	ABMShared.NavigateToPage(page.ws, page.GetPageID, "../periodos360/")
End Sub

Sub objetivos_Clicked(Target As String)
	ABMShared.NavigateToPage(page.ws, page.GetPageID, "../generaretro/generaretro.html")
End Sub

Sub resultados_Clicked(Target As String)
	ABMShared.NavigateToPage(page.ws, page.GetPageID, "../resultados360/resultados360.html")
End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	If Action = "idioma" Then
		
		If Not(langshow) Then
			page.NavigationBar.OpenExtraContent
			langshow = True
		Else
			langshow = False
			page.NavigationBar.CloseExtraContent
		End If
	
		Return
	End If
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
	'	If icon <> "" Then
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	'	Else
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
		combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next
	
	'page.Refresh
	'combo1.Refresh
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)
	Return myModal
End Sub
