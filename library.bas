﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "library"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "10"
	' your own variables		
	Dim myToastId As Int = 1
	Dim lstLib As List
	Private lstDirectorio As ABMList
	Private lstCourses As ABMList
	Private DirectorioX As String	
	Private CoverLib As String = "/www/" & ABMShared.AppName & "/karismadocs/"
	Private idactual As String 'id del elemento de la lista seleccionado
	Dim IsPlaying As Boolean = False 'variable para saber si el video esta corriendo
	Dim IsMuted As Boolean = False 'variable que marca si el video esta en silencio
	Dim Volume As Int = 100 'Variable con el valor del volumen del video
	Dim InitialVideo As Boolean = True 
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
		'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	ConnectPage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------	
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")	
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
End Sub

public Sub BuildTheme()
		
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	theme.AddContainerTheme("conte")
	theme.Container("conte").ZDepth=ABM.ZDEPTH_3
	theme.AddDividerTheme("div1")
	theme.Divider("div1").ForeColor=ABM.COLOR_BLUE
	theme.Divider("div1").ForeColorIntensity=ABM.INTENSITY_LIGHTEN4
	theme.AddImageSliderTheme("sl1")
	theme.ImageSlider("sl1").Height=200
	theme.ImageSlider("sl1").Indicators=False
	theme.ImageSlider("sl1").FullWidth=True
	theme.ImageSlider("sl1").ZDepth=ABM.ZDEPTH_3
	'titulo
    theme.AddLabelTheme("Tittle")
  	theme.Label("Tittle").foreColor = ABM.COLOR_WHITE
	 	'tema de celda titulo
	theme.AddCellTheme("TittleCell")
 	theme.Cell("TittleCell").Align=ABM.CELL_ALIGN_CENTER
 	theme.Cell("TittleCell").ZDepth=ABM.ZDEPTH_3
 	theme.Cell("TittleCell").BackColor=ABM.COLOR_BROWN
 	theme.Cell("TittleCell").BackColorIntensity=ABM.INTENSITY_DARKEN4
	' the footer theme
	theme.AddContainerTheme("cardtheme")
	theme.Container("cardtheme").BackColor = ABM.COLOR_LIME
	theme.Container("cardtheme").BackColorIntensity = ABM.INTENSITY_LIGHTEN5
	theme.Container("cardtheme").ZDepth = ABM.ZDEPTH_5

	' darkred button
	theme.AddButtonTheme("darkred")
	theme.Button("darkred").BackColor = ABM.COLOR_RED
	theme.Button("darkred").BackColorIntensity = ABM.INTENSITY_DARKEN1
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "library.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' Añadir Navigation Bar
	ABMShared.BuildNavigationBar(page,"Librería Karisma","","","","")
	page.AddRows(1,True, "").AddCellsOSMP(1,0,0,0,12,6,4,50,0,1,5,"").AddCellsOSMP(1,0,0,0,12,4,4,50,0,5,0,"").AddCellsOSMP(1,0,0,0,12,4,4,50,1,5,0,"") ' LC1 (2)
	page.AddRows(1,True, "").AddCellsOSMP(1,0,0,0,12,6,4,0,0,1,5,"").AddCellsOSMP(1,6,6,6,12,4,4,0,0,5,0,"").AddCellsOSMP(1,0,0,0,12,4,4,0,1,5,0,"")
	page.AddRows(2,True, "").AddCellsOSMP(1,0,0,0,12,12,12,50,0,1,5,"") '
	
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()
'container de las carpetas
'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
	
	Dim contdir As ABMContainer
	contdir.Initialize(page, "contdir", "cardtheme")
	contdir.AddRows(2,True,"").AddCells12(1,"")
	contdir.AddRowsM(2,True,10,20,"").AddCellsOSMP(3,0,0,0,12,4,4,5,1,5,10,"cardtheme") 
	contdir.BuildGrid
	Dim Tittle1 As ABMLabel
	Tittle1.Initialize(page,"Tittle1","Directorio",ABM.SIZE_H6,False,"Tittle")
	contdir.Cell(1,1).UseTheme("TittleCell")
	contdir.Cell(1,1).AddComponent(Tittle1)
	page.Cell(1,1).AddComponent(contdir)
	lstDirectorio.InitializeWithMaxHeight(page,"lstdire",ABM.COLLAPSE_EXPANDABLE,300,"")
	
	lstLib=File.ListFiles(File.DirApp & "\www\suite\karismadocs")

	For i=0 To lstLib.Size-1
		lstDirectorio.AddItem(lstLib.Get(i),BuildSimpleItem("D"& i,"",lstLib.Get(i)))		
	Next
	contdir.Cell(2,1).AddComponent(lstDirectorio)
	
	'container de los archivos
	
	Dim contcur As ABMContainer
	contcur.Initialize(page, "contcur", "cardtheme")
	contcur.AddRows(2,True,"").AddCells12(1,"")
	contcur.AddRowsM(2,True,10,20,"").AddCellsOSMP(6,0,0,0,3,2,2,5,0,5,5,"cardtheme") 
	contcur.BuildGrid
	Dim Tittle2 As ABMLabel
	Tittle2.Initialize(page,"Tittle2","Cursos",ABM.SIZE_H6,False,"Tittle")
	contcur.Cell(1,1).UseTheme("TittleCell")
	contcur.Cell(1,1).AddComponent(Tittle2)
	lstCourses.InitializeWithMaxHeight(page,"lstcour",ABM.COLLAPSE_EXPANDABLE,300,"")
	contcur.Cell(2,1).AddComponent(lstCourses)
	
	page.Cell(1,2).AddComponent(contcur)
	
	
	'container con la imagen de la caratula
	
	Dim contimg As ABMContainer
	contimg.Initialize(page, "contimg", "cardtheme")
	contimg.AddRows(2,True,"").AddCells12(1,"")
	contimg.AddRowsM(2,True,10,20,"").AddCellsOSMP(6,0,0,0,3,2,2,5,0,5,5,"cardtheme") 
	contimg.BuildGrid
	Dim Tittle3 As ABMLabel
	Tittle3.Initialize(page,"Tittle3","Carátula",ABM.SIZE_H6,False,"Tittle")
	contimg.Cell(1,1).UseTheme("TittleCell")
	contimg.Cell(1,1).AddComponent(Tittle3)

	Dim imgcv As ABMImage
	imgcv.Initialize(page,"cvr","",3)
	imgcv.SetFixedSize(200,310)
	contimg.Cell(2,1).SetFixedHeight(320,False)
	contimg.Cell(2,1).UseTheme("imgctr")
	contimg.Cell(2,1).AddComponent(imgcv)

	page.Cell(1,3).AddComponent(contimg)


	' create the container with animation	
	
	' you ALWAYS need an Initial state, duration should be 0 for the initial state
	Dim myAnim4 As ABMAnimation
	myAnim4.Initialize("initialblur", 0, ABM.TWEEN_EASEINQUAD)
	myAnim4.Opacity(0.0)
	myAnim4.Blur(20)
	page.AddAnimation(myAnim4)
	
	' we create an animation to 'unblur'
	Dim myAnim5 As ABMAnimation
	myAnim5.Initialize("unblur", 500, ABM.TWEEN_EASEINQUAD)
	myAnim5.Opacity(1)
	myAnim5.Blur(0)
	page.AddAnimation(myAnim5)	
	
	' we create an animation to 'blur'
	Dim myAnim6 As ABMAnimation
	myAnim6.Initialize("blur", 500, ABM.TWEEN_EASEINQUAD)
	myAnim6.Opacity(0.0)
	myAnim6.Blur(20)
	page.AddAnimation(myAnim6)


	Dim containerpdf As ABMContainer
	containerpdf.InitializeAnimated(page,"containerpdf","initialblur","")	
	containerpdf.AddRows(1,False,"").AddCells12(1,"")
	containerpdf.BuildGrid
	
	
			Dim pdf As ABMPDFViewer
			pdf.Initialize(page, "pdf", 800, "" ,"")
			pdf.PreparePrintingText = "Preparándose para imprimir..."
			pdf.ReadDirection = ABM.PDF_READDIRECTION_LTR
			pdf.AllowDownload = True
			pdf.AllowOpen = False
			pdf.AllowPrint = True
			pdf.AllowSideBar = True
			pdf.AllowViewBookmark = False
			containerpdf.Cell(1,1).AddComponent(pdf)
	
	page.Cell(3,1).AddComponent(containerpdf)
	
	
	Dim containervideo As ABMContainer
	containervideo.InitializeAnimated(page,"containervideo","initialblur","")	
	containervideo.AddRows(2,False,"").AddCells12(1,"")
	containervideo.BuildGrid
	
			'' add HTML5 video
			Dim video As ABMVideo
			video.InitializeHTML5(page, "video", "", False, "video/mp4")
			containervideo.Cell(1,1).AddComponent(video)
				
			' add youtube controls
			Dim btnPlay As ABMButton
			btnPlay.Initializefloating(page, "btnPlay", "mdi-av-play-arrow", "darkred")
			containervideo.Cell(2,1).AddComponent(btnPlay)
			
			Dim btnStop As ABMButton
			btnStop.Initializefloating(page, "btnStop", "mdi-av-stop", "darkred")
			containervideo.Cell(2,1).AddComponent(btnStop)
			
			Dim btnMute As ABMButton
			btnMute.Initializefloating(page, "btnMute", "mdi-av-volume-mute", "darkred")
			containervideo.Cell(2,1).AddComponent(btnMute)
			
			Dim btnVolumeDown As ABMButton
			btnVolumeDown.Initializefloating(page, "btnVolumeDown", "mdi-av-volume-down", "darkred")
			containervideo.Cell(2,1).AddComponent(btnVolumeDown)
			
			Dim btnVolumeUp As ABMButton
			btnVolumeUp.Initializefloating(page, "btnVolumeUp", "mdi-av-volume-up", "darkred")
			containervideo.Cell(2,1).AddComponent(btnVolumeUp)	
	
	
	page.Cell(4,1).AddComponent(containervideo)
	
	Dim containerboton As ABMContainer
	containerboton.InitializeAnimated(page,"containerboton","initialblur","")	
	containerboton.AddRows(1,False,"").AddCells12(1,"")
	containerboton.BuildGrid
	
	
	
	Dim ver As ABMButton
	ver.InitializeFlat(page,"ver","","","leer/abrir","btn")
	containerboton.Cell(1,1).AddComponent(ver)
	page.Cell(2,2).AddComponent(containerboton)
	
	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub

Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then 
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub ver_Clicked(Target As String)
	
	
	
	
	Dim nombre As String=idactual.SubString2(0,idactual.Length-4).trim
		
		Dim ext As String=idactual.SubString2(idactual.Length-4,idactual.Length).trim
		
		Dim cnty As ABMContainer=page.Component("contimg")
		Dim imgY As ABMImage=cnty.Component("cvr")
		Dim direcX As String
		direcX=File.Combine (File.DirApp,CoverLib & "\" & DirectorioX)
	
	
		Dim cont1 As ABMContainer=page.Cell(3,1).Component("containerpdf")
		Dim pdf1 As ABMPDFViewer = cont1.Cell(1,1).Component("pdf")
		
		Dim cont2 As ABMContainer=page.Cell(4,1).Component("containervideo")
		Dim video1 As ABMVideo = cont2.Cell(1,1).Component("video")
		
		Dim cont3 As ABMContainer=page.Cell(2,2).Component("containerboton")
		Dim boton1 As ABMButton = cont3.Cell(1,1).Component("ver")
	
		
			cont3.RunAnimation("blur")
	
		' CARGADO DEL PDF
		
		If File.Exists(direcX,nombre & ".pdf") And ext=".pdf" Then
			
			page.Cell(2,2).SetFixedHeight(1,False)
			page.Cell(2,2).Refresh
			
			pdf1.SetDocument("../karismadocs/"&DirectorioX&"/"&nombre&".pdf")
			pdf1.Refresh
			page.Cell(3,1).Refresh
			cont2.RunAnimation("blur")
			cont1.RunAnimation("unblur")
			
		End If
		
		If File.Exists(direcX,nombre & ".PDF") And ext=".PDF" Then
			
			page.Cell(2,2).SetFixedHeight(1,False)
			page.Cell(2,2).Refresh
			
			
			pdf1.SetDocument("../karismadocs/"&DirectorioX&"/"&nombre&".PDF")
			pdf1.Refresh
			page.Cell(3,1).Refresh
			cont2.RunAnimation("blur")
			cont1.RunAnimation("unblur")
			
		End If
		
		' CARGADO DE LOS VIDEOS
		
		
		If File.Exists(direcX,nombre & ".mp4") And ext=".mp4" Then
			video1.LoadVideoHTML5("../karismadocs/"&DirectorioX&"/"&nombre&".mp4","video/mp4")
			video1.Refresh
			
			
			page.Cell(2,2).SetFixedHeight(1,False)
			page.Cell(2,2).Refresh
			
			
			page.Cell(3,1).SetFixedHeight(1,False)
			page.Cell(3,1).Refresh
			page.Cell(4,1).Refresh
			cont1.RunAnimation("blur")
			cont2.RunAnimation("unblur")
			
		End If
		
		If File.Exists(direcX,nombre & ".MP4") And ext=".MP4" Then
			video1.LoadVideoHTML5("../karismadocs/"&DirectorioX&"/"&nombre&".MP4","video/mp4")
			video1.Refresh
		
		
			page.Cell(2,2).SetFixedHeight(1,False)
			page.Cell(2,2).Refresh
		
			page.Cell(3,1).SetFixedHeight(1,False)
			page.Cell(3,1).Refresh
		
		
			page.Cell(4,1).Refresh
			cont1.RunAnimation("blur")
			cont2.RunAnimation("unblur")
		End If
	

End Sub


Sub lstdire_Clicked(ItemId As String)
	Dim contX As ABMContainer=page.Cell(1,2).Component("contcur")
	Dim lstx As ABMList=contX.Cell(2,1).Component("lstcour")
	lstx.Clear
	lstx.Refresh
	Dim lstCou As List
	lstCou.Initialize
	lstCou=File.ListFiles(File.DirApp & "\www\suite\karismadocs\" & ItemId)

	For K=0 To lstCou.Size-1
		Dim nombre As String=lstCou.Get(K)
		
		If nombre.SubString2(nombre.Length-4,nombre.Length).ToLowerCase=".pdf" Or nombre.SubString2(nombre.Length-4,nombre.Length).ToLowerCase=".mp4" Then
			lstx.AddItem(lstCou.Get(K),BuildSimpleItem("C" & K,"",lstCou.Get(K)))		
		End If
	Next
	DirectorioX=ItemId
	lstx.Refresh

		Dim cont1 As ABMContainer=page.Cell(3,1).Component("containerpdf")
		Dim pdf1 As ABMPDFViewer = cont1.Cell(1,1).Component("pdf")
		
		Dim cont2 As ABMContainer=page.Cell(4,1).Component("containervideo")
		Dim video1 As ABMVideo = cont2.Cell(1,1).Component("video")
		
		Dim cont3 As ABMContainer=page.Cell(2,2).Component("containerboton")
		Dim boton1 As ABMButton = cont3.Cell(1,1).Component("ver")

	cont1.RunAnimation("blur")
	cont2.RunAnimation("blur")
	cont3.RunAnimation("blur")

End Sub
Sub	 lstcour_Clicked(ItemId As String)
		
		
		idactual = ItemId
		
		Dim nombre As String=ItemId.SubString2(0,ItemId.Length-4).trim
		
		Dim ext As String=ItemId.SubString2(ItemId.Length-4,ItemId.Length).trim
		
		Dim cnty As ABMContainer=page.Component("contimg")
		Dim imgY As ABMImage=cnty.Component("cvr")
		Dim direcX As String
		direcX=File.Combine (File.DirApp,CoverLib & "\" & DirectorioX)
		
		
		Dim cont1 As ABMContainer=page.Cell(3,1).Component("containerpdf")
		Dim pdf1 As ABMPDFViewer = cont1.Cell(1,1).Component("pdf")
		
		Dim cont2 As ABMContainer=page.Cell(4,1).Component("containervideo")
		Dim video1 As ABMVideo = cont2.Cell(1,1).Component("video")
		
		Dim cont3 As ABMContainer=page.Cell(2,2).Component("containerboton")
		Dim boton1 As ABMButton = cont3.Cell(1,1).Component("ver")
		
		' CARGADO DE LAS CARATULAS
		
		If File.Exists(direcX,nombre & ".jpg") Then
					'imgY.Initialize(page,"imgy","../karismadocs/"&DirectorioX&"/"&nombre&".jpg",3)
					imgY.Source="../karismadocs/"&DirectorioX&"/"&nombre&".jpg"
					imgY.Refresh
		Else if File.Exists(direcX,nombre & ".png") Then
					'imgY.Initialize(page,"imgy","../karismadocs/"&DirectorioX&"/"&nombre&".png",3)
					imgY.Source="../karismadocs/"&DirectorioX&"/"&nombre&".png"
					imgY.Refresh
		End If
		
	
		
		
			Select Case ext
		Case ".pdf"
			boton1.Tag = "leer"
			boton1.Text = "Leer"
			boton1.Refresh	
		Case ".PDF"	
			boton1.Tag = "leer"
			boton1.Text = "Leer"
			boton1.Refresh	
		Case ".mp4"	
			boton1.Tag = "abrir"
			boton1.Text = "Abrir"
			boton1.Refresh		
		Case ".MP4"	
			boton1.Tag = "abrir"
			boton1.Text = "Abrir"
			boton1.Refresh	
		End Select	
		
		
		cont1.RunAnimation("blur")
		cont2.RunAnimation("blur")	
		cont3.RunAnimation("unblur")
		
			
	page.Refresh
		
		
End Sub


Sub btnPlay_Clicked(Target As String)
	Dim cont As ABMContainer= 	page.Cell(3,1).Component("containervideo")
	
	Dim video As ABMVideo = cont.Cell(1,1).Component("video")
	If IsPlaying Then
		video.Pause				
	Else
		video.Play						
	End If	
End Sub

Sub btnStop_Clicked(Target As String)
	Dim cont As ABMContainer= 	page.Cell(3,1).Component("containervideo")
	
	Dim video As ABMVideo = cont.Cell(1,1).Component("video")
	video.Stop
End Sub

Sub btnVolumeDown_Clicked(Target As String)
	Dim cont As ABMContainer= 	page.Cell(3,1).Component("containervideo")
	Dim video As ABMVideo = cont.Cell(1,1).Component("video")	
	Volume = Volume - 10
	If Volume < 0 Then Volume = 0
	video.SetVolume(Volume)
End Sub

Sub btnVolumeUp_Clicked(Target As String)
	Dim cont As ABMContainer= 	page.Cell(3,1).Component("containervideo")
	
	Dim video As ABMVideo = cont.Cell(1,1).Component("video")
	Volume = Volume + 10
	If Volume > 100 Then Volume = 100	
	video.SetVolume(Volume)	
End Sub

Sub btnMute_Clicked(Target As String)
	Dim cont As ABMContainer= 	page.Cell(3,1).Component("containervideo")
	Dim btnMute As ABMButton = cont.Cell(2,1).Component("btnMute")				
	Dim video As ABMVideo = cont.Cell(1,1).Component("video")
	If IsMuted Then
		IsMuted = False		
		video.UnMute	
		btnMute.IconName = "mdi-av-volume-mute"			
	Else
		video.Mute			
		IsMuted = True
		btnMute.IconName = "mdi-av-volume-off"
	End If	
	btnMute.Refresh ' IMPORTANT
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
'	If icon <> "" Then
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
'	Else
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int 
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
	     combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next 
	
	'page.Refresh
	'combo1.Refresh	   
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid 
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)	
	Return myModal
End Sub

Sub Page_Ready()
	Log("Lista library")
	ConnectPage
	page.RestoreNavigationBarPosition
End Sub
