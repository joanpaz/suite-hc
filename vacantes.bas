﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "vacantes"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "23"
	' your own variables		
	Dim myToastId As Int = 1
	
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
		'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	ConnectPage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------		
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")	
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
End Sub

public Sub BuildTheme()
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	theme.AddContainerTheme("conte")
	theme.Container("conte").ZDepth=ABM.ZDEPTH_3
	theme.AddDividerTheme("div1")
	theme.Divider("div1").ForeColor=ABM.COLOR_BLUE
	theme.Divider("div1").ForeColorIntensity=ABM.INTENSITY_LIGHTEN4
	theme.AddImageSliderTheme("sl1")
	theme.ImageSlider("sl1").Height=200
	theme.ImageSlider("sl1").Indicators=False
	theme.ImageSlider("sl1").FullWidth=True
	theme.ImageSlider("sl1").ZDepth=ABM.ZDEPTH_3
	
	
	'Combos
	theme.AddComboTheme("combo")
	theme.Combo("combo").ZDepth=ABM.ZDEPTH_3
	
	
	
		'label
	theme.AddlabelTheme("label1")
	theme.Label("label1").ForeColor = ABM.COLOR_WHITE	
	
		' add additional themes specific for this page
	theme.AddTableTheme("tbl1theme")
	theme.Table("tbl1theme").ZDepth = ABM.ZDEPTH_1
	
	theme.Table("tbl1theme").AddCellTheme("headerfooter")
	theme.Table("tbl1theme").Cell("headerfooter").BackColor = ABM.COLOR_BROWN
	theme.Table("tbl1theme").Cell("headerfooter").ActiveBackColorIntensity = ABM.INTENSITY_DARKEN4
	theme.Table("tbl1theme").Cell("headerfooter").ForeColor = ABM.COLOR_WHITE	
	theme.Table("tbl1theme").Cell("headerfooter").Align = ABM.TABLECELL_HORIZONTALALIGN_CENTER
	
	
	theme.Table("tbl1theme").AddCellTheme("headerfooterright")
	theme.Table("tbl1theme").Cell("headerfooterright").BackColor = ABM.COLOR_BLUEGREY
	theme.Table("tbl1theme").Cell("headerfooterright").ForeColor = ABM.COLOR_WHITE
	theme.Table("tbl1theme").Cell("headerfooterright").Align = ABM.TABLECELL_HORIZONTALALIGN_RIGHT
	
	theme.Table("tbl1theme").AddCellTheme("nocolor")
	theme.Table("tbl1theme").Cell("nocolor").ActiveBackColor = ABM.COLOR_BLUEGREY
	theme.Table("tbl1theme").Cell("nocolor").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN2
	
	theme.Table("tbl1theme").AddCellTheme("nocoloredit")
	theme.Table("tbl1theme").Cell("nocoloredit").ActiveBackColor = ABM.COLOR_BLUEGREY
	theme.Table("tbl1theme").Cell("nocoloredit").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN2
	theme.Table("tbl1theme").Cell("nocoloredit").IsEditable = True
	theme.Table("tbl1theme").Cell("nocoloredit").InputMask = "'alias':'decimal', 'digits': 2"
	theme.Table("tbl1theme").Cell("nocoloredit").AllowEnterKey = False
	
	theme.Table("tbl1theme").AddCellTheme("positive")
	theme.Table("tbl1theme").Cell("positive").BackColor = ABM.COLOR_GREEN
	theme.Table("tbl1theme").Cell("positive").BackColorIntensity = ABM.INTENSITY_LIGHTEN3
	theme.Table("tbl1theme").Cell("positive").ActiveBackColor = ABM.COLOR_GREEN
	theme.Table("tbl1theme").Cell("positive").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN1
	theme.Table("tbl1theme").Cell("positive").Align = ABM.TABLECELL_HORIZONTALALIGN_RIGHT
	
	theme.Table("tbl1theme").AddCellTheme("negative")
	theme.Table("tbl1theme").Cell("negative").BackColor = ABM.COLOR_RED
	theme.Table("tbl1theme").Cell("negative").BackColorIntensity = ABM.INTENSITY_LIGHTEN3
	theme.Table("tbl1theme").Cell("negative").ActiveBackColor = ABM.COLOR_RED
	theme.Table("tbl1theme").Cell("negative").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN1
	theme.Table("tbl1theme").Cell("negative").Align = ABM.TABLECELL_HORIZONTALALIGN_RIGHT
	
	theme.Table("tbl1theme").AddCellTheme("aligntop")
	theme.Table("tbl1theme").Cell("aligntop").VerticalAlign = ABM.TABLECELL_VERTICALALIGN_TOP
	theme.Table("tbl1theme").Cell("aligntop").BorderColor = ABM.COLOR_BLACK
	theme.Table("tbl1theme").Cell("aligntop").BorderWidth = 1
	'theme.Table("tbl1theme").Cell("aligntop").ZDepth = ABM.ZDEPTH_1
	theme.Table("tbl1theme").Cell("aligntop").Align = ABM.TABLECELL_HORIZONTALALIGN_CENTER
	
		'tema de celda titulo
	theme.AddCellTheme("titulo1")
 	theme.Cell("titulo1").Align=ABM.CELL_ALIGN_CENTER
 	theme.Cell("titulo1").BackColor=ABM.COLOR_BROWN
 	theme.Cell("titulo1").BackColorIntensity=ABM.INTENSITY_LIGHTEN1
	
	'tema de celda titulo
	theme.AddInputTheme("input1")
	theme.Input("input1").FocusForeColor = ABM.COLOR_WHITE
	theme.Input("input1").InputColor = ABM.COLOR_black
	theme.Input("input1").ValidColor = ABM.COLOR_WHITE
	theme.Input("input1").InvalidColor = ABM.COLOR_WHITE 
	theme.Input("input1").AutoCompleteBackColor = ABM.COLOR_WHITE 
	theme.Input("input1").AutoCompleteHoverBackColor = ABM.COLOR_WHITE 
 	'theme.Input("input1").   '=ABM.INPUT_TEXTALIGN_CENTER
 	'theme.Input("input1").
 	'theme.Input("input1").BackColorIntensity=ABM.INTENSITY_LIGHTEN1
	
	
	 theme.Table("tbl1theme").AddCellTheme("editable")
    theme.Table("tbl1theme").Cell("editable").BorderColor = ABM.COLOR_GREY
    theme.Table("tbl1theme").Cell("editable").BorderWidth = 1
    theme.Table("tbl1theme").Cell("editable").VerticalAlign = ABM.TABLECELL_VERTICALALIGN_TOP
    theme.Table("tbl1theme").Cell("editable").IsEditable = True   '<-- now this cell is editable
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "vacantes.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' adding a navigation bar
	
			
	
	page.AddRows(1,False,"").AddCellsOSMP(12,0,0,0,1,1,1,10,0,-5,0,"")
	page.AddRows(16,False,"").AddCellsOSMP(1,0,0,0,12,12,12,10,0,-5,0,"")
	
	
	
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()
	
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
	
Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=1
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	
	
	Dim img As ABMImage
    img.Initialize(page,"1","../images/klogo.png",1.0)
    img.Caption="Num.1"
    img.Tag=1
	img.IsCircular = False
    img.IsResponsive=True
	
	page.cell(1,1).AddArrayComponent(img,"myimg")
	
	
	Dim contenedora As ABMContainer	
	contenedora.Initialize(page,"contenedora","cardtheme2")
	'contenedora.AddRows
	
	contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,20,0,0,0,"")
	contenedora.AddRows(1,True, "").AddCellsOSMP(1,0,0,0,12,12,12,5,0,30,10,"")
	contenedora.AddRows(1,True, "").AddCellsOSMP(4,0,0,0,12,12,3,5,10,0,10,"")
	'contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	'contenedora.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	
	contenedora.BuildGrid
	page.Cell(2,1).SetOffsetSize(0,0,0,12,11,11)
	page.Cell(2,1).AddComponent(contenedora)
	
	
	
	Dim lbl As ABMLabel
	lbl.Initialize(page,"lbl","Seleccione una oportunidad de trabajo",ABM.SIZE_H6,False,"etiq")
	contenedora.Cell(1,1).AddComponent(lbl)
	contenedora.Cell(1,1).UseTheme("celda2")	
	
	Dim tbl As ABMTable
	tbl.InitializeScrollable(page, "tbl", False, False, True, Array As Int(200,200,200,200,200,200,100), "tbl1theme")
	tbl.SetHeaders(Array As String("División","País","Locación","Área","Departamento","Posición","Cant."  ))
	tbl.SetHeaderThemes(Array As String("headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter"))
	
		tbl.SetFooter("Choose", 12,"headerfooter")
	
	' HABILITAR PARA PRUEBA
		'page.Cell(2,1).SetFixedHeight(1000)
		contenedora.Cell(2,1).AddComponent(tbl)
		'contenedora.Cell(1,1).MarginTop = "5%"
	
	
	Dim r As List
	Dim rCellThemes As List
		
		
	
	
	
	rst=sql1.ExecQuery("Select * from RH_PersonnelRequisition") 'where nom_division='"&t1&"' and nom_country='"&t2&"' and nom_location='"&t3&"' and nom_area='"&t4&"' and nom_department='" & nom_departamento & "' order by 1")
	
	
	
	Do While rst.NextRow
		
		r.Initialize
		rCellThemes.Initialize
		
		
		Dim val1 As ABMLabel
		val1.Initialize(page,pos&"div",rst.GetString2(1),ABM.SIZE_PARAGRAPH,False,"")
		
		r.Add(val1)
		rCellThemes.Add("aligntop")	
		
		Dim val2 As ABMLabel
		val2.Initialize(page,pos&"sub",rst.GetString2(2),ABM.SIZE_PARAGRAPH,False,"")
		
		r.Add(val2)
		rCellThemes.Add("aligntop")	
		
		Dim val3 As ABMLabel
		val3.Initialize(page,pos&"loc",rst.GetString2(3),ABM.SIZE_PARAGRAPH,False,"")
		
		r.Add(val3)
		rCellThemes.Add("aligntop")	
		
		Dim val4 As ABMLabel
		val4.Initialize(page,pos&"are",rst.GetString2(4),ABM.SIZE_PARAGRAPH,False,"")
		
		r.Add(val4)
		rCellThemes.Add("aligntop")	
		
		Dim val5 As ABMLabel
		val5.Initialize(page,pos&"dep",rst.GetString2(5),ABM.SIZE_PARAGRAPH,False,"")
		
		r.Add(val5)
		rCellThemes.Add("aligntop")	
		
		Dim val6 As ABMLabel
		val6.Initialize(page,pos&"pos",rst.GetString2(6),ABM.SIZE_PARAGRAPH,False,"")
		
		r.Add(val6)
		rCellThemes.Add("aligntop")	
		
		Dim val7 As ABMLabel
		val7.Initialize(page,pos&"qty",rst.GetString2(7),ABM.SIZE_PARAGRAPH,False,"")
		
		r.Add(val7)
		rCellThemes.Add("aligntop")	
		
		
			tbl.AddRow("uid"&pos, r)
			tbl.SetRowThemes(rCellThemes)
			
		
		pos=pos+1
	Loop
	
	rst.Close
	
	
	
	tbl.Refresh
	
			 Dim apply As ABMButton
			apply.InitializeFlat(page,"apply","","","Aplicar","btn")
			
			contenedora.Cell(3,1).AddComponent(apply)
			
		
	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub

Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then 
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub  apply_Clicked(Target As String)
	
	
	ABMShared.NavigateToPage(ws,ABMPageId,"../solicitudpuestopub/solicitudpuestopub.html")
	
	
End Sub



Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
'	If icon <> "" Then
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
'	Else
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int 
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
	     combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next 
	
	'page.Refresh
	'combo1.Refresh	   
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid 
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)	
	Return myModal
End Sub

Sub Page_Ready()
	Log("Lista vacantes")
	ConnectPage
'	page.RestoreNavigationBarPosition
End Sub