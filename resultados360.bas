﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@

'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "resultados360"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = ""
	' your own variables
	
	Public calif As Int = 5
	Dim contenedores As List = Array As String ("A","B","C","D","E","F","G","H","I","J")
	
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------
	Log("Connected resultados 360")
	ws = WebSocket1
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
		' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
		page.Refresh
		page.FinishedLoading
	Else
		If page.WebsocketReconnected Then
			Log("Websocket reconnected")
			' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
			' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
			ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
		Else
			' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
			Log("Websocket first connection")
			page.Prepare
			ConnectPage
		End If
	End If
	Log(ABMPageId)
	'----------------------MODIFICATION-------------------------------
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected resultados 360")
End Sub

Sub Page_ParseEvent(Params As Map)
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)
		End Select
	End If
End Sub

public Sub BuildTheme()
	' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "index.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
	'page.AlwaysShowVerticalScrollBar = True
	
		
	' adding a navigation bar
	ABMShared.BuildNavigationBar(page, "Resultados 360°","../images/klogo3.png", "", "resultados360", "")
			
	' create the page grid
	page.AddRows(5,False,"").AddCells12(4,"")
	page.AddRows(5,False,"").AddCellsOSMP(2,0,0,0,6,6,6,10,0,5,5,"")
	page.AddRows(1,False,"").AddCells12(1,"")
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	
		
End Sub

public Sub ConnectPage()
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
	
	Dim cmbDepartamento,cmbPuesto,cmbempleados,cmbsupervisores, cmbrelacion As ABMCombo
	
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
	
		
	'	Dim lbl As ABMLabel
	'	lbl.Initialize(page,"lbl","Evaluación de retroalimentación 360°",ABM.SIZE_H5,False,"")
	'	page.Cell(1,1).AddComponent(lbl)
	'	page.Cell(1,1).PaddingLeft = "5%"
	
	
	'Depatamentos
	cmbDepartamento.Initialize(page,"cmbDepartamento","Seleccione un departamento",250,"combo")
	page.Cell(3,1).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(3,1).AddComponent(cmbDepartamento)
	rst=sql1.ExecQuery("Select * from [karismaHC].[dbo].RH_Dept")
	pos=0
	cmbDepartamento.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbDepartamento.AddItem( rst.GetString2(0),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
		pos=pos+1
		Log("esta en Deptos--->" & pos & "   " & rst.GetString2(1))
	Loop
	rst.close
	'Puesto
	cmbPuesto.Initialize(page,"cmbPuesto","Seleccione una posición",250,"combo")
	page.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(3,2).AddComponent(cmbPuesto)
	
	
	'Empleados
	cmbempleados.Initialize(page,"cmbempleados","Seleccione un evaluado",250,"combo")
	page.Cell(3,3).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(3,3).AddComponent(cmbempleados)
	
	
	'supervisores
'	cmbsupervisores.Initialize(page,"cmbsupervisores","Seleccione evaluador",250,"combo")
'	page.Cell(4,2).SetOffsetSize(0,0,1,12,6,3)
'	page.Cell(4,2).AddComponent(cmbsupervisores)
	'rst=sql1.ExecQuery("Select * from [karismaHC].[dbo].ER_USERS")
	'pos=0
	'cmbsupervisores.AddItem("P","",BuildSimpleItem("P","",""))
	'Do While rst.NextRow
	'cmbsupervisores.AddItem( rst.GetString2(1),rst.GetString2(5),BuildSimpleItem("P"& pos,"",rst.GetString2(5)))
'	cmbsupervisores.AddItem( ws.Session.GetAttribute("idsesion"),ws.Session.GetAttribute("nombre"),BuildSimpleItem("P"& pos,"",ws.Session.GetAttribute("nombre")))
	'	pos=pos+1
	'Loop
	'rst.close
	
	cmbrelacion.Initialize(page,"cmbrelacion","Seleccione la relación",250,"combo")
	cmbrelacion.AddItem("auto","Auto evaluación", BuildSimpleItem("", "", "{NBSP}{NBSP}"&"Auto evaluación"))
	cmbrelacion.AddItem("subordinado","Subordinado", BuildSimpleItem("", "", "{NBSP}{NBSP}"&"Subordinado"))
	cmbrelacion.AddItem("companero","Compañero", BuildSimpleItem("", "", "{NBSP}{NBSP}"&"Compañero"))
	cmbrelacion.AddItem("cliente","Cliente Int-Ext", BuildSimpleItem("", "", "{NBSP}{NBSP}"&"Cliente Int-Ext"))
	cmbrelacion.AddItem("supervisor","Supervisor", BuildSimpleItem("", "", "{NBSP}{NBSP}"&"Supervisor"))
	cmbrelacion.AddItem("todos","Todos", BuildSimpleItem("", "", "{NBSP}{NBSP}"&"Todos"))
	cmbrelacion.AddItem("todos-auto","Todos menos la autoevacuación", BuildSimpleItem("", "", "{NBSP}{NBSP}"&"Todos menos la autoevacuación"))
	
	page.Cell(4,1).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(4,1).AddComponent(cmbrelacion)
	
	
	Dim year As ABMCombo
	year.Initialize(page,"year", "Seleccione el año", 250,"combo")
	page.Cell(4,2).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(4,2).AddComponent(year)
	
	
	
	
	For k=2017 To DateTime.GetYear(DateTime.Now)+50
	
		year.AddItem(k,k, BuildSimpleItem(k,"",k))
		
	Next
	
	year.Refresh
	
	
	Dim rbgroup As ABMRadioGroup
	rbgroup.Initialize(page, "rbgroup", "radio")
	rbgroup.AddRadioButton("Anual", True)
	rbgroup.AddRadioButton("Semestre I", True)
	rbgroup.AddRadioButton("Semestre II", True)
	rbgroup.SetActive(0)
	page.Cell(4,3).AddComponent(rbgroup)
	page.Cell(4,3).SetOffsetSize(0,0,1,12,6,3)

	Dim ver As ABMButton
	ver.InitializeFlat(page,"ver","","","Ver","btn")
	page.Cell(5,1).AddComponent(ver)
	page.Cell(5,1).SetOffsetSize(0,0,1,12,6,3)
	'page.Cell(5,1).PaddingTop = "%"
	
	

	
'	Dim salvar As ABMButton
'	salvar.InitializeFlat(page,"salvar","","","Salvar","btn")
'	page.Cell(11,1).AddComponent(salvar)
'	page.Cell(11,1).SetOffsetSize(0,0,5,12,6,6)
'	page.Cell(11,1).PaddingTop = "2%"
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub
Sub ver_Clicked(Target As String)
	page.Pause
	consulta("A",6,1,"COMUNICACIÓN","A")
	consulta("B",6,2,"PENSAMIENTO ESTRATÉGICO","B")
	consulta("C",7,1,"ORIENTACIÓN A RESULTADOS","C")
	consulta("D",7,2,"ACEPTAR EL CAMBIO","D")
	consulta("E",8,1,"TOMA DE DECISIONES","E")
	consulta("F",8,2,"LIDERAZGO","F")
	consulta("G",9,1,"OPERAR CON DISCIPLINA","G")
	consulta("H",9,2,"AMBIENTE DE TRABAJO","H")
	consulta("I",10,1,"INNOVAR Y REAPLICAR ","I")
	consulta("J",10,2,"DESARROLLO DE PERSONAL","J")
	page.Resume
	page.Refresh
	
End Sub
Sub consulta(conteiner As String,Rx As Int,Cx As Int,titulo As String,archivo As String)
	
	Log ("cargando contenedor "&conteiner)
	Dim cntA As ABMContainer
	cntA.Initialize(page,conteiner,"cnt")
	
	Dim sql1 As SQL
	Dim valores()  As Object
	'sql1.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
	
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	Dim rst As ResultSet
	Dim pos As Int=0
	Dim idobj As Int
	Dim sqlt As String
	
	Dim C1 As ABMCombo=page.Component("year")
	Dim T1 As String=C1.GetActiveItemId
	Dim L1 As ABMLabel=C1.GetComponent(T1)

	Dim R1 As ABMRadioGroup = page.Component("rbgroup")
	Dim periodo As Int = R1.GetActive
	Dim P1 As String = periodo
	
	Dim C2 As ABMCombo=page.Component("cmbempleados")
	Dim T2 As String=C2.GetActiveItemId
	Dim L2 As ABMLabel=C2.GetComponent(T2)

	Dim C4 As ABMCombo=page.Component("cmbDepartamento")
	Dim T4 As String=C4.GetActiveItemId
	Dim L4 As ABMLabel=C4.GetComponent(T4)
	
	Dim C5 As ABMCombo=page.Component("cmbPuesto")
	Dim T5 As String=C5.GetActiveItemId
	Dim L5 As ABMLabel=C5.GetComponent(T5)
	
	Dim C6 As ABMCombo=page.Component("cmbrelacion")
	Dim T6 As String=C6.GetActiveItemId
	Dim L6 As ABMLabel=C6.GetComponent(T6)
		
	
	cntA.AddRows(12,False,"").AddCellsOS(1,0,0,0,5,5,5,"").AddCellsOS(8,0,0,0,1,1,1,"")
	cntA.BuildGrid
	Dim lbltA1 As ABMLabel
	lbltA1.Initialize(page,"lbltA1","{B}" & titulo & "{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,1).SetFixedHeight(50,False)
	cntA.Cell(1,1).UseTheme("celda2")
	cntA.Cell(1,1).AddComponent(lbltA1)
	Dim lbltA2 As ABMLabel
	lbltA2.Initialize(page,"lbltA2","{B}Nunca-1{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,2).SetFixedHeight(50,False)
	cntA.Cell(1,2).UseTheme("celda2")
	cntA.Cell(1,2).AddComponent(lbltA2)
	Dim lbltA3 As ABMLabel
	lbltA3.Initialize(page,"lbltA3","{B}Rara vez-2{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,3).SetFixedHeight(50,False)
	cntA.Cell(1,3).UseTheme("celda2")
	cntA.Cell(1,3).AddComponent(lbltA3)
	Dim lbltA4 As ABMLabel
	lbltA4.Initialize(page,"lbltA4","{B}Algunas veces-3{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,4).SetFixedHeight(50,False)
	cntA.Cell(1,4).UseTheme("celda2")
	cntA.Cell(1,4).AddComponent(lbltA4)
	Dim lbltA5 As ABMLabel
	lbltA5.Initialize(page,"lbltA5","{B}Casi siempre-4{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,5).SetFixedHeight(50,False)
	cntA.Cell(1,5).UseTheme("celda2")
	cntA.Cell(1,5).AddComponent(lbltA5)
	Dim lbltA6 As ABMLabel
	lbltA6.Initialize(page,"lbltA6","{B}Siempre-5{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,6).SetFixedHeight(50,False)
	cntA.Cell(1,6).UseTheme("celda2")
	cntA.Cell(1,6).AddComponent(lbltA6)
	Dim lbltA7 As ABMLabel
	lbltA7.Initialize(page,"lbltA7","{B}NS/NC{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,7).SetFixedHeight(50,False)
	cntA.Cell(1,7).UseTheme("celda2")
	cntA.Cell(1,7).AddComponent(lbltA7)
	Dim lbltA8 As ABMLabel
	lbltA8.Initialize(page,"lbltA8","{B}NOTA{/B}",ABM.SIZE_SMALL,False,"etiq")
	cntA.Cell(1,8).SetFixedHeight(50,False)
	cntA.Cell(1,8).UseTheme("celda2")
	cntA.Cell(1,8).AddComponent(lbltA8)
	
	Dim query As String
	Dim c As Int = 1
	Dim nota,evaluadores, notaobj, evalobj, notatot,evaltot, rnmas1 As Int = 0
	Dim promedio, promedioobj As Float = 0
	
	Dim relacion  As String
	Select Case T6
		Case "todos"
			relacion = " "
		Case "todos-1"
			relacion = " and nom_relacion <>'auto' "
		Case "else"		
			relacion = " nom_relacion='" & T6 & "' "
	End Select
		
	
	query = "Select count(*) from Rh_evaluacion360 where nom_evaluado='" & T2 & "' "&relacion&"  and nom_departamento='" & T4 & "' and nom_posicion='" & T5 & "' and fec_ano='" & L1.Text & "' and fec_periodo='" & P1 & "' and id_categoria='" & conteiner & "' and nom_db='" &  ws.Session.GetAttribute("DBUse") & "'  order by 1"
	
	Dim cantobj As Int = sql1.ExecQuerySingleResult(query)
	
	evaltot = 0
	notatot = 0
	promedio = 0	
	For Rn=0 To cantobj-1
		promedioobj = 0
		notaobj = 0
		evalobj = 0
		
		rnmas1 = Rn +1
		query = "Select nom_objetivo from Rh_evaluacion360 where nom_evaluado='" & T2 & "' "&relacion&" and nom_departamento='" & T4 & "' and nom_posicion='" & T5 & "' and fec_ano='" & L1.Text & "' and fec_periodo='" & P1 & "' and id_categoria='" & conteiner & "' and id_objetivo='" & rnmas1 & "' and nom_db='" &  ws.Session.GetAttribute("DBUse") & "'  order by 1"
		Dim obj As String = sql1.ExecQuerySingleResult(query)
				
		Dim lblcA1 As ABMLabel
		lblcA1.Initialize(page,"O"&conteiner&rnmas1 ,obj,ABM.SIZE_SMALL,False,"")
		cntA.Cell(2+Rn,1).AddComponent(lblcA1)
		cntA.Cell(2+Rn,1).PaddingLeft = "1%"
		cntA.Cell(2+Rn+1,1).AddComponent(ABMShared.Buildline(page,"1"))
				
		For c= 1 To calif
				
			query = "select * from Rh_evaluacion360 where nom_evaluado='" & T2 & "' "&relacion&"  and nom_departamento='" & T4 & "' and nom_posicion='" & T5 & "' and fec_ano='" & L1.Text & "' and fec_periodo='" & P1 & "' and id_categoria='" & conteiner & "' and id_objetivo='" & rnmas1 & "'and num_calificacion='" & c & "' and nom_db='" &  ws.Session.GetAttribute("DBUse") & "'  order by 1"		
			rst=sql1.ExecQuery(query)
			 evaluadores=0
			 nota = 0
			Do While rst.NextRow
				nota = nota + rst.GetInt("num_calificacion")
				evaluadores=evaluadores + 1
			Loop
				
			Dim peval As ABMLabel
			peval.initialize(page,conteiner&rnmas1&c,"("&evaluadores&") "&nota ,ABM.SIZE_A,False,"")
			cntA.Cell(2+Rn,c+1).UseTheme("celda3")
			cntA.Cell(2+Rn,c+1).AddArrayComponent(peval,"C")
			cntA.Cell(2+Rn+1,c+1).AddComponent(ABMShared.Buildline(page,"1"))
			
			evalobj = evalobj + evaluadores
			notaobj = notaobj + nota	
			rst.Close	
		Next
		
		Dim lblAR1 As ABMLabel
		lblAR1.Initialize(page,"lblAR1","-",ABM.SIZE_A,False,"")
		cntA.Cell(2+Rn,8).UseTheme("celda4")
		cntA.Cell(2+Rn,8).AddComponent(lblAR1)
		cntA.Cell(2+Rn+1,8).AddComponent(ABMShared.Buildline(page,"1"))
	
		If Not ( evalobj = 0) Then
			promedioobj = notaobj / evalobj
		Else
			promedioobj = 0
		End If	
		
		Dim lblAR As ABMLabel
		lblAR.Initialize(page,"T"&conteiner&rnmas1,"("&evalobj&") "&promedioobj,ABM.SIZE_A,False,"")
		cntA.Cell(2+Rn,9).UseTheme("celda4")
		cntA.Cell(2+Rn,9).AddComponent(lblAR)
		cntA.Cell(2+Rn+1,9).AddComponent(ABMShared.Buildline(page,"1"))
		
		evaltot = evaltot + evalobj
		notatot = notatot + notaobj
	Next
	
	If Not ( evaltot = 0) Then
		promedio = notatot /evaltot
	Else
		promedio = 0
	End If		
	
	Dim TTT As ABMLabel
	TTT.Initialize(page,"TTT"&conteiner&rnmas1,"Total: ",ABM.SIZE_A,False,"")
	cntA.Cell(2+cantobj,8).UseTheme("celda4")
	cntA.Cell(2+cantobj,8).AddComponent(TTT)
	'cntA.Cell(2+cantobj+1,8).AddComponent(ABMShared.Buildline(page,"1"))
	
	Dim TT As ABMLabel
	evaltot = evaltot / cantobj
	TT.Initialize(page,"TT"&conteiner&rnmas1,"("&evaltot&") "&promedio,ABM.SIZE_A,False,"")
	cntA.Cell(2+cantobj,9).UseTheme("celda4")
	cntA.Cell(2+cantobj,9).AddComponent(TT)
	'cntA.Cell(2+cantobj+1,9).AddComponent(ABMShared.Buildline(page,"1"))
	
	page.Cell(Rx ,Cx).RemoveAllComponents
	page.Cell(Rx ,Cx).AddComponent(cntA)
	page.Cell(Rx ,Cx).Refresh
	
End Sub


Sub cmbPuesto_Clicked(itemId As String)
	'Dim cmbx As ABMCombo=page.Component("cmbPuesto")
	'Dim cmby As ABMCombo=page.Component("cmbDepartamento")
	
	Dim cmbz As ABMCombo=page.Component("cmbempleados")
	cmbz.Clear
	Dim query As String
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	page.Pause
	sql1.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
	query ="Select * FROM OPENQUERY(GIN, 'select * from "&ws.Session.GetAttribute("DBUse")&".dbo.vw_lgen_altasybajas where Pkey_Pos_Key=''"&itemId&"''')"
	rst=sql1.ExecQuery(query)
	pos=0
	'cmbempleados.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbz.AddItem( rst.GetString2(0),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
		pos=pos+1
	Loop
	rst.close
	page.resume
	cmbz.Refresh
End Sub



Sub cmbDepartamento_Clicked(itemId As String)
	
	Dim cmbx As ABMCombo=page.Component("cmbPuesto")
	Dim cmby As ABMCombo=page.Component("cmbPuesto")
	cmby.Clear
	cmby.Refresh
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_Positons where Pos_Dep_Key='" & itemId & "' order by 3")
	cmbx.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbx.AddItem(rst.GetString2(0),rst.GetString2(2),BuildSimpleItem("P"& pos,"",rst.GetString2(2)))
		pos=pos+1
	Loop
	
	page.Resume
	rst.Close
	cmbx.Refresh
End Sub

' clicked on the navigation bar
Sub Page_NavigationbarClicked(Action As String, Value As String)
	' saving the navigation bar position
	page.SaveNavigationBarPosition
	If Action = "LogOff" Then
		ABMShared.LogOff(page)
		Return
	End If

	ABMShared.NavigateToPage(ws, ABMPageId, Value)
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
	'	If icon <> "" Then
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	'	Else
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
		combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next
	
	'page.Refresh
	'combo1.Refresh
	Return combo1
	

End Sub
