﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "micarrera"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = ""
	' your own variables
	Private person,nombreC As String
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------
	Log("Connected")
	ws = WebSocket1
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
		' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
		page.Refresh
		page.FinishedLoading
	Else
		If page.WebsocketReconnected Then
			Log("Websocket reconnected")
			' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
			' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
			ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
		Else
			' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
			Log("Websocket first connection")
			page.Prepare
			ConnectPage
		End If
	End If
	Log(ABMPageId)
	'----------------------MODIFICATION-------------------------------
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")
End Sub

Sub Page_ParseEvent(Params As Map)
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)
		End Select
	End If
End Sub

public Sub BuildTheme()
	' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	' add additional themes specific for this page
	theme.AddChartTheme("chart1theme")
	theme.Chart("chart1theme").Serie(ABM.CHART_SERIEINDEX_C).LinePointStrokeWidthPx=8
	theme.Chart("chart1theme").Serie(ABM.CHART_SERIEINDEX_C).LineStrokeWidthPx=5
	theme.Chart("chart1theme").LabelFontSizePx=20
	'--------------------------------
	
End Sub
public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "micarrera.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
	'page.AlwaysShowVerticalScrollBar = True
	
		
	' adding a navigation bar
	ABMShared.BuildNavigationBar(page,"Mi carrera","../images/klogo3.png","","micarrera","")
			
	' create the page grid
	page.AddRowsM(1,False,0,0, "").AddCellsOSMP(1,0,0,0,12,12,12,10,0,10,10,"")
	page.AddRowsM(2,False,0,0, "").AddCellsOSMP(3,0,0,0,12,6,4,10,0,10,10,"")
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	
		
End Sub

public Sub ConnectPage()
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
	page.Pause
	'cursos
	Dim sqlC As SQL
	Dim rstC As ResultSet
	sqlC.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
	Dim consulta As String
	Dim ctax As ResultSet
	Dim CAx,CBx,CEx,CMx,CTx,OTx As String
	Dim idusuario As String
	idusuario= ws.Session.GetAttribute2("idsesion", "")
	If Main.subordinado=False Then
		consulta="Select * from Openquery(GIN,'Select Person_id,Nombre from "&  ws.Session.GetAttribute("DBUse") & ".dbo.vw_Person_Diagrama where Pkey_key1=''" & idusuario &"''')"
		rstC=sqlC.ExecQuery(consulta)
		Do While rstC.NextRow
			person=rstC.GetString("Person_id")
			nombreC=rstC.GetString("Nombre")
		Loop
		rstC.Close
	Else
		consulta="Select * from Openquery(GIN,'Select Person_id,Nombre from "&  ws.Session.GetAttribute("DBUse") & ".dbo.vw_Person_Diagrama where Pkey_key1=''" & Main.idsesion  &"''')"
		rstC=sqlC.ExecQuery(consulta)
		Do While rstC.NextRow
			person=rstC.GetString("Person_id")
			nombreC=rstC.GetString("Nombre")
		Loop
		rstC.Close
	End If
	consulta="Select * from openquery(GIN,'select count(*) as ctaX from " &  ws.Session.GetAttribute("DBUse") &".dbo.VW_MASTER_LGEN_TRAINNING1 where Person_id="& person  &"')"
	Log("ANTES " &consulta)
	ctax=sqlC.ExecQuery(consulta)
	Dim resuta As String
	Do While ctax.NextRow
		resuta=ctax.GetString("ctax")
	Loop
	If resuta<>"0" Then
		consulta="Select * from Openquery (GIN,'select distinct SUBSTRING(train_name,0,3) as ''Sub'',Count(*) as ''Cta'' from " &  ws.Session.GetAttribute("DBUse") &".dbo.VW_MASTER_LGEN_TRAINNING1 where Person_id="& person  &" Group by SUBSTRING(train_name,0,3)') "
		Log("consulta2 --" & consulta)
		rstC=sqlC.ExecQuery(consulta)
		Do While rstC.NextRow
			If rstC.GetString("Sub")="CA" Then
				CAx=rstC.GetString("Cta")
			else If rstC.GetString("Sub")="CB" Then
				CBx=rstC.GetString("Cta")
			else If rstC.GetString("Sub")="CE" Then
				CEx=rstC.GetString("Cta")
			else If rstC.GetString("Sub")="CM" Then
				CMx=rstC.GetString("Cta")
			else If rstC.GetString("Sub")="CT" Then
				CTx=rstC.GetString("Cta")
			Else
				OTx=rstC.GetString("Cta")
			End If
		Loop
	End If
	If CAx.Length=0 Then CAx="0"
	If CBx.Length=0 Then CBx="0"
	If CEx.Length=0 Then CEx="0"
	If CMx.Length=0 Then CMx="0"
	If CTx.Length=0 Then CTx="0"
	If OTx.Length=0 Then OTx="0"

	Dim SumValues As Int = CAx+CBx+CEx+CMx+CTx+OTx

	Dim titulo As ABMLabel
	titulo.Initialize(page,"titulo","{B}Detalle de cursos tomados por " & nombreC.ToUpperCase & "{/B}" ,ABM.SIZE_A,False,"etiq")
	page.Cell(1,1).UseTheme("celda2")
	page.Cell(1,1).AddComponent(titulo)

	Dim cnt1 As ABMContainer
	cnt1.Initialize(page,"cnt1","cnt")
	cnt1.AddRow(1,True,"","").AddCells12MP(1,10,0,0,0,"")
	cnt1.AddRows(13,True,"").AddCells12(3,"")
	'fila = 3
	'columna = 2
	cnt1.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,4,4,0,10,10,0,"cardtheme") ' ESPACIO LIBRE ABAJO 8
	cnt1.BuildGrid
	cnt1.Cell(1,1).SetOffsetSize(0,0,0,12,12,12)
	cnt1.Cell(1,1).UseTheme("TittleCell")
	cnt1.Cell(12,1).SetFixedHeight(105,False)
	'-----------------------------
	Dim Tittle2 As ABMLabel
	Tittle2.Initialize(page,"Tittle2","Mi carrera",ABM.SIZE_H6,False,"etiq")
	Tittle2.Clickable=True
	cnt1.Cell(1,1).UseTheme("celda2")
	cnt1.Cell(1,1).AddComponent(Tittle2)
	page.Cell(3,1).AddComponent(cnt1)
	
	
	Dim Tittle3 As ABMLabel
	Tittle3.Initialize(page,"Tittle3","Completados:",ABM.SIZE_H6,False,"")
	Tittle3.Clickable=True
	cnt1.Cell(3,2).UseTheme("")
	cnt1.Cell(3,2).AddComponent(Tittle3)
	cnt1.Cell(3,2).SetOffsetSize(0,0,5,12,6,3)
	cnt1.Cell(3,2).PaddingLeft = "10%"
	
	Dim Tittle4 As ABMLabel
	Tittle4.Initialize(page,"Tittle4","Porcentaje (%):",ABM.SIZE_H6,False,"")
	Tittle4.Clickable=True
	cnt1.Cell(3,3).UseTheme("")
	cnt1.Cell(3,3).AddComponent(Tittle4)
	cnt1.Cell(3,3).SetOffsetSize(0,0,1,12,6,3)
	cnt1.Cell(3,3).PaddingLeft = "3%"

	
	'---------------------------------------------------------------------------------------
	Dim space As String = ""

	Dim lbl1, lbl1_1, lbl1_2 As ABMLabel
	Dim pers1 As String= Round((CBx/SumValues)*100)
	lbl1.Initialize(page,"lbl1","Certificación Basica:"&space ,ABM.SIZE_A,False,"")
	lbl1.Clickable=True
	cnt1.Cell(5,1).AddarrayComponent(lbl1,"lbl1")
	cnt1.Cell(5,1).SetOffsetSize(0,0,0,12,6,6)
	space=""
	
	lbl1_1.Initialize(page,"lbl1_1",CBx ,ABM.SIZE_A,False,"")
	lbl1_1.Clickable=True
	cnt1.Cell(5,2).AddArrayComponent(lbl1_1,"lbl1")
	cnt1.Cell(5,2).SetOffsetSize(0,0,1,12,6,2)
	
	lbl1_2.Initialize(page,"lbl1_2", pers1 & " %" ,ABM.SIZE_A,False,"")
	lbl1_2.Clickable=True
	cnt1.Cell(5,3).AddarrayComponent(lbl1_2,"lbl1")
	cnt1.Cell(5,3).SetOffsetSize(0,0,1,12,6,2)

	'-------------------------------------------------	
	Dim lbl2 As ABMLabel
	lbl2.Initialize(page,"lbl2","Certificación Media:"&space ,ABM.SIZE_A,False,"")
	lbl2.Clickable=True
	cnt1.Cell(6,1).AddArrayComponent(lbl2,"lbl2")
	cnt1.Cell(6,1).SetOffsetSize(0,0,0,12,6,5)
	
	Dim lbl2_1 As ABMLabel
	lbl2_1.Initialize(page,"lbl2_1",CMx,ABM.SIZE_A,False,"")
	lbl2_1.Clickable=True
	cnt1.Cell(6,2).AddArrayComponent(lbl2_1,"lbl2")
	cnt1.Cell(6,2).SetOffsetSize(0,0,2,12,6,3)

	Dim pers2 As String= Round((CMx/SumValues)*100)
	Dim lbl2_2 As ABMLabel
	lbl2_2.Initialize(page,"lbl2_2", pers2 & " %" ,ABM.SIZE_A,False,"")
	lbl2_2.Clickable=True
	cnt1.Cell(6,3).AddarrayComponent(lbl2_2,"lbl2")
	cnt1.Cell(6,3).SetOffsetSize(0,0,0,12,6,2)


	
	'-------------------------------------------------

	Dim lbl3 As ABMLabel
	lbl3.Initialize(page,"lbl3","Certificación Avanzada:"&space,ABM.SIZE_A,False,"")
	lbl3.Clickable=True
	cnt1.Cell(7,1).AddArrayComponent(lbl3,"lbl3")
	cnt1.Cell(7,1).SetOffsetSize(0,0,0,12,6,5)
	
	Dim lbl_3 As ABMLabel
	lbl_3.Initialize(page,"lbl_3", CAx,ABM.SIZE_A,False,"")
	lbl_3.Clickable=True
	cnt1.Cell(7,2).AddArrayComponent(lbl_3,"lbl3")
	cnt1.Cell(7,2).SetOffsetSize(0,0,2,12,6,3)
	
	Dim pers3 As String= Round((CAx/SumValues)*100)
	Dim lbl3_2 As ABMLabel
	lbl3_2.Initialize(page,"lbl3_2", pers3 & " %" ,ABM.SIZE_A,False,"")
	lbl3_2.Clickable=True
	cnt1.Cell(7,3).AddarrayComponent(lbl3_2,"lbl3")
	cnt1.Cell(7,3).SetOffsetSize(0,0,1,12,6,2)
	
	'-------------------------------------------------

	Dim lbl4 As ABMLabel
	lbl4.Initialize(page,"lbl4","Certificación Tecnica:"&space,ABM.SIZE_A,False,"")
	lbl4.Clickable=True
	cnt1.Cell(8,1).AddarrayComponent(lbl4,"lbl4")
	cnt1.Cell(8,1).SetOffsetSize(0,0,0,12,6,5)
	
	Dim lbl_4 As ABMLabel
	lbl_4.Initialize(page,"lbl_4",CTx,ABM.SIZE_A,False,"")
	lbl_4.Clickable=True
	cnt1.Cell(8,2).AddarrayComponent(lbl_4,"lbl4")
	cnt1.Cell(8,2).SetOffsetSize(0,0,2,12,6,3)

	Dim pers4 As String= Round((CTx/SumValues)*100)
	Dim lbl4_2 As ABMLabel
	lbl4_2.Initialize(page,"lbl4_2", pers4 & " %" ,ABM.SIZE_A,False,"")
	lbl4_2.Clickable=True
	cnt1.Cell(8,3).AddarrayComponent(lbl4_2,"lbl4")
	cnt1.Cell(8,3).SetOffsetSize(0,0,0,12,6,2)

	'-------------------------------------------------

	Dim lbl5 As ABMLabel
	lbl5.Initialize(page,"lbl5","Certificación Especializada:"&space,ABM.SIZE_A,False,"")
	lbl5.Clickable=True
	cnt1.Cell(9,1).AddarrayComponent(lbl5,"lbl5")
	cnt1.Cell(9,1).SetOffsetSize(0,0,0,12,6,5)
	
	Dim lbl_5 As ABMLabel
	lbl_5.Initialize(page,"lbl_5",CEx,ABM.SIZE_A,False,"")
	lbl_5.Clickable=True
	cnt1.Cell(9,2).AddarrayComponent(lbl_5,"lbl5")
	cnt1.Cell(9,2).SetOffsetSize(0,0,2,12,6,3)

	Dim pers5 As String= Round((CEx/SumValues)*100)
	Dim lbl5_2 As ABMLabel
	lbl5_2.Initialize(page,"lbl5_2", pers5 & " %" ,ABM.SIZE_A,False,"")
	lbl5_2.Clickable=True
	cnt1.Cell(9,3).AddarrayComponent(lbl5_2,"lbl5")
	cnt1.Cell(9,3).SetOffsetSize(0,0,1,12,6,2)
	
	'-------------------------------------------------

	Dim lbl6 As ABMLabel
	lbl6.Initialize(page,"lbl6","Otros:"&space,ABM.SIZE_A,False,"")
	lbl6.Clickable=True
	cnt1.Cell(10,1).AddarrayComponent(lbl6,"lbl6")
	cnt1.Cell(10,1).SetOffsetSize(0,0,0,12,6,5)
	
	Dim lbl_6 As ABMLabel
	lbl_6.Initialize(page,"lbl_6_1",OTx,ABM.SIZE_A,False,"")
	lbl_6.Clickable=True
	cnt1.Cell(10,2).AddarrayComponent(lbl_6,"lbl6")
	cnt1.Cell(10,2).SetOffsetSize(0,0,2,12,6,3)
	
	Dim pers6 As String= Round((OTx/SumValues)*100)
	Dim lbl6_2 As ABMLabel
	lbl6_2.Initialize(page,"lbl6_2", pers6 & " %" ,ABM.SIZE_A,False,"")
	lbl6_2.Clickable=True
	cnt1.Cell(10,3).AddarrayComponent(lbl6_2,"lbl6")
	cnt1.Cell(10,3).SetOffsetSize(0,0,0,12,6,2)

	'--------------------------------
	Dim cnt2 As ABMContainer
	cnt2.Initialize(page,"cnt2","cnt")
	cnt2.AddRow(1,True,"","").AddCells12MP(1,10,0,0,0,"")
	cnt2.AddRow(1,True,"","")
	cnt2.AddRow(1,True,"","")
	cnt2.AddRow(1,True,"","")
	cnt2.AddRow(1,True,"","")
	cnt2.BuildGrid
	cnt2.Cell(1,1).SetOffsetSize(0,0,0,12,12,12)
	cnt2.Cell(1,1).UseTheme("TittleCell")
	
	'-----------------------------
	Dim Tittle3 As ABMLabel
	Tittle3.Initialize(page,"Tittle3","Grafica",ABM.SIZE_H6,False,"etiq")
	Tittle3.Clickable=True
	cnt2.Cell(1,1).UseTheme("celda2")
	cnt2.Cell(1,1).AddComponent(Tittle3)
	page.Cell(3,2).AddComponent(cnt2)

	Dim chart1 As ABMChart
	chart1.Initialize(page, "chart1", ABM.CHART_TYPEPIE, ABM.CHART_RATIO_SQUARE, "chart1theme")
	' Something special, Label interpolation.  the value will transformed into a percentage format (This is Javascript!)

	chart1.OptionsPie.LabelInterpolation = "Math.round(value / " & SumValues  & " * 100) + '%'"
	' we do not add labels, as they will be generated them through the LabelInterpolation function we just declared!
	
	' add ONE serie
	Dim Serie5A As ABMChartSerie
	Serie5A.InitializeForPie
	Serie5A.SetValues(Array As Int(CBx,CMx,CAx,CTx,CEx,OTx))
	chart1.AddSerie(Serie5A)
	
	' add the chart to the cell
	' move one row , first param = 1
	cnt2.CellR(2,1).AddComponent(chart1)
	
	'--------------------------------
	Dim cnt3 As ABMContainer
	cnt3.Initialize(page,"cnt3","cnt")
	cnt3.AddRow(1,True,"","").AddCells12MP(1,10,0,0,0,"")
	cnt3.AddRows(2,True,"").AddCellsOS(1,0,0,0,12,12,12,"")
	cnt3.BuildGrid
	cnt3.Cell(1,1).SetOffsetSize(0,0,0,12,12,12)
	cnt3.Cell(1,1).UseTheme("TittleCell")
	'cnt3.Cell(3,1).SetFixedHeight(5,True)
	'-----------------------------
	Dim Tittle4 As ABMLabel
	Tittle4.Initialize(page,"Tittle4","Detalle",ABM.SIZE_H6,False,"etiq")
	Tittle4.Clickable=True
	cnt3.Cell(1,1).UseTheme("celda2")
	cnt3.Cell(1,1).AddComponent(Tittle4)
	page.Cell(3,3).AddComponent(cnt3)
	
	Dim list1 As ABMList
	list1.InitializeWithMaxHeight(page,"list1",ABM.COLLAPSE_ACCORDION,480,"")
	cnt3.Cell(2,1).AddComponent(list1)
	page.Resume
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub
Sub lbl1_Clicked(Target As String)
	llenaDetalle("CB")
End Sub
Sub lbl2_Clicked(Target As String)
	llenaDetalle("CM")
End Sub
Sub llenaDetalle(tipo As String)
	Dim sqlC As SQL
	Dim rstC As ResultSet
	Dim cntx As ABMContainer=page.Component("cnt3")

	Dim list1 As ABMList=cntx.Component("list1")
	sqlC.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
	Dim consulta As String
	consulta="Select * from Openquery (GIN,'select distinct train_name as ''Det'',Count(*) as ''Cta'' from " & ws.Session.GetAttribute("DBUse") &".dbo.VW_MASTER_LGEN_TRAINNING1 where Person_id=" & person & " and SUBSTRING(train_name,0,3)=''"& tipo &"'' Group by train_name')"
	Log("--> " & consulta)
	rstC=sqlC.ExecQuery(consulta)
	Dim Rx As Int=0
	list1.Clear
	Do While rstC.NextRow
		list1.AddItem(rstC.GetString("Det"),BuildSimpleItem(Rx,"",rstC.GetString("Det") ))'& " CURSOS " & rstC.GetString("Cta")
	Loop
	cntx.Refresh
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

' clicked on the navigation bar
Sub Page_NavigationbarClicked(Action As String, Value As String)
	' saving the navigation bar position
	page.SaveNavigationBarPosition
	If Action = "LogOff" Then
		ABMShared.LogOff(page)
		Return
	End If

	ABMShared.NavigateToPage(ws, ABMPageId, Value)
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

