﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "chatroom"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "7"
	' your own variables		
	Dim myToastId As Int = 1
	
	Dim contact As String
	Private sql1 As SQL
	Dim i As Int
	Public ventana As ABMTable
	Dim rst As ResultSet
	Public username As String
	
	Public ChatName As String
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	ConnectPage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------		
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")	
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
End Sub

public Sub BuildTheme()
		' start with the base theme defined in Comunes
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	
	' add additional themes specific for this page
	
	
	' add additional themes specific for this page
	theme.AddTableTheme("temaventana")
	theme.Table("temaventana").ZDepth = ABM.ZDEPTH_1
	
	theme.Table("temaventana").AddCellTheme("headerfooter")
	theme.Table("temaventana").Cell("headerfooter").BackColor = ABM.COLOR_BROWN
	theme.Table("temaventana").Cell("headerfooter").ForeColor = ABM.COLOR_WHITE	
	
	theme.Table("temaventana").AddCellTheme("headerfooterright")
	theme.Table("temaventana").Cell("headerfooterright").BackColor = ABM.COLOR_BROWN
	theme.Table("temaventana").Cell("headerfooterright").ForeColor = ABM.COLOR_WHITE
	theme.Table("temaventana").Cell("headerfooterright").Align = ABM.TABLECELL_HORIZONTALALIGN_RIGHT
	
	theme.Table("temaventana").AddCellTheme("nocolor")
	theme.Table("temaventana").Cell("nocolor").ActiveBackColor = ABM.COLOR_BROWN
	theme.Table("temaventana").Cell("nocolor").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN2
	
	theme.Table("temaventana").AddCellTheme("nocoloredit")
	theme.Table("temaventana").Cell("nocoloredit").ActiveBackColor = ABM.COLOR_BROWN
	theme.Table("temaventana").Cell("nocoloredit").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN2
	theme.Table("temaventana").Cell("nocoloredit").IsEditable = True
	theme.Table("temaventana").Cell("nocoloredit").InputMask = "'alias':'decimal', 'digits': 2"
	theme.Table("temaventana").Cell("nocoloredit").AllowEnterKey = False
	
	theme.Table("temaventana").AddCellTheme("positive")
	theme.Table("temaventana").Cell("positive").BackColor = ABM.COLOR_GREEN
	theme.Table("temaventana").Cell("positive").BackColorIntensity = ABM.INTENSITY_LIGHTEN3
	theme.Table("temaventana").Cell("positive").ActiveBackColor = ABM.COLOR_GREEN
	theme.Table("temaventana").Cell("positive").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN1
	theme.Table("temaventana").Cell("positive").Align = ABM.TABLECELL_HORIZONTALALIGN_RIGHT
	
	theme.Table("temaventana").AddCellTheme("negative")
	theme.Table("temaventana").Cell("negative").BackColor = ABM.COLOR_RED
	theme.Table("temaventana").Cell("negative").BackColorIntensity = ABM.INTENSITY_LIGHTEN3
	theme.Table("temaventana").Cell("negative").ActiveBackColor = ABM.COLOR_RED
	theme.Table("temaventana").Cell("negative").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN1
	theme.Table("temaventana").Cell("negative").Align = ABM.TABLECELL_HORIZONTALALIGN_RIGHT
	
	theme.Table("temaventana").AddCellTheme("aligntop")
	theme.Table("temaventana").Cell("aligntop").VerticalAlign = ABM.TABLECELL_VERTICALALIGN_TOP
	
	theme.AddCardTheme("whitetitle")
	theme.Card("whitetitle").TitleForeColor = ABM.COLOR_WHITE
	
	
	
	' add additional themes specific for this page
	theme.AddListTheme("list1theme")
	theme.List("list1theme").ItemBackColor = ABM.COLOR_BLUEGREY
	theme.List("list1theme").SubItemBackColor = ABM.COLOR_WHITE	
	theme.List("list1theme").BackColor = ABM.COLOR_WHITE	
	
	theme.AddListTheme("list3theme")
	theme.List("list3theme").ItemBackColor = ABM.COLOR_LIME
	theme.List("list3theme").SubItemBackColor = ABM.COLOR_WHITE	
	theme.List("list3theme").SubItemDividerColor = ABM.COLOR_WHITE	
	theme.List("list3theme").BackColor = ABM.COLOR_WHITE	
	
	theme.AddLabelTheme("header")
	theme.Label("header").ForeColor = ABM.COLOR_BLACK
	

End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "chatroom.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' adding a navigation bar
		ABMShared.BuildNavigationBar(page,"Chat Room de "&Main.nombrecompleto,"../images/klogo3.png","","chatroom","")
			
	' create the page grid
	page.AddRows(1,True, "").AddCellsOSMP(1,0,0,0,12,8,8,0,1,0,10,"").AddCellsOSMP(1,0,0,0,12,4,4,0,1,0,50,"")
	page.AddRows(1,True, "").AddCellsOSMP(1,0,0,0,12,7,7,0,1,0,10,"").AddCellsOSMP(1,0,0,0,12,5,5,0,1,0,10,"")
	page.AddRows(1,True, "").AddCellsOSMP(1,0,0,0,12,8,8,0,1,0,50,"").AddCellsOSMP(1,0,0,0,12,4,4,0,1,0,50,"")
	
	
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()
	
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
 
    If ws.Session.GetAttribute2("authName", "") = "" Or Main.username ="" Or Main.nombrecompleto = "" Then
	ws.Session.SetAttribute("authName", "epaz")
	Main.username = "epaz"
	Main.nombrecompleto = "Estela Paz"
   End If	
	page.AddModalSheetTemplate(Mensajes)
	
	username = Main.username
	
	CallSubDelayed3(chatsharedold, "NewConnection", Me, ws.Session.GetAttribute2("authName", ""))
		
	'INICIALIZA VENTANA DE CHAT
	
	ventana.InitializeScrollable(page, "ventana", True, False, False, Array As Int(800), "temaventana")	
	ventana.SetHeaders(Array As String("Mensajes:"))
	ventana.SetHeaderThemes(Array As String("headerfooter"))
	
	ventana.SetFooter("",30,"headerfooter")
	page.Cell(2,1).SetFixedHeight(500,False)
	page.Cell(2,1).AddComponent(ventana)
	
	'INPUT Y BOTON DE ENVIAR 
		
	Dim cajaenvio As ABMContainer
	cajaenvio.Initialize(page, "cajaenvio","")
		
	cajaenvio.AddRows(1,False,"").AddCellsOSMP(1,0,0,0,12,8,8,0,1,0,50,"").AddCellsOSMP(1,0,0,0,12,4,4,0,1,0,50,"")
	cajaenvio.BuildGrid
	
	Dim mensaje As ABMInput
	mensaje.Initialize(page,"mensaje",ABM.INPUT_TEXT,"", False, "caja") 
	mensaje.Narrow = True
	
	
	Dim enviar As ABMButton
	enviar.InitializeRaised(page,"enviar","mdi-action-label",ABM.ICONALIGN_LEFT,"ENVIAR","btn")
		
	cajaenvio.Cell(1,1).AddComponent(mensaje)
	cajaenvio.Cell(1,2).AddComponent(enviar)
	
	page.Cell(3,1).AddComponent(cajaenvio)
	
	' INICICIALIZA CONTACTOS
	
	Dim contactos As ABMList
	contactos.InitializeWithMaxHeight(page, "contactos", ABM.COLLAPSE_ACCORDION, 400, "list1theme")
	
	contactos.AddItem("online", BuildSimpleItem("online0","mdi-action-account-circle","On Line"))
	contactos.AddItem("offline", BuildSimpleItem("offline0","mdi-action-account-circle","Off Line"))
		
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	'Dim rst As ResultSet
	rst=sql1.ExecQuery("Select * from users inner join ER_USERS on users.UserName = ER_USERS.Usr_Name where users.UserName<>'"&ws.Session.GetAttribute2("authName", "")&"' order by users.UserName")
    
	Dim i As Int = 0
	 Do While rst.NextRow
	 					
			i = i + 1
			contactos.AddSubItem("offline", rst.GetString("UserName"), BuildItem("C"&rst.GetString("UserName"), rst.GetString("Usr_FullName"),"",False))
			'contactos.AddItem(rst.GetString("UserId"), BuildItem(rst.GetString("UserId"), rst.GetString("Usr_FullName"), "0", False))
	Loop
	
    page.Cell(2,2).SetFixedHeight(500,False)
	page.Cell(2,2).AddComponent(contactos)
	
	
	
	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub

Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then 
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub


Public Sub UpdateMembersList (conectados As Map)
	'Users.SetHtml(data)
	'ws.Flush 'server event so we need to flush
	Dim list1 As ABMList = page.Component("contactos")
	
	'Dim u As String = user
	
	For Each c As String In conectados.Keys
		
		If Not(c=ws.Session.GetAttribute2("authName", "")) Then
			list1.Remove(c)		
			list1.AddSubItem("online", c, BuildItem("C"&c, chatsql.nombrecompleto(c),"",False))
		End If
	Next
		
	list1.Refresh
	ws.Flush
End Sub


Public Sub RemoveMembersList (user As String)
	'Users.SetHtml(data)
	'ws.Flush 'server event so we need to flush
	Dim list1 As ABMList = page.Component("contactos")
	
	If Not(user=ws.Session.GetAttribute2("authName", "")) Then
		list1.Remove(user)
		list1.AddSubItem("offline", user, BuildItem("C"&user, chatsql.nombrecompleto(user),"",False))
	End If
	list1.Refresh
	
	'ws.Flush
End Sub



Sub NewMessage(Msg As String, user As String)
		
		If  user = username Or user = contact Then
			Dim tbl2 As ABMTable = page.Cell(2,1).Component("ventana")
			Dim r As List
			Dim rCellThemes As List
			Dim Mesg As String
			r.Initialize
			rCellThemes.Initialize					
			Mesg = DateTime.Date(DateTime.Now)&" "&DateTime.Time(DateTime.Now)&" "& chatsql.nombrecompleto(user) &": "&Msg
			r.Add(Mesg)
		    rCellThemes.Add("")
			'i = i + 1
			tbl2.AddRow("mensaje", r)
			tbl2.SetRowThemes(rCellThemes) ' make sure you have as many items in rCellThemes as in r!
			tbl2.refresh	
		 
		End If
		
	ws.Flush
		
End Sub

Sub enviar_Clicked(Target As String)
		
  
	Dim cont1 As ABMContainer = page.Cell(3,1).Component("cajaenvio")
	Dim input1 As ABMInput = cont1.Component("mensaje")   
	
	Dim val As String = input1.Text  

	If contact <> "" Then
	
		CallSub3(chatsharedold, "NewMessage", Me, val)
	
		CallSubDelayed3(chatsql,"guardamensaje", Me, val)
	End If
	
	input1.Text = ""
	input1.Refresh

	ws.Flush
End Sub


Public Sub llenaventana
	
	Dim tbl2 As ABMTable = page.Cell(2,1).Component("ventana")
		
		tbl2.Clear
		tbl2.Refresh
		page.Refresh

   		sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")

		rst=sql1.ExecQuery("Select * from RH_chat where (nom_origen='"&ws.Session.GetAttribute2("authName", "")&"' and nom_destino='"&contact&"') or (nom_origen='"&contact&"' and nom_destino='"&ws.Session.GetAttribute2("authName", "")&"') order by Id_msg")
	
	 	Dim r As List
		Dim rCellThemes As List
		Dim msg As String
   
		 Dim i As Int = 0
		 Do While rst.NextRow
	 		
						
			r.Initialize
			rCellThemes.Initialize					
			msg =rst.GetString("fec_msg").SubString2(0,10) &"  "&rst.GetString("hor_msg").SubString2(11,19) &"  "&chatsql.nombrecompleto(rst.GetString("nom_origen"))&": "&rst.GetString("nom_msg")
			r.Add(msg)
		    rCellThemes.Add("")
			i = i + 1
			tbl2.AddRow("mensaje"&i, r)
			tbl2.SetRowThemes(rCellThemes) ' make sure you have as many items in rCellThemes as in r!
			

		Loop
		tbl2.refresh		
	page.Refresh
End Sub

Sub contactos_Clicked(ItemId As String)
	' get the list component from the page
	Dim list1 As ABMList = page.Component("contactos")
	Dim tbl2 As ABMTable = page.Cell(2,1).Component("ventana")
	contact = ItemId
	
	If Not(ItemId="online" Or ItemId="offline") Then
		tbl2.SetHeaders(Array As String("Talking with "&chatsql.nombrecompleto(ItemId)))
		tbl2.SetHeaderThemes(Array As String("headerfooter"))
		llenaventana
		tbl2.Refresh
		
		' get the item
		Dim cont As ABMContainer = list1.Item(ItemId)
		' get the badge control
		Dim badge As ABMBadge = cont.Component("C"&ItemId & "Badge")

		badge.IsNew = False	
		badge.Refresh ' IMPORTANT
	Else
	 	'page.refresh
	End If
	
End Sub


Sub BuildItem(id As String, text As String, messages As String, isnew As Boolean) As ABMContainer
	Dim ItemCont As ABMContainer
	ItemCont.Initialize(page, id, "")	
	ItemCont.AddRowsM(1,False,0,0, "").AddCellsOSMP(1,0,0,0,10,10,10,6,0,20,0,"").AddCellsOSMP(1,0,0,0,2,2,2,6,0,0,0,"")
	ItemCont.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	Dim lbl As ABMLabel
	lbl.Initialize(page, id & "lbl", text, ABM.SIZE_H6, False, "")
	ItemCont.Cell(1,1).AddComponent(lbl)
	
	Dim badge As ABMBadge
	badge.Initialize(page, id & "badge", messages, isnew, "")
	ItemCont.Cell(1,2).AddComponent(badge)
	
	Return ItemCont
End Sub

Sub msbtn3_Clicked(Target As String)
	page.CloseModalSheet("fixedsheet")			
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
'	If icon <> "" Then
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
'	Else
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int 
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
	     combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next 
	
	'page.Refresh
	'combo1.Refresh	   
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid 
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)	
	Return myModal
End Sub

Sub Page_Ready()
	Log("chat!")
	ConnectPage
	page.RestoreNavigationBarPosition
End Sub
