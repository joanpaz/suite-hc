﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@

'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "accidentindicators"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = ""
	' your own variables		
	Dim myToastId As Int = 1
	Public DownloadFolder As String = "/www/" & ABMShared.AppName & "/perfiles/"
	
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	ConnectPage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------			
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")	
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
End Sub

public Sub BuildTheme()
	' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	
	theme.AddCellTheme("c1")
	theme.Cell("c1").BackColor=ABM.COLOR_LIME

	theme.AddLabelTheme("tlbl")
	theme.Label("tlbl").ForeColor=ABM.COLOR_RED
	' add additional themes specific for this page
	 theme.AddTableTheme("tbl1theme")
	 theme.Table("tbl1theme").ZDepth = ABM.ZDEPTH_1
	 
	 theme.Table("tbl1theme").AddCellTheme("headerfooter")
	 theme.Table("tbl1theme").Cell("headerfooter").BackColor = ABM.COLOR_BROWN
	 theme.Table("tbl1theme").Cell("headerfooter").BackColorIntensity=ABM.INTENSITY_DARKEN4
	 theme.Table("tbl1theme").Cell("headerfooter").ActiveBackColorIntensity = ABM.INTENSITY_DARKEN4
	 theme.Table("tbl1theme").Cell("headerfooter").ForeColor = ABM.COLOR_WHITE 
	 theme.Table("tbl1theme").Cell("headerfooter").Align = ABM.TABLECELL_HORIZONTALALIGN_CENTER
	 
	 
	 theme.Table("tbl1theme").AddCellTheme("headerfooterright")
	 theme.Table("tbl1theme").Cell("headerfooterright").BackColor = ABM.COLOR_BLUEGREY
	 theme.Table("tbl1theme").Cell("headerfooterright").ForeColor = ABM.COLOR_WHITE
	 theme.Table("tbl1theme").Cell("headerfooterright").Align = ABM.TABLECELL_HORIZONTALALIGN_RIGHT
	 
	 theme.Table("tbl1theme").AddCellTheme("nocolor")
	 theme.Table("tbl1theme").Cell("nocolor").ActiveBackColor = ABM.COLOR_BLUEGREY
	 theme.Table("tbl1theme").Cell("nocolor").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN2
	 
	 theme.Table("tbl1theme").AddCellTheme("nocoloredit")
	 theme.Table("tbl1theme").Cell("nocoloredit").ActiveBackColor = ABM.COLOR_BLUEGREY
	 theme.Table("tbl1theme").Cell("nocoloredit").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN2
	 theme.Table("tbl1theme").Cell("nocoloredit").IsEditable = True
	 theme.Table("tbl1theme").Cell("nocoloredit").InputMask = "'alias':'decimal', 'digits': 2"
	 theme.Table("tbl1theme").Cell("nocoloredit").AllowEnterKey = False
	 
	 theme.Table("tbl1theme").AddCellTheme("positive")
	 theme.Table("tbl1theme").Cell("positive").BackColor = ABM.COLOR_GREEN
	 theme.Table("tbl1theme").Cell("positive").BackColorIntensity = ABM.INTENSITY_LIGHTEN3
	 theme.Table("tbl1theme").Cell("positive").ActiveBackColor = ABM.COLOR_GREEN
	 theme.Table("tbl1theme").Cell("positive").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN1
	 theme.Table("tbl1theme").Cell("positive").Align = ABM.TABLECELL_HORIZONTALALIGN_RIGHT
	 
	 theme.Table("tbl1theme").AddCellTheme("negative")
	 theme.Table("tbl1theme").Cell("negative").BackColor = ABM.COLOR_RED
	 theme.Table("tbl1theme").Cell("negative").BackColorIntensity = ABM.INTENSITY_LIGHTEN3
	 theme.Table("tbl1theme").Cell("negative").ActiveBackColor = ABM.COLOR_RED
	 theme.Table("tbl1theme").Cell("negative").ActiveBackColorIntensity = ABM.INTENSITY_LIGHTEN1
	 theme.Table("tbl1theme").Cell("negative").Align = ABM.TABLECELL_HORIZONTALALIGN_RIGHT
	 
	 theme.Table("tbl1theme").AddCellTheme("aligntop")
	 theme.Table("tbl1theme").Cell("aligntop").VerticalAlign = ABM.TABLECELL_VERTICALALIGN_TOP
	 theme.Table("tbl1theme").Cell("aligntop").BorderColor = ABM.COLOR_BLACK
	 theme.Table("tbl1theme").Cell("aligntop").BorderWidth = 1
	 'theme.Table("tbl1theme").Cell("aligntop").ZDepth = ABM.ZDEPTH_1
	 theme.Table("tbl1theme").Cell("aligntop").Align = ABM.TABLECELL_HORIZONTALALIGN_CENTER
	 
	  'tema de celda titulo
	 theme.AddCellTheme("titulo1")
	  theme.Cell("titulo1").Align=ABM.CELL_ALIGN_CENTER
	  theme.Cell("titulo1").BackColor=ABM.COLOR_BROWN
	  theme.Cell("titulo1").BackColorIntensity=ABM.INTENSITY_LIGHTEN1	
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "accidentindicators.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' adding a navigation bar
	ABMShared.BuildNavigationBar(page,"Indicador de accidentes","../images/klogo3.png","","accidentindicators","")
	
			
	' create the page grid
	'page.AddRows(5,True, "").AddCells12(1,"")
	page.AddRows(6,False, "").AddCellsOSMP(1,0,0,0,12,12,12,0,0,-5,-10,"")

	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

Sub getyears() As List
	Dim year As List
	year.Initialize
	For i=DateTime.GetYear(DateTime.Now) To 1990 Step -1
		year.Add(i)
	Next
	Return year
End Sub
Sub getmonths() As List
	Dim months As List
	months.Initialize
	months.Add("Enero")
	months.Add("Febrero")
	months.Add("Marzo")
	months.Add("Abril")
	months.Add("Mayo")
	months.Add("Junio")
	months.Add("Julio")
	months.Add("Agosto")
	months.Add("Septiembre")
	months.Add("Octubre")
	months.Add("Noviembre")
	months.Add("Diciembre")
	Return months
End Sub

public Sub ConnectPage()
'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
'	init all your own variables (like a List, Map) and add your components
	Dim cnt As ABMContainer
	cnt.Initialize(page,"cnt","cnt")
	cnt.AddRows(1,True,"").AddCells12(1,"")
	cnt.AddRows(2,True,"").AddCellsOSMP(1,0,0,0,6,2,2,0,0,0,0,"").AddCellsOSMP(3,0,1,1,6,2,2,0,0,0,0,"")
	cnt.AddRow(1,True,"","")
	cnt.AddRow(2,True,"","")
	cnt.BuildGrid
	page.Cell(2,1).AddComponent(cnt)
	Dim lblt As ABMLabel
	lblt.Initialize(page,"lblt","Filtros",ABM.SIZE_A,False,"etiq")
	cnt.Cell(1,1).MarginTop="1%"
	cnt.Cell(1,1).UseTheme("celda2")
	cnt.Cell(1,1).AddComponent(lblt)
	'cnt.Cell(2,1).AddComponent(buildcombo("monthin","","Start month", getmonths))
	cnt.Cell(2,2).AddComponent(buildcombo("yearin","","Seleccione el año", getyears))
	'cnt.Cell(2,3).AddComponent(buildcombo("monthfin","","End Month", getmonths))
	'cnt.Cell(2,4).AddComponent(buildcombo("yearfin","","End year", getyears))
	Dim btna As ABMButton
	btna.InitializeFlat(page,"btna","","","Ok","btn")
	cnt.Cell(4,1).UseTheme("celda3")
	cnt.Cell(4,1).AddComponent(btna)
	If  Not(Main.fondoe=Null) And Not(Main.fondoe="") And  Not(Main.fondoe="null") And Not(Main.fondoe="no") And Not(Main.fondoe="#") Then
		If Main.fondoe.Length<10 Then
			Dim script As String = $"$('body').css('background-color', '${Main.fondoe}');
													 $('body').attr('class','');	 
											"$
			page.ws.Eval(script,Null)
		Else
			Dim su As StringUtils
			Dim BT2() As Byte= su.DecodeBase64(Main.fondoe)
			Dim imagen As Image = BytesToImage(BT2)
			Dim carpeta As String = File.Combine(File.DirApp, DownloadFolder)
						
			Dim nomfondo As String =  ws.Session.GetAttribute("idsesion")&"b.jpg"
			Dim borro As Boolean = False
			If File.Exists (carpeta,  ws.Session.GetAttribute("idsesion")&"b.jpg") Then
				borro = File.Delete(carpeta,  ws.Session.GetAttribute("idsesion")&"b.jpg")
			End If
			
			Dim Out As OutputStream = File.OpenOutput(carpeta,  ws.Session.GetAttribute("idsesion")&"b.jpg" , True)
			imagen.WriteToStream(Out)
			Out.Close
						
			Dim script As String = $"$('body').css('background-image', 'url(../perfiles/${nomfondo})');
						
						 $('body').css('background-size', 'cover');
						 
							"$
			page.ws.Eval(script,Null)
			
		End If
	End If
	
	creaflotante
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub
Sub BytesToImage(bytes() As Byte) As Image
	Dim In As InputStream
	In.InitializeFromBytesArray(bytes, 0, bytes.Length)
	Dim bmp As Image
	bmp.Initialize2(In)
   
	Return bmp
End Sub
Sub btna_Clicked(Target As String)
	'crea	
	'-----inicio
	Dim cnt As ABMContainer = page.Component("cnt")
	Dim   yearin, yearfin As ABMCombo
	yearin = cnt.Cell(2,2).Component("yearin") 
	yearfin = cnt.Cell(2,4).Component("yearfin") 
	If yearin.GetActiveItemId = -1 Then
		page.Msgbox("msg1", "Seleccione el año","Error","ok","")
		Return
	End If
	DateTime.DateFormat="MM/dd/yyyy"	
	Dim fecha1, fecha2, year As String
	Dim y,m1, m2,d As Int
	Dim l As ABMLabel 
	l=yearin.GetComponent(yearin.GetActiveItemId)
	y=l.ID
	year = y
	'm1=monthin.GetActiveItemId+1
	fecha1 = DateTime.Date(DateUtils.SetDate( y,1,1))
	'l=yearfin.GetComponent(yearfin.GetActiveItemId)
	'y=l.id
	'm2=monthfin.GetActiveItemId+1
	'd=DateUtils.NumberOfDaysInMonth(m2,y)
	fecha2 = DateTime.Date(DateUtils.SetDate( y,12,31))
	Dim tam As Int = (m2 - m1) + 3
	'---Fin

	page.Pause
	Dim cnt2 As ABMContainer
	cnt2.Initialize(page,"cnt2","cnt")
	cnt2.AddRow(1,False,"","")
	cnt2.AddRow(1,False,"","")
	cnt2.AddRow(1,False,"","")
	cnt2.AddRow(1,False,"","")
	cnt2.AddRows(3,False,"").AddCellsOS(2,0,1,1,12,3,3,"")
	cnt2.AddRows(1,False,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,20,0,"")
	cnt2.AddRow(1,False,"","")
	cnt2.BuildGrid
	Dim img As ABMImage
	img.Initialize(page,"img","../images/klogo2.jpg",3)
	cnt2.Cell(2,1).SetOffsetSize(1,1,1,12,12,12)
	cnt2.Cell(2,1).AddComponent(img)
	cnt2.Cell(3,1).UseTheme("celda3")
	cnt2.Cell(3,1).AddComponent(ABMShared.BuildParagraph(page,"p","{B}Indicador de Accidentes{/B}",ABM.SIZE_H4))
	
	cnt2.Cell(5,1).AddComponent(ABMShared.BuildParagraph(page,"p","{B}HOTEL " & ws.Session.GetAttribute("location") & "{/B}",ABM.SIZE_A))
	
	Dim tbl1 As ABMTable	
	tbl1.Initialize(page,"tbl1",False,False,True,"tbl1theme")
	tbl1.SetHeaders(Array As String("Conceptos","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"))
	tbl1.SetHeaderThemes(Array As String("headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter","headerfooter"))
	
	Dim sql1 As SQL
	Dim rst As ResultSet
	'sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	sql1.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
	
	sql1.ExecNonQuery("EXEC [GIN]." & ws.Session.GetAttribute("DBUse") & ".dbo.PT_ACCIDENT_MES '456465', 'epaz10','"  & fecha1 & "','" & fecha2& "'")
	Dim consulta As String
	consulta="Select * FROM OPENQUERY(GIN,'select * from " & ws.Session.GetAttribute("DBUse") & ".dbo.vw_PT_ACCIDENT_MESepaz10')"
	Log ("---<>" & consulta)
	rst=sql1.ExecQuery(consulta)
	Do While rst.NextRow
		Dim valor() As String
		Log(rst.GetString(	"CONCEPTOS") & " --" & rst.GetString("enero"))
		If  rst.GetString("enero")=Null Then
			valor=Array As String(rst.GetString(	"CONCEPTOS"),"0","0","0","0","0","0","0","0","0","0","0","0")
		Else
			If rst.GetString(	"CONCEPTOS")="Índice de Accidentabilidad" Then
				valor=Array As String(rst.GetString( "CONCEPTOS"),Round2(rst.GetString("enero")*100,2)&"%",Round2(rst.GetString("febrero")*100,2)& "%",Round2(rst.GetString("marzo")*100,2)& "%",Round2(rst.GetString("abril")*100,2)& "%",Round2(rst.GetString("mayo")*100,2)& "%",Round2(rst.GetString("junio")*100,2)& "%",Round2(rst.GetString("julio")*100,2)& "%",Round2(rst.GetString("agosto")*100,2)& "%",Round2(rst.GetString("septiembre")*100,2)& "%",Round2(rst.GetString("octubre")*100,2)& "%",Round2(rst.GetString("noviembre")*100,2)& "%",Round2(rst.GetString("diciembre")*100,2)& "%")				
				'valor=Array As String(rst.GetString(	"CONCEPTOS"),Round2(rst.GetString("enero")*100,2)&"%",rst.GetString("febrero")*100,rst.GetString("marzo")*100,rst.GetString("abril")*100,rst.GetString("mayo")*100,Round2(rst.GetString("junio")*100,2)& "%",rst.GetString("julio")*100,rst.GetString("agosto")*100,rst.GetString("septiembre")*100,rst.GetString("octubre")*100,rst.GetString("nombiembre")*100,rst.GetString("diciembre")*100)
			Else	
				valor=Array As String(rst.GetString(	"CONCEPTOS"),rst.GetString("enero"),rst.GetString("febrero"),rst.GetString("marzo"),rst.GetString("abril"),rst.GetString("mayo"),rst.GetString("junio"),rst.GetString("julio"),rst.GetString("agosto"),rst.GetString("septiembre"),rst.GetString("octubre"),rst.GetString("noviembre"),rst.GetString("diciembre"))
			End If
		End If
		tbl1.AddRow("P" & rst.GetInt("idciunter"),	valor)
		tbl1.SetRowThemes(Array As String ("aligntop","aligntop","aligntop","aligntop","aligntop","aligntop","aligntop","aligntop","aligntop","aligntop","aligntop","aligntop","aligntop"))
	Loop
	tbl1.SetTooltip("Test",ABM.TOOLTIP_TOP,3000)

	tbl1.SetFooter("Tabla informativa", 13,"headerfooter")

	cnt2.Cell(8,1).AddComponent(tbl1)
	page.Cell(6,1).RemoveAllComponents
	page.Cell(6,1).AddComponent(cnt2)
	page.Refresh
	page.Resume
End Sub




Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then
		page.pause 
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
		page.Resume
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
'	If icon <> "" Then
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
'	Else
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,200,"")
	
	Dim i As Int 
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
	     combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next 
	
	'page.Refresh
	'combo1.Refresh	   
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid 
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)	
	Return myModal
End Sub

