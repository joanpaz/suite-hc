﻿Type=Class
Version=5
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "muro"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "26"
	' your own variables
	Dim myToastId As Int = 1
	
	Private postCounter As Int
	Private Posts As Map
	Public foto As String = ""
	'variables de uplload
	Public DownloadFolder As String = "/www/" & ABMShared.AppName & "/post/"
	Public DownloadMaxSize As String = 1000*1024
	
	Public DownloadFolder As String = "/www/" & ABMShared.AppName & "/post/"
	Public DownloadMaxSize As String = 1000*1024
	Private form1 As JQueryElement
	
	Public publicados As List 
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------
	Log("Connected")
	ws = WebSocket1
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
		' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
		page.Refresh
		page.FinishedLoading
	Else
		If page.WebsocketReconnected Then
			Log("Websocket reconnected")
			' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
			' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
			ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
		Else
			' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
			Log("Websocket first connection")
			page.Prepare
			ConnectPage
		End If
	End If
	Log(ABMPageId)
	'----------------------MODIFICATION-------------------------------
	' this page uses uploads, so needs some settings
	ws.Session.SetAttribute("abmcallback", Me)
	ws.Session.SetAttribute("abmdownloadfolder", DownloadFolder)
	ws.Session.SetAttribute("abmmaxsize", DownloadMaxSize)
	
	ws.Session.SetAttribute("file_upload_sender", Me)
	ws.Session.SetAttribute("dfolder", DownloadFolder)
	ws.Session.SetAttribute("msize", DownloadMaxSize)
	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")
	' and remove them again
	Try
		ws.Session.RemoveAttribute("abmcallback")
		ws.Session.RemoveAttribute("abmdownloadfolder")
		ws.Session.RemoveAttribute("abmmaxsize")
		
		ws.Session.RemoveAttribute("file_upload_sender")
		ws.Session.RemoveAttribute("dfolder")
		ws.Session.RemoveAttribute("msize")
	Catch
		Log(LastException.Message)
	End Try
End Sub

Sub Page_ParseEvent(Params As Map)
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)
		End Select
	End If
End Sub

public Sub BuildTheme()
	' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	
	' add additional themes specific for this page
	
	theme.Page.BackColor = ABM.COLOR_BLUEGREY
	theme.Page.BackColorIntensity = ABM.INTENSITY_LIGHTEN5
	
	theme.AddCellTheme("border")
	theme.Cell("border").BorderColor = ABM.COLOR_GREY
	theme.Cell("border").BorderColorIntensity = ABM.INTENSITY_LIGHTEN3
	theme.Cell("border").BorderWidth = 1
	
	theme.AddContainerTheme("zdepth")
	theme.Container("zdepth").BackColor = ABM.COLOR_WHITE
	theme.container("zdepth").ZDepth = ABM.ZDEPTH_1
	
	theme.AddCellTheme("lightgrey")
	theme.Cell("lightgrey").BackColor = ABM.COLOR_GREY
	theme.Cell("lightgrey").BackColorIntensity = ABM.INTENSITY_LIGHTEN5
	
	theme.AddDividerTheme("divider")
	theme.Divider("divider").ForeColor = ABM.COLOR_GREY
	theme.Divider("divider").ForeColorIntensity = ABM.INTENSITY_LIGHTEN3
	
	theme.AddCellTheme("right")
	theme.Cell("right").Align = ABM.CELL_ALIGN_RIGHT
	
	' add additional themes specific for this page
	theme.AddUploadTheme("upload")
	theme.Upload("upload").ErrorColor = ABM.COLOR_RED
	theme.Upload("upload").BackColor = ABM.COLOR_BROWN
	theme.Upload("upload").BackColorIntensity = ABM.INTENSITY_DARKEN4
	theme.Upload("upload").DropZoneBackColor =  ABM.COLOR_WHITE
	'theme.Upload("upload").DropZoneBackColorIntensity =
	theme.Upload("upload").ButtonHoverBackColor = ABM.COLOR_ORANGE
	theme.Upload("upload").ButtonHoverBackColorIntensity = ABM.INTENSITY_DARKEN4
	theme.Upload("upload").ButtonBackColor = ABM.COLOR_BROWN
	theme.Upload("upload").ButtonBackColorIntensity = ABM.INTENSITY_DARKEN4
	theme.Upload("upload").ForeColor = ABM.COLOR_BLACK
	theme.Upload("upload").ForeColorIntensity = ABM.INTENSITY_DARKEN4
	theme.Upload("upload").UploadProgressColor = ABM.COLOR_ORANGE
	theme.Upload("upload").UploadProgressColorIntensity = ABM.INTENSITY_DARKEN4
	theme.Upload("upload").ButtonForeColor = ABM.COLOR_WHITE
	theme.Upload("upload").ZDepth = ABM.ZDEPTH_4
	
	'theme.Upload("upload").
	
	theme.AddInputTheme("inp1")
	theme.Input("inp1").ZDepth = ABM.ZDEPTH_3
	
	
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "muro.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
	
	page.AddExtraCSSFile("custom/reaction.css") ' ACA SE CAMBIAN LOS TEXTOS DE LOS TIPOS DE ME GUSTA
	page.AddExtraJavaScriptFile("custom/reaction.js")
		
	Dim extraSideBar As ABMSideBar
	extraSideBar.Initialize(page, "extrasidebar", 530, 48, 56, Null, ABM.COLLAPSE_ACCORDION, "nav2theme")
	extraSideBar.AddSideBarComponent("esbHello", ABMShared.BuildSideBarComponent(page, "sidebarcomp", "../images2/16.jpg", "This is a sidebar component", "With an image and an editor"))
	page.NavigationBar.AddTopItemWithSideBar("SideBar", "", "mdi-action-dashboard", "", False, extraSideBar)
		
	' adding a navigation bar
	ABMShared.BuildNavigationBar(page,"Muro Karisma","../images/klogo3.png","","muro","")
	
	
			
	' create the page grid
	page.AddRows(1,False, "").AddCellsOSMP(1,0,0,0,12,12,6,0,0,50,0,"")         ''AddCells12(1,"")
	page.AddRows(1,False, "").AddCellsOSMP(2,0,0,0,12,12,6,0,0,50,0,"")
	page.AddRows(2,False, "").AddCellsOSMP(1,0,0,0,12,12,6,0,0,50,0,"")
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
		
	
	'	init all your own variables (like a List, Map) and add your components
	Dim lbl As ABMLabel
	lbl.Initialize(page,"lbl","Muro Karisma",ABM.SIZE_H5,False,"")
	page.Cell(1,1).AddComponent(lbl)
	
	Posts.Initialize
	
	
	Dim contenedor1 As ABMContainer
	contenedor1.Initialize(page,"contenedor1","")
	contenedor1.AddRows(4,True,"").AddCells12(3,"")
	contenedor1.BuildGrid
	page.Cell(2,1).SetOffsetSize(0,0,0,12,6,6)
	page.Cell(2,1).AddComponent(contenedor1)
	'page.Cell(2,1).
	
	Dim comment As ABMInput
	comment.Initialize(page,"comment",ABM.INPUT_TEXT,"¿Que estás pensando?",True,"inp1")
	contenedor1.Cell(2,2).SetOffsetSize(0,0,0,12,12,12)
	contenedor1.Cell(2,2).AddComponent(comment)
	
'	Dim upload1 As ABMUpload
'	upload1.Initialize(page, "upload1", "Publicar foto", "Buscar", "upload")
'	contenedor1.Cell(3,2).SetOffsetSize(0,0,0,12,12,12)
'	contenedor1.Cell(3,2).AddComponent(upload1)
	'	'page.Cell(3,1).AddComponent(upload1)


	
	Dim enviar As ABMButton
	enviar.InitializeFlat(page,"enviar","","","Publicar","btn")
	contenedor1.Cell(4,2).AddComponent(enviar)
	
	Dim fotoprev As ABMImage
	fotoprev.Initialize(page,"fotoprev","",1.0)
	fotoprev.SetFixedSize(300,300)
	fotoprev.IsClickable=True
	fotoprev.IsMaterialBoxed=True
	fotoprev.IsResponsive=True
	page.Cell(2,2).SetOffsetSize(0,1,1,12,3,3)
	page.Cell(2,2).AddComponent(fotoprev)
	
	
	
	publicados.Initialize
	
	
	
	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
	
	
	Dim sql1 As SQL
	Dim valores(), valores2()  As Object
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	Dim rst As ResultSet
	
	rst = sql1.ExecQuery("Select top 100 * from RH_POST order by fec_fecha desc")
	
	Do While rst.NextRow
		
		addNewCard(True,rst.GetInt("id"))
		
		publicados.Add(rst.GetInt("id"))
	Loop
	
	
	rst.Close
	
	ABM.AddHTML(page,"contenedor1-r3c2",$"<form id='form1'  method="post" enctype="multipart/form-data">
								<input type="file" name="file1" id="file1">	
								<input type="submit" name="submit" value="Adjuntar" id='save'>
								</form>
								<output id="list">
								</output>"$)
								
	Dim script As String = $"
	$(document).on("click", '.hola', function(){
    		alert($(this).data('value'));
		}); 

	   "$
	page.ws.Eval(script,Null)
	
	Dim script As String = $"
	
		// Variable to store your files
		var files;

		// Add events
		$('input[type=file]').on('change', prepareUpload);

		// Grab the files and set them to our variable
		function prepareUpload(event)
		{
		  files = event.target.files;
		}
		
	$('#form1').on('submit', uploadFiles);

// Catch the form submit and upload the files
		function uploadFiles(event)
		{
		  	event.stopPropagation(); // Stop stuff happening
		    event.preventDefault(); // Totally stop stuff happening

		    // START A LOADING SPINNER HERE

		    // Create a formdata object and add the files
		    var data = new FormData();
		    $.each(files, function(key, value)
		    {
		        data.append(key, value);
		    });

			    $.ajax({
			        url: 'filehelper',
			        type: 'POST',
			        data: data,
			        cache: false,   
			        processData: false, // Don't process the files
			        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
			    
			    });

			}
		

	   "$
	page.ws.Eval(script,Null)
	
	'addNewCard(True)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)
	myToastId = myToastId + 1
	
	'Dim contenedor1 As ABMContainer = page.Component("contenedor1")
	'Dim fotoprev As ABMImage = contenedor1.Cell(3,2).Component("fotoprev")
	Dim fotoprev As ABMImage = page.Component("fotoprev")
	
	Dim sql1 As SQL
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	'Dim contenedorb As ABMContainer = page.Component("contenedorb")
	' Dim inp1 As ABMFileInput  = contenedorb.Cell(2,1).Component("inp1")
  
	If success Then
		page.ShowToast("toast" & myToastId, "toastgreen", "File " & FileName & " uploaded success!", 5000)
		foto = FileName
		
		Dim carpeta As String = File.Combine(File.DirApp, DownloadFolder)
		
		fotoprev.Source = "../post/"&FileName
		fotoprev.Refresh
		
	Else
		page.ShowToast("toast" & myToastId, "toastred", "File " & FileName & " not uploaded!", 5000)
	End If
   
	'Dim inp1 As ABMFileInput  = page.Cell(3,1).Component("inp1")
	'inp1.Clear
   
	page.Resume
	page.ws.Flush
	page.Refresh
End Sub


Sub enviar_Clicked(Target As String)
	Dim contenedor1 As ABMContainer = page.Component("contenedor1")
	Dim comment As ABMInput = contenedor1.Cell(2,2).Component("comment")
	
	Dim sql1 As SQL
	Dim valores(), valores2()  As Object
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	Dim rst As ResultSet
	Dim pos As Int=0
	Dim sqlt As String
	Dim carpeta As String = File.Combine(File.DirApp, DownloadFolder)
	Dim fotonew As String = ws.Session.GetAttribute("idsesion")&DateTime.now&foto.SubString2(foto.Length-4,foto.Length)
	File.Copy(carpeta,foto,carpeta,fotonew)
	File.Delete(carpeta,foto)
	
	If Not(comment.Text = "") Then		
		valores= Array As Object (ws.Session.GetAttribute("idsesion"),ws.Session.GetAttribute("nombre"),comment.text, fotonew ,DateTime.Date(DateTime.Now) & " " &DateTime.Time(DateTime.Now) )
		sql1.ExecNonQuery2("INSERT INTO RH_POST VALUES (?,?,?,?,? )", valores)
		 
	End If
	
	foto = ""
	
	Log("salvado")
	
	ABMShared.NavigateToPage(ws,ABMPageId,"../muro/muro.html")
	
End Sub

Sub addNewCard(RaiseNext As Boolean, id As Int)
	Dim NewPost As FacebookPost
	NewPost.Initialize
	postCounter = postCounter + 1                       
	page.Cell(3,1).AddComponent(NewPost.Build(page, id))  '"post" & postCounter
	page.Cell(3,1).SetOffsetSize(0,0,0,12,6,6)
	
	Posts.put(postCounter, NewPost)
	
	If RaiseNext Then
		page.Cell(3,1).Refresh
		page.RaiseNextContentOnComponent(NewPost.myCard,200)
	End If
End Sub

Sub page_NextContent(TriggerComponent As String)
	addNewCard(True,0)
End Sub

public Sub LikeComponent_Liked(value As Map)
	
	Dim val As String = value.Get("target")
	Dim idx() As String = Regex.Split("-",val)
	Dim id As String = idx(0)
	Dim emo As String = emocion(value.Get("like"))
	
	Dim sql1 As SQL
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	Dim valores(), valores2()  As Object
	
	
	Dim cont As Int

	cont = sql1.ExecQuerySingleResult("SELECT count(*) FROM RH_REACTIONPOST WHERE id = '"&id&"' and id_user= '"&ws.Session.GetAttribute("idsesion")&"'")
	
	If cont = 0 Then
		valores= Array As Object (id,ws.Session.GetAttribute("idsesion") ,emo, DateTime.Date(DateTime.Now) & " " &DateTime.Time(DateTime.Now) )
		sql1.ExecNonQuery2("INSERT INTO RH_REACTIONPOST VALUES (?,?,?,? )", valores)
	Else
		sql1.ExecNonQuery("UPDATE RH_REACTIONPOST SET id_tipo = '"&emo&"' WHERE id = '"&id&"' and id_user= '"&ws.Session.GetAttribute("idsesion")&"'")
	End If
	
	
		
	
	Log(value.Get("like") & " ----> " & value.Get("target"))
End Sub

public Sub LikeComponent_UnLiked(value As Map)
	
	Dim val As String = value.Get("target")
	Dim idx() As String = Regex.Split("-",val)
	Dim id As String = idx(0)
	Dim emo As String = emocion(value.Get("like"))
	
	Dim sql1 As SQL
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	sql1.ExecNonQuery("DELETE FROM RH_REACTIONPOST WHERE id = '"&id&"' and id_user= '"&ws.Session.GetAttribute("idsesion")&"'")
	
	
	Log("Unliked ----> " & value.Get("target"))
End Sub

public Sub Reactions_Clicked(Target As String)
	Dim postNum As Int = Target.SubString(13)
	Dim NewPost As FacebookPost = Posts.Get(postNum)
	Dim CardReacts As ABMContainer = NewPost.myCard.Component( postNum & "reactions")
	If NewPost.IsOpen Then
		CardReacts.CloseContent
	Else
		CardReacts.OpenContent
	End If
	NewPost.IsOpen = Not(NewPost.IsOpen)
End Sub


Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub



Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub emocion (texto As String)
	
	texto = texto.ToLowerCase
	
	Select Case texto
		Case "like"
		
			Return 1
		Case "love"
			Return 2
		Case "haha"
			Return 3
		Case "wow"
			Return 4
		Case "sad"
			Return 5
		Case "angry"
			Return 6
		Case Else
			Return 0
	End Select
	
End Sub

Sub emociontext (valor As Int)
	
	valor = valor
	
	Select Case valor
		Case 1
			Return "like"
		Case 2
			Return "love"
		Case 3
			Return "haha"
		Case 4
			Return "wow"
		Case 5
			Return "sad"
		Case 6
			Return "angry"
		Case Else
			Return ""
	End Select
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
	'	If icon <> "" Then
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	'	Else
	'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
		combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next
	
	'page.Refresh
	'combo1.Refresh
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)
	Return myModal
End Sub

